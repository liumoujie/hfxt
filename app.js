﻿var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var flash = require('connect-flash');//用来在session中保存数据，但数据读取一次就会删除。

var mongoose = require('./models/db');

//Passport
var passport = require('passport');
var expressSession = require('express-session');
var compression = require('compression');
var initPassport = require('./common/ptt-passport');
var MongoStore = require('connect-mongo')(expressSession);

//for ye test
var app = express();

// use gzip compress every output
app.use(compression());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

app.use(bodyParser.json({limit: '20mb'}));
app.use(bodyParser.urlencoded({extended: false, limit: '20mb'}));

app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/3rd', express.static(path.join(__dirname, 'bower_components')));
app.use(expressSession({
    name: 'ptt-sid',
    secret: 'pttSecretKey',
    resave: true,
    saveUninitialized: false,
    store: new MongoStore({mongooseConnection: mongoose.connection}),
    cookie: {maxAge: 60 * 60 * 1000}
}));

app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
initPassport(passport);

var admin = require('./routes/admin')(passport);
app.use('/admin', admin);

var logging = require('./routes/logging')(passport);
app.use('/logging', logging);

var routes = require('./routes/index');
app.use('/', routes);

var pttApi = require('./routes/ptt-api');
app.use('/api', pttApi);
var imp = require('./routes/imp');
app.use('/imp', imp);

var upload = require('./routes/upload');
app.use('/upload', upload);
// test
//added by xp.qing for GIS 2016.8.23 14:27
var gis = require('./routes/schedule');
app.use('/gis', gis);

var hfxt = require('./routes/hfxt-api');
app.use('/hfxt', hfxt);

var history = require('./routes/history');
app.use('/his', history);

var pdf = require('./routes/pdf');
app.use('/pdf', pdf);

var led = require('./routes/led');
app.use('/led', led);

app.use('/app_config/:idnumber/:version', function (req, res) {
    res.send({
        signal_server_endpoint: "http://192.168.1.104:20000",
        "update_url": "",
        "force_update": false,
        "update_message": ''
    })
});

app.use('/device', function (req, res) {
    res.send("12345678")
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers


// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;
