'use strict';

import io = require('socket.io-client');
import _ = require('lodash');
import {ArgumentParser} from "argparse";
import rp = require('request-promise');
import RequestPromiseOptions = require("request-promise");
import http = require("request");
import RequestPromise = require("request-promise");
import Socket = SocketIOClient.Socket;
import md5 = require('js-md5');
import log = require('loglevel');

let parser = new ArgumentParser({
    version: '1.0',
    addHelp: true,
    description: 'PTT server benchmarking'
});

interface Option {
    signal_server : string;
    user : string;
    password : string;
    group_count : number;
    group_member_count : number;
    enterprise : string;
}

parser.addArgument(['-s', '--signal_server'], { help: 'Specify signal server url', required: true });
parser.addArgument(['-u', '--user'], { help: 'Specify signal server admin user name', required: false, defaultValue: "0" });
parser.addArgument(['-p', '--password'], { help: 'Specify signal server admin password', required: false, defaultValue: "000000" });
parser.addArgument(['-e', '--enterprise'], { help: 'Specify the enterprise idNumber used to test', required: false, defaultValue: null });
parser.addArgument(['group_count'], { help: 'Number of groups to test in the same time'});
parser.addArgument(['group_member_count'], { help: 'Number of member in one group' });

class Measure {
    costs : number[] = [];
    failedCount : number = 0;

    averageCost() : number {
        if (!this.costs) {
            return 0;
        }

        return _.reduce(this.costs, (cost : number, sum : number) => sum + cost, 0) / this.costs.length;
    }

    toString() : string {
        if (!this.costs) {
            return "{ No data }";
        }
        return "{ maxTime: " + _.max(this.costs) + "ms, avgTime: " + this.averageCost().toFixed(2) + "ms, failedCount: " + this.failedCount + "}";
    }
}

class Group {
    idNumber : string = null;
    members : string[] = [];
    roomId : string = null;

    constructor(id:string, members:string[]) {
        this.idNumber = id;
        this.members = members;
    }
}

class State {
    adminCookie : any = null;
    enterpriseCookie : any = null;
    createdEnterpriseId : string = null;
    groups : Group[] = [];
    sockets : Map<string, Socket> = new Map();
    socketEstablishMeasure = new Measure();
    createRoomMeasure = new Measure();
    waitForInviteMeasure = new Measure();
    joinRoomMeasure = new Measure();
    grabMicMeasure = new Measure();
    receiveMicMeasure = new Measure();
}

function createBenchmark(option : Option) : Promise<void> {
    log.info("Benchmarking", option.signal_server, "using", option.group_count * option.group_member_count, "users");

    let state = new State();

    let initRequest = {
        method : 'GET',
        uri : option.signal_server + '/api/init',
        followAllRedirects : true
    };

    log.info("Initializing signal server", option.signal_server);

    return rp(initRequest)
        .then(() => {
            let loginRequest = {
                method: 'POST',
                uri: option.signal_server + '/admin/login',
                form: {username: option.user, password: option.password},
                resolveWithFullResponse: true
            };

            log.info("Logging in using user", option.user, "...");
            return rp(loginRequest)
        })
        .catch(err => {
            if (err.statusCode == 302) {
                return Promise.resolve(err.response);
            }
            else {
                return Promise.reject(err);
            }
        })
        .then(response => {
            log.info("User", option.user, "logged in");
            state.adminCookie = response.headers['set-cookie'];

            if (!option.enterprise) {
                log.info("Creating new enterprise...");
                let createEnterpriseNumberRequest = {
                    method: 'GET',
                    uri: option.signal_server + '/api/enterprise/number',
                    headers: {
                        'Cookie': state.adminCookie
                    }
                };

                return rp(createEnterpriseNumberRequest);
            }
        })
        .then(response => {
            if (!option.enterprise) {
                let result = JSON.parse(response);
                state.createdEnterpriseId = result.number;

                let createEnterpriseRequest = {
                    method: 'POST',
                    uri: option.signal_server + '/api/enterprise',
                    headers: {
                        'Cookie': state.adminCookie
                    },
                    json: {
                        name: "Benchmark enterprise " + state.createdEnterpriseId,
                        idNumber: state.createdEnterpriseId,
                        password: option.password,
                        quota: {
                            staff: option.group_count * option.group_member_count,
                            preGroup: option.group_member_count
                        }
                    }
                };

                return rp(createEnterpriseRequest);
            }
            else {
                state.createdEnterpriseId = option.enterprise;
            }
        })
        .then(() => {
            log.info("Enterprise", state.createdEnterpriseId, "created. Logging in as new enterprise");
            let loginEnterpriseRequest = {
                method: 'POST',
                uri: option.signal_server + '/admin/login',
                form: {username: state.createdEnterpriseId, password: option.password},
                resolveWithFullResponse: true
            };

            return rp(loginEnterpriseRequest);
        })
        .catch(err => {
            if (err.statusCode == 302) {
                return Promise.resolve(err.response).then(res => {
                    log.info('Logged as enterprise', state.createdEnterpriseId)
                    return Promise.resolve(res);
                });
            }
            else {
                return Promise.reject(err);
            }
        })
        .then(response => {
            log.info("Creating", option.group_count, "groups with", option.group_member_count, "members each group. This may take a long time...");
            state.enterpriseCookie = response.headers['set-cookie'];

            let createBenchmarkRequest = {
                method: 'POST',
                uri: option.signal_server + '/api/enterprise/benchmark',
                headers: {'Cookie': state.enterpriseCookie},
                form: {groupCount: option.group_count, groupMemberCount: option.group_member_count}
            };

            return rp(createBenchmarkRequest).then(res => {
                let groups : Group[] = JSON.parse(res).groups;
                log.info("Created", groups.length, "groups");
                state.groups = groups;
            });
        })
        .then(() => {
            let allUsers:string[] = [];
            state.groups.forEach(group => allUsers.push(...group.members));
            log.info("Logging in", allUsers.length, "users...");

            return createConcurrentTasks(allUsers.length, Math.min(50, allUsers.length), (index : number) => {
                let userId = allUsers[index];
                return new Promise<Socket>((resolve, reject) => {
                    let opt = {
                        extraHeaders: {Authorization: "Basic " + new Buffer(userId + ':' + md5('000000')).toString('Base64')},
                        autoConnect: false
                    };
                    let socket = io(option.signal_server, opt);

                    // socket.once('disconnect', reject);
                    socket.on('error', () => {
                        state.socketEstablishMeasure.failedCount++;
                    });

                    let startTime:number;
                    socket.once('s_logon', () => {
                        state.socketEstablishMeasure.costs.push(Date.now() - startTime);
                        socket.off('disconnect');
                        socket.off('error');
                        state.sockets.set(userId, socket);
                        resolve(socket);
                    });

                    startTime = Date.now();
                    socket.connect();
                });
                // .then((socket:Socket) => {
                //     return emit(socket, 'c_sync_contact', {enterMemberVersion: 0, enterGroupVersion: 0})
                //         .then(() => Promise.resolve(socket));
                // })
            }).then(() => {
                log.info("Log in", allUsers.length, "users summary:", state.socketEstablishMeasure.toString());
            })
        })
        .then(() => {
            log.info("Creating", state.groups.length, "rooms...");
            return createConcurrentTasks(state.groups.length, Math.min(1, state.groups.length), (index : number) => {
                let group = state.groups[index];
                let client = state.sockets.get(group.members[0]);
                let startTime = Date.now();
                if (!group.idNumber) {
                    throw "No group idNumber";
                }
                return emit(client, 'c_create_room', null, [group.idNumber], [])
                    .then(response => {
                        if (!response.success) {
                            throw "Error"
                        }
                        const room = response.data;
                        state.createRoomMeasure.costs.push(Date.now() - startTime);
                        group.roomId = room.idNumber;
                    })
                    .catch(e => {
                        state.createRoomMeasure.failedCount++;
                        return Promise.reject(e);
                    })
            }).then(() => log.info("Creating", option.group_count, "rooms summary:", state.createRoomMeasure.toString()));;
        })
        .then(() => {
            log.info("Joining", state.groups.length, "rooms...");
            return Promise.all(_.map(state.groups, (group:Group) => {
                let othersJoinRoomPromise = Promise.all(_.map(group.members.slice(1), (memberId:string) => {
                    let socket = state.sockets.get(memberId);
                    let startTime = Date.now();
                    return createTimedPromise(600000, (resolve, reject) => {
                        socket.once('error', () => state.waitForInviteMeasure.failedCount++);
                        socket.once('s_invite_to_join', result => {
                            state.waitForInviteMeasure.costs.push(Date.now() - startTime);
                            resolve(result);
                            socket.off('error');
                        });
                    }).then((invite:any) => {
                        let startTime = Date.now();
                        return emit(socket, 'c_join_room', invite.room.idNumber)
                            .then(response => {
                                if (!response.success) {
                                    throw "Error"
                                }
                            })
                            .then(() => state.joinRoomMeasure.costs.push(Date.now() - startTime))
                            .catch(() => state.joinRoomMeasure.failedCount++)
                    });
                }));

                return emit(state.sockets.get(group.members[0]), 'c_join_room', group.roomId)
                    .then(() => othersJoinRoomPromise)
            }))
                .then(() => log.info("Waiting for invite summary: ", state.waitForInviteMeasure.toString()))
                .then(() => log.info("Join room summary: ", state.joinRoomMeasure.toString()));
        })
        .then(() => {
            log.info("Grab mic using first user in each room...");

            let receiveMicUsers : string[] = _.reduce<Group, string[]>(state.groups, (memo : string[], group : Group) => {
                memo.push(...group.members.slice(1));
                return memo;
            }, []);

            let startTime = Date.now();
            let receiveMicPromise = Promise.all(_.map(receiveMicUsers, (userId : string) => {
                let client = state.sockets.get(userId);
                return createTimedPromise(15000, resolve => {
                    client.once('s_speaker_changed', resolve);
                }).then(() => state.receiveMicMeasure.costs.push(Date.now() - startTime))
                    .catch(() => state.receiveMicMeasure.failedCount++)
            }));

            return Promise.all(_.map(state.groups, (group : Group) => {
                let client = state.sockets.get(group.members[0]);
                return emit(client, 'c_control_mic', group.roomId)
                    .then(() => state.grabMicMeasure.costs.push(Date.now() - startTime))
                    .catch(() => state.grabMicMeasure.failedCount++);
            })).then(() => receiveMicPromise)
                .then(() => {
                    log.info("Grab mic stats:", state.grabMicMeasure.toString());
                    log.info("Receive mic stats:", state.receiveMicMeasure.toString());
                });
        })
        .catch(err => {
            log.error(err);
        })
        .then(() => {
            log.info("Cleaning up...");
            state.sockets.forEach((socket:Socket) => socket.disconnect());
        });
}

function delayPromise(time : number) : Promise<void> {
    return new Promise<void>(resolve => {
        setTimeout(resolve, time);
    });
}

function createTimedPromise<T>(timeout : number,
                               executor: (resolve: (value?: T | PromiseLike<T>) => void, reject: (reason?: any) => void) => void) : Promise<T> {
    return new Promise<T>((resolve, reject) => {
        let timerHandler = setTimeout(() => reject(new Error("Timeout")), timeout);
        executor(result => {
            clearTimeout(timerHandler);
            resolve(result);
        }, rej => {
            clearTimeout(timerHandler);
            reject(rej);
        });
    });
}

function emit(socket : Socket, event: string, ...args : any[]) : Promise<any> {
    return new Promise((resolve, reject) => {
        socket.emit(event, ...args, res => {
            if (res && res.error) {
                reject(res.error);
            }
            else {
                resolve(res);
            }
        });
    });
}

function createConcurrentTasks(size:number, concurrentSize:number, initializer:Function, measure?:Measure) : Promise<any[]> {
    let results = [];
    let loopCount = Math.ceil(size / concurrentSize);
    let lastPromise : Promise<any> = Promise.resolve();
    let lastDisplayedProgress = 0;
    log.info( "progress: ", 0, "%");
    for (let i = 0; i < loopCount; i++) {
        let startIndex = i * concurrentSize;
        lastPromise = lastPromise.then(() => {
            let promises = [];
            let endIndex = Math.min(startIndex + concurrentSize, size);
            // log.info("Executing", endIndex - startIndex, "concurrent jobs");
            let j = startIndex;
            for (; j < endIndex; j++) {
                let index = j;
                let taskStartTime = measure ? Date.now() : null;
                promises.push(
                    initializer(index).then(result => {
                        if (measure) {
                            measure.costs.push(Date.now() - taskStartTime);
                        }
                        results[index] = result;
                        return Promise.resolve(result);
                    })
                );
            }

            return Promise.all(promises).then(r => {
                var newProgress = Math.floor(Math.min(100, (startIndex + concurrentSize) * 100 / size));
                if (newProgress - lastDisplayedProgress > 10 || j == size - 1) {
                    log.info("progress:", newProgress, "%");
                    lastDisplayedProgress = newProgress;
                }
                return Promise.resolve(r);
            });
        });
    }

    return lastPromise.then(() => {
        return Promise.resolve(results);
    });
}

log.setDefaultLevel(LogLevel.DEBUG);
createBenchmark(parser.parseArgs(process.argv.slice(2)));