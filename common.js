define(function (require, exports, module) {
    function e() {
        this.m_szHostName = location.hostname, this.m_szHostNameOriginal = location.hostname, this.m_iHttpPort = 80, this.m_szHttpProtocol = location.protocol + "//", this.m_iHttpProtocal = "http://" == this.m_szHttpProtocol ? 1 : 2, "" != location.port ? this.m_iHttpPort = location.port : "https://" == this.m_szHttpProtocol && (this.m_iHttpPort = 443), this.m_szHostName.indexOf("[") > -1 && (this.m_szHostNameOriginal = this.m_szHostName.substring(1, this.m_szHostName.length - 1)), o.isIPv6Address(this.m_szHostNameOriginal) && (this.m_szHostName = "[" + this.m_szHostNameOriginal + "]");
        var e = location.pathname.match(/\/doc\/page\/([^.]+).asp/);
        this.m_szPage = "", null != e && (this.m_szPage = e[1]), "" != this.m_szPage && (this.m_oLanCommon = null, this.m_szNamePwd = "", this.m_szSessionId = "", this.m_szPluginNamePwd = "", this.m_szPluginSessionId = "", this.m_bDigest = !0, this.m_bAnonymous = !1, this.m_oLoginUser = {
            szName: "",
            szType: ""
        }, this.m_bWizard = !1, this.m_bSession = !1, this.m_iSessionFailed = 0, this.m_iSessionInterval = 0, this.m_iReconnectFailed = 0, this.m_szDefaultPwd = "")
    }

    require("layout"), require("cookie"), require("json2"), require("angular"), require("websdk"), require("cryptico");
    var t = require("base64"), n = require("webSession"), i = require("translator"), o = require("utils"), s = require("dialog");
    e.prototype = {
        init: function () {
            var e = this;
            window.console = window.console || {
                    log: function () {
                    }
                }, seajs.bDebugMode || "function" != typeof window.console.log || (window.console.log = function () {
            }), $.ajaxSetup({
                timeout: 3e4, beforeSend: function (e) {
                    e.setRequestHeader("If-Modified-Since", "0")
                }, statusCode: {
                    401: function () {
                        e.goLogin()
                    }, 403: function (t) {
                        "notActivated" == o.nodeValue(t, "subStatusCode") && e.goLogin()
                    }
                }
            });
            var i = ["login", "pwdReset"];
            if (-1 === $.inArray(e.m_szPage, i)) {
                if (e.m_szSessionId = n.getItem("sessionId"), e.m_szSessionId && "" != e.m_szSessionId && e.setAuthMode("session"), e.m_szNamePwd = n.getItem("userInfo"), null === e.m_szNamePwd)return e.goLogin(), void 0;
                var s = t.decode(e.m_szNamePwd);
                e.m_bSession && (s = o.decodeAES(s, MD5(e.m_szSessionId)), e.m_szNamePwd = s, s = t.decode(s));
                var a = o.parseNamePwd(s);
                e.m_oLoginUser.szName = a.szName, WebSDK.WSDK_SetLoginInfo(e.m_szHostName, e.m_iHttpProtocal, e.m_iHttpPort, a.szName, a.szPass, {sessionId: e.m_szSessionId}), e.updatePluginAuth(a.szName, a.szPass), e.sessionHeartbeat()
            }
            e.extendJQuery(), e.overrideAngular(), e.initLan(), e.initModule()
        }, updateWebAuth: function (e, i) {
            var s = this, a = t.encode(e + ":" + i);
            s.m_szNamePwd = a, s.m_bSession && (0 === s.m_szSessionId.length && (s.m_szSessionId = n.getItem("sessionId")), a = t.encode(o.encodeAES(a, MD5(s.m_szSessionId)))), n.setItem("userInfo", a), WebSDK.WSDK_SetLoginInfo(s.m_szHostName, s.m_iHttpProtocal, s.m_iHttpPort, e, i, {sessionId: s.m_szSessionId})
        }, setAuthMode: function (e) {
            var t = this;
            t.m_bSession = "session" === e, t.m_bDigest = !t.m_bSession
        }, updatePluginAuth: function (e, n) {
            var i = this, o = "";
            i.m_bSession ? o = "::" : i.m_bDigest && (o = ":"), i.m_bAnonymous ? i.m_szPluginNamePwd = t.encode(o) : (i.m_szPluginNamePwd = t.encode(":" + e + ":" + n), i.m_bSession && (i.m_szPluginSessionId = t.encode(o + i.m_szSessionId)))
        }, getPluginAuth: function () {
            var e = this, t = e.m_szPluginNamePwd;
            return e.m_bSession && (t = e.m_szPluginSessionId), t
        }, initLoginUserInfo: function () {
            var e = this;
            WebSDK.WSDK_GetDeviceConfig(e.m_szHostName, "user", null, {
                async: !1, success: function (t, n) {
                    $(n).find("User").each(function () {
                        if (e.m_oLoginUser.szName === o.nodeValue(this, "userName")) {
                            var t = o.nodeValue(this, "userLevel");
                            "Administrator" === t ? e.m_oLoginUser.szType = "admin" : "Operator" === t ? e.m_oLoginUser.szType = "operator" : "Viewer" === t && (e.m_oLoginUser.szType = "viewer")
                        }
                    })
                }, error: function () {
                    "admin" === e.m_oLoginUser.szName && (e.m_oLoginUser.szType = "admin")
                }
            })
        }, resize: function () {
        }, initLan: function () {
            var e = this, t = $.cookie("language");
            if (null === t) {
                var n = (navigator.language || navigator.browserLanguage).toLowerCase();
                if (t = n.substring(0, 2), "zh" == t) {
                    var o = n.split("-");
                    2 == o.length && (t = o[0] + "_" + o[1].toUpperCase(), "cn" == o[1] && (t = "zh"))
                }
            }
            var s = null !== $.cookie("dispatch");
            s && $.cookie("dispatch", null), $("#language_list").length > 0 || s ? i.initLanguageSelect(t, e) : i.szCurLanguage = t, e.m_oLanCommon = i.getLanguage("Common"), $("#footer").text("©20" + seajs.web_version.substr(seajs.web_version.indexOf("build") + 5, 2) + " Hikvision Digital Technology Co., Ltd. All Rights Reserved.")
        }, initCSS: function () {
        }, initModule: function () {
            var e = this;
            require.async(e.m_szPage, function (e) {
                e && (e.init(), $(window).bind({
                    unload: function () {
                        try {
                            e.unload()
                        } catch (t) {
                        }
                    }
                }))
            })
        }, overrideAngular: function () {
            angular.oldModule = o.cloneFunc(angular.module), angular.module = function () {
                var e = angular.oldModule.apply(angular.oldModule, arguments);
                return e.config(function ($provide) {
                    $provide.decorator("ngBindDirective", ["$delegate", function (e) {
                        return e.shift(), e
                    }])
                }), e.directive("ngBind", function () {
                    return {
                        restrict: "A", replace: !1, link: function (e, t, n) {
                            t.addClass("ng-binding").data("$binding", n.ngBind), e.$watch(n.ngBind, function (e) {
                                if (t.text(void 0 == e ? "" : e), e && "" != e && t.is(":visible")) {
                                    var n = t.get(0).scrollWidth, i = t.outerHeight(!0);
                                    if (0 === n) {
                                        n = t.parent().width(), i = t.parent().outerHeight(!0);
                                        var o = 0, s = $(t).outerHeight(!0), a = t.siblings("input,label").length;
                                        1 == a ? t.parent().children().each(function () {
                                            o += $(this).outerWidth(!0)
                                        }) : o += $(t).outerWidth(!0), (o > n || s > i) && 1 >= t.siblings().length && t.parent().addClass("ellipsis").attr("title", $.trim(t.parent().text()))
                                    } else {
                                        var a = t.siblings("input,label").length, r = t.width();
                                        if (1 == a) {
                                            var c = t.siblings("input,label").eq(0).outerWidth();
                                            (c + n + 5 >= t.parent().width() || i > t.parent().height()) && t.parent().addClass("ellipsis").attr("title", $.trim(t.parent().text()))
                                        } else if (n > r || i > t.height()) {
                                            var u = $.trim(t.text());
                                            if ("BUTTON" === t.get(0).tagName) {
                                                var l = "none" === t.css("max-width") ? 1 / 0 : parseInt(t.css("max-width"));
                                                if (n > l) {
                                                    var m = Math.floor(t.text().length * r / n), d = u.substring(0, m - 3) + "...";
                                                    t.attr("title", $.trim(t.text())).text(d)
                                                } else t.addClass("ellipsis").attr("title", u)
                                            } else t.addClass("ellipsis").attr("title", u)
                                        } else(n > t.parent().width() || i > t.parent().height()) && t.parent().addClass("ellipsis").attr("title", $.trim(t.parent().text()))
                                    }
                                }
                            })
                        }
                    }
                }), e
            }
        }, sessionHeartbeat: function () {
            var e = this;
            if (e.m_bSession && !window.opener) {
                var t = 3e4;
                e.m_iSessionInterval > 0 && clearInterval(e.m_iSessionInterval), e.m_iSessionInterval = setInterval(function () {
                    WebSDK.WSDK_SetDeviceConfig(e.m_szHostName, "sessionHeartbeat", null, {
                        success: function () {
                            e.m_iSessionFailed = 0
                        }, error: function (t, n) {
                            e.m_iSessionFailed++, t > 300 && "notActivated" === o.nodeValue(n, "subStatusCode") && (e.m_iSessionInterval > 0 && clearInterval(e.m_iSessionInterval), e.goLogin())
                        }, complete: function () {
                            e.m_iSessionFailed >= 5 && (clearInterval(e.m_iSessionInterval), e.isDeviceAccessible() ? e.goLogin() : WebSDK.m_bReConnecting ? e.goLogin() : e.reconnect())
                        }
                    })
                }, t)
            }
        }, goLogin: function () {
            var e = this, t = "login.asp?_" + (new Date).getTime() + "&page=" + e.m_szPage;
            window.opener ? window.opener.location.href = t : window.dialogArguments ? (window.close(), window.dialogArguments.location.href = t) : top.window.location.href = t
        }, extendJQuery: function () {
            $.browser || ($.extend({browser: {}}), function () {
                var e = navigator.userAgent.toLowerCase(), t = /(webkit)[ \/]([\w.]+)/, n = /(opera)(?:.*version)?[ \/]([\w.]+)/, i = /(msie) ([\w.]+)/, o = /(trident.*rv:)([\w.]+)/, s = /(mozilla)(?:.*? rv:([\w.]+))?/, a = t.exec(e) || n.exec(e) || i.exec(e) || o.exec(e) || 0 > e.indexOf("compatible") && s.exec(e) || [];
                a.length > 0 && a[1].indexOf("trident") > -1 && (a[1] = "msie"), a[1] && ($.browser[a[1]] = !0, $.browser.version = a[2] || ""), $.browser.webkit && ($.browser.safari = !0)
            }())
        }, doLogin: function (e, i, s, a, r, c) {
            var u = this;
            n.removeItem("sessionId");
            var l = "", m = (new Date).getTime();
            WebSDK.WSDK_Request(u.m_szHostName, u.m_iHttpProtocal, u.m_iHttpPort, {
                cmd: "sessionCap",
                async: !1,
                name: e,
                password: i,
                data: {username: e},
                queryObject: !0,
                processData: !0,
                success: function (t, n) {
                    u.setAuthMode("session");
                    var s = o.nodeValue(n, "sessionID"), a = o.nodeValue(n, "challenge"), r = o.nodeValue(n, "iterations", "i"), c = o.nodeValue(n, "isIrreversible", "b"), m = o.nodeValue(n, "salt"), d = o.encodePwd(i, {
                        challenge: a,
                        userName: e,
                        salt: m,
                        iIterate: r
                    }, c);
                    l = "<SessionLogin>", l += "<userName>" + o.encodeString(e) + "</userName>", l += "<password>" + d + "</password>", l += "<sessionID>" + s + "</sessionID>", l += "</SessionLogin>"
                },
                error: function () {
                    u.setAuthMode("digest")
                }
            }), WebSDK.WSDK_Login(u.m_szHostName, u.m_iHttpProtocal, u.m_iHttpPort, e, i, m, {
                session: u.m_bSession,
                data: l,
                success: function (a, l) {
                    var m = e + ":" + i;
                    u.m_bSession && (u.m_szSessionId = o.nodeValue(l, "sessionID"), n.setItem("sessionId", u.m_szSessionId), m = o.encodeAES(t.encode(m), MD5(u.m_szSessionId)), u.sessionHeartbeat()), n.setItem("userInfo", t.encode(m)), "function" == typeof s && s.apply(r, [a, l].concat(c || []))
                },
                error: function (e, t) {
                    "function" == typeof a && a.apply(r, [e, t].concat(c || []))
                }
            })
        }, exportGuid: function (e, n, a) {
            var r = this, c = r.m_szHttpProtocol + r.m_szHostName + ":" + r.m_iHttpPort + "/ISAPI/Security/GUIDFileData", u = MD5("" + (new Date).getTime());
            c = WebSDK.getSecurityVersion(c, u);
            var l = o.encodeAES(t.encode(o.encodeString(e)), WebSDK.szAESKey, u), m = "<?xml version='1.0' encoding='UTF-8'?><LoginPassword><password>" + l + "</password></LoginPassword>", d = n.exportFile(c, r.getPluginAuth(), m, 2, 0);
            if (0 === d)s.alert(i.getValue("exportOK")); else {
                if (1 === d)return;
                var g = i.getValue("exportFailed"), m = n.getHttpErrorInfo();
                if (m && -1 !== m.indexOf("<?xml")) {
                    var p = o.parseXmlFromStr(m), _ = r.getLockTips(p), f = Number(n.getLastError()), h = {
                        readyState: 4,
                        status: f > 300 ? f : 400,
                        responseXML: p
                    };
                    g = a.saveState(h, _.szTips, void 0, !0)
                }
                s.alert(g)
            }
        }, setSecurityQA: function (e, t, n, a, r) {
            for (var c = this, u = [], l = !0, m = "<?xml version='1.0' encoding='UTF-8'?><SecurityQuestion><QuestionList>", d = 0, g = WebSDK.oSecurityCap.iMaxQANum; g > d; d++) {
                if (d && $.inArray(e[d].szId, u) > -1)return s.alert(i.getValue("sameSQAnswerTips")), !1;
                m += "<Question><id>" + e[d].szId + "</id><answer>" + o.encodeString(e[d].szAnswer) + "</answer></Question>", u.push(e[d].szId)
            }
            return m += "</QuestionList>", m += "<password>" + o.encodeString(t) + "</password>", m += "</SecurityQuestion>", WebSDK.WSDK_SetDeviceConfig(c.m_szHostName, "questionInfoList", null, {
                data: m,
                async: !1,
                success: function () {
                    r && s.alert(i.getValue("saveSucceeded"))
                },
                error: function (e, t, i) {
                    l = !1;
                    var o = c.getLockTips(t);
                    s.alert(n.saveState(i, o.szTips, void 0, !0))
                }
            }), l ? ("function" == typeof a && a(), void 0) : !1
        }, getLockTips: function (e) {
            var t = "", n = o.nodeValue(e, "lockStatus"), s = o.nodeValue(e, "resLockTime", "i"), a = o.nodeValue(e, "retryTimes", "i"), r = "locked" === n;
            if (r) {
                var c;
                60 > s ? c = i.getValue("seconds") : (s = Math.ceil(s / 60), c = i.getValue("minute")), t = i.getValue("userLock", [s, c])
            } else"unlock" === n && (t = i.getValue("lockTimeTips", [a]));
            return {bLocked: r, szTips: t}
        }, isDeviceFileExist: function (e) {
            var t = this, n = !1;
            return WebSDK.WSDK_GetDeviceConfig(t.m_szHostName, "webPing", {cmd: e}, {
                async: !1,
                timeout: 2e3,
                success: function () {
                    n = !0
                },
                error: function () {
                    n = !1
                }
            }), n
        }, isDeviceAccessible: function () {
            var e = this, t = !1;
            return WebSDK.WSDK_DeviceLan(e.m_szHostName, e.m_iHttpProtocal, e.m_iHttpPort, {
                async: !1,
                timeout: 2e3,
                success: function () {
                    t = !0
                }
            }), t
        }, reconnect: function (e, t) {
            var n = this;
            WebSDK.m_bReConnecting || (n.m_iReconnectFailed = 0, n.reconnectProcess(e, t), WebSDK.m_bReConnecting = !0)
        }, reconnectProcess: function (e, t) {
            var n = this;
            if (n.m_iReconnectFailed > 180)return n.goLogin(), void 0;
            if (n.m_bSession)WebSDK.WSDK_DeviceLan(n.m_szHostName, n.m_iHttpProtocal, n.m_iHttpPort, {
                async: !1,
                timeout: 5e3,
                success: function () {
                    e && e.close(), WebSDK.m_bReConnecting = !1, n.goLogin()
                },
                error: function () {
                    n.m_iReconnectFailed++, setTimeout(function () {
                        n.reconnectProcess(e, t)
                    }, 5e3)
                }
            }); else {
                var i = (new Date).getTime();
                WebSDK.WSDK_GetDeviceConfig(n.m_szHostName, "login", {timeStamp: i}, {
                    success: function () {
                        e && e.close(), WebSDK.m_bReConnecting = !1, "function" == typeof t && t()
                    }, error: function () {
                        n.m_iReconnectFailed++, setTimeout(function () {
                            n.reconnectProcess(e, t)
                        }, 5e3)
                    }
                })
            }
        }
    }, module.exports = new e
});