/**
 * Created by ring on 2015/6/8.
 */
var pool = require('./connection_pool');
var logger = require('../config/log').logger;
/*
 * 数据表查询的工具类
 * callBack中需要依次包含 err, rows, fields 这几个参数
 * */
var DbUtil = function () {

}

// include query and insert function
var DBQuery = function (sql, callBack) {
//  console.log('used : -------------------------- query start -------------------------------');
//var t = Date.now();
//  for(var i = 0 ; i < 499999; i ++){
//    for(var j = 0 ; j < 99999; j ++){
//        i * j;
//    }
//  }
//console.log('used :' + (Date.now() - t));

  pool.getConnection(function (err, conn) {
    if (err) {
      callBack(err);
      return;
    } else {
      conn.query(sql, function (err, rows, fields) {
        conn.release();
        if (err) {
          callBack(err);
          return;
        } else if (rows.length == 0) {
          callBack(null, []);
        } else {
          callBack(null, rows, fields);
        }
      });
    }
  });
}

var DBUpdate = function (table, objs, wheres, callBack, sql) {
  var updSql = "update " + table + " set ";
  var wh = "";
  var sets = "";
  for (var o in objs) {
    var isWhere = 0;
    for (var w in wheres) {
      if (o == wheres[w]) {
        isWhere = 1;
        break;
      }
    }
    if (isWhere) {
      wh += " and " + o + " = '" + objs[o] + "' ";
    } else {
      sets += " " + o + " = '" + objs[o] + "' ,";
    }
  }
  updSql = updSql + sets.substring(0, sets.length - 1) + " where 1 = 1 " + wh;
  logger.info('-- updSql:' + updSql);
  if (sql) {
    return updSql;
  } else {
    pool.getConnection(function (err, conn) {
      if (err) {
        callBack(err);
        return;
      } else {
        conn.query(updSql, function (err, rows, fields) {
          conn.release();
          if (err) {
            callBack(err);
            return;
          } else if (rows.length == 0) {
            callBack(null, []);
          } else {
            callBack(null, rows, fields);
          }
        });
      }
    });
  }
}

var DBTransaction = function (sqls, callBack) {
  pool.getConnection(function (err, conn) {
    if (err) {
      callBack(err);
      return;
    } else {
      conn.beginTransaction(function (err) {
        if (err) {
          logger.error('--- error in transaction ----');
          callBack(err);
        } else {
          var error = null;
          var cnt = 0;

          var transCb = function(err){
            cnt ++;
            if(err){
              error = err;
            }
            if(cnt == sqls.length){
              if(error){
                conn.rollback(function (rbErr) {
                  callBack(error);
                });
              }else{
                conn.commit(function (cmErr) {
                  if (cmErr) {
                    //logger.info('commit transaction error:' + JSON.stringify(cmErr));
                    callBack(err);
                  } else {
                    //logger.info('all Transactions committed');
                    conn.release();
                    callBack(null, 1);
                  }
                });
              }
            }
          }

          for (var i in sqls) {
            //logger.info('execute sql:' + sqls[i]);
            // this is un_synchronized function
            conn.query(sqls[i], function (err, rows, fields) {
              transCb (err);
            });
          }
        }
      });
    }
  });
}
DbUtil.DBUpdate = DBUpdate;
DbUtil.DBTransaction = DBTransaction;
DbUtil.DBQuery = DBQuery;
module.exports = DbUtil;