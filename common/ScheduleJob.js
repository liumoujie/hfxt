/**
 * Created by zhoulanhong on 8/26/17.
 */
var sql = require('mssql');
var Id = require('../common/Id');
var pool = require('../common/connection_pool');
var wrapper = require('co-mysql');
var co = require('co');
var cmoment = require('moment');
var client = require('../common/smsClient');
let p = wrapper(pool);

let nextFiveMiunte = null;

var ScheduleJob = function () {
    nextFiveMiunte = Date.now();
}
function noticeJob() {
    if (sql) {
        sql.close();
    }
    pool = new sql.ConnectionPool({
        user: 'sa',
        password: 'admin1!',
        server: '124.161.254.148', // You can use 'localhost\\instance' to connect to named instance
        port: 3480,
        database: 'scshdb'
    }, err => {
        if (err) {
            console.log(err);
        } else {
            console.log("-------- start synchronize data --------------------");
            pool.request()
                //.query('SELECT ID as id, CZBM as czmb, Value as v, TM as t from COLLECT where tm > dateadd(MINUTE,-5,getdate());', (err, result) => {
                .query('SELECT ID as id, CZBM as czmb, Value as v, TM as t from COLLECT ;', (err, result) => {
                    console.log("-- query results : " + JSON.stringify(result));
                    if (err) {
                        console.log(err);
                    } else if (result.recordset.length != 0) {

                        //will sendMsgAndLed
                        //sendMsgAndLed({v: 0.45, czmb: "ZYNL000006", t: "2018-07-05 10:20:00"});
                        co(function *() {
                            let needIns = result.recordset;

                            //找出每所有站点最后一次更新
                            let maxRows = yield p.query("SELECT max(id) as mid, siteid FROM contents GROUP by siteid ");

                            for (let i in  needIns) {
                                console.log(needIns[i].czmb.substr(0, 4) + needIns[i].czmb.substr(8, 2) + cmoment(needIns[i].t).valueOf().toString().substr(0, 10));
                                let curId = needIns[i].czmb.substr(0, 4) + needIns[i].czmb.substr(8, 2) + cmoment(needIns[i].t).valueOf().toString().substr(0, 10);

                                if (needIns[i].czmb.indexOf("NL") > 0) {
                                    processInFiveMinute();
                                    if(needIns[i].v > 0.2){
                                        processNL(needIns[i]);
                                    }
                                }

                                //if (needIns[i].v > 0.2 && needIns[i].czmb.indexOf("NL") > 0) {
                                //    processNL(needIns[i]);
                                //}

                                if (needIns[i].czmb.indexOf("SW") > 0) {
                                    processSW(needIns[i]);
                                }

                                for (let j in maxRows) {
                                    if (needIns[i].czmb == maxRows[j].siteid) {
                                        //如果有新的更新,插入数据
                                        if (curId > maxRows[j].mid) {
                                            let insertSql = "INSERT INTO contents (id,siteid,flow,createtime) values ('" + curId
                                                + "','" + needIns[i].czmb + "','" + needIns[i].v + "','" + cmoment(needIns[i].t).valueOf() + "')";
                                            yield p.query(insertSql);
                                        } else {
                                            break;
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
        }
    });

    pool.on('error', err => {
        console.log(err);
        pool.close();
    });
}

function processSW(data) {

    //let data = {v: 2.67, czmb: "ZYSW000001", t: "2018-07-05 10:20:00"};
    console.log("-----  will start process SW ----- ")

    let querySql = "SELECT a.name,a.fiveh,a.twentyh,a.thirtyh,fiftyh FROM sites a where a.id = '" + data.czmb + "';";

    console.log(querySql);
    co(function *() {

        let rows = yield p.query(querySql);
        console.log("cur row : " + JSON.stringify(rows));

        if (rows[0] && rows[0].fiveh && rows[0].twentyh && rows[0].fiftyh && (parseFloat(rows[0].twentyh) - parseFloat(rows[0].fiveh) < data.v)) {

            let nbrs = rows[0].fiftyh.split(",");
            let signNames = [];
            let paramJsons = [];
            let phoneNbs = [];
            for (let i = 0; i < nbrs.length; i = i + 2) {
                signNames.push('洪水风险管理系统');
                phoneNbs.push(nbrs[i]);
                paramJsons.push({
                    "name": nbrs[i + 1],
                    "infoTime": cmoment(parseInt(t)).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss'),
                    "infoAddress": "水位站:" + rows[0].name,
                    "infoText": "已经超过告警水位" + (data.v - (parseFloat(rows[0].twentyh) - parseFloat(rows[0].fiveh))).toFixed(2) + "米"
                });
            }

            console.log(JSON.stringify(paramJsons));

            client.sendBatchSMS({
                PhoneNumberJson: JSON.stringify(phoneNbs),
                SignNameJson: JSON.stringify(signNames),
                TemplateCode: 'SMS_138064899',
                TemplateParamJson: JSON.stringify(paramJsons)
            }).then(function (res) {
                let {Code}=res;
                console.log("Code : " + Code);
                if (Code === 'OK') {
                    //处理返回参数
                    console.log(res);
                }
            }, function (err) {
                console.log(err);
                response.json({status: 500});
            })
        } else {
            console.log("----------------- no configuration ----------------------");
        }


    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })

}


function processNL(data) {

    //let data = {v: 0.45, czmb: "ZYNL000006", t: "2018-07-05 10:20:00"};
    console.log("---------- ")

    let querySql = "SELECT a.name,a.tenh,a.twentyh,a.thirtyh,fiftyh, b.* FROM sites a, leds b where a.id = b.rellogging and a.id = '" + data.czmb + "';";

    console.log(querySql);
    co(function *() {

        let rows = yield p.query(querySql);
        console.log("cur row : " + JSON.stringify(rows));

        if (rows[0] && rows[0].fiftyh && rows[0].thirtyh && rows[0].twentyh && rows[0].tenh && rows[0].screenid) {

            let showMsg = "";
            //if (data.v > 0.4) {
            //    showMsg = rows[0].thirtyh;
            //} else if (data.v > 0.3) {
            //    showMsg = rows[0].twentyh;
            //} else if (data.v > 0.2) {
            //    showMsg =   rows[0].tenh;
            //}


            if(data.v> 0.2){
                showMsg = "前方水深"+dava.v+"米,请小心";
            }

            //发送短信给负责人
            if (data.v > 0.3) {
                let nbrs = rows[0].fiftyh.split(",");
                let signNames = [];
                let paramJsons = [];
                let phoneNbs = [];
                for (let i = 0; i < nbrs.length; i = i + 2) {
                    signNames.push('洪水风险管理系统');
                    phoneNbs.push(nbrs[i]);
                    paramJsons.push({
                        "name": nbrs[i + 1],
                        "infoTime": cmoment(data.t).format("YYYY-M-D HH:mm:ss"),
                        "infoAddress": "内涝点:" + rows[0].name,
                        "infoText": (data.v > 0.4 ? rows[0].thirtyh : rows[0].twentyh)
                    });
                }

                client.sendBatchSMS({
                    PhoneNumberJson: JSON.stringify(phoneNbs),
                    SignNameJson: JSON.stringify(signNames),
                    TemplateCode: 'SMS_138064899',
                    TemplateParamJson: JSON.stringify(paramJsons)
                }).then(function (res) {
                    let {Code}=res;
                    console.log("Code : " + Code);
                    if (Code === 'OK') {
                        //处理返回参数
                        console.log(res);
                    }
                }, function (err) {
                    console.log(err);
                    response.json({status: 500});
                })
            }

            //设置了自动同步LED屏
            if (rows[0].state == "1") {
                let utf8Length = function (str) {
                    var realLength = 0;
                    var len = str.length;
                    var charCode = -1;
                    for (var i = 0; i < len; i++) {
                        charCode = str.charCodeAt(i);
                        if (charCode >= 0 && charCode <= 128) {
                            realLength += 1;
                        } else {
                            // 如果是中文则长度加3
                            realLength += 3;
                        }
                    }
                    return realLength;
                }

                let postDataJson = {
                    "imei": rows[0].screenid, "fontSize": 16, "fontColor": 1, "content": showMsg
                }
                let vpostData = JSON.stringify(postDataJson);
                var options = {
                    hostname: "localhost",
                    port: "9090",
                    path: "/send",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Content-Length': utf8Length(vpostData.length)
                    }
                };
                console.log(vpostData);
                var body = "";
                var req = http.request(options, function (res) {
                    res.setEncoding('utf8');
                    res.on('data', function (chunk) {
                        body += chunk;
                    });
                    res.on('end', function () {
                        //response.json({status: 200, data: {ret: "ok"}});
                        console.log("body : " + body);
                    });
                });

                req.on('error', function (e) {
                    //response.json({status: 500, message: JSON.stringify(e)})
                });

                req.write(vpostData);
                req.end();
            }


        } else {
            console.log("------------- no configuration ---------------------");
        }


    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })

}

// add update led screen five minutes a time
function processInFiveMinute(data) {

    //let data = {v: 0.45, czmb: "ZYNL000006", t: "2018-07-05 10:20:00"};
    console.log("----- processInFiveMinute ----- ")

    if(Date.now() > nextFiveMiunte ){
        nextFiveMiunte = Date.now() + 5 * 60 * 1000;
    }else{
        console.log("----- return null ------")
        return null;
    }

    let querySql = "SELECT a.name,a.tenh,a.twentyh,a.thirtyh,fiftyh, b.* FROM sites a, leds b where a.id = b.rellogging and a.id = '" + data.czmb + "';";

    console.log(querySql);
    co(function *() {

        let rows = yield p.query(querySql);
        console.log("cur row : " + JSON.stringify(rows));

        if (rows[0] && rows[0].fiftyh && rows[0].thirtyh && rows[0].twentyh && rows[0].tenh && rows[0].screenid) {

            let showMsg = "前方水深"+dava.v+"米,请小心";

            //设置了自动同步LED屏
            if (rows[0].state == "1") {
                let utf8Length = function (str) {
                    var realLength = 0;
                    var len = str.length;
                    var charCode = -1;
                    for (var i = 0; i < len; i++) {
                        charCode = str.charCodeAt(i);
                        if (charCode >= 0 && charCode <= 128) {
                            realLength += 1;
                        } else {
                            // 如果是中文则长度加3
                            realLength += 3;
                        }
                    }
                    return realLength;
                }

                let postDataJson = {
                    "imei": rows[0].screenid, "fontSize": 16, "fontColor": 1, "content": showMsg
                }
                let vpostData = JSON.stringify(postDataJson);
                var options = {
                    hostname: "localhost",
                    port: "9090",
                    path: "/send",
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/x-www-form-urlencoded',
                        'Content-Length': utf8Length(vpostData.length)
                    }
                };
                console.log(vpostData);
                var body = "";
                var req = http.request(options, function (res) {
                    res.setEncoding('utf8');
                    res.on('data', function (chunk) {
                        body += chunk;
                    });
                    res.on('end', function () {
                        //response.json({status: 200, data: {ret: "ok"}});
                        console.log("body : " + body);
                    });
                });

                req.on('error', function (e) {
                    //response.json({status: 500, message: JSON.stringify(e)})
                });

                req.write(vpostData);
                req.end();
            }


        } else {
            console.log("------------- in five  no configuration ---------------------");
        }


    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })

}

ScheduleJob.noticeJob = noticeJob;
module.exports = ScheduleJob;