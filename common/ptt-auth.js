/**
 * Created by suke on 15/11/2.
 */

const PttContactUser = require('../models/ptt-contact-user');
const co = require('co');
const _ = require('underscore');

// 用来验证后台系统发上来的请求
var isAuthenticated = function (req, res, next) {
    if (req.isAuthenticated())
        return next();

    res.redirect('/');
};

// 用来验证客户端或者后台系统发上来的请求
let isClientAuthenticated = function (req, res, next) {
    co(function *() {
        let auth = req.headers.authorization;

        if (!auth) {
            isAuthenticated(req, res, next);
            return;
        }

        let logInfo = new Buffer(auth.substr(6), 'base64').toString().split(':');
        if (_.size(logInfo) != 2) {
            isAuthenticated(req, res, next);
            return;
        }

        let user = yield PttContactUser.findOne({idNumber: logInfo[0], password: logInfo[1]}).exec();
        if (!user) {
            isAuthenticated(req, res, next);
            return;
        }

        req.user = user;
        req.session.enterprise = user.enterprise;
        next();
    });
};

exports.isAuthenticated = isAuthenticated;
exports.isClientAuthenticated = isClientAuthenticated;