/**
 * Created by suke on 15/10/30.
 */

var LocalStrategy = require('passport-local').Strategy;
var PttEnterprise = require('../models/ptt-enterprise');
var crypto = require('crypto');
var co = require('co');
var PttUtil = require('../common/ptt-util');

//统一切换到用企业编号作为用户名,同时密码的维护不单独使用ptt-administrator来实现了.

module.exports = function (passport) {

    passport.serializeUser(function (enterprise, done) {

        //console.log('serializing enterprise : ', enterprise);
        done(null, enterprise.idNumber);

    });

    passport.deserializeUser(function (id, done) {

        PttEnterprise.findOne({idNumber: id}, function (err, enterprise) {
            //console.log('deserializing enterprise : ', enterprise);

            if (enterprise)
                done(null, enterprise);
            else
                done(null, null);
        });
    });

    //登录策略
    passport.use('login', new LocalStrategy({passReqToCallback: true}, function (req, username, password, done) {
        let criteria = {'phoneNumber': username};
        if (username == "0") {
            criteria = {'idNumber': username};
        }
        PttEnterprise.findOne(criteria, function (err, enterprise) {

            if (err) {
                return done(err);
            }

            if (!enterprise) {
                return done(null, false, req.flash('message', '企业编号不存在'));
            }

            if (enterprise.managerPwd !== PttUtil.md5(password)) {
                return done(null, false, req.flash('message', '密码错误'));
            }

            if(enterprise.expTime < Date.now()){
                return done(null, false, req.flash('message', '企业账号已经过期'));
            }

            req.session.enterprise = enterprise;//xp.qing adding for gis orgTree getting enterprise info. 16.9.8
            return done(null, enterprise);
        });


    }));

    //注册策略
    /*passport.use('signup', new LocalStrategy({passReqToCallback: true}, function (req, username, password, done) {

     co(function * ()
     {
     try {
     var user = yield PttManager.findOne({'name': username}).exec();

     if (user) {
     console.log('User already exists with username :', username);
     done(null, 501, req.flash('message', username + ' 该用户已经存在！'));
     } else {

     var newUser = new PttManager();
     newUser.name = username;
     newUser.password = createHash(password);
     newUser.email = req.param('email');
     newUser.gender = req.param('gender');
     newUser.address = req.param('address');

     try {

     yield newUser.save();
     console.log('User Registration successful');
     done(null, newUser);

     } catch (err) {
     console.log('Error in Saving user: ', err);
     done(err);
     }
     }
     } catch (err) {
     console.log('sign up find user error :', err);
     done(err);
     }
     }
     )
     ;
     }));*/
};




