/**
 * Created by suke on 15/12/5.
 */

var mongoose = require('mongoose');

var dbPath = process.env.MONGODB_PATH || "mongodb://localhost/hfxt";

mongoose.connect(dbPath);

console.log("Connecting to mongodb: " + dbPath);

module.exports = mongoose;