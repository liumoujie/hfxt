/**
 * Created by suke on 15/12/7.
 *
 * 管理员账号
 */
var mongoose = require('mongoose');

var PttAdministratorSchema = new mongoose.Schema(
    {
        //所属企业
        enterprise: {type: mongoose.Schema.ObjectId, ref: "PttEnterprise"},

        //用户名
        name: String,

        //密码
        password: String
    },
    {
        collection: "managers" //给collections指定一个名字，而不是系统自动生成
    }
);

PttAdministratorSchema.statics = {
    //listByUser: function (userId, cb)
}

PttAdministratorSchema.methods = {}


var PttAdministrator = mongoose.model("PttAdministrator", PttAdministratorSchema);

module.exports = PttAdministrator;