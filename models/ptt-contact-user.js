/**
 * Created by suke on 15/12/5.
 *
 * PTT用户模型定义
 */
var mongoose = require('mongoose');
const _ = require('underscore');
const co = require('co');

var PttContactUserSchema = new mongoose.Schema(
    {
        //所属企业
        enterprise: {type: mongoose.Schema.ObjectId, ref: "PttEnterprise"},

        //上级,可以是企业,部门,也可以是人.用于保存组织关系.xp.qing added for gis
        father: {type: mongoose.Schema.ObjectId},

        //用户名
        name: String,

        //通讯号码  系统自动生成，全系统唯一,作为各个系统间通讯的标识，也可以作用用户的登录号码
        idNumber: {type: Number, index: true},

        //手机号码   全系统唯一，可以作为用户的登录号码
        phoneNbr: Number,

        //电子邮件   全系统唯一，可以作为用户登录号码
        mail: String,

        //是否在终端打开位置信息
        locationEnable: Boolean,

        //终端GPS扫描间隔时间（毫秒），仅在位置信息开关打开的时候有效
        locationScanInterval: Number,

        //终端上报GPS数据的间隔（毫秒），仅在位置信息开关打开的时候有效
        locationReportInterval: Number,

        //按周分类的上报时间,格式为一个由7个0或者1组成的数组,分别代表周日,周一...周六
        // 如[0,1,1,1,1,1,0]代表周一到周五
        // 而[0,0,1,0,0,1,0]则代表周二和周五
        locationWeekly: Object,

        //每天上报位定位的开始和持续时长(单位为小时)
        locationTime: {
            from: String,
            last: String
        },

        //记录用户上次的位置
        lastLocation: {type: Object, index: '2dsphere'},

        lastLocationTime: {type: Date},

        //头像
        //avatar: String,

        //职务 xp.qing added for gis
        position: {type: mongoose.Schema.ObjectId, ref: "PttPosition"},

        //密码
        password: {type: String, index: true},

        //用户的权限
        privileges: {
            callAble: {type: Boolean, default: true},//发起单呼
            groupAble: {type: Boolean, default: true},//发起群呼
            calledAble: {type: Boolean, default: true},//接受呼
            joinAble: {type: Boolean, default: true},//接受群呼[比较特殊,仅用于在会话组添加成员的时候禁止
            forbidSpeak: {type: Boolean, default: false},//是否允许发言
            callOuterAble: {type: Boolean, default: false},//是否可以呼其它单位
            calledOuterAble: {type: Boolean, default: false},//是否可以被其它单位呼.
            powerInviteAble: {type: Boolean, default: false},//是否可以强拉其它人(被指挥号[权限等级为0]拉起的人除外)
            muteAble: {type: Boolean, default: false},//是否具有免打扰的权限
            viewMap: {type: Boolean, default: false}, //是否具有查看地图的权限
            emergency: {type: Boolean, default: false}, //紧急呼叫权限
            boardcast: {type: Boolean, default: false}, //系统全呼权限
            groupBoardcast: {type: Boolean, default: false}, //组广播权限
            noDevice: {type: Boolean, default: false}, //不绑定设备
            priority: Number
        },

        //可以紧急呼叫的人
        emergencyPerson: [],

        //可以查看的附近的人
        nearbyPerson: [],

        //导入状态，用于控制导入时的回退
        impstatus: String,

        //用户状态
        //0 正常,2016年9月14号以前老用户为空;
        //1 停用,表示此用户暂时不能登录到系统中
        status: Number,

        //是否是指挥权限,亦是企业对话中的最高权限,可以强拉
        //0 或者 空 为否
        //1 是
        commander: Number,

        //职位对应的 icon 用于在树上的图标显示
        icon: String,

        //职位对应的 icon 用于在地图上的图标显示
        locateicon: String,

        //职位名称
        positionname: String,

        //最后一次定位对应的中国标准时间
        lastLocationTimeCST: String,

        //五个设备号,设备一到设备五
        devices: Object,
    },
    {
        collection: "contact.users" //给collections指定一个名字，而不是系统自动生成
    }
);

let PttContactUser;

PttContactUserSchema.statics = {

    /**
     * List users by id numbers.
     * @param idNumbers Promise<Array>
     */
    findByIdNumbers(idNumbers) {
        // Sanitize array first
        for (let i = idNumbers.length - 1; i >= 0; i--) {
            idNumbers[i] = parseInt(idNumbers[i]);
        }

        return PttContactUser.find({
            idNumber: {
                $in: idNumbers
            }
        }).exec();
    }
};

PttContactUserSchema.methods = {

    /**
     * 查找一个企业下所有的指挥员
     *
     * @param enterpriseId 企业objectId
     * @return Promise<List<PttContactUser>>
     */
    findCommanders: function (enterpriseId) {
        "use strict";
        return PttContactUser.find({"commander" : 1, enterprise: enterpriseId}).exec();
    },

    /**
     * 将一个数据库实例呈现为可下发给当前用户的数据。注意，这个数据只可供当前用户查看
     *
     * @param enterprise PttEnterprise 用户所属企业
     * @param commanders {List<PttContactUser>|undefined} 这个企业的管理员。如果这个值不传，则该API会自己去获取
     * @return Promise<LoginResponse>
     */
    toLoginResponse: function (enterprise, commanders) {
        const self = this;
        return co(function *() {
            "use strict";
            if (!_.isEqual(self.enterprise, enterprise._id)) {
                throw "Enterprise is not current user's enterprise"
            }

            //查询出此企业的指挥号
            if (typeof commanders == "undefined") {
                commanders = yield self.findCommanders(enterprise._id);
            }

            console.log("enterPriseCommander:", commanders);

            let retMsg = {
                name: self.name,
                idNumber: self.idNumber,
                privileges: self.simplifyPrivileges(self.privileges),
                password: self.password,
                // avatar: self.avatar,
                enterId: enterprise.idNumber,
                enterObjectId: enterprise._id,
                enterName: enterprise.name,
                key: enterprise.key,
                vedioMode: enterprise.vedioMode,
                maxGroupMembers: enterprise.quota.preGroup,
                enterexpTime: enterprise.expTime,
                commanderIdNumbers: _.map(commanders, c => c.idNumber)
            };

            //额外的紧急呼叫人员名单。客户端会主动将管理员加入名单中，这里只需要提供额外的人员名单
            retMsg.extraEmergencyUserIdNumbers = self.emergencyPerson;

            //如果有查看地图权限,传入可以查看哪些人
            if (retMsg.privileges.viewMap) {
                retMsg.nearbyPerson = self.nearbyPerson;
            }

            if (enterprise.mode.modetype == "1") {
                //指挥模式
                retMsg.locationEnable = true;
                retMsg.locationScanInterval = enterprise.mode.collectFreq;
                retMsg.locationReportInterval = enterprise.mode.collectFreq;
                //观察模式,取用户表里设置的数据
            }
            else if (self.locationEnable) {
                //位置上报打开，向用户发送上传频率等数据
                retMsg.locationEnable = true;
                retMsg.locationScanInterval = self.locationScanInterval;
                retMsg.locationReportInterval = self.locationReportInterval;
                retMsg.locationWeekly = self.locationWeekly;
                retMsg.locationTime = self.locationTime;
            }

            //retMsg.locationEnable = true;
            //retMsg.locationScanInterval = 1000 * 30;
            return retMsg;
        });
    },

    // 简化用户的权限数据：去除所有权限为默认值的选项
    simplifyPrivileges: function (priv) {
        let ret = {};

        _.each(priv.toObject(), (v, k) => {
            if (typeof v == "boolean") {
                if (v) {
                    ret[k] = v;
                }
            }
            else {
                ret[k] = v;
            }
        });

        return ret;
    },
};


PttContactUser = mongoose.model("PttContactUser", PttContactUserSchema);
module.exports = PttContactUser;