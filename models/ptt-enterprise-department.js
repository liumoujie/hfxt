/**
 * Created by mycow on 16/9/8.
 */

var mongoose = require('mongoose');

var PttDepartmentSchema = new mongoose.Schema(
    {
        //所属企业
        enterprise: {type: mongoose.Schema.ObjectId, ref: "PttEnterprise"},

        //表示为父节点,为优化前端组织树
        isParent: Boolean,

        //上级,可以是企业,部门,也可以是人.用于保存组织关系.xp.qing added for gis
        father: {type: mongoose.Schema.ObjectId},

        //部门名称
        name: String
    },
    {
        collection: "departments" //给collections指定一个名字，而不是系统自动生成
    }
);

PttDepartmentSchema.statics = {
    //listByUser: function (userId, cb)
}

PttDepartmentSchema.methods = {}


var PttDepartment = mongoose.model("PttDepartment", PttDepartmentSchema);

module.exports = PttDepartment;