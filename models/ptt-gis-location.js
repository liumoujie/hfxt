/**
 * Created by zhoulanhong on 10/5/16.
 * for user location
 */
var mongoose = require('mongoose');

var PttGisLocationSchema = new mongoose.Schema(
    {
        //对应人员
        user: {type: mongoose.Schema.ObjectId, ref: "PttContactUser"},

        //经度
        lng: Number,

        //纬度
        lat: Number,

        //速度
        speed: Number,

        //精度（单位米）
        radius: Number,

        //海拔
        alt: Number,

        //上报时间
        repTime: {type: Date},

        //缓存的地址信息
        address: {
            formatted_address: String,//格式化之后的地址
            country: String,//国家
            province: String,//省
            city: String,//市
            district: String,//区
            adcode: String,//邮编
            street: String,//街道
            street_number: String,//门牌号
        }
    },
    {
        collection: "gis.location" //给collections指定一个名字，而不是系统自动生成
    }
);

PttGisLocationSchema.statics = {
    //listByUser: function (userId, cb)
};

PttGisLocationSchema.methods = {

};

var PttGisLocation = mongoose.model("PttGisLocation", PttGisLocationSchema);

module.exports = PttGisLocation;