const mongoose = require('mongoose');
const co = require('co');
const PttChatRoom = require('./ptt-chat-room');
const _ = require('underscore');

const PttMessageSchema = new mongoose.Schema(
    {
        localId: {type: String, index: true},

        // 发送者用户ID
        senderId: {type: Number, index: true},

        // 目标房间ID
        roomId: {type: Number, index: true},

        // 发送时间（由服务器填充）
        sendTime: {type: Date, index: true},

        // 消息类型
        type: String,

        // 消息的主体内容
        body: Object,
    },
    {
        collection: "messages" //给collections指定一个名字，而不是系统自动生成
    }
);

var PttMessage;

PttMessageSchema.statics = {
    /**
     * 查找消息
     *
     * @param userId {Number|null} 用户ID，为空则不限制。
     * @param roomId {Number|null} 聊天室ID，为空则不限制
     * @param startDate {Date|null} 消息起始日期（不包含），为空则不限制
     * @param endDate {Date|null} 消息结束日期（包含），为空则不限制
     *
     * @return {Promise<Array<PttMessage>>} 消息列表
     */
    findMessages: function (userId, roomId, startDate, endDate) {
        return co(function *() {
            "use strict";
            const criteria = {};

            if (roomId) {
                criteria["roomId"] = roomId;
            }
            else if (userId) {
                // 如果没有指定RoomId，则必须限定用户的访问房间号
                criteria["roomId"] = { $in: _.map(yield PttChatRoom.listByUser(userId), room => room.idNumber) }
            }

            if (startDate || endDate) {
                const sendTimeCriteria = {};
                if (startDate) {
                    sendTimeCriteria["$gt"] = startDate;
                }

                if (endDate) {
                    sendTimeCriteria["$lte"] = endDate;
                }

                criteria["sendTime"] = sendTimeCriteria;
            }

            return PttMessage.find(criteria);
        });
    },
};

PttMessageSchema.methods = {

};

PttMessage = mongoose.model("PttMessage", PttMessageSchema);
module.exports = PttMessage;