/**
 * Created by zhoulanhong on 10/30/16.
 * for user location add test
 */
var mongoose = require('mongoose');
//索引字段
//db.position.ensureIndex({"enterprise":1,"name":1},{"unique":true});
var PositionSchema = new mongoose.Schema(
    {
        //所属企业
        enterprise: {type: mongoose.Schema.ObjectId, ref: "PttEnterprise"},

        //名称
        name: String,

        //描述
        description: String,

        //图标地址
        icon: String,

        //图标保存的文件名
        image: String,
    },
    {
        collection: "position" //给collections指定一个名字，而不是系统自动生成
    }
);

PositionSchema.statics = {
    //listByUser: function (userId, cb)
}

PositionSchema.methods = {}


var PttPosition = mongoose.model("PositionSchema", PositionSchema);

module.exports = PttPosition;
