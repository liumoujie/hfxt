/**
 * Created by feihe on 2016/12/4.
 * 记录各个终端支持push通道的push token(ios上由手机上报手机device token,后面根据此token来发送push消息)
 */

var mongoose = require('mongoose');

var PttPushTokenSchema = new mongoose.Schema(
    {
        //用户名
        idNumber: {type: Number, index: true, ref: "PttContactUser"},

        //设备类型 0--ios, 1--android
        deviceType: Number,

        //令牌
        token: String
    },

    {
        collection: "pushtoken" //给collections指定一个名字，而不是系统自动生成
    }
);

PttPushTokenSchema.statics = {
    //listByUser: function (userId, cb)
}

PttPushTokenSchema.methods = {}


var PttPushToken = mongoose.model("PttPushToken", PttPushTokenSchema);

module.exports = PttPushToken;