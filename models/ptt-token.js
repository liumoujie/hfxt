/**
 * Created by suke on 15/12/6.
 * 语音房间的模型定义
 */

var mongoose = require('mongoose');

var PttTokenSchema = new mongoose.Schema(
    {
        //用户名
        idNumber: {type: String, index: true},

        //创建时间
        createTime: String,

        //令牌
        token: String
    },
    {
        collection: "tokens" //给collections指定一个名字，而不是系统自动生成
    }
);

PttTokenSchema.statics = {
    //listByUser: function (userId, cb)
}

PttTokenSchema.methods = {}


var PttToken = mongoose.model("PttToken", PttTokenSchema);

module.exports = PttToken;