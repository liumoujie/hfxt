/**
 * Created by suke on 15/12/14.
 */

//浏览器端发送socket.io事件
window.EVENT =
{
    SEND: {
        LOGIN: 'c_login',
        //同步通讯录
        SYNC_CONTACTS: "c_sync_contact",

        //新建一个房间
        CREATE_CHAT_ROOM: "c_create_room",
        //加入一个房间
        JOIN_ROOM: "c_join_room",
        //离开一个房间
        LEAVE_ROOM: "c_leave_room",
        //请求一个房间的基础数据
        GET_ROOM_INFO: "c_room_info",

        //抢麦
        CONTROL_MIC: "c_control_mic",
        //释放麦
        RELEASE_MIC: "c_release_mic",

        // 修改密码
        CHANGE_PASSWORD: "c_change_pwd",

        // 添加房间成员
        ADD_ROOM_MEMBERS: "c_add_room_members",

        // 更改房间名称
        UPDATE_ROOM_NAME: "c_update_room_name",

        /////////////////以下事件不直接发给信号服务器/////////////////////////
        //目前由于webrtc必须运行在https上,而且https里不允许访问非https的网站,且当前voice server开的是普通的的http端口
        //故web端无法直接连接voice server,需要当前https web server来中转命令
        //通知voice media server加入房间
        JOIN_VOICE_SERVER_ROOM: 'c_join_voice_server_room',

        //通知voice media server离开房间
        LEAVE_VOICE_SERVER_ROOM: 'c_leave_voice_server_room',

        //向voice media server发心跳
        HEARTBEAT_TO_VOICE_SERVER: 'c_send_heartbeat_tovoice_server',
        //向voice server发送抢麦指令
        CONTROL_MIC_TO_VOICE_SERVER: 'c_send_control_mic_to_voice_server',

        //浏览器允许接收其它人的声音
        ENABLE_SPEAKER: 'c_enable_speaker',

        //浏览端静音
        DISABLE_SPEAKER: 'c_disable_speaker',

        WEBRTC_MSG: 'c_send_webrtc_msg'
    },

    //浏览器收到的socket.io事件.
    RECV: {
        //用户登录成功
        USER_LOGON: "s_logon",

        USER_LOGIN_FAILED: "s_login_failed",

        //群成员变化
        ROOM_UPDATE: "s_member_update",

        //在线成员变化
        ROOM_ONLINE_MEMBER_UPDATE: "s_online_member_update",

        //当前的发言者发生了变化
        SPEAKER_CHANGED: "s_speaker_changed",

        //房间的状态变化的摘要信息:成员和发言者, 这条消息主要用在用户在线,但没在房间的时候通知.
        ROOM_SUMMARY: "s_room_summary",

        //当第一个人加入一个房间时, 向其它人发出邀请加入的信号
        INVITE_TO_JOIN: "s_invite_to_join",

        //新的登录用户把已有用户踢出了
        USER_KICK_OUT: "s_kick_out",

        //用户被踢出房间
        ROOM_KICK_OUT: "s_kick_out_room",

        //在一个设备上新登录时,接收服务端分配的device id,并保存
        UPDATE_DEVICE_ID: 's_update_device_id',

        WEBRTC_MSG: 's_webrtc_msg',

        //用户主动下线通知
        USER_OFFLINE: "s_user_offline",

        // 位置发生变化
        UPDATE_LOCATION: "c_update_location",

        //通知界面指挥时间结束
        COMMAND_MODE_END: "s_command_mode_end",
    }
}