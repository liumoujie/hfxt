/**
 * Created by zhoulanhong on 3/16/17.
 * 本类为所有关于树操作的基本类
 */

function tree(obj, options) {
    //如果没有传dataFilter,则使用默认的dataFilter
    if (!options.async.dataFilter) {
        options.async.dataFilter = function (treeId, parentNode, childNodes) {

            if (!childNodes) {
                return null;
            }
            if (childNodes.status != 200) {
                return null;
            }

            childNodes = childNodes.data;

            return childNodes;
        };
    }

    //处理数据URL的问题
    var asyncUrl = options.async.url;
    var that = this;
    options.async.url = function (treeId, treeNode) {

        if (null == treeNode) {
            return asyncUrl;
        }

        treeNode.zAsync = true;
        that.treeObj.expandNode(treeNode, true, false, true, false);

        return;
    };

    //初始化zTree
    this.treeObj = $.fn.zTree.init($("#" + obj), options);

    this.getTree = function () {
        return this.treeObj;
    }
    /**
     * 用于给前台页面的组织树上添加图标
     */
    this.getPareNodes = function () {
        var nodes = this.treeObj.transformToArray(this.treeObj.getNodes());
        for (var i in nodes) {
            if (nodes[0]) {
                $("#" + nodes[0].tId + "_ico").css({
                    'background-image': 'url("../images/index2.png")',
                    'background-size': '16px',
                    'background-position': '0 0'
                });
            }
            if (nodes[i].isParent) {
                $("#" + nodes[i].tId + "_ico").css({
                    'background-image': 'url("../images/depart.png")',
                    'background-size': '16px',
                    'background-position': '0 0'
                });
                for (var j in nodes[i].children) {
                    $("#" + nodes[i].children[j].tId + "_ico").css({
                        'background-image': 'url("../images/46.png")',
                        'background-size': '13px',
                        'background-position': '0 0'
                    });
                }

            }
        }
    }
    /**
     * 本方法用于获取当前树上所选择的所有节点
     * @retrun {
     *  "_id": checkedNodes[i]._id,
     *  "idNumber": checkedNodes[i].idNumber,
     *  "phoneNbr": checkedNodes[i].phoneNbr,
     *  "staffName": checkedNodes[i].name
     *  }
     * */
    this.getAllSelectedNode = function () {
        var checkedNodes = this.treeObj.getCheckedNodes();
        var selectedNodes = [];
        for (var i in checkedNodes) {
            // 不是叶子节点
            if (!checkedNodes[i].isParent) {
                selectedNodes.push(
                    {
                        "_id": checkedNodes[i]._id,
                        "idNumber": checkedNodes[i].idNumber,
                        "phoneNbr": checkedNodes[i].phoneNbr,
                        "staffName": checkedNodes[i].name
                    }
                );
            }
        }
        return selectedNodes;
    }

    /**
     * 本方法用于获取当前树上的所有叶子节点
     * @retrun nodes
     * */
    this.getAllLeafNodes = function () {
        var allNodes = this.treeObj.transformToArray(this.treeObj.getNodes());
        var retNodes = [];
        for (var i in allNodes) {
            // 不是叶子节点
            if (!allNodes[i].isParent) {
                retNodes.push(allNodes[i]);
            }
        }
        return retNodes;
    }

    /**
     *  为叶子节点设置css样式
     * @retrun nodes
     * */
    this.setLeafNodeStyle = function (style) {
        var allNodes = this.treeObj.transformToArray(this.treeObj.getNodes());
        for (var i in allNodes) {
            // 不是叶子节点
            if (!allNodes[i].isParent) {
                $("#" + allNodes[i].tId + "_span").addClass(style);
            }
        }
    }

    /**
     * 本方法用于获取当前树上的所有节点
     * @retrun nodes
     * */
    this.getAllNodes = function () {
        return this.treeObj.transformToArray(this.treeObj.getNodes());
    }

    /**
     * 根据传入的ID,将ID对应的节点选中,同时返回选中的节点的对应属性
     * @para ids 传入参数的id值
     * @para idPropName 传入参数对应的属性名称,如id是树的_id 则传入_id,如果是对应idNumber,则传入idNumber
     * @part retPropName 需要传出的数据名称
     * */
    this.checkNodeByIds = function (ids, idPropName, retPropName) {
        if (!idPropName) {
            alert("参数属性必填!");
            return null;
        }
        retPropName = retPropName || "name";
        var retVal = "";
        var allNodes = this.treeObj.transformToArray(this.treeObj.getNodes());
        for (var id in ids) {
            for (var i in allNodes) {
                // 不是叶子节点
                if (allNodes[i][idPropName] == ids[id]) {
                    this.treeObj.checkNode(allNodes[i], true, true);
                    retVal += allNodes[i][retPropName] + " ";
                }
            }
        }
        return retVal;
    }

    /**
     * 根据指定的ID返回ID对应的节点
     * */
    this.getTreeNodeById = function (id) {
        var allNodes = this.treeObj.transformToArray(this.treeObj.getNodes());
        for (var i in allNodes) {
            // 不是叶子节点
            if (allNodes[i].idNumber == id) {
                return allNodes[i];
            }
        }
    }

    /**
     * 通过再线离线来搜索树
     * @param status 要搜索的状态 0 表示全部 1 代表在线, 2 代表离线
     * @param nodes  在线的人员列表
     */
    this.searchByStatus = function (status, nodes) {
        var that = this;
        //alert(status);
        //alert(nodes);
        var zNodes = this.treeObj.getNodesByFilter(function (node) {
            //在线人员
            if (status == 1 && !nodes.has(node.idNumber + "")) {
                that.treeObj.hideNode(node);
                return false;
                //不在线人员
            } else if (status == 2 && nodes.has(node.idNumber + "")) {
                that.treeObj.hideNode(node);
                return false;
            } else {
                //子结点符合条件时,显示其所有父结点
                var curNode = node;
                do {
                    var parentNode = curNode.getParentNode();
                    if (null == parentNode) {
                        break;
                    }
                    //父结点已是显示的就不用再往上找父了
                    if (!parentNode.isHidden) {
                        break;
                    }
                    that.treeObj.showNode(parentNode);
                    curNode = parentNode;
                } while (true);
                //修改前的搜索条件时可能会让当前结点已处于hide状态,所以要显示
                that.treeObj.showNode(node);
            }
            return true;
        }, false);
    }

    /**
     * 搜索树
     * @param key 要搜索的参数
     */
    this.search = function (key) {
        var that = this;
        if (key.length == 0) {//为提升效率
            this.treeObj.showNodes(this.treeObj.getNodesByParam("isHidden", true));
            return;
        }
        var zNodes = this.treeObj.getNodesByFilter(function (node) {

            if (node.name.indexOf(key) == -1) {
                that.treeObj.hideNode(node);
                return false;
            } else {
                //子结点符合条件时,显示其所有父结点
                var curNode = node;
                do {
                    var parentNode = curNode.getParentNode();
                    if (null == parentNode) {
                        break;
                    }
                    //父结点已是显示的就不用再往上找父了
                    if (!parentNode.isHidden) {
                        break;
                    }
                    that.treeObj.showNode(parentNode);
                    curNode = parentNode;
                } while (true);
                //修改前的搜索条件时可能会让当前结点已处于hide状态,所以要显示
                that.treeObj.showNode(node);
            }
            return true;
        }, false);
    }

    this.serchline = function (valus) {
        that = this;
        var nodes = this.treeObj.getNodesByFilter(function (node) {
            if (!$("#" + node.tId + "_span").hasClass(valus)) {
                that.treeObj.hideNode(node);
                return false;
            } else {
                //子结点符合条件时,显示其所有父结点
                var curNode = node;
                do {
                    var parentNode = curNode.getParentNode();
                    if (null == parentNode) {
                        break;
                    }
                    //父结点已是显示的就不用再往上找父了
                    if (!parentNode.isHidden) {
                        break;
                    }
                    that.treeObj.showNode(parentNode);
                    curNode = parentNode;
                } while (true);
                //修改前的搜索条件时可能会让当前结点已处于hide状态,所以要显示
                that.treeObj.showNode(node);
            }
            return true;
        }, false);

    };

}