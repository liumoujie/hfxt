/**
 * Created by zhoulanhong on 10/6/16.
 */

function ScheduleWSClient(singalServerUrl) {
    this.singalServerUrl = singalServerUrl || "http://localhost:3000";
    this.onlineMap = new Map();
    this.login();
}

ScheduleWSClient.prototype.getOnlineMap = function () {
    return this.onlineMap;
}

ScheduleWSClient.prototype.login = function () {
    var _this = this;
    try {

        $.get('/api/contact/commander', function (data) {
            if (data && data.status == 200) {

                login = data.data.idNumber + ':' + data.data.password;
                opt = {
                    transports: ['websocket'],
                    secure: true,
                    autoConnect: true
                };
                //web界面采用 cookie 的方式,将 Authorization 传入到后台
                $.cookie('Authorization', "Basicw" + base64.encode(login));
                console.log('will use commander : ' + data.data.idNumber + ' to login');
                socket = io(_this.singalServerUrl, opt);

                socket.on('connect', function () {
                    console.log('-- changed Client has connected to the server!');
                    //setOnlieStaff();
                });

                socket.once('s_logon', function (data) {
                    console.log("login success --" + JSON.stringify(data));
                });

                socket.on(window.EVENT.RECV.ROOM_ONLINE_MEMBER_UPDATE, function (data) {
                    _this.onlineMap.set(data + "", data);
                    setOnlieStaff(data);
                });

                socket.on(window.EVENT.RECV.USER_OFFLINE, function (data) {
                    _this.onlineMap.delete(data + "", data);
                    setOfflieStaff(data);
                });

                socket.on(window.EVENT.RECV.COMMAND_MODE_END, function (data) {
                    clearCommandMode();
                });

                socket.on(window.EVENT.RECV.UPDATE_LOCATION, function (data) {
                    //mapIns.addPoints(data.coordinates[0], data.coordinates[1]);
                    //alert(JSON.stringify(data));
                    //if (map) {
                    //    //map.locate(data.coordinates[0], data.coordinates[1], data.icon, data.name, data.userId);
                    //
                    //    map.locate({
                    //        id: data._id,
                    //        lat: data.coordinates[0],
                    //        lng: data.coordinates[1],
                    //        speed: "0.0KM/H",
                    //        iconPath: data.icon,
                    //        name: data.name,
                    //        userid: data.userId,
                    //        lastLocationTime: data.lastLocationTimeCST,
                    //        position: data.positionname
                    //    });
                    //
                    //}
                });

                socket.on("roomOpreate", function(data){
                    console.log("-someBody createdRoom- " + data);
                });

            } else {
                alert("查询指挥号出错,请联系管理员.");
            }
        });

    } catch (e) {
        console.log(e);
    }
}

ScheduleWSClient.prototype.logout = function () {
    this.socket.disconnect();
    this.socket = null;
}