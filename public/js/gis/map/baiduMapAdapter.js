/**
 * Created by zhoulanhong on 12/16/16.
 * 本类实现通过 baidu 地图来进行操作
 */
var cleck = 0;
var deepLabel;
function BaiduMapAdapter() {
    this.mapObj = null;    // 创建Map实例
    //回放轨迹数据在地图上显示的标记点信息
    var replayLocMapMarker = new Map();
    //回放轨迹数据在地图上显示的标记信息
    var replayLocInfoWin = new Map();
    //人员在地图上显示的标记信息
    var userLocMapMarker = new Map();
    //人员在地图上显示的说明信息
    var userLocMapLabel = new Map();
    //地图上的所有回放点和线
    var allReplayPointOrLine = [];
    //地图回放所使用的动画
    var repInterval = null;
    //地图上已经有了的回放点,当有新的回放点过来的时候,这个点就要被remove掉
    var locatedReplayTrace = null;

    //标记在地图上显示的标记信息
    var markLocMapMarker = new Map();
    //返回当前map
    this.getMap = function (startX, startY) {
        return this.mapObj.pixelToPoint(new BMap.Pixel(startX, startY));
    }


    //返回当前map对象
    this.getMapObj = function () {
        return this.mapObj;
    }

    /*
     * 初如化地图
     *
     * @param overview 是否需要显示类型和缩略图
     * @param geolocation 是否需要定位的导航控件
     * */
    this.initMap = function (divId, overview, geolocation) {
        var that = this;
        this.mapObj = new BMap.Map(divId, {mapType: BMAP_HYBRID_MAP});
        //this.mapObj = new BMap.Map(divId);
        //that.getCurLocation().then(function (data) {
        //    console.log(JSON.stringify(data));
        //    that.mapObj.centerAndZoom(new BMap.Point(data.center.lng, data.center.lat), 11);  // 初始化地图,设置中心点坐标和地图级别
        //    that.mapObj.addControl(new BMap.MapTypeControl());   //添加地图类型控件
        //    that.mapObj.setCurrentCity(data.name);          // 设置地图显示的城市 此项是必须设置的
        //    that.mapObj.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
        //
        //    if (overview) {
        //        that.mapObj.addControl(new BMap.OverviewMapControl());
        //    }
        //    if (geolocation) {
        //        that.mapObj.addControl(new BMap.NavigationControl({
        //            // 靠左上角位置
        //            anchor: BMAP_ANCHOR_TOP_LEFT,
        //            // LARGE类型
        //            type: BMAP_NAVIGATION_CONTROL_LARGE,
        //            // 启用显示定位
        //            enableGeolocation: true
        //        }));
        //        // 添加定位控件
        //        var geolocationControl = new BMap.GeolocationControl();
        //    }
        //}).catch(function (err) {
        //    console.log(err);
        //104.622937,30.131459lng:104.67741,lat:30.136894
        that.mapObj.centerAndZoom(new BMap.Point(104.67741, 30.132894), 14);  // 初始化地图,设置中心点坐标和地图级别


        //let mapType1 = new BMap.MapTypeControl({mapTypes: [BMAP_NORMAL_MAP, BMAP_HYBRID_MAP]});
        //that.mapObj.removeControl(mapType1);


        that.mapObj.addControl(new BMap.MapTypeControl());   //添加地图类型控件
        that.mapObj.setCurrentCity("成都");
        that.mapObj.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放


        that.mapObj.addEventListener("click", function (e) {
            //alert("----");
            console.log("{lng:" + e.point.lng + ",lat:" + e.point.lat + "},");
        });


        if (overview) {
            that.mapObj.addControl(new BMap.OverviewMapControl());
        }
        if (geolocation) {
            that.mapObj.addControl(new BMap.NavigationControl({
                // 靠左上角位置
                //anchor: BMAP_ANCHOR_TOP_LEFT,
                // LARGE类型
                //type: BMAP_NAVIGATION_CONTROL_LARGE,
                // 启用显示定位
                enableGeolocation: true
            }));
            // 添加定位控件
            var geolocationControl = new BMap.GeolocationControl();
        }
        //});
    }

    /**
     * 过经纬度将标记显示到地图上的对应位置
     */
    this.markLocate = function (markObject) {

        var _that = this;
        var lat = markObject.lat;
        var lng = markObject.lang;
        var iconPath = markObject.iconPath;
        var name = markObject.name;
        var desc = markObject.descriptions;
        var markimages = markObject.markimages;
        var _id = markObject._id;

        iconPath = iconPath || "/images/sigo/ui-28.gif";
        var pt = new BMap.Point(lng, lat);
        var myIcon = new BMap.Icon(iconPath, new BMap.Size(24, 24));
        var locMarker = new BMap.Marker(pt, {icon: myIcon});  // 创建标注

        locMarker.addEventListener("click", function () {
            // alert("pop your own window here");
            Marker.editMark({
                _id: _id,
                lag: lat,
                lng: lng,
                iconPath: iconPath,
                name: name,
                desc: desc,
                markimages: markimages
            });
        });

        _that.mapObj.addOverlay(locMarker);
    }


    /**
     * 过经纬度将标记显示到地图上的对应位置
     */
    this.markSquareLocate = function (markObject) {
        var _that = this;
        let polygonPoints = [];
        for (let i in markObject) {
            polygonPoints.push(new BMap.Point(markObject[i].lng, markObject[i].lat));
        }

        var rectangle = new BMap.Polygon(polygonPoints, {
            strokeColor: "none",
            fillColor: "#0000dd",
            fillOpacity: 0.5
        });  //创建矩形

        rectangle.addEventListener("mouseout", function (e) {
            if (deepLabel) {
                _that.mapObj.removeOverlay(deepLabel);
            }
        });

        rectangle.addEventListener("mouseover", function (e) {
            if (deepLabel) {
                _that.mapObj.removeOverlay(deepLabel);
            }
            deepLabel = new BMap.Label("淹没深度" + markObject[markObject.length - 1].count, {
                position: new BMap.Point(e.point.lng, e.point.lat),    // 指定文本标注所在的地理位置
                offset: new BMap.Size(5, -10)
            });  // 创建文本标注对象
            deepLabel.setStyle({
                color: "red",
                fontSize: "12px",
                height: "20px",
                lineHeight: "20px",
                fontFamily: "微软雅黑"
            });
            _that.mapObj.addOverlay(deepLabel);
        });

        _that.mapObj.addOverlay(rectangle);
    }

    /**
     * 过经纬度将标记显示到地图上的对应位置
     */
    this.markBGSquareLocate = function (markObject) {
        var _that = this;
        let polygonPoints = [];
        for (let i in markObject) {
            polygonPoints.push(new BMap.Point(markObject[i].lng, markObject[i].lat));
        }

        var rectangle = new BMap.Polygon(polygonPoints, {
            strokeColor: "none",
            fillColor: "#0000dd",
        });
        _that.mapObj.addOverlay(rectangle);

        _that.mapObj.centerAndZoom(new BMap.Point(104.67971, 30.115028), 13);

    }

    /**
     * 根据数组画线的路径
     */
    this.markPathLocate = function (paths) {
        var _that = this;

        for (let pi = 0; pi < paths.length - 1; pi++) {
            var polyline = new BMap.Polyline([
                new BMap.Point(paths[pi].lng, paths[pi].lat),
                new BMap.Point(paths[pi + 1].lng, paths[pi + 1].lat)
            ], {strokeColor: "red", strokeWeight: 2, strokeOpacity: 0.5});   //创建折线
            _that.mapObj.addOverlay(polyline);   //增加折线
        }
    }


    /**
     * 设置每个点的流量和流速
     */
    this.markAllSatations = function (stats) {

        var _that = this;
        _that.clearOverlays();
        if (!this.mapObj) {
            throw new Error("please init this.mapObj first");
        }

        let allPoints = [];

        let iconPath = "/logo/loc.png";
        let picSize = 28;
        for (var i in stats) {
            allPoints.push({x: parseFloat(stats[i].lng), y: parseFloat(stats[i].lat)});

            if (stats[i].type == 1) {
                iconPath = "/logo/loc.png";
            } else if (stats[i].type == 2) {
                iconPath = "/logo/nld.png";
            }
            if (stats[i].alarm && stats[i].alarm == 1) {
                picSize = 64;
                iconPath = "/images/alarmSec.gif";
            }
            let myIcon = new BMap.Icon(iconPath, new BMap.Size(picSize, picSize));
            let locMarker = new BMap.Marker(new BMap.Point(stats[i].lng, stats[i].lat), {icon: myIcon});  // 创建标注

            //if (stats[i].type != 3) {
            (function (pt) {

                locMarker.addEventListener("click", function () {
                    $.ajax({
                        url: '/hfxt/queryContents?siteid=' + pt.id,
                        type: 'get',
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            //成功
                            if (data.status == 200) {

                                let ddd = data.data[0];
                                _that.mapObj.openInfoWindow(new BMap.InfoWindow(
                                    "<div class=\"layer_basic_tittle\">" +
                                    "<div class=\"layer_basic_tittle_1\"><div>" +
                                    "<h6 class=\"con_text_cut\">" + (pt.name ? pt.name : '') +

                                    (pt.type == 1 ? "&nbsp;&nbsp;<img src='/images/vedio1.png' title='全景图' onclick=\"showFullScreenAction('" + pt.name + "','" + pt.id + "')\">" : "") +

                                    (pt.type == 1 ? "&nbsp;&nbsp;<img src='/images/hongshui.png' title='洪峰图' width='42px' height='42px' onclick=\"showSigleImgMgt('1', '', '', '')\">" : "") +

                                    "&nbsp;&nbsp;<img src='/images/pic.png' width='42px' height='42px' title='" + (pt.type == 1 ? "断面图" : "内涝图") + "' onclick=\"showSigleImgMgt('2', '', '', '" + pt.id + "')\">" +

                                    "</h6></div>" +
                                    "<div class=\"layer_basic_tittle_1_ico\"></div>" +
                                    "<h6 class=\"color_1\"></h6></div>" +
                                    "<div class=\"layer_basic_line_1\"></div>" +
                                    "<div class=\"layer_basic_info\">" +
                                    "<div class=\"layer_basic_info_1\">" +
                                    "<div class=\"layer_basic_info_1_1\">当前水位：<span>" + (ddd.flow ? ddd.flow.toFixed(2) : 0) + " 米</span></div></br>" +
                                    "<div class=\"layer_basic_info_1\"><div class=\"layer_basic_info_1_11\">采集时间：<span>" + moment(parseInt(ddd.createtime)).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss') + "</span></div>" +
                                    "</div><div class=\"clear\"></div>" +
                                    "<div class=\"layer_basic_line_2\"></div>" +
                                    "<div class=\"layer_basic_function\">" +
                                    "<div class=\"layer_basic_function_3\">" +
                                    "</div></div>"
                                    , new BMap.Point(pt.lng, pt.lat)), new BMap.Point(pt.lng, pt.lat));
                            } else {
                                //显示错误提示信息
                                dialogAlert("操作结果", data.message, "确定");
                                location.href = '/';
                            }
                        },
                        error: function (err) {
                            alert(JSON.stringify(err));
                            //location.href = '/';
                        }
                    });


                });
            })(stats[i]);
            //}

            let color = "red";
            if (stats[i].type == 1) {
                color = "red";
            } else {
                color = "blue";
            }
            var label = new BMap.Label(stats[i].name, {
                position: new BMap.Point(stats[i].lng, stats[i].lat),    // 指定文本标注所在的地理位置
                offset: new BMap.Size(5, -10)
            });  // 创建文本标注对象
            label.setStyle({
                color: color,
                fontSize: "12px",
                height: "20px",
                lineHeight: "20px",
                fontFamily: "微软雅黑"
            });
            _that.mapObj.addOverlay(label);
            _that.mapObj.addOverlay(locMarker);
        }

        let centerAndZoomObj = _that.getCenterAndZoom(allPoints, {
            center: {lng: 30.131459, lat: 104.622937}, zoom: 11
        });

        if (stats.length == 1) {
            centerAndZoomObj = _that.getCenterAndZoom(allPoints, {
                center: {lng: stats[0].lat, lat: stats[0].lng}, zoom: stats[0].zoom
            });
        }

        _that.mapObj.centerAndZoom(new BMap.Point(centerAndZoomObj.center.lat, centerAndZoomObj.center.lng), centerAndZoomObj.zoom);

    }


    /**
     * 设置每个内涝点的流量和流速
     */
    this.markLoggingSatations = function (stats) {
        var _that = this;
        _that.clearOverlays();
        if (!this.mapObj) {
            throw new Error("please init this.mapObj first");
        }
        let allPoints = [];
        let iconPath = "/logo/loc.png";
        for (var i in stats) {
            allPoints.push({x: parseFloat(stats[i].lng), y: parseFloat(stats[i].lat)});
            let myIcon = new BMap.Icon(iconPath, new BMap.Size(28, 28));
            let locMarker = new BMap.Marker(new BMap.Point(stats[i].lng, stats[i].lat), {icon: myIcon});  // 创建标注

            (function (pt) {

                locMarker.addEventListener("click", function () {
                    $.ajax({
                        url: '/hfxt/queryContents?siteid=' + pt.id,
                        type: 'get',
                        contentType: "application/json; charset=utf-8",
                        success: function (data) {
                            //+ pt.type == 1 ? "&nbsp;&nbsp;<img src='/images/vedio1.png' onclick=\"showFullScreenAction('" + pt.name + "','" + pt.id + "')\">" : "" +
                            //成功
                            if (data.status == 200) {

                                let ddd = data.data[0];
                                _that.mapObj.openInfoWindow(new BMap.InfoWindow(
                                    "<div class=\"layer_basic_tittle\">" +
                                    "<div class=\"layer_basic_tittle_1\"><div>" +
                                    "<h6 class=\"con_text_cut\">" + (pt.name ? pt.name : '') +

                                        //"&nbsp;&nbsp;<img src='/images/vedio1.png' onclick=\"showFullScreenAction('" + pt.name + "','" + pt.id + "')\">" +


                                    "<img src='/images/pic.png' width='42px' height='42px' onclick=\"imgMgt('', '', '', '" + pt.id + "')\"></h6></div>" +
                                    "<div class=\"layer_basic_tittle_1_ico\"></div>" +
                                    "<h6 class=\"color_1\"></h6></div>" +
                                    "<div class=\"layer_basic_line_1\"></div>" +
                                    "<div class=\"layer_basic_info\">" +
                                    "<div class=\"layer_basic_info_1\">" +
                                    "<div class=\"layer_basic_info_1_1\">当前水位：<span>" + ddd.flow + " 米</span></div></br>" +
                                    "<div class=\"layer_basic_info_1\"><div class=\"layer_basic_info_1_11\">采集时间：<span>" + moment(parseInt(ddd.createtime)).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss') + "</span></div>" +
                                    "</div><div class=\"clear\"></div>" +
                                    "<div class=\"layer_basic_line_2\"></div>" +
                                    "<div class=\"layer_basic_function\">" +
                                    "<div class=\"layer_basic_function_3\">" +
                                    "</div></div>"
                                    , new BMap.Point(pt.lng, pt.lat)), new BMap.Point(pt.lng, pt.lat));
                            } else {
                                //显示错误提示信息
                                dialogAlert("操作结果", data.message, "确定");
                                location.href = '/';
                            }
                        },
                        error: function (err) {
                            alert(JSON.stringify(err));
                            //location.href = '/';
                        }
                    });


                });
            })(stats[i]);


            var label = new BMap.Label(stats[i].name, {
                position: new BMap.Point(stats[i].lng, stats[i].lat),    // 指定文本标注所在的地理位置
                offset: new BMap.Size(5, -10)
            });  // 创建文本标注对象
            label.setStyle({
                color: "red",
                fontSize: "12px",
                height: "20px",
                lineHeight: "20px",
                fontFamily: "微软雅黑"
            });
            _that.mapObj.addOverlay(label);
            _that.mapObj.addOverlay(locMarker);
        }

        let centerAndZoomObj = _that.getCenterAndZoom(allPoints, {
            center: {lng: 30.131459, lat: 104.622937}, zoom: 11
        });

        _that.mapObj.centerAndZoom(new BMap.Point(centerAndZoomObj.center.lat, centerAndZoomObj.center.lng), centerAndZoomObj.zoom);

    }


    /**
     * 设置每个点的流量和流速
     */
    this.showAllSatations = function (stats) {

        var _that = this;
        _that.clearOverlays();
        //alert();
        if (!this.mapObj) {
            throw new Error("please init this.mapObj first");
        }
        let allPoints = [];

        let iconPath = "/logo/loc.png";
        for (var i in stats) {
            allPoints.push({x: parseFloat(stats[i].lng), y: parseFloat(stats[i].lat)});
            let myIcon = new BMap.Icon(iconPath, new BMap.Size(28, 28));
            let locMarker = new BMap.Marker(new BMap.Point(stats[i].lng, stats[i].lat), {icon: myIcon});  // 创建标注

            var label = new BMap.Label("到达" + stats[i].name + ",预计时间:" + stats[i].time + ",预计水位:" + stats[i].flow, {
                position: new BMap.Point(stats[i].lng, stats[i].lat),    // 指定文本标注所在的地理位置
                offset: new BMap.Size(5, -10)
            });  // 创建文本标注对象
            label.setStyle({
                color: "red",
                fontSize: "12px",
                height: "20px",
                lineHeight: "20px",
                fontFamily: "微软雅黑"
            });
            _that.mapObj.addOverlay(label);

            //(function (pt) {
            //    let user = {};
            //    //var showInfoWindow = new BMap.InfoWindow("当前信息", pt);  // 创建信息窗口对象
            //    locMarker.addEventListener("click", function () {
            //
            //        _that.mapObj.openInfoWindow(new BMap.InfoWindow(
            //            "<div class=\"layer_basic_tittle\">" +
            //            "<div class=\"layer_basic_tittle_1\"><div>" +
            //            "<h6 class=\"con_text_cut\">" + (pt.name ? pt.name : '') + "</h6></div>" +
            //            "<div class=\"layer_basic_tittle_1_ico\"></div>" +
            //            "<h6 class=\"color_1\"></h6></div>" +
            //            "<div class=\"layer_basic_line_1\"></div>" +
            //            "<div class=\"layer_basic_info\">" +
            //            "<div class=\"layer_basic_info_1\">" +
            //            "<div class=\"layer_basic_info_1_1\">当前水位：<span>" + pt.flow + " 米</span></div></br>" +
            //            "<div class=\"layer_basic_info_1\"><div class=\"layer_basic_info_1_11\">采集时间：<span>" + pt.time + "</span></div>" +
            //            "</div><div class=\"clear\"></div>" +
            //            "<div class=\"layer_basic_line_2\"></div>" +
            //            "<div class=\"layer_basic_function\">" +
            //            "<div class=\"layer_basic_function_3\">" +
            //
            //            "</div></div>"
            //
            //            , new BMap.Point(pt.lng, pt.lat)), new BMap.Point(pt.lng, pt.lat));
            //        //开启信息窗口
            //    });
            //})(stats[i]);

            _that.mapObj.addOverlay(locMarker);
        }

        let centerAndZoomObj = _that.getCenterAndZoom(allPoints, {
            center: {lat: 30.131459, lng: 104.622937}, zoom: 11
        });

        _that.mapObj.centerAndZoom(new BMap.Point(centerAndZoomObj.center.lat, centerAndZoomObj.center.lng), centerAndZoomObj.zoom);

    }


    /*
     * 定位,通过经纬度将位置显示到地图上的对应位置
     *
     * @param userObj.lat 经度
     * @param userObj.lng 纬度
     * @param userObj.speed 速度
     * @param userObj.iconPath 显示的图标
     * @param userObj.name 名字
     * @param userObj.userid 用户id
     * @param userObj.lastLocationTime 最后上报位置的时间
     * @param userObj.position 职位
     * @param locType 0:单人定位; 1:群体定位
     * */
    this.locate = function (userObj, locType) {
        var lat = userObj.lat;
        var lng = userObj.lng;
        var speed = userObj.speed;
        var iconPath = userObj.iconPath;
        var name = userObj.name;
        var userid = userObj.userid;
        var lastLocationTime = userObj.lastLocationTime;
        var position = userObj.position;

        var _that = this;
        return new Promise(function (resolve, reject) {

            if (!_that.mapObj) {
                throw new Error("please init this.mapObj first");
            }

            iconPath = iconPath || "/logo/loc.png";

            var pt = new BMap.Point(lat, lng);
            var myIcon = new BMap.Icon(iconPath, new BMap.Size(30, 30));
            if (userLocMapMarker.get(userid) != null) {
                _that.mapObj.removeOverlay(userLocMapMarker.get(userid));
            }
            var locMarker = new BMap.Marker(pt, {icon: myIcon});  // 创建标注

            userLocMapMarker.set(userid, locMarker);
            _that.mapObj.addOverlay(locMarker);
            var opts = {
                position: pt,    // 指定文本标注所在的地理位置
                offset: new BMap.Size(1, 1)
            }
            var label = new BMap.Label(name, opts);  // 创建文本标注对象
            label.setStyle({
                color: "#f0f",
                fontSize: "12px",
                height: 16,
                lineHeight: "16px",
                fontFamily: "微软雅黑"
            });

            centerAndZoomObj = _that.getCenterAndZoom(null, {
                center: {lat: lat, lng: lng}, zoom: 16
            });
            _that.mapObj.centerAndZoom(new BMap.Point(centerAndZoomObj.center.lat, centerAndZoomObj.center.lng), centerAndZoomObj.zoom);

            var infoWindow = new BMap.InfoWindow(_that.createInfoHtml(userObj));// 创建信息窗口对象
            locMarker.addEventListener("click", function () {
                _that.mapObj.openInfoWindow(infoWindow, pt); //开启信息窗口
            });
        });
    };

    /*
     *轨迹回放之前先把所有的点和连线显示到地图上
     *
     * @param replayPoints 坐标数组
     * @param nextPoint 第二一个点,用来画线的
     * @param icon      图标
     * @param lineColor 轨迹颜色
     * */
    this.markPoints = function (replayPoints, nextPoint, icon, lineColor) {
        var _that = this;

        //alert();
        if (!this.mapObj) {
            throw new Error("please init this.mapObj first");
        }
        //alert(replayPoints.length);
        //for (var i = 0; i < replayPoints.length; i++) {
        var iconPath = icon || "https://" + SERV.signal_host + ":" + SERV.signal_port + "/images/point.png";
        //alert(JSON.stringify(replayPoints));
        var pt = new BMap.Point(replayPoints.lng, replayPoints.lat);
        var myIcon = new BMap.Icon(iconPath, new BMap.Size(28, 28));
        var locMarker = new BMap.Marker(pt, {icon: myIcon});  // 创建标注
        var infoHtml = _that.createReplayInfoHtml(replayPoints);
        var opts = {
            height: 100     // 信息窗口高度
        }
        var repInfoWindow = new BMap.InfoWindow(infoHtml, opts);  // 创建信息窗口对象
        locMarker.addEventListener("click", function () {
            (function (info) {
                var $objTr = $('#tracedetailtable tr[name="' + replayPoints.repTime + '"]'); //找到要定位的地方  tr
                var objTr = $objTr[0]; //转化为dom对象
                $(".table_title").animate({scrollTop: objTr.offsetTop - 47}, "slow"); //定位

                _that.mapObj.openInfoWindow(info, pt);
                $('#tracedetailtable tr[name="' + replayPoints.repTime + '"]').css('background', '#d03030').css('color', 'white').siblings().css('background', 'white').css('color', '#333');
            })(repInfoWindow) //开启信息窗口
        });

        replayLocInfoWin.set(replayPoints.idx, repInfoWindow);
        replayLocMapMarker.set(replayPoints.idx, locMarker);
        this.mapObj.addOverlay(locMarker);
        if (nextPoint) {
            var polyline = new BMap.Polyline([
                new BMap.Point(replayPoints.lng, replayPoints.lat),
                new BMap.Point(nextPoint.lng, nextPoint.lat)
            ], {strokeColor: lineColor, strokeWeight: 2, strokeOpacity: 0.5});   //创建折线
            this.mapObj.addOverlay(polyline);   //增加折线
            allReplayPointOrLine.push(polyline);
        }

        //计算中心位置以及地图缩放的位置
        var centerAndZoomObj = null;
        if (replayLocMapMarker.size > 1) {
            var allPoints = [];
            replayLocMapMarker.forEach(function (value, key, map) {
                allPoints.push({x: value.getPosition().lng, y: value.getPosition().lat});
            });

            centerAndZoomObj = _that.getCenterAndZoom(allPoints, {
                center: {lat: replayPoints.lat, lng: replayPoints.lng}, zoom: 11
            });

        } else {
            centerAndZoomObj = _that.getCenterAndZoom(null, {
                center: {lat: replayPoints.lng, lng: replayPoints.lat}, zoom: 16
            });
        }
        this.mapObj.centerAndZoom(new BMap.Point(centerAndZoomObj.center.lat, centerAndZoomObj.center.lng), centerAndZoomObj.zoom);
    };

    /*
     * 定位,通过经纬度将位置显示到地图上的对应位置
     *
     * @param lat 经度
     * @param lng 纬度
     * @param iconPath 显示的图标
     * */
    this.replay = function (locs, iconPath, lineColor, id) {
        if (!this.mapObj) {
            throw new Error("please init this.mapObj first");
        }

        var i = 0;
        repInterval = setInterval(function () {
            if (i < locs.length) {

                iconPath = iconPath || "/logo/loc.png";

                var pt = new BMap.Point(locs[i].point.lng, locs[i].point.lat);

                this.mapObj.centerAndZoom(pt, 13);  // 设置中心点坐标和地图级

                var myIcon = new BMap.Icon(iconPath, new BMap.Size(24, 24));

                var repMarker = new BMap.Marker(pt, {icon: myIcon});  // 创建标注
                this.mapObj.addOverlay(repMarker);
                allReplayPointOrLine.push(repMarker);

                if (i > 0) {
                    $('#sa' + id + (i - 1)).removeClass("onLineStaff");
                }
                $('#sa' + id + i).addClass("onLineStaff");
                //$('#span_address' + i).focus();

                if (i + 1 < locs.length) {
                    var polyline = new BMap.Polyline([
                        new BMap.Point(locs[i].point.lng, locs[i].point.lat),
                        new BMap.Point(locs[i + 1].point.lng, locs[i + 1].point.lat)
                    ], {strokeColor: lineColor, strokeWeight: 2, strokeOpacity: 0.5});   //创建折线
                    this.mapObj.addOverlay(polyline);   //增加折线
                    allReplayPointOrLine.push(polyline);
                }
                i++;
            } else {
                //alert(" data end");
                if (repInterval) {
                    clearInterval(repInterval);
                }
            }
        }, 1000);

    };

    /*
     *通过指定的ID,在地图上显示回放的动画效果
     * @param userId 需要回放的用户ID
     * @param step   回放的步骤, 如果此参数有值,代表是单步播放,如此参数为2,则显示1-->2的轨迹
     * */
    this.replayTrace = function (userId, step) {
        var _that = this;
        var iconReplayPoints = [];
        replayLocMapMarker.forEach(function (value, key, map) {
            if (key.substring(0, userId.length) == userId) {
                iconReplayPoints.push(value.getPosition());
            }
        });

        if (iconReplayPoints.length == 0) {
            alert("当前时间段无轨迹");
            return null;
        }

        if (step) {

        } else {
            _that.autoReplay(iconReplayPoints);
        }
    }

    this.autoReplay = function (points) {
        alert(points.length);
    }

    this.stepReplay = function (points, step) {
        alert('step by step');
    }

    /*
     *逆地址解析
     *
     * @param lat
     * @param lng
     * @return 返回一个bluebird的promise的对象
     * */
    this.getAddressByLoc = function (lat, lng, func) {
        if (typeof func == "function") {
            var geoc = new BMap.Geocoder();
            var point = new BMap.Point(lat, lng);
            geoc.getLocation(point, function (rs) {
                var addComp = rs.addressComponents;
                func(null, rs);
            });
        } else {
            return new Promise(function (resolve, reject) {
                var geoc = new BMap.Geocoder();
                var point = new BMap.Point(lat, lng);
                geoc.getLocation(point, function (rs) {
                    resolve(rs);
                });
            });
        }
    };

    /*
     *通过地图,获取当前位置--此处使用的是通过 ip 地址的方式
     *
     * @param func 回调的地址函数,如果此参数为空,则返回bluebird的promise的对象
     * */
    this.getCurLocation = function (func) {

        if (typeof func == "function") {

            var myCity = new BMap.LocalCity();
            myCity.get(func);
        } else {
            return new Promise(function (resolve, reject) {

                var myCity = new BMap.LocalCity();
                myCity.get(function (city) {
                    resolve(city);
                });
            });
        }

    }

    /*
     *通过div上选择的坐标位置,来确定这个位置对应的经纬度范围
     *@para x 坐标的x点
     *@para y 从标的y点
     * */
    this.pixelToPoint = function (x, y) {
        return this.mapObj.pixelToPoint(new BMap.Pixel(x, y));
    }

    /*
     *设置信息显示窗口
     * {
     *      name:
     *      position:
     *      lng:
     *      lat:
     *      speed:
     *      lastLocationTime:
     *      lastLocationAddress:
     * }
     * */
    this.createInfoHtml = function (user) {
        var retHtml = "<div class=\"layer_basic_tittle\">" +
            "<div class=\"layer_basic_tittle_1\"><div>" +
            "<h6 class=\"con_text_cut\">" + (user.name ? user.name : '') + "</h6></div>" +
            "<div class=\"layer_basic_tittle_1_ico\"></div>" +
            "<h6 class=\"color_1\"></h6></div>" +
                //"<div class=\"layer_basic_tittle_btn_1\"><a href=\"#\" onclick=\"rep.phoneCtrl('" + 1 + "','" + user.name + "');\">对讲</a></div>" +
                //"<div class=\"layer_basic_tittle_btn_1\"><a href=\"#\" onclick=\"rep.phoneCtrl('" + 4 + "','" + user.name + "');\">会话</a></div>"
                //+ "<div class=\"layer_basic_tittle_btn_1\"><a href=\"#\" onclick=\"rep.phoneCtrl('" + 3 + "','" + user.name + "');\">语音</a></div>" +
                //"<div class=\"layer_basic_tittle_btn_1\"><a href=\"#\" onclick=\"rep.phoneCtrl('" + 2 + "','" + user.name + "');\">视频</a></div>" +
                //"<div class=\"layer_basic_tittle_btn_1\"><a href=\"#\"  onclick=\"winReplay2('" + user.id + "','" + user.name + "');\">追踪</a></div>" +
                //"<div class=\"layer_basic_tittle_btn_1\"><a href=\"#\"  onclick=\" rep.winReplay('" + user.id + "','" + user.name + "');\">轨迹</a></div>" +
                //"</div><div class=\"clear\"></div>" +
            "<div class=\"layer_basic_line_1\"></div>" +
            "<div class=\"layer_basic_info\">" +
            "<div class=\"layer_basic_info_1\">" +
                //"<div class=\"layer_basic_info_1_1\">水位：<span>" + (user.lat ? (user.lat.toString().length > 8 ? user.lat.toString().substring(0, 8) : user.lat) : '') + "</span></div>" +
            "<div class=\"layer_basic_info_1_2\">水位：<span>" + user.flow + "</span></div></div>" +
            "<div class=\"layer_basic_info_1\">" +
            "<div class=\"layer_basic_info_1_2\">时间：<span>" + user.lastLocationTime + "</span></div><br>" +
            "</div>" +
            "<div class=\"clear\"></div>" +
            "<div class=\"layer_basic_info_1\">最后位置：<span>" + (user.address ? user.address : '') + "</span></div>" +
            "</div><div class=\"clear\"></div>" +
            "<div class=\"layer_basic_line_2\"></div>" +
            "<div class=\"layer_basic_function\">" +
            "<div class=\"layer_basic_function_3\">" +

            "</div></div>";

        return retHtml;
    }

    /*
     *在轨迹回放的时候,设置信息显示窗口
     * {
     *      name:
     *      time:
     *      speed:
     *      address:
     * }
     * */
    this.createReplayInfoHtml = function (user) {
        var retHtml = "<div class=\"layer_basic_tittle\">" +
            "<div style='margin-top: 8px'>姓名:" + user.name + "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;当前第" + user.remmber + "步" + "</div>" +
            "<div>时间:" + user.repTime + "</div>" +
            "<div>速度:" + user.speed + "km/h</div>" +
            "<div>地点:" + user.address + "</div>" +
            "</div>";
        return retHtml;
    }

    /*
     *通过地图上所有的点,算出中心位置以及地图要缩放的比例
     *@para points 坐标数组
     *@para defaultResult 默认的中心位置和缩放比例
     *@return 中心点和缩放比例 {center:{x:12,y:23},zoom:11}
     * */
    this.getCenterAndZoom = function (points, defaultResult) {
        if (_.isArray(points) && points.length > 1) {
            var minX = 0;
            var minY = 0;
            var maxX = 0;
            var maxY = 0;
            var x = 0;
            var y = 0;
            var getZoom = function (m1, m2) {
                console.log(m1 + "  " + m2);
                var m = m1 > m2 ? m1 : m2;

                if (m < 0.004799000000001996)
                    return 18;
                else if (m < 0.009582999999999231)
                    return 17;
                else if (m < 0.019152000000001834)
                    return 16;
                else if (m < 0.03835499999999925)
                    return 15;
                else if (m < 0.07664700000000124)
                    return 14;
                else if (m < 0.15327800000000025)
                    return 13;
                else if (m < 0.3059750000000001)
                    return 12;
                else if (m < 0.6177649999999986)
                    return 11;
                else if (m < 1.2369299999999974)
                    return 10;
                else if (m < 2.486722999999998)
                    return 9;
                else if (m < 5.026495999999998)
                    return 8;
                else if (m < 10.260604999999998)
                    return 7;
                else if (m < 21.316947999999996)
                    return 6;
                return 5;
            }

            minX = points[0].x;
            minY = points[0].y;
            maxX = points[0].x;
            maxY = points[0].y;

            for (i = 1; i < points.length; i++) {
                x = points[i].x;
                y = points[i].y;
                if (x == 0 || y == 0)
                    continue;

                if (x > maxX)
                    maxX = x;
                if (x < minX)
                    minX = x;
                if (y > maxY)
                    maxY = y;
                if (y < minY)
                    minY = y;
            }
            if (maxX != 0 && maxY != 0 && minX != 0 && minY != 0) {
                return {
                    center: {lat: (minX + maxX) / 2, lng: (minY + maxY) / 2}, zoom: getZoom(maxX - minX, maxY - minY)
                }
            }

            return defaultResult;

        } else {
            return defaultResult;
        }
    }

    /*
     *关闭地图上打开的信息窗口
     * */
    this.closeInfoWindow = function () {
        return this.mapObj.closeInfoWindow();
    }

    /*
     *清除掉所有的轨迹回放加入的点
     * */
    this.clearAllReplayObjects = function () {

        //清除所有的回放线
        if (allReplayPointOrLine.length > 0) {
            allReplayPointOrLine.forEach(function (it) {
                console.log(it);
                this.mapObj.removeOverlay(it);
            });
            allReplayPointOrLine = [];
        }
        //清除所有回放点
        if (replayLocMapMarker.size > 1) {
            replayLocMapMarker.forEach(function (value, key, map) {
                console.log(value);
                this.mapObj.removeOverlay(value);
            });
            replayLocMapMarker = new Map();
        }
        //清除回放的箭头图标
        if (locatedReplayTrace) {
            this.mapObj.removeOverlay(locatedReplayTrace);
            locatedReplayTrace = null;
        }

    }

    /*
     *清除地图上所有的覆盖物
     *
     * */
    this.clearOverlays = function () {
        this.mapObj.clearOverlays();
    }
    /*

     /*通过指定的ID,显示地图上某一点的信息
     * idx
     * */
    this.locateByReplayId = function (idx) {
        //alert(idx);
        if (replayLocMapMarker && replayLocMapMarker.get(idx)) {
            var marker = replayLocMapMarker.get(idx);
            marker.openInfoWindow(replayLocInfoWin.get(idx));
            this.mapObj.centerAndZoom(new BMap.Point(marker.getPosition().lng, marker.getPosition().lat), 11);
        }
        //replayLocMapMarker.set(replayPoints.idx, locMarker);
    }

    /*
     *通过地图上经纬度的图标,获取点在地图上的位置
     *对于百度地图的实现来说,这里就是直接使用百度地图的方式
     * @param lng 需要转换的点的经度
     * @param lat 需要转换的点的纬度
     * @return pixels 转换出来之后,页面上的位置
     * */
    this.pointToPixels = function (lng, lat) {
        //return this.mapObj.pointToPixels(lng,lat);
        return this.mapObj.pointToPixel(new BMap.Point(lng, lat));
    }

    /*
     *在地图上显示回放轨迹的图标
     * @param point 需要转换的点的纬度
     * @param icon 转换出来之后,页面上的位置
     * @param infoIdx 显示窗口的id
     * @param rotation 图标需要旋转的角度
     * */
    this.locateReplayIcon = function (point, icon, infoIdx, rotation) {
        if (locatedReplayTrace) {
            this.mapObj.removeOverlay(locatedReplayTrace);
        }

        var pt = new BMap.Point(point.lng, point.lat);
        var myIcon = new BMap.Icon(icon, new BMap.Size(24, 24));
        locatedReplayTrace = new BMap.Marker(pt, {icon: myIcon, rotation: rotation});
        //通过传入的参数,找出应该显示的信息窗口,保证在标记点和导航图标上都可以显示信息窗口
        var curWindow = replayLocInfoWin.get(infoIdx);

        locatedReplayTrace.addEventListener("click", function () {
            this.mapObj.openInfoWindow(curWindow, pt); //开启信息窗口
        });
        this.mapObj.addOverlay(locatedReplayTrace);
    }

    this.centerAndZoom = function (lng, lat) {
        this.mapObj.centerAndZoom(new BMap.Point(lng, lat), 15);
    }
    this.panTo = function (lng, lat) {
        this.mapObj.panTo(new BMap.Point(lng, lat), true);

    }

}
