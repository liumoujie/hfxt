﻿var IMG_LENGTH = 20971520;//图片最大20MB
var IMG_MAXCOUNT = 5;//最多选中图片张数
var imgAray = [];

var UP_IMGCOUNT = 0;//上传图片张数记录
//打开文件选择对话框
$("#div_imgfile").click(function () {
    if ($(".lookimg").length >= IMG_MAXCOUNT) {
        dialogAlert("一次最多上传" + IMG_MAXCOUNT + "张图片");
        return;
    }

    var _CRE_FILE = document.createElement("input");
    if ($("#imgfile").length <= $(".lookimg").length) {//个数不足则新创建对象
        _CRE_FILE.setAttribute("type", "file");
        _CRE_FILE.setAttribute("id", "imgfile");
        _CRE_FILE.setAttribute("name", "imgData");
        _CRE_FILE.setAttribute("accept", ".png,.jpg,.jpeg");
        _CRE_FILE.setAttribute("num", UP_IMGCOUNT);//记录此对象对应的编号
        $("#div_imgfile").after(_CRE_FILE);
    }
    else { //否则获取最后未使用对象
        _CRE_FILE = $("#imgfile").eq(0).get(0);
    }
    return $(_CRE_FILE).click();//打开对象选择框
});

//创建预览图，在动态创建的file元素onchange事件中处理
$(".sigoInfileRow").on("change", '#imgfile', function () {
    if ($(this).val().length > 0) {//判断是否有选中图片

        //判断图片格式是否正确
        var FORMAT = $(this).val().substr($(this).val().length - 3, 3);
        if (FORMAT !== "png" && FORMAT !== "jpg" && FORMAT !== "peg") {
            dialogAlert("文件格式不正确！！！");
            return;
        }

        //判断图片是否过大，当前设置1MB
        var file = this.files[0];//获取file文件对象
        if (file.size > (IMG_LENGTH)) {
            dialogAlert("图片大小不能超过20M");
            $(this).val("");
            return;
        }
        //创建预览外层
        var _prevdiv = document.createElement("div");
        _prevdiv.setAttribute("class", "lookimg");
        //创建内层img对象
        var preview = document.createElement("img");
        $(_prevdiv).append(preview);
        //创建放大按钮
        var IMG_MOREBIG = document.createElement("div");
        IMG_MOREBIG.setAttribute("class", "lookimg_moreBig");
        IMG_MOREBIG.setAttribute("id", "moreBig");
        $(_prevdiv).append(IMG_MOREBIG);

        //创建删除按钮
        var IMG_DELBTN = document.createElement("div");
        IMG_DELBTN.setAttribute("class", "lookimg_delBtn");
        IMG_DELBTN.innerHTML = "移除";
        $(_prevdiv).append(IMG_DELBTN);

        //在预览图中创建li容器
        var IMG_SHOWMOREBIG = document.createElement("li");
        $('#imgUl').append(IMG_SHOWMOREBIG);
        //在li里面创建对应的img图片
        var showBigImg = document.createElement("img");

        $(IMG_SHOWMOREBIG).append(showBigImg);
        //在图片下面创建对应的小圆点
        var showImgIcon = document.createElement("span");
        showImgIcon.setAttribute("class", "i");
        $('#iconUl').append(showImgIcon);
        //创建进度条
        var IMG_PROGRESS = document.createElement("div");
        IMG_PROGRESS.setAttribute("class", "lookimg_progress");
        $(IMG_PROGRESS).append(document.createElement("div"));
        $(_prevdiv).append(IMG_PROGRESS);
        //记录此对象对应编号
        _prevdiv.setAttribute("num", $(this).attr("num"));
        //对象注入界面
        $("#div_imglook").children("div:last").before(_prevdiv);
        UP_IMGCOUNT++;//编号增长防重复

        //预览功能 start
        var reader = new FileReader();//创建读取对象
        reader.onloadend = function () {
            preview.src = reader.result;//读取加载，将图片编码绑定到元素
            showBigImg.setAttribute("src", reader.result);
        }
        if (file) {//如果对象正确
            reader.readAsDataURL(file);//获取图片编码
        } else {
            preview.src = "";//返回空值
        }
        //预览功能 end
    }
});

//删除选中图片
$(".sigoInfileRow").on("click", '.lookimg_delBtn', function () {
    $("#imgfile[num=" + $(this).parent().attr("num") + "]").remove();//移除图片file
    $(this).parent().remove();//移除图片显示
    $('#');
});

//删除按钮移入移出效果
$(".sigoInfileRow").on("mouseover", '.lookimg', function () {
    if ($(this).attr("ISUP") != "1")
        $(this).children(".lookimg_delBtn").eq(0).css("display", "block");
});
$(".sigoInfileRow").on("mouseout", '.lookimg', function () {
    $(this).children(".lookimg_delBtn").eq(0).css("display", "none");
});
var NOWLOOK = 0;
//确定上传按钮
$("#btn_ImgUpStart").click(function () {
    IMG_IND = 0;
    if ($(".lookimg").length <= 0) {
        dialogAlert("还未选择需要上传的图片");
        return;
    }

    //全部图片上传完毕限制
    if ($(".lookimg[ISUP=1]").length == $(".lookimg").length) {
        dialogAlert("图片已全部上传完毕！");
        return;
    }

    //循环所有已存在的图片对象，准备上传
    for (var i = 0; i < $(".lookimg").length; i++) {
        NOWLOOK = $(".lookimg").eq(i);//当前操作的图片预览对象
        NOWLOOK.index = i;
        //如果当前图片已经上传，则不再重复上传
        if (NOWLOOK.attr("ISUP") == "1")
            continue;

        //上传图片准备
        var IMG_BASE = NOWLOOK.children("img").eq(0).attr("src"); //要上传的图片的base64编码
        var IMG_IND = Number(NOWLOOK.attr("num")) + 1;
        //图片正式开始上传
        var ajaxi = 0;
        var indexs = 0;
        $.ajax({
            type: "post",
            url: '/upload/do/image',
            data: {'imgData': IMG_BASE},//图片路径
            dataType: "text",
            success: function (data) {
                imgAray[ajaxi] = data;
                ajaxi = ajaxi + 1;

                if (ajaxi >= UP_IMGCOUNT) {
                    dialogAlert("操作结果", "文件上传成功", "确定");
                    setImgSrc(imgAray);
                }
            },
            error: function (XMLHttpRequest) {
                if (XMLHttpRequest.status == 413) {
                    dialogAlert("第" + IMG_IND + "张图片过大，请重新上传,上传最大尺寸为20M");
                }
            },
            beforeSend: function () {
                //图片上传之前执行的操作，当前为进度条显示
                NOWLOOK.children(".lookimg_progress").eq(0).css("display", "block");//进度条显示
            }
        });
    }
});

$('.closeBigImgShow').click(function () {
    $('.BigImgShow').hide();
    clearInterval(interval);
});
$(".sigoInfileRow").on("click", '#moreBig', function () {
    var pindex = $(this).parent().attr("num");
    //var imgsrc=$(this).prev().attr("src");
    //$('.imgShowBox ul li').eq(pindex).children("img").attr('src',imgsrc);
    $('.BigImgShow').show();
    imgLbt(pindex);
});

function resetImgBox() {
    $(".lookimg").remove();
    UP_IMGCOUNT = 0;
}

var interval;
function imgLbt(index) {
    //var index=0;
    //开启一个定时器
    function clierIntervaldiv() {
        clearInterval(interval);
    }

    function starinterval() {
        interval = setInterval(function () {
            if (index > UP_IMGCOUNT) {
                index = 0;
            } else {
                index++;
            }
            $('.imgShowBox ul li').eq(index).stop(true).css('display', 'block').siblings().css('display', 'none');
            $('.i').eq(index).css('background', '#FFF').siblings().css('background', '#999');
        }, 3000);
    }

    starinterval();
    //停止计时clearInterval(interval);
    $('.imgShowBox').hover(function () {
        clearInterval(interval);
    }, function () {
        starinterval();
    });
    $('#iconUl span').hover(function () {
        var index = $(this).index();
        $('.imgShowBox ul li').eq(index).stop(true).css('display', 'block').siblings().css('display', 'none');
        $('.i').eq(index).css('background', '#FFF').siblings().css('background', '#999');
    }, function () {

    });

    $('.BigImgright').click(function () {
        if (index > UP_IMGCOUNT) {
            index = 0;
        } else {
            index++;
        }
        $('.imgShowBox ul li').eq(index).css('display', 'block').siblings().css('display', 'none');
        $('.i').eq(index).css('background', '#FFF').siblings().css('background', '#999');
    });

    $('.BigImgleft').click(function () {
        if (index < 0) {
            index = UP_IMGCOUNT;
        } else {
            index--;
        }
        $('.imgShowBox ul li').eq(index).css('display', 'block').siblings().css('display', 'none');
        $('.i').eq(index).css('background', '#FFF').siblings().css('background', '#999');
    });

}


function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                //alert('Yup.');
                cnfFun(result);
            } else {
                //alert('Nope.');
            }
        }
    });

}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}