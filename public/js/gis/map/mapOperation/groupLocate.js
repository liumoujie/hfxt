/**
 * Created by zhoulanhong on 3/16/17.
 */
function groupLocate() {
    var treeObj = gisOrgTree.getTree();
    var checkedNodes = treeObj.getCheckedNodes();
    if (!checkedNodes || checkedNodes.length == 0) {
        $('#choiseZtree').html('请选择要群体定位的人员').removeClass('replayTitle');
        //alert("请选择要群体定位的人员");
        return;
    }
    for (var i in checkedNodes) {
        // 不是叶子节点
        if (!checkedNodes[i].isParent && checkedNodes[i].lastLocation) {

            m.locate({
                id: checkedNodes[i]._id,
                lat: checkedNodes[i].lastLocation.coordinates[0],
                lng: checkedNodes[i].lastLocation.coordinates[1],
                speed: "0.0KM/H",
                iconPath: checkedNodes[i].locateicon,
                name: checkedNodes[i].name,
                userid: checkedNodes[i].idNumber,
                lastLocationTime: checkedNodes[i].lastLocationTimeCST,
                position: checkedNodes[i].positionname
            }, 1);

        }
    }
}