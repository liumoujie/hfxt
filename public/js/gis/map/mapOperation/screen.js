/**
 * Created by zhoulanhong on 3/29/17.
 */

var dataTablesLang = {
    "sProcessing": "处理中...",
    "sLengthMenu": "显示 _MENU_ 项结果",
    "sZeroRecords": "没有匹配结果",
    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
    "sInfoPostFix": "",
    "sSearch": "搜索:",
    "sUrl": "",
    "sEmptyTable": "表中数据为空",
    "sLoadingRecords": "载入中...",
    "sInfoThousands": ",",
    "oPaginate": {
        "sFirst": "首页",
        "sPrevious": "上页",
        "sNext": "下页",
        "sLast": "末页"
    },
    "oAria": {
        "sSortAscending": ": 以升序排列此列",
        "sSortDescending": ": 以降序排列此列"
    }
};

var contentTablesLang = {
    "sProcessing": "处理中...",
    "sLengthMenu": "显示 _MENU_ 项结果",
    "sZeroRecords": "没有匹配结果",
    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
    "sInfoPostFix": "",
    "sSearch": "搜索:",
    "sUrl": "",
    "sEmptyTable": "表中数据为空",
    "sLoadingRecords": "载入中...",
    "sInfoThousands": ",",
    "oPaginate": {
        "sFirst": "首页",
        "sPrevious": "上页",
        "sNext": "下页",
        "sLast": "末页"
    },
    "oAria": {
        "sSortAscending": ": 以升序排列此列",
        "sSortDescending": ": 以降序排列此列"
    }
};


var screenTable = null;
var contentTable = null;
var htmls = [];
var prevHtmls = "";

$(document).ready(function () {
    $('#closeScreen').click(function () {
        $('.screenBox').hide();
        screenTable.destroy();
    });

    $('#addScreenContent').click(function () {
        $('#screenId').val('');
        $('#fColor').val('red');
        $('#fSize').val('16');
        $('#previewDiv').css("color", "red");
        $('#previewDiv').css("font-size", "16px");
        $('#screenText').val('');
        $('#previewDiv').text('');
        //弹出对话框
        $('#dlgScreenSet').modal({
            backdrop: 'static'
        });
    });

    $('#closeScreenContent').click(function () {
        "use strict";
        $('.screenContentBox').hide();
    });

    //添加防汛联系人
    $('#addscreen').on('click', function (event) {
        event.preventDefault();

        $('#dlgscreenFormInputInvalid').addClass('hidden');
        $('#screenSaveMode').val("")
        $('#wpName').val("");
        $('#wpPosition').val("");
        $('#wpNbr').val("");
        $('#wpEmgNbr').val("");

        //弹出对话框
        $('#dlgAddscreen').modal({
            backdrop: 'static'
        });
    });

    //对话框点击保存
    $('#btnScreenContentSave').on('click', function (event) {
            event.preventDefault();
            //输入合法
            if ($('#screenText').val().length > 0) {

                $('#btnGroupSave').text("数据保存中...");
                $('#btnGroupSave').prop('disabled', true);

                let url = "/hfxt/saveScreenContent";

                var dataJson = {
                    ledid: $('#screenSaveMode').val(),
                    orgtext: $('#screenText').val(),
                    color: $('#fColor').val(),
                    fsize: $('#fSize').val(),
                }

                if ($('#screenId').val().length > 0) {
                    url = "/hfxt/updScreenContent";
                    dataJson.id = $('#screenId').val();
                }

                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(dataJson),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {

                            $('#dlgScreenSet').modal('hide');
                            //重新加载table数据.
                            contentTable.ajax.reload();
                        } else {
                            //显示错误提示信息
                            $('#dlgContentFormInputInvalid').text(data.message);
                            $('#dlgContentFormInputInvalid').removeClass('hidden');

                            $('#btnScreenContentSave').text("保存");
                            $('#btnScreenContentSave').prop('disabled', false);
                        }
                    },
                    error: function (err) {
                        location.href = '/';
                    }
                });
            }
            else {
                //输入不合法
                $('#dlgContentFormInputInvalid').text("请输入要显示的内容");
                $('#dlgContentFormInputInvalid').removeClass('hidden');
            }
        }
    );
});

function screenContentEdit(data) {
    $('#screenId').val(data.id);

    $('#screenText').val(data.orgtext);
    $('#screenSaveMode').val(data.screenid);
    $('#fColor').val(data.color);
    $('#fSize').val(data.size);

    $('#previewDiv').text(data.orgtext);

    $('#previewDiv').css("color", data.color);
    $('#previewDiv').css("font-size", $('#fSize').val() + "px");

    $('#dlgScreenSet').modal({
        backdrop: 'static'
    });
}

function operAutoScreen(data) {
    let oper = (data.state == "0" ? "开启" : "关闭");
    let valueAfter = (data.state == "0" ? "1" : "0");
    dialogConfirm("请确认", "确定要" + oper + "自动显示吗？", function (result) {
        if (result) {
            $.ajax({
                url: '/hfxt/updScreenAuto',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({"state": valueAfter,"id": data.id}),
                success: function (data) {
                    dialogAlert("操作结果", oper + "成功", "确定");
                    screenTable.ajax.reload();
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                    //location.href = '/';
                }
            });
        }
    });

}

function operScreen(data, path) {
    let oper = "";
    if (path == "clear") {
        oper = "清除";
    } else if (path == "pon") {
        oper = "开屏";
    } else {
        oper = "关屏";
    }
    dialogConfirm("请确认", "确定要" + oper + "吗？", function (result) {
        if (result) {
            $.ajax({
                url: '/led/' + path,
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({"imei": data.screenid}),
                success: function (data) {
                    dialogAlert("操作结果", oper + "成功", "确定");
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                    //location.href = '/';
                }
            });
        }
    });

}

//编辑群
function screenEdit(data) {
    $('.screenBox').hide();
    screenTable.destroy();

    $('#screenSaveMode').val(data.screenid);

    if ($('.screenContentBox').is(':hidden')) {
        $('.screenContentBox').show();

        contentTable = $('#tabScreenContent').DataTable({
            "language": dataTablesLang,
            "order": [[0, 'desc']],
            "ajax": "/hfxt/screencontent?ledid=" + data.screenid,
            "columns": [
                {'data': "id"},
                {'data': "orgtext"},
                {'data': "id"},
            ],

            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $('td:eq(2)', nRow).html(
                    "<a class=\"btn btn-link btn-sm wpTableLink pPreview\" role=\"button\">编辑</a>" +
                    "<a class=\"btn btn-link btn-sm wpTableLink pPublish\" role=\"button\">发布</a>" +
                    "<a class=\"btn btn-link btn-sm wpTableLink batchPublish\" role=\"button\">批量发布</a>" +
                    "<a class=\"btn btn-link btn-sm wpTableLink pDelete\" role=\"button\">删除</a>"
                );
                return nRow;
            }
        });

        contentTable.on('draw', function () {
            $('.wpTableLink').unbind('click').on('click', function (event) {
                event.preventDefault();

                //本行对应的数据
                var data = contentTable.row($(this).parents('tr')).data();
                if ($(this).hasClass('pPublish')) {
                    screenPublish(data);
                }else if ($(this).hasClass('batchPublish')) {
                    screenBatchPublish(data);
                }else if ($(this).hasClass('pPreview')) {
                    screenContentEdit(data);
                }else if($(this).hasClass('pDelete')){
                    screenDelete(data);
                }
            });
        });

    } else {
        return false;
    }
}

function screenDelete(data) {

    dialogConfirm("请确认", "确定要删除本条数据？", function (result) {
        if (result) {
            $.ajax({
                url: '/hfxt/deleteScreenContent',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({"id": data.id}),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        //重新加载table数据.
                        dialogAlert("操作结果", "删除成功", "确定");
                        contentTable.ajax.reload();
                    } else {
                        //显示错误提示信息
                        dialogAlert("操作结果", data.message, "确定");
                    }
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                    //location.href = '/';
                }
            });
        }
    });

}

// batch publish
function screenBatchPublish(data) {
    let colorIdx = 1;
    if (data.color == "green") {
        colorIdx = 2;
    }
    if (data.color == "yellow") {
        colorIdx = 3;
    }

    dialogConfirm("请确认", "确定要批量布发本条数据到所有屏？", function (result) {
        if (result) {
            $.ajax({
                url: '/led/sendAll',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    "imei": data.ledid, "fontSize": data.size, "fontColor": colorIdx, "content": data.orgtext
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        //重新加载table数据.
                        dialogAlert("操作结果", "发布成功", "确定");
                    } else {
                        //显示错误提示信息
                        dialogAlert("操作结果", data.message, "确定");
                    }
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                    //location.href = '/';
                }
            });
        }
    });
}


function screenPublish(data) {
    let colorIdx = 1;
    if (data.color == "green") {
        colorIdx = 2;
    }
    if (data.color == "yellow") {
        colorIdx = 3;
    }

    dialogConfirm("请确认", "确定要发布本条数据？", function (result) {
        if (result) {
            $.ajax({
                url: '/led/send',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    "imei": data.ledid, "fontSize": data.size, "fontColor": colorIdx, "content": data.orgtext
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        //重新加载table数据.
                        dialogAlert("操作结果", "发布成功", "确定");
                    } else {
                        //显示错误提示信息
                        dialogAlert("操作结果", data.message, "确定");
                    }
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                    //location.href = '/';
                }
            });
        }
    });

}

// 删除防汛负责人
function screenRemove(data) {

    dialogConfirm("请确认", "确定要删除本条数据？", function (result) {
        if (result) {
            $.ajax({
                url: '/hfxt/screenDel',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    id: data.id
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        //重新加载table数据.
                        screenTable.ajax.reload();
                    } else {
                        //显示错误提示信息
                        dialogAlert("操作结果", data.message, "确定");
                    }
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                    //location.href = '/';
                }
            });
        }
    });
}

function genPreview() {
    var t = $('#screenText').val();
    $('#previewDiv').text(t);
    //let color = ;
    $('#previewDiv').css("color", $('#fColor').val());
    $('#previewDiv').css("font-size", $('#fSize').val() + "px");
    //let showText = '';
    //let selStart = 0;
    //let selEnd = 0;
    //if (window.getSelection) {
    //    if (t.selectionStart != undefined && t.selectionEnd != undefined) {
    //        showText = t.value.substring(t.selectionStart, t.selectionEnd);
    //        selStart = t.selectionStart;
    //        selEnd = t.selectionEnd;
    //    } else {
    //        showText = "";
    //    }
    //} else {
    //    showText = document.selection.createRange().text;
    //}
    //alert(showText + selStart + "  " + selEnd);
    //let txt = $('#screenText').val();
    //let family = $('#fFamily').val();
    //let size = $('#fSize').val();
    //let space = $('#fSpace').val();
    //let color = $('#fColor').val();
    //alert(family + "  " + size + "   " + color + "   " + space);
    //for (let i = 0; i < txt.length; i++) {
    //    if (i >= selStart && i < selEnd) {
    //        htmls[i] = {
    //            text: txt.substring(i, i + 1),
    //            font: family,
    //            space: space,
    //            color: color,
    //            size: size,
    //        }
    //        //htmls[i] = '<div style="float: left; font-family: \'Songti TC\'; color: ' + color + '; font-size: ' + size + '; padding-right: ' + space + 'px ">' + txt[i] + '</div>';
    //    } else {
    //        if (!htmls[i]) {
    //            //htmls[i] = '<div style="float: left; ">' + txt[i] + '</div>';
    //            htmls[i] = {
    //                text: txt.substring(i, i + 1),
    //            }
    //        }
    //    }
    //    //alert(htmls[i].text)
    //}
    //for (let i in htmls) {
    //    prevHtmls += '<div style="float: left; font-family: \'' + htmls[i].family + '\'; color: ' + htmls[i].color + '; font-size: ' + htmls[i].size + '; padding-right: ' + htmls[i].space + 'px ">' + htmls[i].text + '</div>';
    //}
    //$('#previewTd').html(prevHtmls);
}

function screen() {

    if(contentTable){
        contentTable.destroy();
    }

    if ($('.screenBox').is(':hidden')) {
        $('.screenBox').show();

        screenTable = $('#tabScreen').DataTable({
            "language": dataTablesLang,
            "order": [[0, 'desc']],
            //"ajax": "/hfxt/screen?siteid=ZYNL000006",
            "ajax": "/hfxt/screen?siteid=" + $('#allLogging').val(),
            "columns": [
                {'data': "screenname"},
                {'data': "address"},
                {'data': "description"},
                {'data': "id"},
            ],

            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $('td:eq(3)', nRow).html(
                    "<a class=\"btn btn-link btn-sm wpTableLink pSetting\" role=\"button\">设置</a>"
                    +
                    "<a class=\"btn btn-link btn-sm wpTableLink pClear\" role=\"button\">清屏</a>"
                    +
                    "<a class=\"btn btn-link btn-sm wpTableLink pPowerOn\" role=\"button\">开屏</a>"
                    +
                    "<a class=\"btn btn-link btn-sm wpTableLink pPowerOff\" role=\"button\">关屏</a>"
                    +
                    "<a class=\"btn btn-link btn-sm wpTableLink pSetAuto\" role=\"button\">" + (aData.state == 0 ? "开启" : "关闭") + "自动显示</a>"
                );
                return nRow;
            }
        });

        screenTable.on('draw', function () {
            $('.wpTableLink').unbind('click').on('click', function (event) {
                event.preventDefault();

                //本行对应的数据
                var data = screenTable.row($(this).parents('tr')).data();
                if ($(this).hasClass('pSetting')) {
                    screenEdit(data);
                }
                else if ($(this).hasClass('pClear')) {
                    operScreen(data, "clear");
                }
                else if ($(this).hasClass('pPowerOn')) {
                    operScreen(data, "pon");
                }
                else if ($(this).hasClass('pPowerOff')) {
                    operScreen(data, "poff");
                }else if ($(this).hasClass('pSetAuto')) {
                    operAutoScreen(data);
                }
                else {
                    return false;
                }
            });
        });

    } else {
        return false;
    }
}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                //alert('Yup.');
                cnfFun(result);
            } else {
                //alert('Nope.');
            }
        }
    });

}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}