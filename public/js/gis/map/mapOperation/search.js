/**
 * Created by zhoulanhong on 3/16/17.
 */

/**
 * Created by zhoulanhong on 3/14/17.
 * 本类用于处理地图搜索相关的业务
 */
(function () {
    var myValue;
    var searchMap = m.getMapObj();//获取当前地图//获取当前地图
    var ac = new BMap.Autocomplete(    //建立一个自动完成的对象
        {
            "input": "serchMapInput",
            "location": searchMap
        });
    ac.addEventListener("onconfirm", function (e) {    //鼠标点击下拉列表后的事件
        var _value = e.item.value;
        myValue = _value.province + _value.city + _value.district + _value.street + _value.business;
        setPlace();
    });

    function setPlace() {
         // searchMap.clearOverlays();    //清除地图上所有覆盖物
        function myFun() {
            var pp = local.getResults().getPoi(0).point;    //获取第一个智能搜索的结果
            searchMap.centerAndZoom(pp, 18);
            searchMap.addOverlay(new BMap.Marker(pp));    //添加标注
            $("#searchText").hide();
        }
        var local = new BMap.LocalSearch(searchMap, { //智能搜索
            onSearchComplete: myFun
        });
        local.search(myValue);
    }
})();