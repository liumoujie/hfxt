/**
 * Created by zhoulanhong on 3/29/17.
 */

function showWaterLogging() {
    //alert('clicked');
    //changeLoggingCss(idx);
    //let id = $('#allLogging').val();
    //genLoggingContents(id);
    //let id = $('#allLogging').val();


    $.ajax({
        url: '/hfxt/querySitesById?id=' + $('#allLogging').val(),
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let d = data.data;
                m.markAllSatations([{
                    "id": d.id,
                    "name": d.name,
                    "lng": d.longitude,
                    "lat": d.latitude,
                    "type": d.type,
                    "zoom": 19
                }]);
                genLoggingContents($('#allLogging').val());
            } else {
                //显示错误提示信息
                location.href = '/';
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });
}

function drawLoggingRuler(cur, near, year, d) {
    console.log(JSON.stringify(d));
    //低于告警水位
    let htmls = '';
    if (parseFloat(cur) <= parseFloat(near)) {
        htmls =
            '<div style="margin-top: 10px">预警水位' + near + '<hr style="height:3px;border:none;border-top:4px solid blue;"/></div>' +
            '<div id="rulerLineCur" style="margin-top: 36px">当前水深' + (cur ? cur.toFixed(2) : 0) +
            '米<hr style="height:3px;border:none;border-top:4px solid white;"/></div>';

        $('#alarm').html('<i class="alarmIcon">');
    } else {
        htmls =
            '<div id="rulerLineCur" style="margin-top: 36px">当前水深' + (cur ? cur.toFixed(2) : 0) +
            '米<hr style="height:3px;border:none;border-top:4px solid white;"/></div>' +
            '<div style="margin-top: 10px">预警水位' + near + '<hr style="height:3px;border:none;border-top:4px solid blue;"/></div>';
        //alert("sadgdfg");
        //$('#alarm').html('<i class="onAlarmIcon">');
        m.markAllSatations([{
            "id": d.siteid, "name": d.name, "lng": d.longitude, "lat": d.latitude, "type": d.type, "alarm":"1",
            "zoom": 13
        }]);

    }
    $('#loggingRulerText').html(htmls);
}

function changeLoggingCss(idx) {
    alert(idx)
    for (var i = 0; i < 7; i++) {
        $('#showWaterLogging' + i).removeClass('waterStateBackGroundColor');
    }
    $('#showWaterLogging' + idx).addClass('waterStateBackGroundColor');
}

//编辑内容
function waterLoggingContentEdit(id, speed, flow, siteid) {
    alert(id);
    $('#WaterPersonSaveMode').val(id);
    $('#siteId').val(siteid);
    $('#conSpeed').val(speed);
    $('#conFlow').val(flow);

    //弹出对话框
    $('#dlgContent').modal({
        backdrop: 'static'
    });

}

function genLoggingContents(siteId) {
    $.ajax({
        url: '/hfxt/queryContents?siteid=' + siteId,
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let datas = data.data;
                let htmls = '';
                let nowTime = moment();
                nowTime.seconds(0);
                let nowMinutes = nowTime.minute();
                let offSet = nowMinutes % 5;
                nowTime.subtract(offSet, 'm');
                nowTime.add(5, 'm');
                for (var i = 0; i < datas.length; i++) {
                    let showTime = nowTime.subtract( 5, 'm');
                    htmls += '<tr><td  style="padding: 0px;">' + showTime.format('YYYY-MM-DD HH:mm:ss') + '</td>' +
                        '<td style="padding: 0px;">' + (datas[i].flow ? datas[i].flow.toFixed(2) : 0) + '</td>' +
                            //'<td style="padding: 0px;"><a onclick="waterLoggingContentEdit(\'' + datas[i].id + '\',\'' + datas[i].speed + '\',\'' + datas[i].flow + '\',\'' + siteId + '\')">校核</a></td><' +
                        '/tr>';
                }
                $('#waterLoggingTb').html(htmls);
                calcLoggingNear(datas[0]);
            } else {
                //显示错误提示信息
                dialogAlert("操作结果", data.message, "确定");
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });
}

function calcLoggingNear(d) {
    if (d && d.flow) {
        drawLoggingRuler(d.flow, d.fiveh, "五", d);
    }
    //if (d && d.flow) {
    //    if (parseInt(d.flow) > parseInt(d.fiftyh)) {//百年不遇
    //        drawRuler(d.flow, d.fiftyh, "百");
    //    } else if (parseInt(d.flow) > parseInt(d.thirtyh)) {//五十年不遇
    //        drawRuler(d.flow, d.thirtyh, "五十");
    //    } else if (parseInt(d.flow) > parseInt(d.twentyh)) {//二十年不遇
    //        drawRuler(d.flow, d.twentyh, "二十");
    //    } else if (parseInt(d.flow) > parseInt(d.tenh)) {//十年不遇
    //        drawRuler(d.flow, d.tenh, "十");
    //    } else {//五年不遇
    //        drawRuler(d.flow, d.fiveh, "五");
    //    }
    //}
}

$(document).ready(function () {

    $('#btnContentSave').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var speed = $('#conSpeed').val();
        var flow = $('#conFlow').val();

        //输入合法
        if (speed && flow) {

            $('#btnGroupSave').text("数据保存中...");
            $('#btnGroupSave').prop('disabled', true);

            var url = '/hfxt/contentsUpd';
            dataJson = {
                id: $('#WaterPersonSaveMode').val(),
                speed: speed,
                flow: flow,
            }

            $.ajax({
                url: url,
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(dataJson),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        $('#dlgContent').modal('hide');
                        //重新加载table数据.
                        //alert('will reload siteid : ' + $('#siteId').val());
                        genLoggingContents($('#siteId').val());
                    } else {
                        //显示错误提示信息
                        $('#dlgContentFormInputInvalid').text(data.message);
                        $('#dlgContentFormInputInvalid').removeClass('hidden');

                        $('#btnWaterPersonSave').text("保存");
                        $('#btnWaterPersonSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        }
        else {
            //输入不合法
            if (!speed) {
                $('#dlgContentFormInputInvalid').text("流速不能为空");
            }
            else if (!flow) {
                $('#dlgContentFormInputInvalid').text("流量不能为空");
            }
            $('#dlgContentFormInputInvalid').removeClass('hidden');
        }
    });


    $.ajax({
        url: '/hfxt/querySitesByType?type=2',  //内涝点type是2
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let datas = data.data;
                let htmls = '';
                for (var i = 0; i < datas.length; i++) {
                    //m.markLoggingSatations([{"id": d.id, "name": d.name, "lng": d.longitude, "lat": d.latitude}]);
                    if (i == 0) {
                        htmls += '<option value="' + datas[i].id + '">' + datas[i].name + '</option>';
                        genLoggingContents(datas[i].id);
                    } else {
                        htmls += '<option value="' + datas[i].id + '">' + datas[i].name + '</option>';
                    }
                }
                $('#allLogging').html(htmls);
            } else {
                //显示错误提示信息
                dialogAlert("操作结果", data.message, "确定");
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });
});


//设置水位站内容
function waterLoggingSetting() {
    $('#waterStateSetId').val($('#allLogging').val());

    //$.ajax({
    //    url: '/hfxt/querySitesById?id=' + $('#allLogging').val(),
    //    type: 'get',
    //    contentType: "application/json; charset=utf-8",
    //    success: function (data) {
    //        //成功
    //        if (data.status == 200) {
    //            //重新加载table数据.
    //            let d = data.data;
    //
    //            //$('#WaterName').val(d.name);
    //            $('#waterStateSetingMgtTitle').text("设置" + d.name + "预警水位");
    //            $('#F5').val(d.fiveh);
    //            $('#F10').val(d.tenh);
    //            $('#F20').val(d.twentyh);
    //            $('#F50').val(d.thirtyh);
    //            $('#F100').val(d.fiftyh);
    //        } else {
    //            //显示错误提示信息
    //            location.href = '/';
    //        }
    //    }
    //});

    let html = '<tr><td class="text-right">预警水位</td><td class="text-left"><input type="number" id="F5" class="form-control input-sm"></td><td class="text-left">米</td></tr>'
        + '<tr><td class="text-right">20cm预警消息</td><td colspan="2" class="text-left"><textarea id="F10" class="form-control input-sm"></textarea></td></tr>'
        + '<tr><td class="text-right">30cm预警消息</td><td colspan="2" class="text-left"><textarea id="F20" class="form-control input-sm"></textarea></td></tr>'
        + '<tr><td class="text-right">40cm预警消息</td><td colspan="2" class="text-left"><textarea id="F50" class="form-control input-sm"></textarea></td></tr>';
    //$('#waterStateSetTable').html(html);


    $.getJSON('/hfxt/waterPerson', function (json) {

        let persons = json.data;
        html += '<tr><td class="text-right">负责人</td><td colspan="2" class="text-left"><div id="selContactPerson" style="overflow: scroll">';
        for (let i in persons) {
            html += '<div style="float: left"><input style="margin-top: 7px; height: 20px; width: 20px" name="selSitePerson" type="checkbox"  value="' + persons[i].phoneNbr + ',' + persons[i].name +'"><div style="margin-top: 8px; float: left">' + (persons[i].name.length < 3 ? persons[i].name + "&nbsp;&nbsp;&nbsp;&nbsp;" : persons[i].name) + '</div></div>';
        }

        html += ' </div> </td> </tr>';
        console.log(html);
        $('#waterStateSetTable').html(html);
        //弹出对话框

        $.ajax({
            url: '/hfxt/querySitesById?id=' + $('#allLogging').val(),
            type: 'get',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //成功
                if (data.status == 200) {
                    //重新加载table数据.
                    let d = data.data;

                    //$('#WaterName').val(d.name);
                    $('#waterStateSetingMgtTitle').text("设置" + d.name + "预警水位");
                    $('#F5').val(d.fiveh);
                    $('#F10').val(d.tenh);
                    $('#F20').val(d.twentyh);
                    $('#F50').val(d.thirtyh);
                    //$('#F100').val(d.fiftyh);
                    $('input[name="selSitePerson"]').each(function () {
                        if (d.fiftyh && d.fiftyh.indexOf($(this).val()) >= 0) {
                            $(this).prop("checked", true);
                        }
                    });

                } else {
                    //显示错误提示信息
                    location.href = '/';
                }
            }
        });

        $('#waterStateSettingMgt').modal({
            backdrop: 'static'
        });

    });


    //弹出对话框
    //$('#waterStateSettingMgt').modal({
    //    backdrop: 'static'
    //});
}


function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                //alert('Yup.');
                cnfFun(result);
            } else {
                //alert('Nope.');
            }
        }
    });

}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}