/**
 * Created by zhoulanhong on 3/29/17.
 */

var dataTablesLang = {
    "sProcessing": "处理中...",
    "sLengthMenu": "显示 _MENU_ 项结果",
    "sZeroRecords": "没有匹配结果",
    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
    "sInfoPostFix": "",
    "sSearch": "搜索:",
    "sUrl": "",
    "sEmptyTable": "表中数据为空",
    "sLoadingRecords": "载入中...",
    "sInfoThousands": ",",
    "oPaginate": {
        "sFirst": "首页",
        "sPrevious": "上页",
        "sNext": "下页",
        "sLast": "末页"
    },
    "oAria": {
        "sSortAscending": ": 以升序排列此列",
        "sSortDescending": ": 以降序排列此列"
    }
};

var waterPersonTable = null;

$(document).ready(function () {
    $('#closeWaterPerson').click(function () {
        $('.waterPersonBox').hide();
        waterPersonTable.destroy();
    });

    //添加防汛联系人
    $('#addWaterPerson').on('click', function (event) {
        event.preventDefault();

        $('#dlgWaterPersonFormInputInvalid').addClass('hidden');
        $('#WaterPersonSaveMode').val("")
        $('#wpName').val("");
        $('#wpPosition').val("");
        $('#wpNbr').val("");
        $('#wpEmgNbr').val("");

        //弹出对话框
        $('#dlgAddWaterPerson').modal({
            backdrop: 'static'
        });
    });

    //对话框点击保存
    $('#btnWaterPSave').on('click', function (event) {
            event.preventDefault();

            //有效性验证
            var wpName = $('#wpName').val();
            var wpPosition = $('#wpPosition').val();
            var wpNbr = $('#wpNbr').val();

            //输入合法
            if (wpName && wpPosition && wpNbr) {

                $('#btnWaterPSave').text("数据保存中...");
                $('#btnWaterPSave').prop('disabled', true);

                var url = '';
                var dataJson = null;
                if ($('#WaterPersonSaveMode').val().length == 0) {
                    url = '/hfxt/waterPerson';
                    dataJson = {
                        wpName: wpName,
                        wpPosition: wpPosition,
                        wpEmgNbr: $('#wpEmgNbr').val(),
                        wpNbr: wpNbr
                    }
                } else {
                    url = '/hfxt/waterPersonUpd';
                    dataJson = {
                        id: $('#WaterPersonSaveMode').val(),
                        wpName: wpName,
                        wpPosition: wpPosition,
                        wpEmgNbr: $('#wpEmgNbr').val(),
                        wpNbr: wpNbr,
                    }
                }

                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(dataJson),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {

                            $('#dlgAddWaterPerson').modal('hide');
                            $('#btnWaterPSave').text("保存");
                            $('#btnWaterPSave').prop('disabled', false);
                            //重新加载table数据.
                            waterPersonTable.ajax.reload();
                        } else {
                            //显示错误提示信息
                            $('#dlgWaterPersonFormInputInvalid').text(data.message);
                            $('#dlgWaterPersonFormInputInvalid').removeClass('hidden');

                            $('#btnWaterPSave').text("保存");
                            $('#btnWaterPSave').prop('disabled', false);
                        }
                    },
                    error: function (err) {
                        location.href = '/';
                    }
                });
            }
            else {
                //输入不合法
                if (!wpName) {
                    $('#dlgWaterPersonFormInputInvalid').text("防汛负责人名称不能为空");
                }
                else if (!wpPosition) {
                    $('#dlgWaterPersonFormInputInvalid').text("防汛负责人职位不能为空");
                }
                else if (!wpNbr) {
                    $('#dlgWaterPersonFormInputInvalid').text("防汛负责人电话不能为空");
                }
                $('#dlgWaterPersonFormInputInvalid').removeClass('hidden');
            }
        }
    );

});

//编辑群
function waterPersonEdit(data) {

    $('#WaterPersonSaveMode').val(data.id);
    $('#wpName').val(data.name);
    $('#wpPosition').val(data.position);
    $('#wpNbr').val(data.phoneNbr);
    $('#wpEmgNbr').val(data.emergencyNbr);

    //弹出对话框
    $('#dlgAddWaterPerson').modal({
        backdrop: 'static'
    });

}

// 删除防汛负责人
function waterPersonRemove(data) {

    dialogConfirm("请确认", "确定要删除本条数据？", function (result) {
        if (result) {
            $.ajax({
                url: '/hfxt/waterPersonDel',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    id: data.id
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        //重新加载table数据.
                        waterPersonTable.ajax.reload();
                    } else {
                        //显示错误提示信息
                        dialogAlert("操作结果", data.message, "确定");
                    }
                },
                error: function (err) {
                    alert(JSON.stringify(err));
                    //location.href = '/';
                }
            });
        }
    });

}

function waterPerson() {
    if ($('.waterPersonBox').is(':hidden')) {
        $('.waterPersonBox').show();

        let columns = [
            {'data': "name"},
            {'data': "name"},
            {'data': "emergencyNbr"},
            {'data': "position"},
            {'data': "phoneNbr"}
        ];

        if(show){
            columns=[
                {'data': "name"},
                {'data': "name"},
                {'data': "emergencyNbr"},
                {'data': "position"},
                {'data': "phoneNbr"},
                {'data': "emergencyNbr"},
            ]
        }

        waterPersonTable = $('#tabWaterPerson').DataTable({
            "language": dataTablesLang,
            "order": [[0, 'desc']],
            "ajax": "/hfxt/waterPerson",
            "columns": columns,

            "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                $('td:eq(0)', nRow).html(parseInt(iDisplayIndex) + 1);
                if(show){
                $('td:eq(5)', nRow).html(
                    "<a class=\"btn btn-link btn-sm wpTableLink pEdit\" role=\"button\">编辑</a>" +
                    "<a class=\"btn btn-link btn-sm wpTableLink pRemove\" role=\"button\">删除</a>"
                    //+ "<a class=\"btn btn-link btn-sm wpTableLink pImages\" role=\"button\">关联图件</a>"
                );}
                return nRow;
            }
        });

        waterPersonTable.on('draw', function () {
            $('.wpTableLink').unbind('click').on('click', function (event) {
                event.preventDefault();

                //本行对应的数据
                var data = waterPersonTable.row($(this).parents('tr')).data();
                if ($(this).hasClass('pEdit')) {
                    waterPersonEdit(data);
                }
                else if ($(this).hasClass('pRemove')) {
                    waterPersonRemove(data);
                }
                if ($(this).hasClass('pImages')) {
                    waterPersonTable.destroy();
                    $('.waterPersonBox').hide();
                    imgMgt("0", data.name, '');
                }
            });
        });

    } else {
        return false;
    }
}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                //alert('Yup.');
                cnfFun(result);
            } else {
                //alert('Nope.');
            }
        }
    });

}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}