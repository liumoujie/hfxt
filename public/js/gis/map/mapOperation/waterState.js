/**
 * Created by zhoulanhong on 3/29/17.
 */

let linkClicked = false;

function trClick(id, long, lat, name, desc, flow, lt) {
    //alert('trclicked');
    //if (linkClicked) {
    //    linkClicked = false;
    //} else {
    //
    //    m.locate({
    //        id: id,
    //        lat: long,
    //        lng: lat,
    //        speed: "0.0KM/H",
    //        iconPath: "",
    //        name: name,
    //        flow: flow,
    //        lastLocationTime: lt,
    //        address: desc
    //    });
    //}
}

setInterval(showWaterStateInterval, 300 * 1000);

function showWaterStateInterval() {
    let id = $('#allStates').val();
    $.ajax({
        url: '/hfxt/querySitesById?id=' + $('#allStates').val(),
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let d = data.data;
                genContents(id);
            } else {
                //显示错误提示信息
                location.href = '/';
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });
}

function showWaterState() {
    let id = $('#allStates').val();
    $.ajax({
        url: '/hfxt/querySitesById?id=' + $('#allStates').val(),
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let d = data.data;
                m.markAllSatations([{
                    "id": d.id, "name": d.name, "lng": d.longitude, "lat": d.latitude, "type": d.type,
                    "zoom": 13
                }]);
                genContents(id);
            } else {
                //显示错误提示信息
                location.href = '/';
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });
}

function changeCss(idx) {
    //alert(idx)
    for (var i = 0; i < 7; i++) {
        $('#showWaterState' + i).removeClass('waterStateBackGroundColor');
    }
    $('#showWaterState' + idx).addClass('waterStateBackGroundColor');
}

//编辑内容
function waterSiteContentEdit(id, speed, flow, siteid) {
    //alert(id);
    linkClicked = true;

    $.ajax({
        url: '/hfxt/querySitesById?id=' + siteid,
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let d = data.data;

                //$('#WaterName').val(d.name);
                $('#dlgContentTitle').text("校核数据-" + d.name);
                $('#WaterPersonSaveMode').val(id);
                $('#siteId').val(siteid);
                $('#conArriveTime').val(speed);
                $('#conFlow').val(flow);

                //弹出对话框
                $('#dlgContent').modal({
                    backdrop: 'static'
                });
            } else {
                //显示错误提示信息
                location.href = '/';
            }
        }
    });
}

function homePage() {
    location.href = '/gis';
}

function showHistory() {
    let y = moment(moment.now()).year();
    let m = (moment(moment.now()).month() + 1) < 10 ? "-0" + (moment(moment.now()).month() + 1) : "-" + (moment(moment.now()).month() + 1);
    let d = (moment(moment.now()).date()) < 10 ? "-0" + (moment(moment.now()).date()) : "-" + (moment(moment.now()).date());
    //alert(y + "  " + m + "   " + d);
    let st = moment(y + m + d + " 00:00:00").utc("+08:00").valueOf();
    //let et = moment(y + m + d + " 23:59:59").valueOf();
    //alert(st);
    //alert(et);
    //return;
    window.open("/his/lists?id=" + $('#allStates').val() + "&qType=d&st=" + st + "&et=" + st);
}

function showLoggingHistory() {
    let y = moment(moment.now()).year();
    let m = (moment(moment.now()).month() + 1) < 10 ? "-0" + (moment(moment.now()).month() + 1) : "-" + (moment(moment.now()).month() + 1);
    let d = (moment(moment.now()).date()) < 10 ? "-0" + (moment(moment.now()).date()) : "-" + (moment(moment.now()).date());
    //alert(y + "  " + m + "   " + d);
    let st = moment(y + m + d + " 00:00:00").utc("+08:00").valueOf();
    //let et = moment(y + m + d + " 23:59:59").valueOf();
    //alert(st);
    //alert(et);
    //return;
    window.open("/his/loggingLists?id=" + $('#allLogging').val() + "&qType=d&st=" + st + "&et=" + st);
}

//设置水位站内容
function waterSiteSetting() {
    $('#waterStateSetId').val($('#allStates').val());

    let html = '<tr><td class="text-right">高程</td><td class="text-left"><input type="number" id="F5" class="form-control input-sm">' +
        '</td><td class="text-left">米</td></tr><tr>' +
        '<td class="text-right">警戒水位</td><td class="text-left"><input type="number" id="F20" class="form-control input-sm"></td>' +
        '<td class="text-left">米</td></tr>';

    if ($('#allStates').val() == "ZYSW000003" || $('#allStates').val() == "ZYSW000004") {
        html = '<tr><td class="text-right">高程</td><td class="text-left"><input type="number" id="F5" class="form-control input-sm">' +
            '</td><td class="text-left">米</td></tr><tr><td class="text-right">保证水位</td><td class="text-left">' +
            '<input type="number" id="F10" class="form-control input-sm"></td><td class="text-left">米</td></tr><tr>' +
            '<td class="text-right">警戒水位</td><td class="text-left"><input type="number" id="F20" class="form-control input-sm"></td>' +
            '<td class="text-left">米</td></tr>';
    }


    $.getJSON('/hfxt/waterPerson', function (json) {

        let persons = json.data;
        html += '<tr><td class="text-right">负责人</td><td colspan="2" class="text-left"><div id="selContactPerson" style="overflow: scroll">';
        for (let i in persons) {
            html += '<div style="float: left"><input style="margin-top: 7px; height: 20px; width: 20px" name="selSitePerson" type="checkbox"  value="' + persons[i].phoneNbr + ',' + persons[i].name + '"><div style="margin-top: 8px; float: left">' + (persons[i].name.length < 3 ? persons[i].name + "&nbsp;&nbsp;&nbsp;&nbsp;" : persons[i].name) + '</div></div>';
        }

        html += ' </div> </td> </tr>';

        $('#waterStateSetTable').html(html);
        //弹出对话框

        $.ajax({
            url: '/hfxt/querySitesById?id=' + $('#allStates').val(),
            type: 'get',
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                //成功
                if (data.status == 200) {
                    //重新加载table数据.
                    let d = data.data;

                    //$('#WaterName').val(d.name);
                    $('#waterStateSetingMgtTitle').text("设置" + d.name + "预警水位");
                    $('#F5').val(d.fiveh);
                    $('#F10').val(d.tenh);
                    $('#F20').val(d.twentyh);
                    $('#F50').val(d.thirtyh);
                    //$('#F100').val(d.fiftyh);
                    if (d.fiftyh) {
                        $('input[name="selSitePerson"]').each(function () {
                            if (d.fiftyh.indexOf($(this).val()) >= 0) {
                                $(this).prop("checked", true);
                            }
                        });
                    }

                } else {
                    //显示错误提示信息
                    location.href = '/';
                }
            }
        });

        $('#waterStateSettingMgt').modal({
            backdrop: 'static'
        });

    });


}


function waterStateSet() {
    let url = '/hfxt/waterSiteSeting';

    let showSelPerson = '';
    $('input[name="selSitePerson"]:checked').each(function () {
        showSelPerson += $(this).val() + ',';
    });

    if (showSelPerson.length < 1) {
        dialogAlert("操作结果", "请选择本站负责人", "确定");
        return;
    }

    let dataJson = {
        id: $('#waterStateSetId').val(),
        A: $('#F5').val(),
        B: $('#F10').val(),
        C: $('#F20').val(),
        D: $('#F50').val(),
        E: showSelPerson.substring(0, showSelPerson.length - 1),
    }
    $.ajax({
        url: url,
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify(dataJson),
        success: function (data) {
            //成功
            if (data.status == 200) {
                dialogAlert("操作结果", "设置成功", "确定");
                $('#waterStateSettingMgt').modal('hide');
            } else {
                //显示错误提示信息
                dialogAlert("操作结果", "设置失败", "确定");
            }
        },
        error: function (err) {
            location.href = '/';
        }
    });
}

function genContents(siteId) {
    $.ajax({
        url: '/hfxt/queryContents?siteid=' + siteId,
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let datas = data.data;
                let htmls = '';
                let nowTime = moment();
                nowTime.seconds(0);
                let nowMinutes = nowTime.minute();
                let offSet = nowMinutes % 5;
                nowTime.subtract(offSet, 'm');
                nowTime.add(5, 'm');
                for (var i = 0; i < datas.length; i++) {
                    let showTime = nowTime.subtract( 5, 'm');
                    htmls += '<tr onclick="trClick(\'' + datas[i].id + '\',' + datas[i].longitude + ',' + datas[i].latitude + ',\'' + datas[i].name + '\',\'' + datas[i].description + '\',\'' + datas[i].flow + '\',\'' + showTime.format('YYYY-MM-DD HH:mm:ss') + '\')"><td  style="padding: 0px;">' + showTime.format('YYYY-MM-DD HH:mm:ss') + '</td>' +
                            // '<td style="padding: 0px;">' + datas[i].speed + '</td>' +
                        '<td style="padding: 0px;">' + (datas[i].flow ? datas[i].flow.toFixed(2) : 0) + '</td>' +
                            //'<td style="padding: 0px;"><a onclick="waterSiteContentEdit(\'' + datas[i].id + '\',\'' + moment(parseInt(datas[i].createtime)).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss') + '\',\'' + datas[i].flow + '\',\'' + siteId + '\')">校核</a></td>' +
                        '</tr>';
                }
                $('#waterStateTb').html(htmls);

                //console.log("htmls : " + htmls);
                //drawRuler(40, 30, '五');
                calcNear(datas[0]);
            } else {
                //显示错误提示信息
                dialogAlert("操作结果", data.message, "确定");
                location.href = '/';
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });
}

$(document).ready(function () {

    $('#btnContentSave').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var speed = $('#conSpeed').val();
        var flow = $('#conFlow').val();

        //输入合法
        if (speed && flow) {

            $('#btnGroupSave').text("数据保存中...");
            $('#btnGroupSave').prop('disabled', true);

            var url = '/hfxt/contentsUpd';
            dataJson = {
                id: $('#WaterPersonSaveMode').val(),
                speed: speed,
                flow: flow,
            }

            $.ajax({
                url: url,
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify(dataJson),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        $('#dlgContent').modal('hide');
                        //重新加载table数据.
                        //alert('will reload siteid : ' + $('#siteId').val());
                        genContents($('#siteId').val());
                    } else {
                        //显示错误提示信息
                        $('#dlgContentFormInputInvalid').text(data.message);
                        $('#dlgContentFormInputInvalid').removeClass('hidden');

                        $('#btnWaterPersonSave').text("保存");
                        $('#btnWaterPersonSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        }
        else {
            //输入不合法
            if (!speed) {
                $('#dlgContentFormInputInvalid').text("流速不能为空");
            }
            else if (!flow) {
                $('#dlgContentFormInputInvalid').text("流量不能为空");
            }
            $('#dlgContentFormInputInvalid').removeClass('hidden');
        }
    });


    $.ajax({
        url: '/hfxt/querySitesByType?type=1',
        type: 'get',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            //成功
            if (data.status == 200) {
                //重新加载table数据.
                let datas = data.data;
                let htmls = '';
                for (var i = 0; i < datas.length; i++) {
                    if (i == 0) {
                        htmls += '<option value="' + datas[i].id + '">' + datas[i].name + '</option>';
                        genContents(datas[i].id);
                    } else {
                        htmls += '<option value="' + datas[i].id + '">' + datas[i].name + '</option>';
                    }
                }

                console.log(htmls);

                $('#allStates').html(htmls);
            } else {
                //显示错误提示信息
                dialogAlert("操作结果", data.message, "确定");
            }
        },
        error: function (err) {
            alert(JSON.stringify(err));
            //location.href = '/';
        }
    });
});

function calcNear(d) {
    //drawRuler(d.flow, d.fiveh, "五");
    if (d && d.flow) {
        drawRuler(d);
    }
    //if (d && d.flow) {
    //    if (parseInt(d.flow) > parseInt(d.fiftyh)) {//百年不遇
    //        drawRuler(d.flow, d.fiftyh, "百");
    //    } else if (parseInt(d.flow) > parseInt(d.thirtyh)) {//五十年不遇
    //        drawRuler(d.flow, d.thirtyh, "五十");
    //    } else if (parseInt(d.flow) > parseInt(d.twentyh)) {//二十年不遇
    //        drawRuler(d.flow, d.twentyh, "二十");
    //    } else if (parseInt(d.flow) > parseInt(d.tenh)) {//十年不遇
    //        drawRuler(d.flow, d.tenh, "十");
    //    } else {//五年不遇
    //        drawRuler(d.flow, d.fiveh, "五");
    //    }
    //}
}

function showFullScreenAction(n, id) {
    $('#showFullPageTitle').text("全景图-" + n);
    //弹出对话框
    if (id == "ZYSW000001") {
        $('#fullPageSrc').attr("src", "http://www.upinvr.com/360/aggk2fsfy5rbqt2k/?sceneid=2");
        $('#showFullPage').modal({
            backdrop: 'static'
        });
    }
    if (id == "ZYSW000002") {
        $('#fullPageSrc').attr("src", "http://www.upinvr.com/360/aggk2fsfy5rbqt2k/?sceneid=3");
        $('#showFullPage').modal({
            backdrop: 'static'
        });
    }
    if (id == "ZYSW000003") {
        $('#fullPageSrc').attr("src", "http://www.upinvr.com/360/aggk2fsfy5rbqt2k/?sceneid=1");
        $('#showFullPage').modal({
            backdrop: 'static'
        });
    }
    if (id == "ZYSW000004") {
        window.open("/cn/jnbdShow.html");
    }
    if (id == "ZYSW000005") {
        window.open("/cn/show.html");
    }
    if (id == "ZYSW000006") {
        $('#fullPageSrc').attr("src", "http://www.upinvr.com/360/aggk2fsfy5rbqt2k/?sceneid=4");
        $('#showFullPage').modal({
            backdrop: 'static'
        });
    }
    if (id == "ZYSW000007") {
        $('#fullPageSrc').attr("src", "http://www.upinvr.com/360/aggk2fsfy5rbqt2k/?sceneid=5");
        $('#showFullPage').modal({
            backdrop: 'static'
        });
    }
    //window.open("http://www.upinvr.com/360/aggk2fsfy5rbqt2k/");
}


//cur    当前水位
//near   警戒水位高度
//year   本水位站设置的高程是多少米
function drawRuler(d) {
    console.log(JSON.stringify(d));
    //低于警戒水位
    let htmls = '';
    if (parseFloat(d.flow) <= parseFloat(d.twentyh)) {
        htmls =
            ( d.siteid == "ZYSW000003" || d.siteid == "ZYSW000004" ? '<div style="margin-top: 8px">保证水位' + (d.tenh ? parseFloat(d.tenh).toFixed(2) : 0) + '米<hr style="height:3px;border:none;border-top:4px solid red;"/></div>' : '') +
            '<div style="margin-top: 0px">' + ( d.siteid == "ZYSW000003" || d.siteid == "ZYSW000004" ? '预警水位' : '警戒水位') + d.twentyh + '米<hr style="height:3px;border:none;border-top:4px solid yellow;"/></div>' +
            '<div id="rulerLineCur" style="margin-top: 50px">当前水位' + (d.flow ? d.flow.toFixed(2) : 0) +
            '米<hr style="height:3px;border:none;border-top:4px solid white;"/></div>' +
            '<div style="margin-top: 36px">高程' + d.fiveh + '<hr style="height:3px;border:none;border-top:4px solid blue;"/></div>';

        $('#alarm').html('<i class="alarmIcon">');
    } else {
        if (parseFloat(d.flow) <= parseFloat(d.tenh)) {

            htmls =
                '<div id="rulerLineCur" style="margin-top: 10px">保证水位' + (d.tenh ? parseFloat(d.tenh).toFixed(2) : 0) + '米<hr style="height:3px;border:none;border-top:4px solid red;"/>' +
                '</div>' + ( d.siteid == "ZYSW000003" || d.siteid == "ZYSW000004" ? '<div style="margin-top: 8px">  当前水位' + (d.flow ? d.flow.toFixed(2) : 0) + '米<hr style="height:3px;border:none;border-top:4px solid white;"/></div>' : '') +
                '<div style="margin-top: 0px">' + ( d.siteid == "ZYSW000003" || d.siteid == "ZYSW000004" ? '预警水位' : '警戒水位') + d.twentyh + '米<hr style="height:3px;border:none;border-top:4px solid yellow;"/></div>' +
                '<div style="margin-top: 36px">高程' + d.fiveh + '<hr style="height:3px;border:none;border-top:4px solid blue;"/></div>';
        } else {
            htmls =
                '<div id="rulerLineCur" style="margin-top: 10px">当前水位' + (d.flow ? d.flow.toFixed(2) : 0) +
                '米<hr style="height:3px;border:none;border-top:4px solid white;"/></div>' +
                ( d.siteid == "ZYSW000003" || d.siteid == "ZYSW000004" ? '<div style="margin-top: 8px">保证水位' + (d.tenh ? parseFloat(d.tenh).toFixed(2) : 0) + '米<hr style="height:3px;border:none;border-top:4px solid red;"/></div>' : '') +
                '<div style="margin-top: 0px">' + ( d.siteid == "ZYSW000003" || d.siteid == "ZYSW000004" ? '预警水位' : '警戒水位') + d.twentyh + '米<hr style="height:3px;border:none;border-top:4px solid yellow;"/></div>' +
                '<div style="margin-top: 36px">高程' + d.fiveh + '<hr style="height:3px;border:none;border-top:4px solid blue;"/></div>';
        }

        m.markAllSatations([{
            "id": d.siteid, "name": d.name, "lng": d.longitude, "lat": d.latitude, "type": d.type, "alarm": "1",
            "zoom": 13
        }]);

        $('#alarm').html('<i class="onAlarmIcon">');
    }
    $('#rulerText').html(htmls);
}

function threeSystem() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    if (userAgent.indexOf("Chrome") > -1) {
        window.open("http://124.161.254.149:9066/flood");
    } else {
        var objShell = new ActiveXObject("WScript.Shell");
        objShell.Run("cmd.exe /c start chrome http://124.161.254.149:9066/flood", 0, true);
    }
    //}else if(userAgent.indexOf("MSIE") > -1){
    //    var objShell= new ActiveXObject("WScript.Shell");
    //    /*命令参数说明
    //    cmd.exe /c dir 是执行完dir命令后关闭命令窗口。
    //    cmd.exe /k dir 是执行完dir命令后不关闭命令窗口。
    //    cmd.exe /c start dir 会打开一个新窗口后执行dir指令，原窗口会关闭。
    //    cmd.exe /k start dir 会打开一个新窗口后执行dir指令，原窗口不会关闭。
    //    这里的dir是start chrome www.baidu.com//用谷歌浏览器打开百度*/
    //    objShell.Run("cmd.exe /c start chrome http://124.161.254.149:9066/flood",0,true);
    //}else{
    //    dialogAlert("操作结果", "请使用IE或者Chrome浏览器", "确定");
    //}
}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                //alert('Yup.');
                cnfFun(result);
            } else {
                //alert('Nope.');
            }
        }
    });

}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}