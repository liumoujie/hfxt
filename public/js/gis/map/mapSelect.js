/**
 * Created by zhoulanhong on 1/6/17.
 */
var topMargin;
var leftMargin;
//当前正在画的矩形对象
var Rectagle = {

    obj: null,//画布
    container: null,//初始化函数
    init: function (containerId) {
        Rectagle.container = document.getElementById(containerId);
        if (Rectagle.container) {
            Rectagle.container.onmousedown = Rectagle.start;//鼠标按下时开始画
            Rectagle.container.onmouseout = Rectagle.destory;
        } else {
            alert('请指定正确的容器!');
        }
    },
    destory: function (e) {
        Rectagle.container.onmousedown = null;
        Rectagle.obj = null;

        document.getElementById("divMouseSelect").style.display = "none";
    },
    start: function (e) {
        Rectagle.container.onmouseout = null;

        var o = Rectagle.obj = document.createElement('div');
        o.style.position = "absolute";
        // mouseBeginX，mouseBeginY是辅助变量，记录下鼠标按下时的位置
        o.mouseBeginX = Rectagle.getEvent(e).x - leftMargin;
        o.mouseBeginY = Rectagle.getEvent(e).y - topMargin;
        o.style.left = o.mouseBeginX + "px";
        o.style.top = o.mouseBeginY + "px";
        o.style.height = 0;
        o.style.width = 0;
        o.style.border = "dotted black 1px";
        o.style.backgroundColor = "#FFFF00";

        //把当前画出的对象加入到画布中
        Rectagle.container.appendChild(o);
        //处理onmousemove事件
        Rectagle.container.onmousemove = Rectagle.move;
        //处理onmouseup事件
        Rectagle.container.onmouseup = Rectagle.end;
    },
    move: function (e) {
        var o = Rectagle.obj;
        //dx，dy是鼠标移动的距离
        var dx = Rectagle.getEvent(e).x - o.mouseBeginX - leftMargin;
        var dy = Rectagle.getEvent(e).y - o.mouseBeginY - topMargin;
        //如果dx，dy <0,说明鼠标朝左上角移动，需要做特别的处理
        if (dx < 0) {
            o.style.left = (Rectagle.getEvent(e).x - leftMargin) + "px";
        }
        if (dy < 0) {
            o.style.top = (Rectagle.getEvent(e).y - topMargin) + "px";
        }
        o.style.height = Math.abs(dy) + "px";
        o.style.width = Math.abs(dx) + "px";
    },
    removeAllChild: function (obj) {
        while (obj.lastChild) {
            obj.removeChild(obj.lastChild);
        }
    },
    end: function (e) {
        //alert('--end--');
        //通过div的位置和长宽,计算出选择的区域
        Rectagle.chooseResult();
        //去掉所有新创建的 div
        Rectagle.removeAllChild(Rectagle.container);
        //onmouseup时释放onmousemove，onmouseup事件句柄
        Rectagle.container.onmousedown = null;
        Rectagle.container.onmousemove = null;
        Rectagle.container.onmouseup = null;
        Rectagle.obj = null;

        //document.getElementById("divMouseSelect").style.display = "none";
        $('#divMouseSelect').hide();
        //if (document.getElementById("multipleChooseEmps").value.length > 0) {
        //    //www.openWin(270, 70, 500, 350, "./page/mis-create.jsp");
        //    alert('end');
        //}
    },
    //辅助方法，处理IE和FF不同的事件模型
    getEvent: function (e) {
        if (e == undefined) {
            e = window.event;
        }
        if (e.x == undefined) {
            e.x = e.pageX;
        }
        if (e.y == undefined) {
            e.y = e.pageY;
        }
        return e;
    },
    chooseResult: function () {
        var startX = Rectagle.obj.offsetLeft;
        var startY = Rectagle.obj.offsetTop;
        var endX = Rectagle.obj.offsetWidth + startX;
        var endY = Rectagle.obj.offsetHeight + startY;
        //alert(startX + " " + startY + "  " + endX + "  " + endY);
        //alert(map.pixelToPoint(startX,startY));
        var startPoint = m.getMap().pixelToPoint(startX, startY);
        var endPoint = m.getMap().pixelToPoint(endX, endY);
        chooseBySelect(startPoint, endPoint);
        //document.getElementById("multipleChooseEmps").value = groupSelected(startPoint, endPoint);
    }
};

function mouseSelect() {
    //    $('#divMouseSelect').show();
    showdivMouseSelect();
    Rectagle.init("divMouseSelect");
}

function showdivMouseSelect() {
    //复制整个 allmap 的位置属性,用于完全 cover 做 allmap
    var dwidth = document.getElementById("allmap").offsetWidth;
    var dheight = document.getElementById("allmap").offsetHeight;
    var dtop = document.getElementById("allmap").offsetTop;
    var dleft = document.getElementById("allmap").offsetLeft;
    document.getElementById("divMouseSelect").style.width = dwidth + "px";
    document.getElementById("divMouseSelect").style.height = dheight + "px";
    document.getElementById("divMouseSelect").style.top = dtop + "px";
    document.getElementById("divMouseSelect").style.left = dleft + "px";

    topMargin = dtop;
    leftMargin = dleft;
    $('#divMouseSelect').show();
}