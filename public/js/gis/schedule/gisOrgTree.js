/**
 * Created by mycow on 16/8/23.
 */

var lineColors = ["#ff0000", "#00ffff", "#00aeef", "#fff200", "#05d221", "#ff530d", "#ff8b00", "#00ff48", "#0cf8b8", "#0133f8"];
var showFlag = false;
var localFlag = false;
var realTraceFlg = 0;
var realTimeSlected = "";
var allSelectedStaff = "";
//用于保存每个用户对应的 html 数据,方便切换人员的时间,自动用生成好的html替换表格的内容
var replayPoints = new Map();
//用于保存每个用户对应的 所有轨迹点 数据,播放轨迹
var replayPointsForTrace = new Map();
//保存当前播放到了第几步,用于在手动播放的时候,控制步数
var currentStep = 0;
//自动播放轨迹时的定时器
var repInternal = null;
//自动播放轨迹时的播放步部
var repIndex = 0;
//人员选择idx计数
var selStaffIndex = 0;
//当前选择的人员的Id
var selStaffId = 0;
//设置当前是全部还是单人模式,默认是1:单人模式 0:查询出所有
var replayType = 1;
//重置搜索条件,重置之后,需要进行重新查询
var resetQueryCondition = 0;
//保存当前的开始时间,用于判断时间是否已经重新修改
var saveCurSt = null;
//保存当前的结束时间,用于判断时间是否已经重新修改
var saveCurEt = null;
var lastTreeId = "";
var zTreeObj;


//function setReplayTyep(type) {
//    replayType = type;
//}

var settingOrganizationStaff = {
    async: {//同ajax设置
        enable: true,
        url: getAsyncUrlStaff,
        contentType: "application/x-www-form-urlencoded",
        //autoParam:["_id=father"],
        dataType: "json",
        //otherParam:{"otherParam":"orgZTreeAsync"},
        dataFilter: filter,
        type: "get"
    },
    check: {
        enable: true
    },
    callback: {
        //onClick: locateUser,
        //onCheck: selectStaff,
        //onNodeCreated: createHideStaffElement,
        //beforeAsync: loadTreeSucess
    },
    data: {
        simpleData: {
            enable: true,
            idKey: '_id',
            pIdKey: 'father',
            rootPId: -1,
        }
    }
};
//按钮点击后下拉replay
// function shrinkReplay() {
//     $('.shirinkBtn').hide();
//     $('.stretchBtn').slideDown("slow");
//     $('#rePlay').hide();
//     $('.replayBottm').show();
// }

function createHideStaffElement(event, treeId, treeNode) {
    var aObj = $("#" + treeNode.tId + "_a");
    var editStr = "<input type='hidden' value='" + treeNode.tId + "' id='hideStaffElement_" + treeNode.idNumber + "'>";
    aObj.append(editStr);

    if (onlineMap.get(treeNode.idNumber + "")) {
        setOnlieStaff(treeNode.idNumber);
    }
    treeNode.icon = "http://" + SERV.fdfs_img_host + ":" + SERV.fdfs_img_port + "/group1/M00/00/00/eSkWC1hWOHWAdQlNAACAb7wI6hA699.png";

};

//轨迹导出报表
$('#exportBtn').on('click', function () {

    if ($("#stDate").val() == "" || $("#edDate").val() == "") {

        $('#choiseZtree').html('请设置开始和结束时间').removeClass('replayTitle');
        return;
    }

    var selStaffs = "";
    var selStaffNams = "";
    var preSelect = $(".btn.btn-primary");
    if (preSelect && preSelect.attr("id")) {
        selStaffNams = preSelect.attr("value");
        var selStaffs = preSelect.attr("id").substring(4, preSelect.attr("id").length);
    }

    //全部查询出来
    if (replayType == 0) {
        selStaffs = "";
        selStaffNams = "";
        selectedStaffs.forEach(function (staff) {
            selStaffs += selStaffs.length > 0 ? ("," + staff._id ) : staff._id;
            selStaffNams += selStaffNams.length > 0 ? ("," + staff.staffName ) : staff.staffName;
        });
    }

    $.get('/api/exportLocs?startTime=' + getTimeByDate($("#stDate").val()) + '&endTime='
        + getTimeByDate($("#edDate").val()) + '&userId=' + selStaffs + '&names=' + selStaffNams, function (data) {
        //$.get('/api/queryLocs?startTime=1482355501441&endTime=1482417601441&userId=57c44a100e9657946a87e3d6,57c44a240e9657946a87e3d7', function (data) {
        if (data && data.status == 200) {
            window.open(data.data);
        } else {
            $('#choiseZtree').html('查询轨迹信息出错').removeClass('replayTitle');
        }
    });
});



function realTrace() {
    if (selectedStaffs.szie == 0) {
        $('#choiseZtree').html('请先选择人员').removeClass('replayTitle');
        return;
    }

    var selStaffs = [];

    selectedStaffs.forEach(function (staff) {
        selStaffs.push(staff.idNumber);
    });

    realTraceFlg = 1 - realTraceFlg;
    $.ajax({
        url: '/api/contact/realTrace',
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            idNumber: selStaffs,
            type: realTraceFlg > 0 ? "" : "cancel"
        }),
        success: function (data) {
            alert(realTraceFlg > 0 ? "设置成功,5秒后开始追踪" : "取消追踪成功");
        },
        error: function (err) {
            location.href = '/';
        }
    });

}


//根据界面上框选的范围,来对组织树进行选择
function chooseBySelect(startPoint, endPoint) {

    var treeObj = $.fn.zTree.getZTreeObj("orgtree");
    var allNodes = treeObj.transformToArray(treeObj.getNodes());

    for (var i in allNodes) {
        /* 四个条件:
         * 1.有最后一条坐标记录
         * 2.当前在线
         * 3.在组织树中没有被选中
         * 4.经纬度在坐标之内
         * */

        if (allNodes[i].children && allNodes[i].children.length > 0) {
            //alert(treeObj.expandNode(allNodes[i], true, true, true));
            continue;
        }

        //console.log(i + "-i-" + JSON.stringify(allNodes[i]));
        //  onlineMap.get(allNodes[i].idNumber + "") &&   //如果需要只选择在线的,那么就要加上这个条件
        if (allNodes[i].lastLocation && !allNodes[i].checked &&
            (allNodes[i].lastLocation.coordinates[0] > startPoint.lng &&
            allNodes[i].lastLocation.coordinates[0] < endPoint.lng &&
            allNodes[i].lastLocation.coordinates[1] < startPoint.lat &&
            allNodes[i].lastLocation.coordinates[1] > endPoint.lat)) {

            var pnode = allNodes[i].getParentNode();
            if (pnode) {
                treeObj.expandNode(pnode, true, true, true);
                $('#' + ($('#hideStaffElement_' + allNodes[i].idNumber).val()) + '_check').click();
            }
        }
    }
}

function staffRemoveHoverDom(treeId, treeNode) {
    localFlag = false;
    $("#diyBtn_" + treeNode.idNumber).unbind().remove();
};


function generateStaffButton(nodeName, id, btnIdx) {
    allSelectedStaff.length > 0 ? allSelectedStaff +=
        "&nbsp;&nbsp;<input type='button' id='btn_" + id + "' class='btn btn-default' onclick=\"rep.changeReplayStaff('" + id + "','" + btnIdx + "')\" value='" + nodeName + "'>" :
        allSelectedStaff = "<input type='button' id='btn_" + id + "' class='btn btn-primary'  onclick=\"rep.changeReplayStaff('" + id + "','" + btnIdx + "')\" value='" + nodeName + "'>";
}

function changeReplayStaff(id, btnIdx) {
    //切换人员之后,重置所有的回放设置
    if (replayType == "0") {
        //如果是全部选择,那么重新选人,不需要修改查询条件,直接读取数据
        resetReplay(1);
    } else {
        resetReplay(0);
    }

    //先对button进行着色变换,标志当前是选择的哪个button(即哪名员工)
    if (btnIdx) {
        selStaffIndex = btnIdx;
        selStaffId = id;
    }
    var preSelect = $(".btn.btn-primary");
    preSelect.removeClass();
    preSelect.addClass("btn btn-default");
    $('#btn_' + id).removeClass();
    $('#btn_' + id).addClass("btn btn-primary");
    //显示当前选中的人员姓名
    var btnName = $('#btn_' + id).val();
    $('.chisePeopel').html(btnName);

    var repPoints = replayPoints.get(id);


    if (repPoints) {
        $('#tracedetailtable').html(repPoints);
    } else {
        $('#tracedetailtable').html('<tr><td colspan="8" align="center">数据查询中...</td></tr>');
    }
    //立即开发播放
    repaly();
}

function selPreStaff() {
    var staffArray = [];
    selectedStaffs.forEach(function (staff) {
        staffArray.push(staff._id);
    });
    if (staffArray.length == 0) {
        $('#choiseZtree').html('当前无人员').removeClass('replayTitle');
        return false;
    }
    if (selStaffIndex == 0) {
        $('#choiseZtree').html('已经是第一个人了').removeClass('replayTitle');
        return false;
    } else {
        selStaffIndex--;
        changeReplayStaff(staffArray[selStaffIndex]);
    }
}

function closeDialog() {
    $('.diydiaolog').hide();
}

function selNextStaff() {
    var staffArray = [];
    selectedStaffs.forEach(function (staff) {
        staffArray.push(staff._id);
    });
    if (staffArray.length == 0) {
        $('#choiseZtree').html('当前无人员').removeClass('replayTitle');
        return false;
    }
    if (selStaffIndex >= staffArray.length - 1) {
        $('#choiseZtree').html('已经是最后一个人了').removeClass('replayTitle');
    } else {
        selStaffIndex++;
        changeReplayStaff(staffArray[selStaffIndex]);
    }
}

function showLeftMenu() {
    if (showFlag) {
        $('#showLeftBtn').show();
        $('#leftCon').hide();
        showFlag = false;
    } else {
        $('#leftCon').show();
        $('#showLeftBtn').hide();
        showFlag = true;
    }
}

function closeReplay() {
    $('#rePlay').hide();
    $('.authorize').show();
    $('.shirinkBtn').hide();
}

function closeCommand() {
    $('#commandModeDiv').hide();
}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                cnfFun(result);
            }
        }
    });

}

/**
 *
 * @param treeId 对应 zTree 的 treeId，便于用户操控
 * @param treeNode 需要异步加载子节点的的父节点 JSON 数据对象,针对根进行异步加载时，treeNode = null
 * @returns {string} url
 */
function getAsyncUrlDepartment(treeId, treeNode) {

    if (null == treeNode) {
        return "/api/contact/departments";
    }

    treeNode.zAsync = true;
    zTreeObj.expandNode(treeNode, true, false, true, false);

    return;
    //return "/api/contact/syncUsers";//未进行异步加载
};

function getAsyncUrlStaff(treeId, treeNode) {

    if (null == treeNode) {
        return "/api/contact/organization";
    }

    treeNode.zAsync = true;
    zTreeObj.expandNode(treeNode, true, false, true, false);

    return;
    //return "/api/contact/syncUsers";//未进行异步加载
};

function filter(treeId, parentNode, childNodes) {

    if (!childNodes) {
        return null;
    }
    if (childNodes.status != 200) {
        return null;
    }

    childNodes = childNodes.data;

    return childNodes;
}

function updateTips(t) {
    tips = $(".validateTips");
    tips
        .text(t)
        .addClass("ui-state-highlight");
    setTimeout(function () {
        tips.removeClass("ui-state-highlight", 1500);
    }, 500);
}

function checkLength(o, n, min, max) {
    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        updateTips(min + "长度必须在" + min + "到" + max + "之间");
        return false;
    } else {
        return true;
    }
}

//emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
function checkRegexp(o, regexp, n) {
    if (!( regexp.test(o.val()) )) {
        o.addClass("ui-state-error");
        updateTips(n);
        return false;
    } else {
        return true;
    }
}
//页面刷新后判定
$(document).ready(function () {

});
//点击指挥模式时，改变整体风格
function commadeType() {
    $('#commandErrorMsg').html(" ");
    $('.commanParamBox input').removeAttr("disabled");
    $('#commandTime').val("0");
    $('#collectFreq').val("5");
    $('#commandApply').css('background-color', '#5fb1fa');
}
function viewType() {
    $('#commandApply').css('background-color', '#5fb1fa');
    $('.commanParamBox input').attr('disabled', true);
}

function submitCommandMode() {
    //有效性验证
    var type = $('input[name="type"]:checked').val();
    var commandTime = $('#commandTime').val();
    var collectFreq = $('#collectFreq').val();
    var commandEndTime = "";
    if (type == 1) {
        if (commandTime == "") {
            $('#commandErrorMsg').html("指挥时间不能为空");
            return;
        }
        if (collectFreq == "") {
            $('#commandErrorMsg').html("上报频率不能为空");
            return;
        }
        if (commandTime == 0) {
            $('#commandEndTime').html("长期指挥");
            var curTime = new Date();
            var endTime = curTime.getTime() + (365 * 24 * 3600 * 60 * 1000);
            curTime = new Date(endTime);
            commandEndTime = curTime.getTime();
            changetStyle(type);
        } else {
            var curTime = new Date();
            var endTime = curTime.getTime() + (commandTime * 60 * 1000);
            curTime = new Date(endTime);
            commandEndTime = curTime.getTime();
            $('#commandEndTime').html((curTime.getHours() < 10 ? ("0" + curTime.getHours()) : curTime.getHours()) + ":"
                + (curTime.getMinutes() < 10 ? ("0" + curTime.getMinutes()) : curTime.getMinutes()) + "结束指挥");
            changetStyle(type);
        }
    }
    if (type == 0) {
        $('#commandTime').val("");
        $('#collectFreq').val("");
        changetStyle(type);

        commandEndTime = "";
    }

    $.ajax({
        url: '/api/enterprise/chgCmdMode',
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            type: type, //0 是观察   1 是指挥
            commandTime: commandTime,
            collectFreq: collectFreq,
            endTime: commandEndTime
        }),
        success: function (data) {
            console.log(JSON.stringify(data));
            //成功
            if (data.status == 200) {
                $('#commandErrorMsg').html("设置成功");
                $('#commandMod').text('模式/指挥');
                $('#commandApply').attr('disabled', true);
            } else {
                //显示错误提示信息
                $('#commandErrorMsg').text(data.msg);
            }
        },
        error: function (err) {
            alert(err);
            location.href = '/';
        }
    });
}

//function selectCommandMode() {
//    $('#commandApply').attr('disabled', false);
//    $('input[name="type"]:eq(1)').click();
//}

function clearCommandMode() {
    $('input[name="type"]:eq(0)').click();
    $('#commandErrorMsg').html("");
    $('#commandEndTime').html('');
    $('#commandTime').val('');
    $('#collectFreq').val('');
    $('#commandApply').attr('disabled', false);
    $('#commandMod').text('模式/观察');
    //颜色恢复成普通模式
    changeviw();
}

function removeHoverDom(treeId, treeNode) {
    $("#addBtn_" + treeNode.tId).unbind().remove();
};


/**
 * 搜索树
 * @param inputID, String, input输入框id
 */
function searchInTree(inputID) {
    gisOrgTree.search($('#' + inputID).val());
}

//性能测试产生模拟数据
function dataMaker(count) {
    var nodes = [], pId = -1,
        min = 10, max = 90, level = 0, curLevel = [], prevLevel = [], levelCount,
        i = 0, j, k, l, m;

    while (i < count) {
        if (level == 0) {
            pId = -1;
            levelCount = Math.round(Math.random() * max) + min;
            for (j = 0; j < levelCount && i < count; j++, i++) {
                var n = {id: i, pId: pId, name: "Big-" + i};
                nodes.push(n);
                curLevel.push(n);
            }
        } else {
            for (l = 0, m = prevLevel.length; l < m && i < count; l++) {
                pId = prevLevel[l].id;
                levelCount = Math.round(Math.random() * max) + min;
                for (j = 0; j < levelCount && i < count; j++, i++) {
                    var n = {id: i, pId: pId, name: "Big-" + i};
                    nodes.push(n);
                    curLevel.push(n);
                }
            }
        }
        prevLevel = curLevel;
        curLevel = [];
        level++;
    }
    return nodes;
}

//组织树的数据加载成功后,再连指挥账号
function loadTreeSucess() {
    init("gis");
}

//在弹出的窗口中点击的轨迹回放
function winReplay(id, name) {
    var stGmt = $("#startGmt").val();
    var edGmt = $("#endGmt").val();

    if ($("#startGmt").val() == "" || $("#endGmt").val() == "") {
        $('#choiseZtree').html("请设置开始和结束时间").removeClass('replayTitle');
        return;
    }

    map.closeInfoWindow();

    $('#replaySelectedStaff').html("<input type='button' id='btn_" + id + "' class='btn btn-primary' onclick=\"changeReplayStaff('" + id + "')\" value='" + name + "'>");

    $("#stDate").val($("#startGmt").val());
    $("#edDate").val($("#endGmt").val());

    $("#rePlay").show();

    selectedStaffs = new Set();
    selectedStaffs.add(
        {
            "_id": id,
            "staffName": name
        }
    );

    commonReplay(id, stGmt, edGmt);
}

//当通过"轨迹回放"button进行轨迹回放时,将所选数据设置为组织树上勾选的数据,因为通过定位点上回放数据时,会重新设置一次数据
function resetTreeSelect() {
    selectStaff(null, "orgtree", null);
}

//自动播放的时候,暂停回放
function pause() {
    if (repInternal) {
        clearInterval(repInternal);
        repInternal = null;
    }
}



function getCurrentSelStaff() {
    var preSelect = $(".btn.btn-primary");
    if (preSelect && preSelect.attr("id")) {
        return preSelect.attr("id").substring(4, preSelect.attr("id").length);
    }
    return null;
}

function beginReplay() {
    if (repInternal) {
        //如果正在回放,直接返回
        return false;
    }

    var uId = getCurrentSelStaff();
    if (uId) {
        var sigles = replayPoints.get(uId) ? replayPoints.get(uId) :
            '<tr><td colspan="8" align="center">当前时间段无数据</td></tr>';
        $('#tracedetailtable').html(sigles);
        var tmpi = replayPointsForTrace.get(uId);
        //默认将回放的图标设置到第一个人的第一个点,如果是有第二个点,同时加上方向
        if (tmpi && tmpi.length > 1) {
            autoReplayTrace();
        }

    }
}



function jumpStep(from, to, infoIdx) {
    var pixel = map.pointToPixels(from.lng, from.lat);
    var nextPixel = map.pointToPixels(to.lng, to.lat);
    var ang = calcAngle(pixel, nextPixel);

    map.locateReplayIcon(to, "/images/navigation.png", infoIdx, ang);
}

//单步显示轨迹前一步
function preRaceStep() {
    if (currentStep == 0) {

        $('#choiseZtree').html("当前已经是第一步了").removeClass('replayTitle');
        return null;
    } else {
        var uId = getCurrentSelStaff();
        if (uId) {
            var points = replayPointsForTrace.get(uId);
            if (points && points.length > 1) {
                hightCurrentTr(uId, currentStep);
                jumpStep({lng: points[currentStep].point.lng, lat: points[currentStep].point.lat},
                    {lng: points[currentStep - 1].point.lng, lat: points[currentStep - 1].point.lat},
                    uId + "" + (currentStep - 1));
                currentStep--;
            }
        } else {

            $('#choiseZtree').html("请选择要回放的人员").removeClass('replayTitle');
            return null;
        }
    }
}

//高亮并且自动滚动 tr
function hightCurrentTr(uId, id) {
    var trId = uId + "" + id;
    $('.highLigthTr').removeClass("highLigthTr");
    $('#' + trId).addClass("highLigthTr");

    var tb = $('.table_title');
    tb.scrollTop(id * 29);
}

//单步显示轨迹后一步
function nextRaceStep() {
    var uId = getCurrentSelStaff();
    if (uId) {
        var points = replayPointsForTrace.get(uId);
        if (points && points.length > 1) {
            if (currentStep == points.length - 1) {
                $('#choiseZtree').html("已经到了最后一步").removeClass('replayTitle');
                return;
            }
            else {
                hightCurrentTr(uId, currentStep);
                jumpStep({lng: points[currentStep].point.lng, lat: points[currentStep].point.lat},
                    {lng: points[currentStep + 1].point.lng, lat: points[currentStep + 1].point.lat},
                    uId + "" + (currentStep + 1));
                currentStep++;
            }
        }
    } else {

        $('#choiseZtree').html("请选择要回放的人员").removeClass('replayTitle');
        return null;
    }
}

function setNigavateIcon(point, nextPoint, infoIdx) {
    var ang = 0;
    var pixel = map.pointToPixels(point.lng, point.lat);
    //如果有第二个点,那么计算第一个点到第二个点的方向
    if (nextPoint) {
        var nextPixel = map.pointToPixels(nextPoint.lng, nextPoint.lat);
        ang = calcAngle(pixel, nextPixel);
    }

    map.locateReplayIcon(point, "https://localhost:20007/images/navigation.png", infoIdx, ang);

}


function loctionFocus(idx) {
    map.locateByReplayId(idx);
}

