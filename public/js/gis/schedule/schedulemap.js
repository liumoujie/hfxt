/**
 * Created by mycow on 16/9/5.
 * deprecated!!! use map/mapOpeation && baiduMapAdapter
 */


var baiduMap = new BMap.Map("allmap");    // 创建Map实例

var mapIns = new Object();

var marker2 = null;

mapIns.showMap = function (mapObj) {
    //navigator.geolocation.getCurrentPosition(function (data) {
    //    //alert("-----"+data.coords.latitude);
    //    var currentLat = data.coords.latitude;
    //    var currentLon = data.coords.longitude;
    //
    //    mapObj.centerAndZoom(new BMap.Point(currentLon, currentLat), 11);  // 初始化地图,设置中心点坐标和地图级别
    //    mapObj.addControl(new BMap.MapTypeControl());   //添加地图类型控件
    //    //mapObj.setCurrentCity("北京");          // 设置地图显示的城市 此项是必须设置的
    //    mapObj.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
    //
    //});

// 百度地图API功能
    mapObj.centerAndZoom(new BMap.Point(104.047357999, 30.6326078), 11);  // 初始化地图,设置中心点坐标和地图级别
    mapObj.addControl(new BMap.MapTypeControl());   //添加地图类型控件
    mapObj.setCurrentCity("成都");          // 设置地图显示的城市 此项是必须设置的
    mapObj.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
}

//添加地图类型和缩略图
mapIns.add_control_Overview = function (mapObj) {
    //map.addControl(new BMap.MapTypeControl({mapTypes: [BMAP_NORMAL_MAP,BMAP_HYBRID_MAP]}));          //2D图，卫星图
    //map.addControl(new BMap.MapTypeControl({anchor: BMAP_ANCHOR_TOP_LEFT}));          //左上角，默认地图控件
    mapObj.addControl(new BMap.OverviewMapControl());          //添加默认缩略地图控件
    //mapObj.addControl(new BMap.OverviewMapControl({isOpen: true, anchor: BMAP_ANCHOR_BOTTOM_RIGHT}));      //右下角，打开

}

// 添加带有定位的导航控件
mapIns.add_control_Geolocation = function (mapObj) {
    mapObj.addControl(new BMap.NavigationControl({
        // 靠左上角位置
        anchor: BMAP_ANCHOR_TOP_LEFT,
        // LARGE类型
        type: BMAP_NAVIGATION_CONTROL_LARGE,
        // 启用显示定位
        enableGeolocation: true
    }));
    // 添加定位控件
    var geolocationControl = new BMap.GeolocationControl();
    geolocationControl.addEventListener("locationSuccess", function (e) {
        // 定位成功事件
        var address = '';
        address += e.addressComponent.province;
        address += e.addressComponent.city;
        address += e.addressComponent.district;
        address += e.addressComponent.street;
        address += e.addressComponent.streetNumber;
//            alert("当前定位地址为：" + address);
    });
    geolocationControl.addEventListener("locationError", function (e) {
        // 定位失败事件
        alert(e.message);
    });
    mapObj.addControl(geolocationControl);
}

mapIns.addPoints = function (x, y) {
    //alert(x + "  " + y);
    //var pt = new BMap.Point(104.047, 30.43);
    var pt = new BMap.Point(x, y);
    var myIcon = new BMap.Icon("http://localhost:3000/logo/loc.png", new BMap.Size(128, 128));
    if (marker2 != null) {
        baiduMap.removeOverlay(marker2);
    }
    marker2 = new BMap.Marker(pt, {icon: myIcon});  // 创建标注
    baiduMap.addOverlay(marker2);
}

//移除地图类型和缩略图
mapIns.delete_control = function (map) {
    mapObj.removeControl(mapType1);   //移除2D图，卫星图
    mapObj.removeControl(mapType2);
    mapObj.removeControl(new BMap.OverviewMapControl());
    mapObj.removeControl(overViewOpen);
}

mapIns.showMap(baiduMap);
mapIns.add_control_Overview(baiduMap);
mapIns.add_control_Geolocation(baiduMap);
