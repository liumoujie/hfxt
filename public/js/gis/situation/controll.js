/**
 * Created by 123456 on 2017/3/3.
 */

/*
 data:{type,commandTime,collectFreq}
 */

$(function () {
    let curHeigth = ($(window).height() - 25) + 'px';
    $('#mapCover').css('margin-top', curHeigth);
    $(window).resize(function () {          //当浏览器大小变化时
        //alert($(window).height());          //浏览器时下窗口可视区域高度
        let curHeigth = ($(window).height() - 25) + 'px';
        $('#mapCover').css('margin-top', curHeigth);
    });

    //initDepartmentTree();
    $(".panel-heading").click(function (e) {
        /*切换折叠指示图标*/
        $(this).find("span").toggleClass("glyphicon-chevron-down");
        $(this).find("span").toggleClass("glyphicon-chevron-up");
    });
    getNowFormatDate();
    menuHover();
    $('.leftZoomclose').click(function () {
        //$('.mainLeft').css('overflow', 'hidden');
        //$('.mainLeft').animate({width:'0px'});
        $('.mainLeft').hide('slide', 1000);
        $('.leftZoomopen').fadeIn("slow");
        $(this).hide();
        $(".leftOpenBack").show();
        $('.mainBottom').css('padding-left', '20px');
        $('.shirinkBtn').css('margin-left', '0px');
        $('.stretchBtn').css('margin-left', '0px');
        $('#mapCover').css('margin-left', '0px');
    });
    $('.leftZoomopen').click(function () {
        //$('.mainLeft').css('overflow', 'inherit');
        //$('.mainLeft').animate({width:'300px'});
        $('.mainLeft').show('slide', 1000);
        $('.leftZoomclose').fadeIn("slow");
        $(this).hide();
        $('.mainBottom').css('padding-left', '300px');
        $(".leftOpenBack").hide();
        $('.shirinkBtn').css('margin-left', '150px');
        $('.stretchBtn').css('margin-left', '-100px');
        $('#mapCover').css('margin-left', '300px');
    });
    $('#operator li').hover(function () {
        $(this).addClass("commandHover2");
    }, function () {
        $(this).removeClass("commandHover2");
    });
    $('.palyBtn > span').hover(function () {
        var hovindex = $(this).index();
        $('.palyBtnHover > span').eq(hovindex).show('1000').siblings().css('display', 'none');
    }, function () {
        $('.palyBtnHover > span').hide('1000');
    });
    queryMode();
    var onlineNum = $('.ztreeOnlieNameStyle').length;
    $('.personOnline span').text(onlineNum);

    $('#oranizatStruct').on('click', function () {
        $(this).css('background-color', '#f90808').siblings().css('background-color', '#dd3131');
        $('.oranizatBox').slideDown("slow");
        $('.systemBox').hide();
        $('.talksBox').hide();
    });
    $('#systemState').on('click', function () {
        $(this).css('background-color', '#f90808').siblings().css('background-color', '#dd3131');
        $('.oranizatBox').hide();
        $('.systemBox').slideDown("slow");
        $('.talksBox').hide();
    });
    $('#talksRecord').on('click', function () {
        $(this).css('background-color', '#f90808').siblings().css('background-color', '#dd3131');
        $('.oranizatBox').hide();
        $('.systemBox').hide();
        $('.talksBox').slideDown("slow");
    });

});

function showOrgSearchInput() {

    if ($("#sSearch").is(":visible") == false) {
        $("#sSearch").show('slow');
        $("#sSearch").focus();
    }

}

function chekInClass() {
    if ($('#collapseListGroup1').hasClass('in')) {
        setTimeout(function () {
            $('.preGroups').css('position', 'inherit');
        }, 300);
    } else {
        $('#collapseListGroup2').removeClass('in');
        $('.preGroups').css('position', 'absolute');
    }
}
function chekInClass2() {
    // if(!$('#collapseListGroup2').hasClass('in')){
    //     $('#collapseListGroup1').removeClass('in');
    // }else{
    //     // $('#collapseListGroup1').addClass('in');
    // }
    // if($('#collapseListGroup2').hasClass('in')){
    //     $('#collapseListGroup1').addClass('in');
    // }else{
    //     $('#collapseListGroup1').removeClass('in');
    // }
}
//点击指挥模式时，改变整体风格
function commadeType() {
    $('#commandErrorMsg').html(" ");
    $('.commanParamBox input').removeAttr("disabled");
    $('#commandTime').val("0");
    $('#collectFreq').val("5");
    $('#commandApply').css('background-color', '#5fb1fa');
}
function viewType() {
    $('#commandApply').css('background-color', '#5fb1fa');
    $('.commanParamBox input').attr('disabled', true);
}
function closeCommand() {
    $('#commandModeDiv').hide();
}

function chanegStyleView(data, callback) {
    var type = $('input[name="type"]:checked').val();
    var commandTime = $('#commandTime').val();
    var collectFreq = $('#collectFreq').val();
    var commandEndTime = "";
    $(".commanParamBox input").keyup(function () {
        if (checkValuesChanged('#commandTime') || checkValuesChanged('#collectFreq')) {
            $('#commandApply').css('background-color', 'rgb(95, 177, 250)');
        } else {
            $('#commandApply').css('background-color', 'rgb(182, 185, 189)');
        }
    });
    if (type == 1) {
        if (commandTime == "") {
            $('#commandErrorMsg').html("指挥时间不能为空");
            return;
        }
        if (collectFreq == "") {
            $('#commandErrorMsg').html("上报频率不能为空");
            return;
        }
        if (commandTime == 0) {
            $('#commandEndTime').html("长期指挥");
            var curTime = new Date();
            var endTime = curTime.getTime() + (365 * 24 * 3600 * 60 * 1000);
            curTime = new Date(endTime);
            commandEndTime = curTime.getTime();
        } else {
            var curTime = new Date();
            var endTime = curTime.getTime() + (commandTime * 60 * 1000);
            curTime = new Date(endTime);
            commandEndTime = curTime.getTime();
            $('#commandEndTime').html((curTime.getHours() < 10 ? ("0" + curTime.getHours()) : curTime.getHours()) + ":"
                + (curTime.getMinutes() < 10 ? ("0" + curTime.getMinutes()) : curTime.getMinutes()) + "结束指挥");
        }

    } else if (type == 0) {
        $('#commandTime').val("");
        $('#collectFreq').val("");

        $('#commandEndTime').html("");
        commandEndTime = "";
    }
    changeviw(type);
    setOldValues();
    $('#commandApply').css('background-color', 'rgb(182, 185, 189)');
    var datas = {type: type, commandTime: commandTime, collectFreq: collectFreq, commandEndTime: commandEndTime};

    commandAjax(datas, function (data) {
        //成功
        if (data.status == 200) {
            if (type == 1) {
                $('#commandMod').text('模式/指挥');
            } else {
                $('#commandMod').text('模式/观察');
            }
        } else {
            //显示错误提示信息
            $('#commandErrorMsg').text(data.msg);
        }
    }, function (err) {
        alert(err);
        location.href = '/';
    });


}

function setOldValues() {
    $("#commandTime").attr("oldValue", $("#commandTime").val());
    $("#collectFreq").attr("oldValue", $("#collectFreq").val());
}

function checkValuesChanged(id) {
    return checkItemValueChanged(id, $(id).val());
}

function checkItemValueChanged(id, curValue) {
    oldValue = $(id).attr("oldValue");

    if (oldValue == null) {
        return false;
    }
    if (oldValue == curValue) {
        return false;
    } else {
        return true;
    }
    return true;
}
function changeviw(types) {
    if (types == 1) {
        $('.leftTop').css('background-color', '#fb5b0c');
        $('#operator div').css({
            'background': '#fb5b0c',
            'box-shadow': '1px 1px 2px #f11818',
            'color': '#fff'
        });
        $('#operator li').css('border-bottom', '1px solid rgba(243, 150, 84, 0.91)');
        $('#operator li').hover(function () {
            $(this).addClass("commandHover").removeClass("commandHover2");
        }, function () {
            $(this).removeClass("commandHover").removeClass("commandHover2");
        });
    } else {
        $('.leftTop').css({
            'background-color': '#5fb1fa'
        });
        $('#operator div').css({
            'background': '#fff',
            'box-shadow': '1px 2px 5px rgba(0, 0, 0, 0.27)',
            'color': 'rgb(121, 117, 117)'
        });
        $('#commandEndTime').html("");
        $('#commandErrorMsg').html(" ");

        $('#operator li').css('border-bottom', '1px solid #d2caca');
        $('#operator li').hover(function () {
            $(this).addClass("commandHover2");
        }, function () {
            $(this).removeClass("commandHover2");
        });
    }
    cahngeIcon(types);
}
function cahngeIcon(type) {
    if (type == 1) {
        $('.operIcon').css('background-image', 'url(../images/mapOpera2Icon.png)');
        $('.callIcon').css('background-image', 'url("../images/call2Icon.png")');
        $('.clearIcon').css('background-image', 'url(../images/clear2Icon.png)');
        $('.realIcon').css('background-image', 'url(../images/realTrace2Icon.png)');
        $('.replayIcon').css('background-image', 'url(../images/repalyRol2Icon.png)');
        $('.commandIcon').css('background-image', 'url(../images/command2Icon.png)');
        $('.celectIcon').css('background-image', 'url(../images/select2Icon.png)');
        $('.groupIcon').css('background-image', 'url(../images/group2Icon.png)');
        $('.searchNavIcon').css('background-image', 'url(../images/serchNav2Icon.png)');
        $('.measureIcon').css('background-image', 'url("../images/testlong2Icon.png")');
        $('.markNavIcon').css('background-image', 'url(../images/sigo2Icon.png)');
        $('.barrierNavIcon').css('background-image', 'url(../images/weilan2Icon.png)');
        $('.downIcon').css('background-image', 'url(../images/down2.png)');
        $('.tvViewIcon').css('background-image', 'url(../images/view2Icon.png)');
        $('.tvSayIcon').css('background-image', 'url(../images/say2Icon.png)');
    } else {
        $('.operIcon').css('background-image', 'url(../images/mapOperaIcon.png)');
        $('.callIcon').css('background-image', 'url("../images/callIcon.png")');
        $('.clearIcon').css('background-image', 'url(../images/clearIcon.png)');
        $('.realIcon').css('background-image', 'url(../images/realTraceIcon.png)');
        $('.replayIcon').css('background-image', 'url(../images/repalyRolIcon.png)');
        $('.commandIcon').css('background-image', 'url(../images/commandIcon.png)');
        $('.celectIcon').css('background-image', 'url(../images/selectIcon.png)');
        $('.groupIcon').css('background-image', 'url(../images/groupIcon.png)');
        $('.searchNavIcon').css('background-image', 'url(../images/serchNavIcon.png)');
        $('.measureIcon').css('background-image', 'url(../images/testlongIcon.png)');
        $('.markNavIcon').css('background-image', 'url(../images/sigoIcon.png)');
        $('.barrierNavIcon').css('background-image', 'url(../images/weilanIcon.png)');
        $('.downIcon').css('background-image', 'url(../images/down.png)');
        $('.tvViewIcon').css('background-image', 'url(../images/viewIcon.png)');
        $('.tvSayIcon').css('background-image', 'url(../images/sayIcon.png)');
    }
}
/*
 指挥模式参数的传递
 type: type, //0 是观察   1 是指挥
 commandTime: commandTime,    //指挥时长
 collectFreq: collectFreq,    //定位频率
 endTime: commandEndTime       //指挥结束时间
 */
function commandAjax(data, success, error) {
    $.ajax({
        url: '/api/enterprise/chgCmdMode',
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            type: data.type, //0 是观察   1 是指挥
            commandTime: data.commandTime,
            collectFreq: data.collectFreq,
            endTime: data.commandEndTime
        }),
        success: success,
        error: error
    });
}

/**
 *
 *
 */
function queryMode() {
    $.ajax({
        url: "/api/curEnterprise",
        dataType: "json",
        type: "get",
        success: function (jsonData) {
            if (jsonData.status == 200) {
                //  console.log(JSON.stringify(jsonData.data));

                //设置为指挥模式下之后退出了
                if (jsonData.data.mode && jsonData.data.mode.modetype == '1') {
                    if ((moment(jsonData.data.mode.endTime).valueOf() - moment().valueOf()) > 24 * 3600 * 1000) {
                        $('#commandEndTime').html("长期指挥");
                    } else {
                        $('#commandEndTime').html(moment(jsonData.data.mode.endTime).utcOffset("+08:00").format('HH:mm') + "结束指挥");
                    }
                    $("input[name=type]:eq(1)").attr("checked", "true");
                    var commandTime = $('#commandTime').val(jsonData.data.mode.commandTime);
                    var collectFreq = $('#collectFreq').val(jsonData.data.mode.collectFreq);
                    $('#commandErrorMsg').html("");
                    $('#commandMod').text("模式/指挥");
                    changeviw(1);
                } else {
                    $("input[name=type]:eq(0)").attr("checked", "true");
                    var commandTime = $('#commandTime').val('');
                    var collectFreq = $('#collectFreq').val('');
                    $('#commandEndTime').html("");
                    $('#commandErrorMsg').html("");
                    $('#commandEndTime').html("");
                    changeviw(0);
                }

            } else {
                $('#commandErrorMsg').html("获取本企业下的模式失败");
            }
        },
        error: function (jsonData) {
            alert(JSON.stringify(jsonData));
        }
    });
}

//按钮点击后下拉replay
function shrinkReplay() {
    //$('.stretchBtn').slideDown("slow");
    //$('.shirinkBtn').slideUp("slow");
    $('#rePlay').slideUp("slow");
    $('.authorize').show();
}

/**
 * 搜索树
 * @param inputID, String, input输入框id
 */
function searchInTree(inputID) {
    gisOrgTree.search($('#' + inputID).val());
}

/**
 * 在线，离线点击搜索
 */
//var onlineNum;
$('#onlineCheck').on('click', function () {
    //gisOrgTree.searchByStatus(1, onlineSet);
    showTreeData();
});
$('.allpersonBox').on('click', function () {
    gisOrgTree.search("");
});
$('#notOnlineCheck').on('click', function () {
    //gisOrgTree.searchByStatus(0, onlineSet);
    showTreeData();
});

//通过在线和离线前面的两个 checkbox 来控制过滤结果
function showTreeData() {
    var onLine = $('#onlineCheck').prop("checked");
    var notOnLine = $('#notOnlineCheck').prop("checked");
    if (!(onLine ^ notOnLine)) {
        gisOrgTree.searchByStatus(0, onlineSet);
    } else {
        if (onLine) {
            gisOrgTree.searchByStatus(1, onlineSet);
        } else {
            gisOrgTree.searchByStatus(2, onlineSet);
        }
    }
}

function grtTableWidth() {
    $('.tb_title td')[0];
}

function menuHover() {
    $('.MenuBox').hover(function () {
        $('.menuHBox').css('display', 'block');
    }, function () {
        $('.menuHBox').css('display', 'none');
    });
    $('.selectsBox').hover(function () {
        $('.selectHBox').css('display', 'block');
    }, function () {
        $('.selectHBox').css('display', 'none');
    });
    $('.mapOperBox').hover(function () {
        $('.mapOperHBox').css('display', 'block');
    }, function () {
        $('.mapOperHBox').css('display', 'none');
    });
}

function sigoMap() {
    $('.sigoTypeList').show();
    $('.sigoTypeList').hover(function () {
        $('.sigoTypeList >ul > li').hover(function () {
            var hovindex = $(this).index();
            var imgsrc = $(this).children("img").attr("src");
            $(this).click(function () {
                $('.sigoType img').attr('src', imgsrc);
                $('.sigoTypeList').hide();
            });
            $(this).css({'background-color': '#ffffff', 'opacity': '0.5'}).siblings().css('opacity', '1');
            var eTop = $(this).offset().top;
            var eRight = $(this).offset().left;
            $('.sigoTypeListH >ul >li').eq(hovindex).css({
                'top': eTop - 80 + 'px',
                'left': eRight - 380 + 'px',
                'display': 'block'
            }).siblings().css('display', 'none');
        }, function () {
            $('.sigoTypeListH >ul >li').css('display', 'none');
        });
    }, function () {
    });

}
/**
 * 显示当前时间
 * @returns {string}
 */
function getNowFormatDate() {

    setInterval(function () {
        var date = new Date();
        var seperator1 = "-";
        var seperator2 = ":";
        var month = date.getMonth() + 1;
        var strDate = date.getDate();
        var hours = date.getHours();
        var minutes = date.getMinutes();
        var seconds = date.getSeconds();
        if (month >= 1 && month <= 9) {
            month = "0" + month;
        }
        if (strDate >= 0 && strDate <= 9) {
            strDate = "0" + strDate;
        }
        if (hours >= 0 && hours < 9) {
            hours = "0" + hours;
        }
        if (minutes >= 0 && minutes < 9) {
            minutes = "0" + minutes;
        }
        if (seconds >= 0 && seconds < 9) {
            seconds = "0" + seconds;
        }
        var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
            + " " + hours + seperator2 + minutes
            + seperator2 + seconds;
        $('.timeShow').html(currentdate);
    }, 1000);
}
