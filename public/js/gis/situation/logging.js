/**
 * Created by xzkj1234 on 2017/6/1.
 */
var allSelectedStaff = "";
var selStaffIndex = 0;
var selStaffId = 0;
function loggingShow(){
    if (allSelectedStaff.length>0){
        allSelectedStaff = '';
    }
    $('.loggingBox').show();
    var treeObj = gisOrgTree.getTree();
    var node = treeObj.getCheckedNodes();
    var btnIdx = 0;

    for (var i in node) {
        if (!node[i].isParent) {
            loggingStaffButton(node[i].name, node[i]._id, btnIdx);
            btnIdx++;
        }
    }
    $('#loggingReplaySelectedStaff').html(allSelectedStaff);
        $("#commandModeDiv").show();
}
function loggingStaffButton(nodeName, id, btnIdx) {
    allSelectedStaff.length > 0 ?allSelectedStaff +=
        "&nbsp;&nbsp;<input type='button' id='btns_" + id + "' class='btn btn-default' onclick=\"rep.changeStaff('" + id + "','" + btnIdx + "')\" value='" + nodeName + "'>" :
        allSelectedStaff = "<input type='button' id='btns_" + id + "' class='btn btn-primary'  onclick=\"rep.changeStaff('" + id + "','" + btnIdx + "')\" value='" + nodeName + "'>";
}
function closeLogging(){
    $('.loggingBox').hide();
}
function hideFuncShow(){
    if($('.hideFuncBox').css("display")=="block"){
            $('.hideFuncBox').hide();
    }else if($('.hideFuncBox').css("display")=="none"){
        $('.hideFuncBox').show();
    }
}
function closeContactInfo(){
    $('.contanctInfo').hide();
}
function lookforContanctInfo(){
    $('.contanctInfo').show();
}