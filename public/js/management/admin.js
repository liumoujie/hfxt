/**
 * Created by suke on 15/12/8.
 */

var dataTablesLang = {
    "sProcessing": "处理中...",
    "sLengthMenu": "显示 _MENU_ 项结果",
    "sZeroRecords": "没有匹配结果",
    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
    "sInfoPostFix": "",
    // "allCleck":"全选",
    "sSearch": "搜索:",
    "sUrl": "",
    "sEmptyTable": "表中数据为空",
    "sLoadingRecords": "载入中...",
    "sInfoThousands": ",",
    "oPaginate": {
        "sFirst": "首页",
        "sPrevious": "上页",
        "sNext": "下页",
        "sLast": "末页"
    },
    "oAria": {
        "sSortAscending": ": 以升序排列此列",
        "sSortDescending": ": 以降序排列此列"
    }
};
var clipArea = null;
var showImport = false;

$(document).ready(function () {
    initAddUser();
    initAddGroup();
    initAddIcon();
    initAddDepartment();
    initPositions();
    initAudits();
    initLocateSet();
    initEmergencyLocateSetTree();
    initNearbyLocateSetTree();
});

function initAudits() {
    $("#stDate").val(genTodayDateTime("st"));
    $("#edDate").val(genTodayDateTime("ed"));

    var table = $('#tabAudits').DataTable({
        "language": dataTablesLang,
        "order": [[0, 'desc']],
        "pageLength": 10,
        "ajax": "/api/logs?startTime=" + getTimeByDate($("#stDate").val()) + "&endTime=" + getTimeByDate($("#edDate").val()),
        "columns": [
            {'data': "name"},
            {'data': "time"},
            {'data': "eventName"},
            {'data': "ip"}
        ]
    });

    $("#queryAuditBtn").on("click", function () {
        var stDate = new Date($("#stDate").val());
        var edDate = new Date($("#edDate").val());
        if (stDate.getTime() > edDate.getTime()){
            alert('请输入正确开始、结束时间');
        }
        table.ajax.url("/api/logs?startTime="
            + getTimeByDate($("#stDate").val())
            + "&endTime=" + getTimeByDate($("#edDate").val())
            + ($("#qryName").val() ? "&name=" + $("#qryName").val() : ""));
        table.ajax.reload();
    });
}

function genTodayDateTime(type) {
    var today = new Date();
    var ret = today.getFullYear() + "-"
        + ((today.getMonth() + 1) < 10 ? ("0" + (today.getMonth() + 1)) : (today.getMonth() + 1)) + "-"
        + (today.getDate() < 10 ? ("0" + today.getDate()) : today.getDate());
    if (type == "st") {
        ret += " 00:00";
    } else {
        ret += " 23:55";
    }
    return ret;
}

//function dialogConfirm(title, msg, cnfFun) {
//
//    BootstrapDialog.confirm({
//        title: title,
//        message: msg,
//        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
//        closable: true, // <-- Default value is false
//        draggable: true, // <-- Default value is false
//        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
//        btnOKLabel: '确定', // <-- Default value is 'OK',
//        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
//        callback: function (result) {
//            // result will be true if button was click, while it will be false if users close the dialog directly.
//            if (result) {
//                //alert('Yup.');
//                cnfFun(result);
//            } else {
//                //alert('Nope.');
//            }
//        }
//    });
//
//}

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}

function finishAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
                //opener.location.reload();
            }
        }]
    });
}

//管理职位相关的代码
function initPositions() {
    positionImg();
    var table = $('#tabPositions').DataTable({
        "language": dataTablesLang,
        "order": [[0, 'desc']],
        "pageLength": 10,
        "ajax": "/api/position",
        "columns": [
            {'data': "name"},
            {'data': "description"},
            {'data': "name"}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $('td:eq(2)', nRow).html(
                "<a class=\"btn btn-link btn-sm positionTableLink posEdit\" role=\"button\">编辑</a>" +
                "<a class=\"btn btn-link btn-sm positionTableLink posRemove\" role=\"button\">删除</a>"
            );
            return nRow;
        }
    });

    //当前指向的组
    var curGroup = null;

    //"操作"链接点击的处理
    table.on('draw', function () {
        $('.positionTableLink').unbind('click').on('click', function (event) {
            event.preventDefault();

            //本行对应的数据
            var data = table.row($(this).parents('tr')).data();
            curGroup = data;
            if ($(this).hasClass('posEdit')) {
                positionEdit(data);
            }
            else if ($(this).hasClass('posRemove')) {
                positionRemove(data);
            }
        });
    });

    //编辑群
    function positionEdit(data) {
        $('#positionSaveMode').val(data._id);
        $('#positionName').val(data.name);
        // $('#positionIcon').val(data.icon);
        $('#positionDescription').val(data.description);
        $('#btnPositionSave').text("保存");
        $('#btnPositionSave').prop('disabled', false);

        $('#view').html('<img id="positionHeadImg" onclick="chooseImg()" src="'
            + data.icon + '" width="100px" height = "100px" >');
        $('#file').val("");
        $('#clipArea').html('');
        $('#clipAreaTr').hide();
        $('#buttonTr').hide();
        $('#positionBtn').show();

        //弹出对话框
        $('#dlgAddPosition').modal({
            backdrop: 'static'
        });

    }

    //删除群
    function positionRemove(data) {

        dialogConfirm("请确认", "确定要删除本条数据？", function (result) {
            if (result) {
                $.ajax({
                    url: '/api/positionDelete',
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        position: data._id,
                    }),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {
                            //重新加载table数据.
                            table.ajax.reload();
                        } else {
                            //显示错误提示信息
                            dialogAlert("操作结果", data.message, "确定");
                        }
                    },
                    error: function (err) {
                        alert(JSON.stringify(err));
                        //location.href = '/';
                    }
                });
            }
        });
    }

    //添加职位
    $('#btnAddPosition').on('click', function (event) {
        event.preventDefault();

        $('#dlgPositionFormInputInvalid').text("");
        $('#dlgPositionFormInputInvalid').addClass('hidden');
        $('#positionSaveMode').val("");
        $('#positionName').val("");
        $('#positionIcon').val("");
        $('#positionDescription').val("");
        $('#btnPositionSave').text("保存");
        $('#btnPositionSave').prop('disabled', false);

        //$('#positionHeadImg').attr("src", "http://localhost:3000/choose.png");
        $('#view').html('<img id="positionHeadImg" onclick="chooseImg()" src="/choose.png" ' +
            'width="100px" height = "100px" >');
        $('#clipArea').html('');
        $('#file').val("");
        $('#clipAreaTr').hide();
        $('#buttonTr').hide();
        $('#positionBtn').hide();

        //弹出对话框
        $('#dlgAddPosition').modal({
            backdrop: 'static'
        });
    });

    //对话框点击保存
    $('#btnPositionSave').on('click', function (event) {
            event.preventDefault();

            //有效性验证
            var icon = $('#positionIcon').val();
            var name = $('#positionName').val();
            var desc = $('#positionDescription').val();
            //输入合法
            if (name && icon && desc) {

                $('#btnPositionSave').text("数据保存中...");
                $('#btnPositionSave').prop('disabled', true);

                var url = '';
                var dataJson = null;
                if ($('#positionSaveMode').val().length == 0) {
                    url = '/api/position';
                    dataJson = {
                        icon: icon,
                        name: name,
                        description: desc
                    }
                } else {
                    url = '/api/positionUpd';
                    dataJson = {
                        _id: $('#positionSaveMode').val(),
                        icon: icon,
                        name: name,
                        description: desc
                    }
                }
                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(dataJson),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {

                            $('#dlgAddPosition').modal('hide');
                            table.ajax.reload();
                        } else {
                            //显示错误提示信息
                            $('#dlgPositionFormInputInvalid').text(data.message);
                            $('#dlgPositionFormInputInvalid').removeClass('hidden');

                            $('#btnPositionSave').text("保存");
                            $('#btnPositionSave').prop('disabled', false);
                        }
                    },
                    error: function (err) {
                        location.href = '/';
                    }
                });
            }
            else {
                //输入不合法
                if (!name) {
                    $('#dlgPositionFormInputInvalid').text("职位名称不能为空");
                }
                else if (!icon) {
                    $('#dlgPositionFormInputInvalid').text("职位头像不能为空");
                }
                else if (!desc) {
                    $('#dlgPositionFormInputInvalid').text("职位描述不能为空");
                }
                $('#dlgPositionFormInputInvalid').removeClass('hidden');
            }
        }
    );
}

//添加用户管理功能相关的初始化
function initAddUser() {

    var xhr = null;

    var table = $('#tabUsers').DataTable({
        "language": dataTablesLang,
        "order": [[0, 'desc']],
        "ajax": "/api/contact/user",
        "columns": [
            {'data': "idNumber"},
            {'data': "name"},
            {'data': "phoneNbr"},
            // {'data': "mail"},
            //{'data': "father"},
            {'data': "privileges"},
            {'data': null}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            //TODO:需要显示得更好一些: like: 接收/呼叫

            //email
            // $('td:eq(3)',nRow).html(((aData.email != 'undefined') ? aData.email: ""));
            //部门
            //$('td:eq(4)',nRow).html((aData.father != 'undefined' ? aData.father: ""));
            $('td:eq(3)', nRow).html(
                (aData.privileges.callAble ? "呼叫/" : "")
                + (aData.privileges.calledAble ? "被呼/" : "")
                + (aData.privileges.groupAble ? "建组/" : "")
                + (aData.privileges.joinAble ? "进组/" : "")
                + (aData.privileges.callOuterAble ? "外呼/" : "")
                + (aData.privileges.calledOuterAble ? "被外呼/" : "")
                + (aData.privileges.forbidSpeak ? "禁言/" : "")
                + (aData.privileges.powerInviteAble ? "强拉/" : "")
                + (aData.privileges.viewMap ? "查看周围的人/" : "")
            );
            $('td:eq(4)', nRow).html(
                "<a class=\"btn btn-link btn-sm userTableLink cUserEdit\" id='editUser' role=\"button\" href='#'>编辑</a>" +
                "<a class=\"btn btn-link btn-sm " + (aData.status == 1 ? "disabled" : "") + " userTableLink dUserMove\" id='disableUser' role=\"button\" href='#'>停用</a>" +
                "<a class=\"btn btn-link btn-sm " + (aData.status == 1 ? "" : "disabled") + " userTableLink enableUser\" id='enableUser' role=\"button\" href='#'>启用</a>"
            );
            return nRow;
        }
    });

    table.on('draw', function (event) {

        $('.userTableLink').unbind('click').on('click', function (event) {
            event.preventDefault();

            //本行对应的数据
            var data = table.row($(this).parents('tr')).data();
            curGroup = data;
            if ($(this).hasClass('cUserEdit')) {
                userEdit(data);
            }
            else if ($(this).hasClass('dUserMove')) {
                userRemove(data, 1);
            } else if ($(this).hasClass('enableUser')) {
                userRemove(data, 0);
            }
        });

    });

    function userEdit(data) {
        //重置数据
        getDepartments("departments", departments, data.father);
        getPositions("positions", positions, data.position);
        $('#dlgFormInputValid').addClass('hidden');
        $('#btnUserSave').text("保存");
        $('#btnUserSave').prop('disabled', false);
        $('#userSaveMode').val(data.idNumber);
        $('#dlgUserForm')[0].reset();

        $('#dlgAddUserTitle').text("编辑用户");

        //$('#userNumber').val(data.idNumber);
        $('#userName').val(data.name);
        $('#pwd').val('********');
        $('#repwd').val('********');
        $('#phoneNbr').val(data.phoneNbr);
        $('#priority').val(data.privileges.priority);
        $('#email').val(data.mail);
        $('#emergencyUsers').val(data.emergencyPerson);
        $('#nearbyUsers').val(data.nearbyPerson);

        $('#privCall').attr('checked', data.privileges.callAble);
        $('#privGroup').prop('checked', data.privileges.groupAble);
        $('#privRecvCall').prop('checked', data.privileges.calledAble);
        $('#privRecvGroup').prop('checked', data.privileges.joinAble);
        $('#forbidSpeak').prop('checked', data.privileges.forbidSpeak);
        $('#callOuter').prop('checked', data.privileges.callOuterAble);
        $('#recvCallOuter').attr('checked', data.privileges.calledOuterAble);
        $('#powerInviteAble').attr('checked', data.privileges.powerInviteAble);
        $('#muteAble').attr('checked', data.privileges.muteAble);
        $('#viewMap').attr('checked', data.privileges.viewMap);
        $('#emergency').attr('checked', data.privileges.emergency);
        $('#boardcast').attr('checked', data.privileges.boardcast);
        $('#noDevice').attr('checked', data.privileges.noDevice);
        $('#groupBoardcast').attr('checked', data.privileges.groupBoardcast);

        if (data.devices && data.devices.length) {
            for (var i = 0; i < data.devices.length; i++) {
                $('#device' + (i + 1)).val(data.devices[i]);
            }
        }

        noDevices();

        //弹出对话框
        $('#dlgAddUser').modal({
            backdrop: 'static'
        });
    }

    function userRemove(data, status) {

        var confirmMsg = status == 1 ? "确定要停用本用户？停用后，此用户无法再继续使用对讲服务。" : "确定要启用本用户";
        //本行对应的数据
        dialogConfirm("请确认", confirmMsg, function (result) {
            if (result) {
                $.ajax({
                    url: '/api/contact/enableDisableUser',
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        idNumber: data.idNumber,
                        set_status: status
                    }),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {
                            //重新加载table数据.
                            table.ajax.reload();
                        } else {
                            //显示错误提示信息
                            dialogAlert("操作结果", data.message, "确定");
                        }
                    },
                    error: function (err) {
                        location.href = '/';
                    }
                });
            }
        });
    }

    //导入用户
    $('#importUsers').on('click', function (event) {
        event.preventDefault();

        if (!showImport) {
            $('#impFile').show();
            showImport = true;
        } else {
            $('#impFile').hide();
            showImport = false;
        }

    });

    //导入用户按钮响应
    $('#userFile').fileinput({
        language: 'zh',
        uploadUrl: '/imp/importUsers',
        uploadAsync: false
    });

    $('#userFile').on('filebatchuploadsuccess', function (event, data, prev) {
        if (data.response.status == '200') {
            dialogAlert("操作结果", data.response.message, "确定");
            $('#impFile').hide();
            showImport = false;
            //重新加载table数据.
            table.ajax.reload();
        } else {
            dialogAlert("操作结果", data.response.message, "确定");
            $('#impFile').hide();
            showImport = false;
        }
    });

    //修改企业视频清晰度
    $('#changeVdeioMode').on('click', function (event) {

        $('#dlgVedioFormInputValid').text("");
        $('#dlgVedioFormInputValid').addClass('hidden');

        $.get('/api/enterprise/' + $('#ent_idNumber').text(), function (data) {
            if (data && data.status == 200) {
                if (data.data.vedioMode) {
                    $('#vedioMode').val(data.data.vedioMode);
                } else {
                    $('#vedioMode').val('NORMAL');
                }

                //弹出对话框
                $('#dlgVedio').modal({
                    backdrop: 'static'
                });
            }
        });

    });

    //重置企业视频清晰度
    $('#btnVedioSave').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var vedioMode = $('#vedioMode').val();
        //输入合法
        if (vedioMode) {

            $('#btnExpSave').text("数据保存中...");
            $('#btnExpSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/chgVedioMode',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    vedioMode: vedioMode
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgVedio').modal('hide');
                        dialogAlert("操作结果", "修改企业视频分辨率成功", "确定");

                        $('#btnVedioSave').text("保存");
                        $('#btnVedioSave').prop('disabled', false);
                        table.ajax.reload();

                    } else {
                        //显示错误提示信息
                        $('#dlgVedioFormInputValid').text(data.message);
                        $('#dlgVedioFormInputValid').removeClass('hidden');

                        $('#btnVedioSave').text("保存");
                        $('#btnVedioSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            $('#dlgVedioFormInputValid').text("密钥不能为空");
            $('#dlgVedioFormInputValid').removeClass('hidden');
        }
    });


    //修改企业密钥
    $('#changeKey').on('click', function (event) {

        $('#dlgKeyFormInputValid').text("");
        $('#dlgKeyFormInputValid').addClass('hidden');

        $.get('/api/enterprise/' + $('#ent_idNumber').text(), function (data) {
            if (data && data.status == 200) {
                $('#newKey').val(data.data.key);

                //弹出对话框
                $('#dlgKey').modal({
                    backdrop: 'static'
                });
            }
        });

    });

    //重置密钥
    $('#btnKeySave').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var exp = $('#newKey').val();

        //输入合法
        if (exp) {

            $('#btnExpSave').text("数据保存中...");
            $('#btnExpSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/chgKey',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    key: exp
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgKey').modal('hide');
                        dialogAlert("操作结果", "修改企业密钥成功", "确定");

                        $('#btnKeySave').text("保存");
                        $('#btnKeySave').prop('disabled', false);
                        table.ajax.reload();

                    } else {
                        //显示错误提示信息
                        $('#dlgKeyFormInputValid').text(data.message);
                        $('#dlgKeyFormInputValid').removeClass('hidden');

                        $('#btnKeySave').text("保存");
                        $('#btnKeySave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            $('#dlgKeyFormInputValid').text("密钥不能为空");

            $('#dlgKeyFormInputValid').removeClass('hidden');
        }
    });


    //修改企业名称
    $('#changeEntName').on('click', function (event) {

        $('#dlgChangeEntNameInputInvalid').text("");
        $('#dlgChangeEntNameInputInvalid').addClass('hidden');

        $.get('/api/enterprise/' + $('#ent_idNumber').text(), function (data) {
            if (data && data.status == 200) {
                $('#enterpriseName').val(data.data.name);

                //弹出对话框
                $('#dlgChangeEntName').modal({
                    backdrop: 'static'
                });
            }
        });
    });

    //修改企业名称保存按钮
    $('#btnChangEntName').on('click', function (event) {
        event.preventDefault();
        //有效性验证
        var enterpriseName = $('#enterpriseName').val();
        //输入合法
        if (!enterpriseName) {
            $('#dlgChangeEntNameInputInvalid').text("企业名称不能为空");
            $('#dlgChangeEntNameInputInvalid').removeClass('hidden');
        } else if (enterpriseName.length > 24) {
            $('#dlgChangeEntNameInputInvalid').text("企业名称不能大于24个字符，请重新输入");
            $('#dlgChangeEntNameInputInvalid').removeClass('hidden');
        }
        else {

            $('#btnUserSave').text("数据保存中...");
            $('#btnUserSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/chgEntName',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    name: enterpriseName
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgChangeEntName').modal('hide');
                        dialogAlert("操作结果", "修改企业名称成功", "确定");
                        $('#td_entName').text(enterpriseName);
                        $('#t_entName').text(enterpriseName);

                    } else {
                        //显示错误提示信息
                        $('#dlgChangePwdInputInvalid').text(data.message);
                        $('#dlgChangePwdInputInvalid').removeClass('hidden');

                        $('#btnUserSave').text("保存");
                        $('#btnUserSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        }
    });

    //修改密码
    $('#changePwd').on('click', function (event) {
        //弹出对话框
        $('#dlgChangePwd').modal({
            backdrop: 'static'
        });
        $('#dlgChangePwdInputInvalid').addClass('hidden');
        $('#dlgChangePwdInputInvalid').text('');
    });

    //修改密码保存按钮
    $('#btnChangePwd').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var curPassword = $('#curPassword').val();
        var chgNewPassword = $('#chgNewPassword').val();
        var repChgNewPassword = $('#repChgNewPassword').val();

        //输入合法
        if (curPassword === chgNewPassword) {
            $('#dlgChangePwdInputInvalid').text("当前密码与原密码相同，请重新输入");
            $('#dlgChangePwdInputInvalid').removeClass('hidden');
        } else if (repChgNewPassword === chgNewPassword && repChgNewPassword.length < 6 || repChgNewPassword.length > 18) {
            $('#dlgChangePwdInputInvalid').text("密码必须大于6个字符小于18个字符");
            $('#dlgChangePwdInputInvalid').removeClass('hidden');
        }
        else if (curPassword && chgNewPassword && repChgNewPassword && chgNewPassword === repChgNewPassword) {
            $('#btnUserSave').text("数据保存中...");
            $('#btnUserSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/chgpwd',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    curPassword: curPassword,
                    chgNewPassword: chgNewPassword,
                    repChgNewPassword: repChgNewPassword
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgChangePwd').modal('hide');
                        $('#curPassword').val('');
                        $('#chgNewPassword').val('');
                        $('#repChgNewPassword').val('');
                        //BootstrapDialog.alert("密码修改成功");
                        dialogAlert("操作结果", "密码修改成功", "确定");

                    } else {
                        //显示错误提示信息
                        $('#dlgChangePwdInputInvalid').text(data.message);
                        $('#dlgChangePwdInputInvalid').removeClass('hidden');

                        $('#btnUserSave').text("保存");
                        $('#btnUserSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            if (!curPassword) {
                $('#dlgChangePwdInputInvalid').text("当前密码不能为空");
            } else if (!chgNewPassword || !repChgNewPassword) {
                $('#dlgChangePwdInputInvalid').text("密码不能为空");
            } else if (chgNewPassword !== repChgNewPassword) {
                $('#dlgChangePwdInputInvalid').text("两次输入的密码需要一致");
            }

            $('#dlgChangePwdInputInvalid').removeClass('hidden');
        }
    });

    //添加用户
    $('#addUser').on('click', function (event) {
        event.preventDefault();

        //重置数据

        $('#privCall').attr('checked', true);
        $('#privGroup').prop('checked', true);
        $('#privRecvCall').prop('checked', true);
        $('#privRecvGroup').prop('checked', true);
        $('#forbidSpeak').prop('checked', false);
        $('#callOuter').prop('checked', false);
        $('#recvCallOuter').attr('checked', false);
        $('#powerInviteAble').attr('checked', false);
        $('#muteAble').attr('checked', false);
        $('#viewMap').attr('checked', false);
        $('#emergency').attr('checked', false);
        $('#boardcast').attr('checked', false);
        $('#boardcast').attr('checked', false);

        for (var i = 1; i < 6; i++) {
            $('#device' + i).val('');
        }

        $('#dlgAddUserTitle').text("添加用户");

        $('#dlgFormInputValid').addClass('hidden');
        $('#btnUserSave').text("保存");
        $('#btnUserSave').prop('disabled', true);
        $('#userSaveMode').val('');
        $('#dlgUserForm')[0].reset();

        //弹出对话框
        $('#dlgAddUser').modal({
            backdrop: 'static'
        });

        $('#btnUserSave').prop('disabled', false);

        getDepartments("departments", departments);
        getPositions("positions", positions);

    });
    /**
     * 列出所有的部门,并列生成select,出错信息输出在tipId上
     * @param selectId: select控件ID
     * @param tipId:提示信息控制ID
     * @param departmentId:默认选中的属性值
     */
    function getDepartments(selectId, tipId, departmentId) {
        $.ajax({
            url: "/api/contact/queryDepartments",
            dataType: "json",
            //data: {father: dialog.dialog("option","treeNode")._id, name: $("#departmentName").val()},
            type: "get",
            filter: function (data, type) {
                //alert(JSON.stringify(data));
                location.href = '/';
            },
            success: function (jsonData) {
                //alert(JSON.stringify(jsonData));
                departments2Select(selectId, jsonData.data, departmentId);
            },
            error: function (jsonData) {
                //alert(JSON.stringify(jsonData));
                location.href = '/';
            }
        });
    }

    /**
     *
     * @param selectId,select控件ID
     * @param departments:获取的结果
     * @param departmentId:默认选中的属性值
     */

    function departments2Select(selectId, departments, departmentId) {
        if (departmentId == null) {
            departmentId = '';
        }
        $("#" + selectId).empty();
        for (i = 0; i < departments.length; ++i) {
            if (departmentId == departments[i]._id) {
                $("#" + selectId).append("<option departmentId='" + departments[i]._id + "' selected='selectedb' value='" + departments[i].name + "'> " + departments[i].name + " </option>");
            }
            else {
                $("#" + selectId).append("<option departmentId='" + departments[i]._id + "' value='" + departments[i].name + "'> " + departments[i].name + " </option>");
            }
        }
    }

    /**
     * 列出当前企业下的所有职位,并列生成select,出错信息输出在编辑控件上
     * @param selectId: select控件ID
     * @param tipId:提示信息控制ID
     * @param positionId:默认选中的属性值
     */
    function getPositions(selectId, tipId, positionId) {
        $.ajax({
            url: "/api/position",
            dataType: "json",
            //data: {father: dialog.dialog("option","treeNode")._id, name: $("#departmentName").val()},
            type: "get",
            success: function (jsonData) {
                positions2Select(selectId, jsonData.data, positionId);
            },
            error: function (jsonData) {
                //alert(JSON.stringify(jsonData));
                location.href = '/';
            }
        });
    }

    /**
     *
     * @param selectId,select控件ID
     * @param positions:获取的结果
     * @param positionId:默认选中的属性值
     */

    function positions2Select(selectId, positions, positionId) {
        if (positionId == null) {
            positionId = '';
        }
        $("#" + selectId).empty();
        for (i = 0; i < positions.length; ++i) {
            if (positionId == positions[i]._id) {
                $("#" + selectId).append("<option positionId='" + positions[i]._id + "' selected='selectedb' value='" + positions[i].name + "'> " + positions[i].name + " </option>");
            }
            else {
                $("#" + selectId).append("<option positionId='" + positions[i]._id + "' value='" + positions[i].name + "'> " + positions[i].name + " </option>");
            }
        }
    }

    //当添加用户对话框关闭时.
    $('#dlgAddUser').on('hidden.bs.modal', function () {
        if (xhr) {
            xhr.abort();
            xhr = null;
        }
    });

//对话框点击保存
    $('#btnUserSave').on('click', function (event) {
        event.preventDefault();
        //有效性验证
        var userName = $('#userName').val();
        var pwd = $('#pwd').val();
        var repwd = $('#repwd').val();
        var phoneNbr = $('#phoneNbr').val();
        var priority = $('#priority').val();
        var devices = [];
        var phoneNumReg = /(^[0-9]{3,4}\-{0,1}[0-9]{3,8}$)|(^[0-9]{3,8}$)|(^\([0-9]{3,4}\)[0-9]{3,8}$)|(^0{0,1}13[0-9]{9}$)/
        for (var i = 1; i < 6; i++) {
            if ($('#device' + i).val()) {
                devices.push($('#device' + i).val());
            }
        }

        var userHeadImg = $('#userHeadImg').val();
        //输入合法
        if (!userName) {
            $('#dlgFormInputValid').text("用户名不能为空");
            $('#dlgFormInputValid').removeClass('hidden');
        }else if(!phoneNumReg.test(phoneNbr)) {
            $('#dlgFormInputValid').text("请填写正确电话号码!");
            $('#dlgFormInputValid').removeClass('hidden');
        }
        else if (userName.length > 24) {
            $('#dlgFormInputValid').text("用户名不能大于24个字符");
            $('#dlgFormInputValid').removeClass('hidden');
        }
        else if (!pwd || !repwd) {
            $('#dlgFormInputValid').text("密码不能为空");
            $('#dlgFormInputValid').removeClass('hidden');
        }
        else if (pwd.length < 6 || pwd.length > 18) {
            $('#dlgFormInputValid').text("密码必须大于6个字符小于18个字符");
            $('#dlgFormInputValid').removeClass('hidden');
        }
        else if (pwd !== repwd) {
            $('#dlgFormInputValid').text("两次输入的密码需要一致");
            $('#dlgFormInputValid').removeClass('hidden');
        }
        //else if(!userHeadImg){
        //    $('#dlgFormInputValid').text("用户名头像不能为空");
        //    $('#dlgFormInputValid').removeClass('hidden');
        //
        //}
        else {

            $('#btnUserSave').text("数据保存中...");
            $('#btnUserSave').prop('disabled', true);

            var url = $('#userSaveMode').val().length > 0 ? '/api/contact/updUser' : '/api/contact/user';
            var emergencyDatas = [];
            if ($('#emergencyUsers').val().length > 0) {
                emergencyDatas = $('#emergencyUsers').val().split(",");
            }
            var nearbyDatas = [];
            if ($('#nearbyUsers').val().length > 0) {
                nearbyDatas = $('#nearbyUsers').val().split(",");
            }
            $.ajax({
                url: url,
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    name: userName,
                    father: $("#departments").find(" option:selected").attr("departmentId"),//提交部门参数
                    position: $("#positions").find(" option:selected").attr("positionId"),//提交部门参数
                    password: pwd,
                    mail: $('#email').val(),
                    idNumber: $('#userSaveMode').val(),
                    phoneNbr: phoneNbr,
                    devices: devices,
                    privileges: {
                        callAble: $('#privCall').prop('checked'),
                        groupAble: $('#privGroup').prop('checked'),
                        calledAble: $('#privRecvCall').prop('checked'),
                        joinAble: $('#privRecvGroup').prop('checked'),
                        forbidSpeak: $('#forbidSpeak').prop('checked'),
                        callOuterAble: $('#callOuter').prop('checked'),
                        calledOuterAble: $('#recvCallOuter').prop('checked'),
                        powerInviteAble: $('#powerInviteAble').prop('checked'),
                        muteAble: $('#muteAble').prop('checked'),
                        viewMap: $('#viewMap').prop('checked'),
                        emergency: $('#emergency').prop('checked'),
                        boardcast: $('#boardcast').prop('checked'),
                        groupBoardcast: $('#groupBoardcast').prop('checked'),
                        noDevice: $('#noDevice').prop('checked'),
                        priority: priority,
                    },
                    emergencyPerson: emergencyDatas,
                    nearbyPerson: nearbyDatas,
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgAddUser').modal('hide');

                        //重新加载table数据.
                        table.ajax.reload();
                    } else {
                        //显示错误提示信息
                        $('#dlgFormInputValid').text(data.message);
                        $('#dlgFormInputValid').removeClass('hidden');

                        $('#btnUserSave').text("保存");
                        $('#btnUserSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        }


    });
}

//添加图片管理功能相关的初始化 张帅2017/7/7
function initAddIcon() {


    var xhr = null;
    //初始化
    var table = $('#tabIcons').DataTable({
        "language": dataTablesLang,
        "order": [[0, 'desc']],
        "ajax": "/api/contact/picture",
        "columns": [
            {'data': "positionName"},
            {'data': "enterprise"},
            {'data': "dfsPath"}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            // alert("<img src='http://192.168.8.7:19999/" + aData.dfsPath + "' />");
            $('td:eq(1)', nRow).html(
                "<img src='/api/imgproxy?fpath=" + aData.dfsPath + "' width='35px'/>"
            );
            $('td:eq(2)', nRow).html(
                "<a class=\"btn btn-link btn-sm pictureTableLink pictEdit\" role=\"button\">编辑</a>" +
                "<a class=\"btn btn-link btn-sm pictureTableLink pictRemove\" role=\"button\" >删除</a>"
            );
            return nRow;
        }
    });

    table.on('draw', function (event) {

        $('.pictureTableLink').unbind('click').on('click', function (event) {
            event.preventDefault();

            //本行对应的数据
            var data = table.row($(this).parents('tr')).data();
            curGroup = data;
            if ($(this).hasClass('pictEdit')) {
                pictEdit(data);
            }
            else if ($(this).hasClass('pictRemove')) {
                pictRemove(data, 1);
            }
        });

    });

    //添加图片

    $('#addIcon').on('click', function (event) {

        event.preventDefault();
        $('#dlgPictureFormInputInvalid').text("");
        $('#dlgPictureFormInputInvalid').addClass('hidden');
        $('#pictureSaveMode').val("");
        $('#pictureIcon').val("");
        $('#pictureName').val("");
        $('#btnPicture').text("保存");
        $('#btnPictureSave').prop('disabled', false);//
        $('#view1').html('<img id="pictureHeadImg" onclick="chooseImg1()" src="/choose.png" ' +
            'width="100px" height = "100px" >');
        $('#clipAreaTr1').hide();
        $('#file1').val("");
        $('#buttonTr1').hide();

        //弹出对话框
        $('#dlgAddPicture').modal({
            backdrop: 'static'
        });
    });
    
    //对话框点击保存

    $('#btnPictureSave').on('click', function (event) {
            //有效性验证
            event.preventDefault();
            var pictureName=$('#pictureName').val();
            var enterprise =$('#enterprise1').val();
            var imgData    =document.getElementsByName("imgData")[0].value;
            var dfspath    =$('#pictureIcon').val();
            var url='';
            var dataJson=null;

        if(dfspath.length == 0){
            dataJson={
                pictureName:pictureName,
                enterprise:enterprise,
                imgData:imgData
            };
            url="/api/picture";
        }else{
            dataJson={
                pictureName:pictureName,
                enterprise:enterprise,
                imgData:imgData,
                dfspath:dfspath
            };
            url="/api/pictureUpd";
        }
        //document.getElementsByName("pictureName")[0].value = $('#pictureName').val();
        $.ajax({
            dataType: 'json',
            type: 'post',
            contentType: "application/json; charset=utf-8",
            url: url,//"/api/picture",JSON.stringify(dataJson)
            data: JSON.stringify(dataJson),//new FormData($('#uploadImg')[0]),
            success: function (data) {
                //成功
                if (data.status == 200) {

                    $('#dlgAddPicture').modal('hide');
                    table.ajax.reload();
                } else {
                    //显示错误提示信息
                    $('#dlgPictureFormInputInvalid').text(data.message);
                    $('#dlgPictureFormInputInvalid').removeClass('hidden');

                    $('#btnPictureSave').text("保存");
                    $('#btnPictureSave').prop('disabled', false);
                }
            },
            error: function (err) {
                location.href = '/';
            }
        });

        });

    //编辑图片
    function pictEdit(data) {

        $('#pictureSaveMode').val(data._id);
        $('#pictureIcon').val(data.dfsPath);
        $('#pictureName').val(data.positionName);
        $('#enterprise').val(data.enterprise);
        $('#btnPicture').text("保存");
        $('#btnPictureSave').prop('disabled', false);//"<img id='pictureHeadImg' onclick='chooseImg1()' src='http://192.168.8.7:19999/" + data.dfsPath + "' width='100px' height = '100px'/>"
        $('#view1').html("<img id='pictureHeadImg' onclick='chooseImg1()' src='http://192.168.8.7:19999/" + data.dfsPath + "' width='100px' height = '100px'/>");//'<img id="pictureHeadImg" onclick="chooseImg1()" src="http://192.168.8.7:19999/"+data.dfsPath  width="100px" height = "100px" >'
        $('#clipAreaTr1').hide();
        $('#file1').val("");
        $('#buttonTr1').hide();

        //弹出对话框
        $('#dlgAddPicture').modal({
            backdrop: 'static'
        });

    }

    //删除图片
    function pictRemove(data) {

        dialogConfirm("请确认", "确定要删除本条数据？", function (result) {
            if (result) {
                $.ajax({
                    url: '/api/pictureremove',
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        dfsPath: data.dfsPath,
                    }),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {
                            //重新加载table数据.
                            table.ajax.reload();
                        } else {
                            //显示错误提示信息
                            dialogAlert("操作结果", data.message, "确定");
                        }
                    },
                    error: function (err) {
                        alert(JSON.stringify(err));
                        //location.href = '/';
                    }
                });
            }
        });
    }
}

//添加图片管理功能相关的初始化结束 张帅2017/7/7
//添加组相关的代码
function initAddGroup() {

    //全局的变量,用于分页显示
    var selected = [];
    var cancel = [];

    var memberTable = null;
    var saveFirstClk = false;
    $('#btnGroupMemberSave').css({color: "white"});
    $('#btnGroupMemberSave').css({background: "#265a88"});
    $('#dlgGroupMemberInputInvalid').css({color: "#8a6d3b"});
    var table = $('#tabGroups').DataTable({
        "language": dataTablesLang,
        "order": [[0, 'desc']],
        "pageLength": 10,
        "ajax": "/api/contact/group",
        "columns": [
            {'data': "idNumber"},
            {'data': "name"},
            {'data': "members"},
            {'data': "description"},
            {'data': "idNumber"}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $('td:eq(4)', nRow).html(
                "<a class=\"btn btn-link btn-sm groupTableLink cEdit\" role=\"button\">编辑</a>" +
                "<a class=\"btn btn-link btn-sm groupTableLink cMember\" role=\"button\">成员</a>" +
                "<a class=\"btn btn-link btn-sm groupTableLink cRemove\" role=\"button\">删除</a>"
            );
            return nRow;
        }
    });

    //当前指向的组
    var curGroup = null;

    //"操作"链接点击的处理
    table.on('draw', function () {
        $('.groupTableLink').unbind('click').on('click', function (event) {
            event.preventDefault();

            //本行对应的数据
            var data = table.row($(this).parents('tr')).data();
            curGroup = data;
            if ($(this).hasClass('cEdit')) {
                groupEdit(data);
            } else if ($(this).hasClass('cMember')) {
                groupMember(data);
            }
            else if ($(this).hasClass('cRemove')) {
                groupRemove(data);
            }
        });
    });

    //组成员对话框点击保存
    $('#btnGroupMemberSave').on('click', function (event) {
        event.preventDefault();
        if (!saveFirstClk) {
            $('#dlgGroupMemberInputInvalid').text("本次保存将影响群组内的人员变更，点击继续保存");
            $('#dlgGroupMemberInputInvalid').css({background: "white"});
            $('#dlgGroupMemberInputInvalid').css({color: "red"});
            $('#btnGroupMemberSave').css({background: "white"});
            $('#btnGroupMemberSave').css({color: "red"});
            $('#dlgGroupMemberInputInvalid').removeClass('hidden');

            saveFirstClk = true;
            return false;
        }

        if (selected.length > curGroup.quota.preGroup) {
            $('#dlgGroupMemberInputInvalid').text('成员超过最大限制:' + curGroup.quota.preGroup);
            $('#dlgGroupMemberInputInvalid').removeClass('hidden');
            return;
        }

        $('#btnGroupMemberSave').text("数据保存中...");
        $('#btnGroupMemberSave').prop('disabled', true);

        $.ajax({
            url: '/api/contact/member/' + curGroup.idNumber,
            dataType: 'json',
            type: 'put',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                members: selected,
                cancels: cancel
            }),
            error: function () {
                $('#dlgGroupMemberInputInvalid').text('保存失败,请检查网络或者联系管理员.');
                $('#dlgGroupMemberInputInvalid').removeClass('hidden');
                $('#btnGroupMemberSave').text("保存");
                $('#btnGroupMemberSave').prop('disabled', false);
            },
            success: function (data) {
                //成功
                if (data.status == 200) {
                    $('#dlgGroupMember').modal('hide');
                    //重新加载table数据.
                    table.ajax.reload(null, false);
                } else {
                    //显示错误提示信息
                    $('#dlgGroupMemberInputInvalid').text(data.message);
                    $('#dlgGroupMemberInputInvalid').removeClass('hidden');

                    $('#btnGroupMemberSave').text("保存");
                    $('#btnGroupMemberSave').prop('disabled', false);
                }
            }
        });
    });


    function groupMember(data) {
        selected = [];
        cancel = [];
        //重新弹出人员选择前，先设置第一次保存为false
        saveFirstClk = false;
        //将颜色复位
        $('#btnGroupMemberSave').css({background: "#265a88"});
        $('#btnGroupMemberSave').css({color: "white"});
        $('#dlgGroupMemberInputInvalid').css({color: "#8a6d3b"});
        var mbrDataTablesLang = {
            "sProcessing": "处理中...",
            "sLengthMenu": "显示 _MENU_ 项结果",
            "sZeroRecords": "没有匹配结果",
            "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
            "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
            "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
            "sInfoPostFix": "",
            "sSearch": "搜索:",
            "sUrl": "",
            "sEmptyTable": "表中数据为空",
            "sLoadingRecords": "载入中...",
            "sInfoThousands": ",",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "上页",
                "sNext": "下页",
                "sLast": "末页"
            }
        };

        if (memberTable == null) {
            memberTable = $('#tabGroupMember').DataTable({
                "language": mbrDataTablesLang,
                "order": [[2, 'desc']],
                "pageLength": 10,
                "ajax": '/api/contact/member/' + data.idNumber,
                "columns": [
                    {'data': "name"},
                    {'data': "phoneNbr"},
                    {'data': "isMember"}
                ],

               // 添加全选按钮
            // initComplete:function(){
            //     $("#tabGroupMember_length").append("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input id='allCheck' onchange='allCheck();' type='checkbox'>全选");
            // },
                "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    $('td:eq(2)', nRow).html(
                        "<input value=" + aData.id + " type='checkbox'"
                        + (aData.isMember ? "checked" : "") + " class='checkboxGroupMember'>"
                    );
                    return nRow;
                }
            });
        } else {
            memberTable.ajax.url('/api/contact/member/' + data.idNumber).load();
        }
        //"操作"链接点击的处理
        memberTable.on('draw', function () {

            $('.checkboxGroupMember').unbind('click').on('click', function (event) {
                var curId = $(this).val();
                if ($(this).prop('checked')) {
                    selected.push(curId);
                } else {
                    var idx = selected.indexOf(curId);
                    if (idx >= 0) {
                        selected.splice(idx, 1);
                    } else {
                        cancel.push(curId);
                    }
                }
            });

            //回显已勾选的
            var allElements = $('.checkboxGroupMember');
            for (var i = 0; i < allElements.length; i++) {
                var jEle = $(allElements[i]);
                if (selected.indexOf(jEle.val()) >= 0) {
                    jEle.attr("checked", true);
                }
                if (cancel.indexOf(jEle.val()) >= 0) {
                    jEle.attr("checked", false);
                }
            }

        });

        $('#dlgGroupMemberTitle').html('群成员管理&nbsp;-&nbsp;' + curGroup.name);

        //重置数据
        $('#dlgGroupMemberInputInvalid').addClass('hidden');
        $('#btnGroupMemberSave').text("保存");
        $('#btnGroupMemberSave').prop('disabled', false);

        //弹出对话框
        $('#dlgGroupMember').modal({
            backdrop: 'static'
        })
    }

    
    //编辑群
    function groupEdit(data) {

        $('#groupSaveMode').val(data.idNumber);
        $('#groupName').val(data.name);
        $('#groupDesc').val(data.description);

        //弹出对话框
        $('#dlgAddGroup').modal({
            backdrop: 'static'
        });

    }

    //删除群
    function groupRemove(data) {

        dialogConfirm("请确认", "确定要删除本条数据？", function (result) {
            if (result) {
                $.ajax({
                    url: '/api/group/delGroup',
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        idNumber: data.idNumber
                    }),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {
                            //重新加载table数据.
                            table.ajax.reload();
                        } else {
                            //显示错误提示信息
                            dialogAlert("操作结果", data.message, "确定");
                        }
                    },
                    error: function (err) {
                        location.href = '/';
                    }
                });
            }
        });


    }

    //修改企业指挥账号
    $('#changeCommander').on('click', function (event) {

        $('#dlgCmdFormInputValid').text("");
        $('#dlgCmdFormInputValid').addClass('hidden');

        $.get('/api/enterprise/' + $('#ent_idNumber').text(), function (data) {
            if (data && data.status == 200) {
                //弹出对话框
                $('#enpCommander').val(data.commander);
                //弹出对话框
                $('#dlgCommander').modal({
                    backdrop: 'static'
                });
            }
        });

    });

    //重置指挥账号
    $('#btnCommanerSave').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var exp = $('#enpCommander').val();

        //输入合法
        if (exp) {

            $('#btnCommanerSave').text("数据保存中...");
            $('#btnCommanerSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/chgCommander',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    commander: exp
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgCommander').modal('hide');
                        dialogAlert("操作结果", "修改指挥账号成功", "确定");

                        $('#btnCommanerSave').text("保存");
                        $('#btnCommanerSave').prop('disabled', false);
                        table.ajax.reload();

                    } else {
                        //显示错误提示信息
                        $('#dlgCmdFormInputValid').text(data.msg);
                        $('#dlgCmdFormInputValid').removeClass('hidden');

                        $('#btnCommanerSave').text("保存");
                        $('#btnCommanerSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            $('#dlgCmdFormInputValid').text("指挥账号不能为空");

            $('#dlgCmdFormInputValid').removeClass('hidden');
        }
    });

    //添加群组
    $('#btnAddGroup').on('click', function (event) {
        event.preventDefault();

        //重置数据
        $('#dlgGroupFormInputInvalid').addClass('hidden');
        $('#btnGroupSave').text("保存");
        $('#btnGroupSave').prop('disabled', false);
        $('#dlgGroupForm')[0].reset();
        $('#groupSaveMode').val('');

        //弹出对话框
        $('#dlgAddGroup').modal({
            backdrop: 'static'
        });
    });

    //对话框点击保存
    $('#btnGroupSave').on('click', function (event) {
            event.preventDefault();

            //有效性验证
            var name = $('#groupName').val();
            var desc = $('#groupDesc').val();

            //输入合法
            if (name && desc) {

                $('#btnGroupSave').text("数据保存中...");
                $('#btnGroupSave').prop('disabled', true);

                var url = '';
                var dataJson = null;
                if ($('#groupSaveMode').val().length == 0) {
                    url = '/api/contact/group';
                    dataJson = {
                        name: name,
                        description: desc
                    }
                } else {
                    url = '/api/contact/groupUpd';
                    dataJson = {
                        idNumber: $('#groupSaveMode').val(),
                        name: name,
                        description: desc
                    }
                }

                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(dataJson),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {

                            $('#dlgAddGroup').modal('hide');
                            $('#btnGroupSave').text("保存");
                            $('#btnGroupSave').prop('disabled', false);
                            //重新加载table数据.
                            table.ajax.reload();
                        } else {
                            //显示错误提示信息
                            $('#dlgGroupFormInputInvalid').text(data.message);
                            $('#dlgGroupFormInputInvalid').removeClass('hidden');

                            $('#btnGroupSave').text("保存");
                            $('#btnGroupSave').prop('disabled', false);
                        }
                    },
                    error: function (err) {
                        location.href = '/';
                    }
                });
            }
            else {
                //输入不合法
                if (!name) {
                    $('#dlgGroupFormInputInvalid').text("群组名不能为空");
                }
                else if (!desc) {
                    $('#dlgGroupFormInputInvalid').text("群组描述不能为空");
                }
                $('#dlgGroupFormInputInvalid').removeClass('hidden');
            }
        }
    );
}

//添加部门相关的代码
function initAddDepartment() {
    dialog = $("#dlgAddDepartment").dialog({
        autoOpen: false,
        height: 180,
        width: 350,
        modal: true,
        buttons: {
            "创建": addDepartment,
            "取消": function () {
                dialog.dialog("close");
            }
        },
        close: function () {
            updateTips("");
            $("#departmentName").val("");
        }
    });
    initDepartmentTree();
}

//定位设置相关的代码
function initLocateSet() {
    initLocateSetTree();
}
function chooseImg1() {
    $("#clipAreaTr1").show();
    $("#buttonTr1").show();
    clipArea1 = new bjj.PhotoClip("#clipArea1", {
        size: [100, 100],
        outputSize: [200, 200],
        file: "#file1",
        view: "#view1",
        ok: "#clipBtn1",
        outputType: "png",
        loadComplete: function () {
            $('#pictureBtn').show();
        },
        clipFinish: function (dataURL) {
            $("#view1").html("");
            $("#imgData1").val(dataURL);
            document.getElementsByName("imgData")[0].value = dataURL;
            $("#clipAreaTr1").hide();
            $("#buttonTr1").hide();
            // $.ajax({
            //     type: "POST",
            //     cache: false,
            //     contentType: false,
            //     processData: false,
            //     url: "/upload/dopic",
            //     data: new FormData($('#uploadImg')[0]),
            //     success: function (result) {
            //         $("#clipAreaTr1").hide();
            //         $("#buttonTr1").hide();
            //         $("#pictureIcon").val(result);
            //     },
            //     error: function (data) {
            //         alert("error:" + data.responseText);
            //     }
            // });
        }
    });
}
function chooseImg() {

    $("#clipAreaTr").show();
    $("#buttonTr").show();
    clipArea = new bjj.PhotoClip("#clipArea", {
        size: [100, 100],
        outputSize: [200, 200],
        file: "#file",
        view: "#view",
        ok: "#clipBtn",
        outputType: "png",
        loadComplete: function () {
            $('#pictureBtn').show();
        },
        clipFinish: function (dataURL) {
            $("#view").html("");
            document.getElementsByName("imgData")[0].value = dataURL;
            $.ajax({
                type: "POST",
                cache: false,
                contentType: false,
                processData: false,
                url: "/upload/do",
                data: new FormData($('#uploadImg')[0]),
                success: function (result) {
                    $("#clipAreaTr").hide();
                    $("#buttonTr").hide();
                    $("#pictureIcon").val(result);
                },
                error: function (data) {
                    alert("error:" + data.responseText);
                }
            });
        }
    });
}

function allCheck() {
    // var alldata=$('#tabGroupMember').dataTable().fnGetData();//得到页面中所有对象
    var  isCheck = $("#allCheck").is(":checked");
    if (isCheck){
        $(".checkboxGroupMember").prop("checked", true);
        }else{
        $(".checkboxGroupMember").prop("checked", false);
    }
}

//更多设备
function moreDevice(idx) {
    if (idx < 5) {
        if ($("#device" + idx).val().length > 0) {
            $("#device" + (idx + 1)).attr("disabled", false);
        } else {
            $("#device" + (idx + 1)).attr("disabled", true);
        }
    }
}


//定位管理-》管控周期全选功能

function checkSetAll() {
    var ischecke = $('#setall').is(":checked");
    if (ischecke) {
        $(".period input[type='checkbox']").prop("checked", true);
    } else {
        $(".period input[type='checkbox']").prop("checked", false);
    }

}


/**
 * 职位里面可以选择列出的头像
 */
function positionImg() {

    $(".positionImgBox img").on('click', function () {
        var imgurl = $(this).attr('src');
        $("#positionIcon").val(imgurl);
        // $('#view img').attr('src', imgurl);
        $('#view').html('<img id="positionHeadImg" onclick="chooseImg()" src="'
            + imgurl + '" width="100px" height = "100px" >');
    });
}

function checkE() {
    //修复可以输入字母e的问题
    if (!$("#phoneNbr").val()) {
        $("#phoneNbr").val('');
    }
}

function noDevices() {
    if ($("#noDevice").is(":checked")) {
        $("input[id^='device']").attr("readOnly", true);
    } else {
        $("input[id^='device']").attr("readOnly", false);
    }
}

function showZtree(index) {
    var treeObj_Near = userNearbyTree.getTree();
    treeObj_Near.checkAllNodes(false);
    var treeObj_Emergency = userEmergencyTree.getTree();
    treeObj_Emergency.checkAllNodes(false);
    $("#userNearbySelected").html('');
    $("#userEmergencySelected").html('');
    if (index == 1) {
        $('#dlgAddUserZtree').show();
        $('#btnUserZtreeSave').prop('disabled', false);
        if ($('#nearbyUsers').val()) {
            var selectedNearbyIds = $('#nearbyUsers').val().split(",");
            $('#userNearbySelected').text(checkNearbyTreeNodeById(selectedNearbyIds));
        }
    } else {
        $('#dlgEmergencyAddUserZtree').show();
        $('#btnEmergencyUserZtreeSave').prop('disabled', false);
        if ($('#emergencyUsers').val()) {
            var selectedEmergencyIds = $('#emergencyUsers').val().split(",");
            $('#userEmergencySelected').text(checkEmergencyTreeNodeById(selectedEmergencyIds));
        }
    }
}

function closedigUser() {
    $('.dlgAddUserZtree').hide();
}