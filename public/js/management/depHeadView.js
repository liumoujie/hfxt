/**
 * Created by 123456 on 2017/3/25.
 */
/**
 * 点击更多图片，展示不系列更多的图片，动态生成放图片的div
 * @param color:传递过来的图片系列名称
 */
function moreImag(color){
    $(".departMsk").show(1000);
    $(".depheadMiN").show(1000);
    for(var i=1;i<21;i++){
        var childdiv=$('<div></div>');
        childdiv.addClass('chiseDepAndImg'+i);
        childdiv.appendTo('.imgBox');
        $("<span class='headImgclose' onclick='headImgclose(this)'>").appendTo(".chiseDepAndImg"+i);
        $("<img src='../images/depHeadImage/"+color+"/"+i+".png' />").appendTo(".chiseDepAndImg"+i);
        $("<select id='departchose"+i+"' class='form-control depchose'>").appendTo(".chiseDepAndImg"+i);
        var departchose='departchose'+i;
        getDepartments("departchose"+i, departchose);


    }
}

/**
 * 列出所有的部门,并列生成select,出错信息输出在tipId上
 * @param selectId: select控件ID
 * @param tipId:提示信息控制ID
 * @param departmentId:默认选中的属性值
 */
function getDepartments(selectId, departmentId) {
    $.ajax({
        url: "/api/contact/departments",
        dataType: "json",
        //data: {father: dialog.dialog("option","treeNode")._id, name: $("#departmentName").val()},
        type: "get",
        filter: function (data, type) {
            alert(JSON.stringify(data));
        },
        success: function (jsonData) {
            //alert(JSON.stringify(jsonData));
            departments2Select(selectId, jsonData.data, departmentId);
        },
        error: function (jsonData) {
            alert(JSON.stringify(jsonData));
        }
    });
}

/**
 *
 * @param selectId,select控件ID
 * @param departments:获取的结果
 * @param departmentId:默认选中的属性值
 */

function departments2Select(selectId, departments, departmentId) {
    if (departmentId == null) {
        departmentId = '';
    }
    $("#" + selectId).empty();
    for (i = 0; i < departments.length; ++i) {
        if (departmentId == departments[i]._id) {
            $("#" + selectId).append("<option departmentId='" + departments[i]._id + "' selected='selectedb' value='" + departments[i].name + "'> " + departments[i].name + " </option>");
        }
        else {
            $("#" + selectId).append("<option departmentId='" + departments[i]._id + "' value='" + departments[i].name + "'> " + departments[i].name + " </option>");
        }
    }
}
/**
 *点击图片上面的关闭按钮去掉这个图片
 * @param i:表示本span
 */
function headImgclose(i){
    $(i).parent().remove();
}
/**
 * 关闭
 */
function closeHeadImg(){
    $('.imgBox div').remove();
    $(".departMsk").hide();
    $(".depheadMiN").hide();
}

/**
 * 将选择好的部门头像与对应的部门保存起来
 */
function useHeadImg(){
    $.ajax({
        //url:
        dataType: 'json',
        type: 'post',
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({

        }),
        success: function (data) {
            //成功
            if (data.status == 200) {

            } else {
                //显示错误提示信息
                $('.headImgInfor').text(data.message);
            }
        },
        error: function (err) {
            $('.headImgInfor').text(err.message);
        }
    });
}