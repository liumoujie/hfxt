/**
 * Created by mycow on 16/8/23.
 */

var departmentTree;
var settingOrganizationDepartment = {
    async: {//同ajax设置
        enable: true,
        url: "/api/contact/queryDepartments",
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        type: "get"
    },
    view: {
        expandSpeed: "",
        addHoverDom: addHoverDom,
        removeHoverDom: removeHoverDom,
        selectedMulti: false
    },
    edit: {
        enable: true
    },
    data: {
        simpleData: {
            enable: true,
            idKey: '_id',
            pIdKey: 'father',
            rootPId: -1,
        }
    },
    callback: {
        beforeRemove: beforeRemove,
        beforeRename: beforeRename,
        beforeDrop: beforeDrop
    }
};

/**
 *
 * @param treeId 对应 zTree 的 treeId，便于用户操控
 * @param treeNode 需要异步加载子节点的的父节点 JSON 数据对象,针对根进行异步加载时，treeNode = null
 * @returns {string} url
 */
//function getAsyncUrlDepartment(treeId, treeNode) {
//
//    if (null == treeNode) {
//        return "/api/contact/departments";
//    }
//
//    treeNode.zAsync = true;
//    departmentTree.getTree().expandNode(treeNode, true, false, true, false);
//
//    return;
//};

function beforeDrop(treeId, treeNodes, targetNode, moveType, isCopy) {
    //不可以拖动到公司一级,平级拖动不会保存
    if ("inner" !== moveType) {
        if (targetNode.father == -1) {
            return false;
        } else {
            return true;
        }
    }
    dialogConfirm("请确认", "确认移动 " + treeNodes[0].name + " 到 " + targetNode.name, function (result) {
        $.ajax({
            url: "/api/contact/departmentUpd",
            dataType: "json",
            data: {_id: treeNodes[0]._id, father: targetNode._id, name: treeNodes[0].name},
            type: "post",
            filter: function (data, type) {
                alert(JSON.stringify(data));
            },
            success: function (jsonData) {
                if (jsonData.status != 200) {
                    alert(jsonData.message);
                }
            },
            error: function (jsonData) {
                alert(JSON.stringify(jsonData));
            }
        });

        return true;
    });
}

function beforeRemove(treeId, treeNode) {

    departmentTree.getTree().selectNode(treeNode);

    dialogConfirm("请确认", "确认删除" + treeNode.name + " 吗？", function (result) {
        if (result) {
            $.ajax({
                url: "/api/contact/delDepartment",
                dataType: "json",
                data: {_id: treeNode._id},
                type: "post",
                filter: function (data, type) {
                    alert(JSON.stringify(data));
                },
                success: function (jsonData) {
                    if (jsonData.status != 200) {
                        alert(jsonData.message);
                    } else {
                        departmentTree.getTree().removeNode(treeNode, false);
                    }
                },
                error: function (jsonData) {
                    alert(JSON.stringify(jsonData));
                }
            });
        } else {
            departmentTree.getTree().reAsyncChildNodes(null, "refresh");
        }
    })

}

function beforeRename(treeId, treeNode, newName) {
    if (newName.length == 0) {
        alert("节点名称不能为空.");
        return false;
    }
    dialogConfirm("请确认", "确认修改为: " + newName, function (result) {

        $.ajax({
            url: "/api/contact/departmentUpd",
            dataType: "json",
            data: {_id: treeNode._id, father: treeNode.father, name: newName},
            type: "post",
            filter: function (data, type) {
                alert(JSON.stringify(data));
            },
            success: function (jsonData) {
                if (jsonData.status != 200) {
                    alert(jsonData.message);
                }
            },
            error: function (jsonData) {
                alert(JSON.stringify(jsonData));
            }
        });

        return true;
    });
}

function showAddDepartmnetDialog(treeNode) {
    dialog.dialog("option", "treeNode", treeNode);
    dialog.dialog("open");
}

function updateTips(t) {
    tips = $(".validateTips");
    tips
        .text(t)
        .addClass("ui-state-highlight");
    setTimeout(function () {
        tips.removeClass("ui-state-highlight", 1500);
    }, 500);
}

function checkLength(o, n, min, max) {
    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        updateTips(min + "长度必须在" + min + "到" + max + "之间");
        return false;
    } else {
        return true;
    }
}

//emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/
function checkRegexp(o, regexp, n) {
    if (!( regexp.test(o.val()) )) {
        o.addClass("ui-state-error");
        updateTips(n);
        return false;
    } else {
        return true;
    }
}

function addDepartment() {
    valid = checkLength($("#departmentName"), "名称", 3, 16);

    valid = valid && checkRegexp($("#departmentName"), /^[a-z\u4E00-\u9FA5]([0-9a-z_\u4E00-\u9FA5])+$/i,
            "名字必须是 a-z, 0-9,汉字,下划线,不能以数字开头");
    if (!valid) {
        // $('#errorId').text('名称长度必须是3-16位的字符或下划线,不能以数字开头').removeClass('state2').addClass('state1').addClass('erroText');
        // $('#errorId').removeClass('hidden');
        return false;
    }else {
        // $('#errorId').addClass('hidden');
    }
    createDepartment();
    return true;
}

function createDepartment() {
    $.ajax({
        url: "/api/contact/department",
        dataType: "json",
        data: {isParent: true, father: dialog.dialog("option", "treeNode")._id, name: $("#departmentName").val()},
        type: "post",
        success: function (jsonData) {
            departmentTree.getTree().addNodes(dialog.dialog("option", "treeNode"), jsonData.data);
            dialog.dialog("close");
        },
        error: function (jsonData) {
            alert(JSON.stringify(jsonData));
        }
    });
}

function addHoverDom(treeId, treeNode) {
    var sObj = $("#" + treeNode.tId + "_span");
    if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;
    var addStr = "<span class='button add' id='addBtn_" + treeNode.tId
        + "' title='add node' onfocus='this.blur();'></span>";
    sObj.after(addStr);
    var btn = $("#addBtn_" + treeNode.tId);
    if (btn) btn.bind("click", function () {
        // var departmentName = $("#departmentName").val();
        // if (departmentName == ""){
        //     $('#errorId').addClass('hidden');
        // }
        showAddDepartmnetDialog(treeNode);
    });
};

function removeHoverDom(treeId, treeNode) {
    $("#addBtn_" + treeNode.tId).unbind().remove();
};

function initDepartmentTree() {

    dialog = $("#dlgAddDepartment").dialog({
        autoOpen: false,
        height: 190,
        width: 400,
        modal: true,
        buttons: {
            "创建": addDepartment,
            "取消": function () {
                dialog.dialog("close");
            }
        },
        close: function () {
            updateTips("");
            $("#departmentName").val("");
        }
    });
    departmentTree = new tree("departmentTree", settingOrganizationDepartment);

}

/**
 *
 * @param inputID, String, input输入框id,
 */
function searchInTree(inputID) {
    departmentTree.search($('#' + inputID).val());
}

//init("manage");