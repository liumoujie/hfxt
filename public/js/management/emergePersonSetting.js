/**
 * Created by 123456 on 2017/5/7.
 */
var showFlag = false;
var lastTreeId = "";
var userEmergencyTree;
var userEmergencySelectedStaffs = new Set();

var settingEmergencyUserOrganizationStaff = {
    async: {//同ajax设置
        enable: true,
        url: "/api/contact/organization",
        contentType: "application/x-www-form-urlencoded",
        dataType: "json",
        type: "get"
    },
    check: {
        enable: true
    },
    callback: {
        //onClick: locateUser,
        onCheck: selectEmergencyStaff
    },
    data: {
        simpleData: {
            enable: true,
            idKey: '_id',
            pIdKey: 'father',
            rootPId: -1,
        }
    }
};

function selectEmergencyStaff(event, treeId, treeNode) {
    userEmergencySelectedStaffs = new Set();
    var allSelectedStaff = "";
    var treeObj = userEmergencyTree.getTree();
    var checkedNodes = treeObj.getCheckedNodes();
    for (var i in checkedNodes) {
        // 不是叶子节点
        if (!checkedNodes[i].isParent) {
            allSelectedStaff += checkedNodes[i].name + " ";
            userEmergencySelectedStaffs.add(
                {
                    "_id": checkedNodes[i]._id,
                    "idNumber": checkedNodes[i].idNumber,
                    "phoneNbr": checkedNodes[i].phoneNbr,
                    "staffName": checkedNodes[i].name
                }
            );
        }
    }
    $('#userEmergencySelected').text(allSelectedStaff);
}
function initEmergencyLocateSetTree() {
    userEmergencyTree = new tree("orgEmergencyLocUserTree", settingEmergencyUserOrganizationStaff);
}

//通过id数组,将数组内的成员勾选上
function checkEmergencyTreeNodeById(ids) {
    return userEmergencyTree.checkNodeByIds(ids, "idNumber", "name");
}

function selectCustom() {
    $("input[name='locateUpd']:eq(5)").click();
}

function saveEmergencyUser() {
    $('#userEmergencyErrMsg').addClass('hidden');
    var selStaffs = [];
    userEmergencySelectedStaffs.forEach(function (staff) {
        selStaffs.push(staff.idNumber);
    });

    if ((!userEmergencySelectedStaffs || userEmergencySelectedStaffs.size == 0) && $('#userEmergencySelected').text().length == 0) {
        $('#userEmergencyErrMsg').text('请选择人员');
        $('#userEmergencyErrMsg').removeClass('hidden');
        return;
    }

    $('#emergency').prop('checked', true);
    $('#btnEmergencyUserZtreeSave').prop('disabled', true);
    $('#emergencyUsers').val(selStaffs);
    $('.dlgAddUserZtree').hide();
}

/**
 *
 * @param inputID, String, input输入框id,
 */
function searchInLocTree3(inputID) {
    var condition = $('#' + inputID).val();
    if (condition.length == 0) {//为提升效率
        userTree2.showNodes(userTree2.getNodesByParam("isHidden", true));
        return;
    }
    var zNodes = userTree2.getNodesByFilter(function (node) {
        if (node.name.indexOf(condition) == -1) {
            userTree2.hideNode(node);
            return false;
        } else {
            //子结点符合条件时,显示其所有父结点
            var curNode = node;
            do {
                var parentNode = curNode.getParentNode();
                if (null == parentNode) {
                    break;
                }
                //父结点已是显示的就不用再往上找父了
                if (!parentNode.isHidden) {
                    break;
                }
                userTree2.showNode(parentNode);
                curNode = parentNode;
            } while (true);
            //修改前的搜索条件时可能会让当前结点已处于hide状态,所以要显示
            userTree2.showNode(node);
        }
        return true;
    }, false);


}
