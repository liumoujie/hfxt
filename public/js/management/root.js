/**
 * Created by suke on 16/3/26.
 */


var dataTablesLang = {
    "sProcessing": "处理中...",
    "sLengthMenu": "显示 _MENU_ 项结果",
    "sZeroRecords": "没有匹配结果",
    "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
    "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
    "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
    "sInfoPostFix": "",
    "sSearch": "搜索:",
    "sUrl": "",
    "sEmptyTable": "表中数据为空",
    "sLoadingRecords": "载入中...",
    "sInfoThousands": ",",
    "oPaginate": {
        "sFirst": "首页",
        "sPrevious": "上页",
        "sNext": "下页",
        "sLast": "末页"
    },
    "oAria": {
        "sSortAscending": ": 以升序排列此列",
        "sSortDescending": ": 以降序排列此列"
    }
};

$(document).ready(function () {
    initEnterpriseManager();
    initPrivileges();
});

function dialogAlert(title, msg, btn) {
    BootstrapDialog.show({
        title: title,
        message: msg,
        buttons: [{
            label: btn,
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}

function dialogConfirm(title, msg, cnfFun) {

    BootstrapDialog.confirm({
        title: title,
        message: msg,
        type: BootstrapDialog.TYPE_WARNING, // <-- Default value is BootstrapDialog.TYPE_PRIMARY
        closable: true, // <-- Default value is false
        draggable: true, // <-- Default value is false
        btnCancelLabel: '取消', // <-- Default value is 'Cancel',
        btnOKLabel: '确定', // <-- Default value is 'OK',
        btnOKClass: 'btn-warning', // <-- If you didn't specify it, dialog type will be used,
        callback: function (result) {
            // result will be true if button was click, while it will be false if users close the dialog directly.
            if (result) {
                //alert('Yup.');
                cnfFun(result);
            } else {
                //alert('Nope.');
            }
        }
    });

}

//企业管理功能相关的初始化
function initEnterpriseManager() {

    var selected = [];
    var cancel = [];
    var curRowId = null;

    var xhr = null;
    var hideLineIdNumber = "";

    var privilegeTable = null;

    var table = $('#tabEnterpriseList').DataTable({
        "language": dataTablesLang,
        "order": [[0, 'desc']],
        "ajax": "/api/enterprise",
        "columns": [
            {'data': "name"},
            {'data': "phoneNbr"},
            {'data': "expDate"},
        ],

        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $('td:eq(2)', nRow).html(
                "<a class=\"btn btn-link btn-sm enterpriseTableLink privilege\" role=\"button\">权限</a>" +
                "<a class=\"btn btn-link btn-sm enterpriseTableLink pwd\" role=\"button\">密码</a>" +
                "<a class=\"btn btn-link btn-sm enterpriseTableLink expdate\" role=\"button\">过期日期</a>"
            );
            return nRow;
        }

        //"fnRowCallback": function (nRow, aData, iDisplayIndex) {
        //    $('td:eq(2)', nRow).html(
        //        "<a class=\"btn btn-link btn-sm enterpriseTableLink privilege\" role=\"button\">权限</a>" +
        //        "<a class=\"btn btn-link btn-sm enterpriseTableLink pwd\" role=\"button\">密码</a>" +
        //        "<a class=\"btn btn-link btn-sm enterpriseTableLink quota\" role=\"button\">配额</a>" +
        //        "<a class=\"btn btn-link btn-sm enterpriseTableLink expdate\" role=\"button\">过期日期</a>" +
        //        "<a class=\"btn btn-link btn-sm enterpriseTableLink disabled stop\" role=\"button\">禁用</a>"
        //    );
        //    return nRow;
        //}
    });

    //"操作"链接点击的处理
    table.on('draw', function () {
        $('.enterpriseTableLink').unbind('click').on('click', function (event) {
            event.preventDefault();

            //本行对应的数据
            var data = table.row($(this).parents('tr')).data();

            if ($(this).hasClass('pwd')) {
                resetPwd(data);
            } else if ($(this).hasClass('quota')) {
                editQuota(data);
            } else if ($(this).hasClass('expdate')) {
                editExpDate(data);
            } else if ($(this).hasClass('key')) {
                editKey(data);
            } else if ($(this).hasClass('privilege')) {
                privilege(data);
            }
            else if ($(this).hasClass('commander')) {
                setcommander(data);
            }
            else if ($(this).hasClass('stop')) {
                //groupRemove(data);
            }
        });
    });

    function setcommander(data) {
        hideLineIdNumber = data.idNumber;
        $('#enpCommander').val("");
        //弹出对话框
        $('#dlgCommander').modal({
            backdrop: 'static'
        });
    }

    //重置指挥账号
    $('#btnCommanerSave').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var exp = $('#enpCommander').val();

        //输入合法
        if (exp) {

            $('#btnCommanerSave').text("数据保存中...");
            $('#btnCommanerSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/chgCommander',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    idNumber: hideLineIdNumber + "",
                    commander: exp
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgCommander').modal('hide');
                        dialogAlert("操作结果", "修改指挥账号成功", "确定");

                        $('#btnCommanerSave').text("保存");
                        $('#btnCommanerSave').prop('disabled', false);
                        table.ajax.reload();

                    } else {
                        //显示错误提示信息
                        $('#dlgCmdFormInputValid').text(data.msg);
                        $('#dlgCmdFormInputValid').removeClass('hidden');

                        $('#btnCommanerSave').text("保存");
                        $('#btnCommanerSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            $('#dlgCmdFormInputValid').text("指挥账号不能为空");

            $('#dlgCmdFormInputValid').removeClass('hidden');
        }
    });

    function privilege(data) {
        curRowId = data.idNumber;

        var mbrDataTablesLang = {
            "sProcessing": "处理中...",
            "sLengthMenu": "显示 _MENU_ 项结果",
            "sZeroRecords": "没有匹配结果",
            "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
            "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
            "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
            "sInfoPostFix": "",
            "sSearch": "搜索:",
            "sUrl": "",
            "sEmptyTable": "表中数据为空",
            "sLoadingRecords": "载入中...",
            "sInfoThousands": ",",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "上页",
                "sNext": "下页",
                "sLast": "末页"
            }
        };

        if (privilegeTable == null) {
            privilegeTable = $('#tabPrivilegeMember').DataTable({
                "language": mbrDataTablesLang,
                "order": [[2, 'desc']],
                "pageLength": 10,
                "ajax": '/api/enpPrivilege/' + curRowId + "?t=" + Date.now(),
                "columns": [
                    {'data': "code"},
                    {'data': "name"},
                    {'data': "code"}
                ], "fnRowCallback": function (nRow, aData, iDisplayIndex) {
                    $('td:eq(2)', nRow).html(
                        "<input value=" + aData.code + " type='checkbox'"
                        + (  aData.hasPrivilege ? "checked" : "") + " class='checkboxGroupMember'>"
                        //"<input type='checkbox' class='checkboxGroupMember'>"
                    );
                    return nRow;
                }
            });
        } else {
            privilegeTable.ajax.url('/api/enpPrivilege/' + curRowId + "?t=" + Date.now()).load();
        }

        //"操作"链接点击的处理
        privilegeTable.on('draw', function () {
            $('.checkboxGroupMember').unbind('click').on('click', function (event) {
                var curId = $(this).val();
                if ($(this).prop('checked')) {
                    selected.push(curId);
                } else {
                    var idx = selected.indexOf(curId);
                    if (idx >= 0) {
                        selected.splice(idx, 1);
                    } else {
                        cancel.push(curId);
                    }
                }
            });

            //回显已勾选的
            var allElements = $('.checkboxGroupMember');
            for (var i = 0; i < allElements.length; i++) {
                var jEle = $(allElements[i]);
                if (selected.indexOf(jEle.val()) >= 0) {
                    jEle.attr("checked", true);
                }
                if (cancel.indexOf(jEle.val()) >= 0) {
                    jEle.attr("checked", false);
                }
            }

        });

        //弹出对话框
        $('#dlgPrivilegeMember').modal({
            backdrop: 'static'
        })
    }


    //组成员对话框点击保存
    $('#btnPrivilegeMemberSave').on('click', function (event) {
        event.preventDefault();
        //if (!saveFirstClk) {
        //    $('#dlgGroupMemberInputInvalid').text("本次保存将影响群组内的人员变更，击保存继续");
        //    $('#dlgGroupMemberInputInvalid').css({background: "white"});
        //    $('#dlgGroupMemberInputInvalid').css({color: "red"});
        //    $('#btnGroupMemberSave').css({background: "white"});
        //    $('#btnGroupMemberSave').css({color: "red"});
        //    $('#dlgGroupMemberInputInvalid').removeClass('hidden');
        //
        //    saveFirstClk = true;
        //    return false;
        //}

        $('#btnPrivilegeMemberSave').text("数据保存中...");
        $('#btnPrivilegeMemberSave').prop('disabled', true);

        $.ajax({
            url: '/api/privilege',
            dataType: 'json',
            type: 'put',
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify({
                idNumber: curRowId,
                members: selected,
                cancels: cancel
            }),
            error: function () {
                $('#dlgPrivilegeMemberInputInvalid').text('保存失败,请检查网络或者联系管理员.');
                $('#dlgPrivilegeMemberInputInvalid').removeClass('hidden');
                $('#btnPrivilegeMemberSave').text("保存");
                $('#btnPrivilegeMemberSave').prop('disabled', false);
            },
            success: function (data) {
                //成功
                if (data.status == 200) {
                    $('#btnPrivilegeMemberSave').text("保存");
                    $('#btnPrivilegeMemberSave').prop('disabled', false);
                    $('#dlgPrivilegeMember').modal('hide');
                    //重新加载table数据.
                    table.ajax.reload(null, false);
                } else {
                    //显示错误提示信息
                    $('#dlgPrivilegeMemberInputInvalid').text(data.message);
                    $('#dlgPrivilegeMemberInputInvalid').removeClass('hidden');

                    $('#btnPrivilegeMemberSave').text("保存");
                    $('#btnPrivilegeMemberSave').prop('disabled', false);
                }
            }
        });
    });

    //设置企业密钥
    function editKey(data) {
        hideLineIdNumber = data.idNumber;
        $('#newKey').val(data.key);
        //弹出对话框
        $('#dlgKey').modal({
            backdrop: 'static'
        });
    };

    //设置过期时间
    function editExpDate(data) {
        hideLineIdNumber = data.idNumber;
        $('#mdExpDate').val(data.expDate);
        //弹出对话框
        $('#dlgExp').modal({
            backdrop: 'static'
        });
    };

    //重置过期时间
    $('#btnExpSave').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var exp = $('#mdExpDate').val();

        //输入合法
        if (exp) {

            $('#btnExpSave').text("数据保存中...");
            $('#btnExpSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/chgEntExpTime',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    idNumber: hideLineIdNumber,
                    expDate: exp
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgExp').modal('hide');
                        //BootstrapDialog.alert("密码修改成功");
                        dialogAlert("操作结果", "修改过期时间成功", "确定");

                        $('#btnExpSave').text("保存");
                        $('#btnExpSave').prop('disabled', false);
                        table.ajax.reload();

                    } else {
                        //显示错误提示信息
                        $('#dlgExpFormInputValid').text(data.message);
                        $('#dlgExpFormInputValid').removeClass('hidden');

                        $('#btnExpSave').text("保存");
                        $('#btnExpSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            $('#dlgExpFormInputValid').text("过期时间不能为空");

            $('#dlgExpFormInputValid').removeClass('hidden');
        }
    });

    //修改密码
    $('#changeRootPwd').on('click', function (event) {
        //弹出对话框
        $('#dlgChangeRootPwd').modal({
            backdrop: 'static'
        });
        $('#dlgChangeRootPwdInputInvalid').addClass('hidden');
        $('#dlgChangeRootPwdInputInvalid').text('');
        $('#curPassword').val('');
        $('#chgNewRootPassword').val('');
        $('#repChgNewRootPassword').val('');
    });
    //修改密码保存按钮
    $('#btnChangeRootPwd').on('click', function (event) {
        event.preventDefault();
        //有效性验证
        var curPassword = $('#curPassword').val();
        var chgNewPassword = $('#chgNewRootPassword').val();
        var repChgNewPassword = $('#repChgNewRootPassword').val();

        //输入合法
        if (chgNewPassword === repChgNewPassword && chgNewPassword.length < 6 || repChgNewPassword.length >18) {
            $('#dlgChangeRootPwdInputInvalid').text("密码必须大于6个字符小于18个字符");
            $('#dlgChangeRootPwdInputInvalid').removeClass('hidden');

        }else  if(curPassword === chgNewPassword && chgNewPassword === repChgNewPassword){
            $('#dlgChangeRootPwdInputInvalid').text("新旧密码相同，修改失败！");
            $('#dlgChangeRootPwdInputInvalid').removeClass('hidden');
        }
        else if (curPassword && chgNewPassword && repChgNewPassword && chgNewPassword === repChgNewPassword) {
            $('#btnUserSave').text("数据保存中...");
            $('#btnUserSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/chgpwd',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    curPassword: curPassword,
                    chgNewPassword: chgNewPassword,
                    repChgNewPassword: repChgNewPassword
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgChangeRootPwd').modal('hide');
                        $('#curPassword').val('');
                        $('#chgNewPassword').val('');
                        $('#repChgNewPassword').val('');
                        //BootstrapDialog.alert("密码修改成功");
                        dialogAlert("操作结果", "密码修改成功", "确定");

                    } else {
                        //显示错误提示信息
                        $('#dlgChangeRootPwdInputInvalid').text(data.message);
                        $('#dlgChangeRootPwdInputInvalid').removeClass('hidden');

                        $('#btnUserSave').text("保存");
                        $('#btnUserSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            if (!curPassword) {
                $('#dlgChangeRootPwdInputInvalid').text("当前密码不能为空");
            } else if (!chgNewPassword || !repChgNewPassword) {
                $('#dlgChangeRootPwdInputInvalid').text("密码不能为空");
            } else if (chgNewPassword !== repChgNewPassword) {
                $('#dlgChangeRootPwdInputInvalid').text("两次输入的密码需要一致");
            }
            $('#dlgChangeRootPwdInputInvalid').removeClass('hidden');
        }
    });

    //重置密码
    function resetPwd(data) {
        hideLineIdNumber = data.idNumber;
        //弹出对话框
        $('#dlgChangePwd').modal({
            backdrop: 'static'
        });
        $('#dlgChangePwdInputInvalid').text('');
        $('#dlgChangePwdInputInvalid').addClass('hidden');
        $('#chgNewPassword').val('');
        $('#repChgNewPassword').val('');
    };

    //重置密码保存按钮
    $('#btnChangePwd').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var chgNewPassword = $('#chgNewPassword').val();
        var repChgNewPassword = $('#repChgNewPassword').val();

        //输入合法
        if (chgNewPassword === repChgNewPassword && chgNewPassword.length < 6 || chgNewPassword.length > 18){
            $('#dlgChangePwdInputInvalid').text('密码必须大于6个字符小于18个字符');
            $('#dlgChangePwdInputInvalid').removeClass('hidden');
        }else  if(curPassword === chgNewPassword && chgNewPassword === repChgNewPassword){
            $('#dlgChangePwdInputInvalid').text("新旧密码相同，修改失败！");
            $('#dlgChangePwdInputInvalid').removeClass('hidden');
        }
        else if (chgNewPassword && repChgNewPassword && chgNewPassword === repChgNewPassword) {

            $('#btnUserSave').text("数据保存中...");
            $('#btnUserSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/adminChgPwd',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    idNumber: hideLineIdNumber,
                    chgNewPassword: chgNewPassword
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgChangePwd').modal('hide');
                        $('#chgNewPassword').val('');
                        $('#repChgNewPassword').val('');
                        //BootstrapDialog.alert("密码修改成功");
                        dialogAlert("操作结果", "密码修改成功", "确定");

                    } else {
                        //显示错误提示信息
                        $('#dlgChangePwdInputInvalid').text(data.message);
                        $('#dlgChangePwdInputInvalid').removeClass('hidden');

                        $('#btnUserSave').text("保存");
                        $('#btnUserSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            if (!chgNewPassword || !repChgNewPassword) {
                $('#dlgChangePwdInputInvalid').text("密码不能为空");
            } else if (chgNewPassword !== repChgNewPassword) {
                $('#dlgChangePwdInputInvalid').text("两次输入的密码需要一致");
            }

            $('#dlgChangePwdInputInvalid').removeClass('hidden');
        }
    });


    function editQuota(data) {

        //弹出对话框
        $('#dlgQuota').modal({
            backdrop: 'static'
        });

        $('#dlgQuotaFormInputValid').text("");
        $('#dlgQuotaFormInputValid').addClass('hidden');

        $('#entStaff').val(data.quota.staff);
        $('#entMaxGroupPer').val(data.quota.preGroup);
        $('#hideIdNumber').val(data.idNumber);

    }

    $('#btnQuotaSave').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var maxGroupPer = 100;
        var staff = 500;

        //输入合法
        if (maxGroupPer && staff) {

            if (maxGroupPer < 0 || staff < 0) {
                $('#dlgQuotaFormInputValid').text("配额不能小于0");
                $('#dlgQuotaFormInputValid').removeClass('hidden');
                return null;
            }

            $('#btnQuotaSave').text("数据保存中...");
            $('#btnQuotaSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise/quota',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    idNumber: $('#hideIdNumber').val(),
                    quota: {staff: staff, preGroup: maxGroupPer}
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {

                        $('#dlgQuota').modal('hide');
                        $('#btnQuotaSave').text("保存");
                        $('#btnQuotaSave').prop('disabled', false);

                        dialogAlert("操作结果", "修改配额成功", "确定");

                        //重新加载table数据.
                        table.ajax.reload();
                    } else {
                        //显示错误提示信息
                        $('#dlgQuotaFormInputValid').text(data.message);
                        $('#dlgQuotaFormInputValid').removeClass('hidden');

                        $('#btnQuotaSave').text("保存");
                        $('#btnQuotaSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            if (!maxGroupPer) {
                $('#dlgQuotaFormInputValid').text("群组成员最大数不能为空");
            }
            else if (!staff) {
                $('#dlgQuotaFormInputValid').text("最大员工数不能为空");
            }
            $('#dlgQuotaFormInputValid').removeClass('hidden');
        }
    });

    //添加企业
    $('#idBtnCreateEnterprise').on('click', function (event) {
        event.preventDefault();
        //弹出对话框
        $('#dlgAddEnterprise').modal({
            backdrop: 'static'
        });

        $('#enterName').val('');
        $('#pwd').val('');
        $('#repwd').val('');
        $('#maxGroupPer').val('');
        $('#staff').val('');
        $('#commanderPhoneNbr').val('');
        $('#expDate').val('');

        $('#btnEnterSave').text("保存");
        $('#btnEnterSave').prop('disabled', false);
        $('#dlgFormInputValid').text("");
        $('#dlgFormInputValid').addClass('hidden');

        //请求生成用户号码
        //xhr = $.get('/api/enterprise/number', function (data) {
        //    if (data && data.status == 200) {
        //
        //        $('#enterName').val('');
        //        $('#pwd').val('');
        //        $('#repwd').val('');
        //        $('#maxGroupPer').val('');
        //        $('#staff').val('');
        //
        //        $('#enterNumber').val(data.number);
        //        $('#btnEnterSave').prop('disabled', false);
        //    }
        //    xhr = null;
        //}).error(function () {
        //    xhr = null;
        //});
    });


    //对话框点击保存
    $('#btnEnterSave').on('click', function (event) {
        event.preventDefault();

        //有效性验证
        var enterName = $('#enterName').val();
        var enterNumber = $('#enterNumber').val();
        var pwd = $('#pwd').val();
        var repwd = $('#repwd').val();
        var maxGroupPer = 100;
        var staff = 500;
        var expDate = "2099-12-31";
        var key = 12345678;
        var vedioMode = $('#vedioMode').val();
        var commanderPhoneNbr = $('#commanderPhoneNbr').val();
        //输入合法
         if(pwd === repwd && pwd.length < 6 || pwd.length > 18){
            $('#dlgFormInputValid').text("密码必须大于6个字符小于18个字符");
            $('#dlgFormInputValid').removeClass('hidden');
        }else if (staff < 0 || staff >5000){
            $('#dlgFormInputValid').text("成员上限范围0-5000");
            $('#dlgFormInputValid').removeClass('hidden');
        }else if(maxGroupPer < 0 || maxGroupPer > 5000){
            $('#dlgFormInputValid').text("群成员上限范围0-5000");
            $('#dlgFormInputValid').removeClass('hidden');
        }else if (expDate && enterName && pwd && repwd && pwd === repwd && maxGroupPer && staff && commanderPhoneNbr) {
            $('#btnEnterSave').text("数据保存中...");
            $('#btnEnterSave').prop('disabled', true);

            $.ajax({
                url: '/api/enterprise',
                dataType: 'json',
                type: 'post',
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    name: enterName,
                    password: pwd,
                    idNumber: enterNumber,
                    expDate: expDate,
                    key: key,
                    vedioMode: vedioMode,
                    quota: {staff: staff, preGroup: maxGroupPer},
                    commanderPhoneNbr: commanderPhoneNbr
                }),
                success: function (data) {
                    //成功
                    if (data.status == 200) {
                        $('#dlgAddEnterprise').modal('hide');
                        $('#btnEnterSave').text("保存");
                        $('#btnEnterSave').prop('disabled', false);

                        dialogAlert("操作结果", data.message, "确定");

                        //重新加载table数据.
                        table.ajax.reload();
                    } else {
                        //显示错误提示信息
                        $('#dlgFormInputValid').text(data.message);
                        $('#dlgFormInputValid').removeClass('hidden');

                        $('#btnEnterSave').text("保存");
                        $('#btnEnterSave').prop('disabled', false);
                    }
                },
                error: function (err) {
                    location.href = '/';
                }
            });
        } else {
            //输入不合法
            if (!enterName) {
                $('#dlgFormInputValid').text("企业名称不能为空");
            } else if (!maxGroupPer) {
                $('#dlgFormInputValid').text("群组成员最大数不能为空");
            } else if (!staff) {
                $('#dlgFormInputValid').text("最大员工数不能为空");
            } else if (!expDate) {
                $('#dlgFormInputValid').text("过期时间不能为空");
            } else if (!pwd || !repwd) {
                $('#dlgFormInputValid').text("密码不能为空");
            } else if (pwd !== repwd) {
                $('#dlgFormInputValid').text("两次输入的密码需要一致");
            } else if (!commanderPhoneNbr) {
                $('#dlgFormInputValid').text("指挥号电话号码必填");
            }
            $('#dlgFormInputValid').removeClass('hidden');
        }
    });
}

//添加权限相关的代码
function initPrivileges() {

    var table = $('#tabPrivileges').DataTable({
        "language": dataTablesLang,
        "order": [[0, 'desc']],
        "pageLength": 10,
        "ajax": "/api/privilege",
        "columns": [
            {'data': "code"},
            {'data': "name"},
            {'data': "description"},
            {'data': "code"}
        ],
        "fnRowCallback": function (nRow, aData, iDisplayIndex) {
            $('td:eq(3)', nRow).html(
                "<a class=\"btn btn-link btn-sm privilegeTableLink pEdit\" role=\"button\">编辑</a>" +
                "<a class=\"btn btn-link btn-sm privilegeTableLink pRemove\" role=\"button\">删除</a>"
            );
            return nRow;
        }
    });

    //当前指向的组
    var curGroup = null;

    //"操作"链接点击的处理
    table.on('draw', function () {
        $('.privilegeTableLink').unbind('click').on('click', function (event) {
            event.preventDefault();

            //本行对应的数据
            var data = table.row($(this).parents('tr')).data();
            curGroup = data;
            if ($(this).hasClass('pEdit')) {
                privilegeEdit(data);
            }
            else if ($(this).hasClass('pRemove')) {
                privilegeRemove(data);
            }
        });
    });

    //编辑群
    function privilegeEdit(data) {

        $('#privilegeSaveMode').val(data._id);
        $('#privilegeName').val(data.name);
        $('#privilegeCode').val(data.code);
        $('#privilegeDescription').val(data.description);

        //弹出对话框
        $('#dlgAddPrivilege').modal({
            backdrop: 'static'
        });

    }

    //删除群
    function privilegeRemove(data) {

        dialogConfirm("请确认", "确定要删除本条数据？", function (result) {
            if (result) {
                $.ajax({
                    url: '/api/privilegeDelete',
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        _id: data._id,
                        privilege: data.code
                    }),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {
                            //重新加载table数据.
                            table.ajax.reload();
                        } else {
                            //显示错误提示信息
                            dialogAlert("操作结果", data.message, "确定");
                        }
                    },
                    error: function (err) {
                        alert(JSON.stringify(err));
                        //location.href = '/';
                    }
                });
            }
        });
    }

    //添加群组
    $('#btnAddPrivilege').on('click', function (event) {
        event.preventDefault();

        $('#dlgPrivilegeFormInputInvalid').addClass('hidden');
        $('#privilegeSaveMode').val("");
        $('#privilegeCode').val("");
        $('#privilegeName').val("");
        $('#privilegeDescription').val("");

        //弹出对话框
        $('#dlgAddPrivilege').modal({
            backdrop: 'static'
        });
    });

    //对话框点击保存
    $('#btnWaterPersonSave').on('click', function (event) {
            event.preventDefault();

            //有效性验证
            var code = $('#privilegeCode').val();
            var name = $('#privilegeName').val();
            var desc = $('#privilegeDescription').val();

            //输入合法
            if (name && code) {

                $('#btnGroupSave').text("数据保存中...");
                $('#btnGroupSave').prop('disabled', true);

                var url = '';
                var dataJson = null;
                if ($('#privilegeSaveMode').val().length == 0) {
                    url = '/api/privilege';
                    dataJson = {
                        code: code,
                        name: name,
                        description: desc
                    }
                } else {
                    url = '/api/privilegeUpd';
                    dataJson = {
                        _id: $('#privilegeSaveMode').val(),
                        code: code,
                        name: name,
                        description: desc
                    }
                }

                $.ajax({
                    url: url,
                    dataType: 'json',
                    type: 'post',
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify(dataJson),
                    success: function (data) {
                        //成功
                        if (data.status == 200) {

                            $('#dlgAddPrivilege').modal('hide');
                            //$('#btnGroupSave').text("保存");
                            //$('#btnGroupSave').prop('disabled', false);
                            //重新加载table数据.
                            table.ajax.reload();
                        } else {
                            //显示错误提示信息
                            $('#dlgPrivilegeFormInputInvalid').text(data.message);
                            $('#dlgPrivilegeFormInputInvalid').removeClass('hidden');

                            $('#btnGroupSave').text("保存");
                            $('#btnGroupSave').prop('disabled', false);
                        }
                    },
                    error: function (err) {
                        location.href = '/';
                    }
                });
            }
            else {
                //输入不合法
                if (!name) {
                    $('#dlgPrivilegeFormInputInvalid').text("权限名称不能为空");
                }
                else if (!code) {
                    $('#dlgPrivilegeFormInputInvalid').text("权限编码不能为空");
                }
                $('#dlgPrivilegeFormInputInvalid').removeClass('hidden');
            }
        }
    );
}