/**
 * Created by zhoulanhong on 1/16/18.
 */
var express = require('express');
var router = express.Router();
var co = require('co');
var pttAuth = require('../common/ptt-auth');
var ID = require('../common/Id');
var pool = require('../common/connection_pool');
var Util = require('../common/ptt-util');
var wrapper = require('co-mysql');
var fYear = require('../common/fivePoints');
var fPath = require('../common/fivePath');
var client = require('../common/smsClient');
var _ = require('lodash');

var cmoment = require('moment');
var tYear = require('../common/tenPoints');
var tPath = require('../common/tenPath');

var twYear = require('../common/twentyPoints');
var twPath = require('../common/twentyPath');

var fifYear = require('../common/fiftyPoints');
var fifPath = require('../common/fiftyPath');

var huYear = require('../common/hundredPoints');
var huPath = require('../common/hundredPath');

var bgPoints = require('../common/bgPoints');
var http = require('http');

let p = wrapper(pool);

// 删除分组
router.post('/groupDel', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {

        let delSql = "delete from wpgroup where id in (" + req.body.id + ")";
        console.log(JSON.stringify(delSql));
        let rows = yield p.query(delSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 添加人员分组
router.post('/group', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log(JSON.stringify(req.body));
        let insertSql = "insert into wpgroup value (" + ID.get() + ",'" + req.body.gpName + "','" + req.body.wpIds + "','" + Date.now() + "')";
        console.log(JSON.stringify(insertSql));
        let rows = yield p.query(insertSql)
        res.json({status: 200, data: {res: "success"}});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});


// 列出所有的短信人员分组
router.get('/group', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log("------------------  SELECT * from hfxt.wpgroup  ");
        let rows = yield p.query("SELECT * from hfxt.wpgroup ");
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});


// 找出站点对应的led屏幕
router.get('/queryRelLed', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log("------------------  SELECT * from hfxt.wpgroup  ");
        let rows = yield p.query("SELECT b.* FROM sites a, leds b where a.id = b.rellogging and a.id = '" + req.query.siteid + "'");
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});


// 添加屏幕显示的内容
router.post('/saveScreenContent', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log(JSON.stringify(req.body));
        let insertSql = "insert into ledcontents values (" + ID.get() + ",'" + req.body.ledid + "','" + req.body.orgtext + "','" + req.body.color + "','" + req.body.fsize + "','" + Date.now() + "')";
        console.log(JSON.stringify(insertSql));
        let rows = yield p.query(insertSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 添加屏幕显示的内容
router.post('/deleteScreenContent', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log(JSON.stringify(req.body));
        let insertSql = "delete from ledcontents where id ='" + req.body.id + "'";
        console.log(JSON.stringify(insertSql));
        let rows = yield p.query(insertSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 修改屏幕显示的内容
router.post('/updScreenContent', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log(JSON.stringify(req.body));
        let insertSql = "update ledcontents set orgtext = '" + req.body.orgtext + "', color = '" + req.body.color + "', size = '" + req.body.fsize + "' where id = '" + req.body.id + "'";
        console.log(JSON.stringify(insertSql));
        let rows = yield p.query(insertSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 修改屏幕自动显示状态
router.post('/updScreenAuto', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log(JSON.stringify(req.body));
        let insertSql = "update leds set state = '" + req.body.state + "' where id = '" + req.body.id + "'";
        console.log(JSON.stringify(insertSql));
        let rows = yield p.query(insertSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 列出所有屏幕
router.get('/screencontent', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log("--------abc----------");
        let rows = yield p.query("select * from ledcontents where ledid ='" + req.query.ledid + "'; ");
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 列出所有屏幕
router.get('/screen', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log("------------------" + req.query.siteid);
        let rows = yield p.query("SELECT * from hfxt.leds where rellogging = '" + req.query.siteid + "'; ");
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});


//发送短信息
//{phoneNbrs: [],phoneNames: [],msgtext: "",msgtime: "",msgaddress: ""};
router.post('/sendSmsNew', pttAuth.isClientAuthenticated, function (req, response, next) {
    let nbrs = req.body.phoneNbrs;
    let signNames = [];
    let paramJsons = [];
    for (let i = 0; i < nbrs.length; i++) {
        signNames.push('洪水风险管理系统');
        paramJsons.push({
            "name": req.body.phoneNames[i],
            "infoTime": req.body.msgtime,
            "infoAddress": req.body.msgaddress,
            "infoText": req.body.msgtext
        });
    }

    client.sendBatchSMS({
        PhoneNumberJson: JSON.stringify(req.body.phoneNbrs),
        SignNameJson: JSON.stringify(signNames),
        TemplateCode: req.body.msgtype == "1" ? 'SMS_137666844' : 'SMS_138064899',
        TemplateParamJson: JSON.stringify(paramJsons)
    }).then(function (res) {
        let {Code}=res;
        console.log("Code : " + Code);
        if (Code === 'OK') {
            //处理返回参数
            console.log(res);
            response.json({status: 200});
        }
    }, function (err) {
        console.log(err);
        response.json({status: 500});
    })
});


//发送短信息
router.post('/sendSms', pttAuth.isClientAuthenticated, function (req, response, next) {
    console.log(req.body.phoneNbrs);
    console.log(req.body.phoneNames);
    console.log(req.body.msgtext);
    let nbrs = req.body.phoneNbrs;
    let signNames = [];
    let paramJsons = [];
    for (let i = 0; i < nbrs.length; i++) {
        signNames.push('洪水风险管理系统');
        paramJsons.push({
            "name": req.body.phoneNames[i],
            "siteName": "云通信",
            "content": req.body.msgtext
        });
    }

    client.sendBatchSMS({
        PhoneNumberJson: JSON.stringify(req.body.phoneNbrs),
        SignNameJson: JSON.stringify(signNames),
        TemplateCode: 'SMS_135035676',
        TemplateParamJson: JSON.stringify(paramJsons)
    }).then(function (res) {
        let {Code}=res;
        console.log("Code : " + Code);
        if (Code === 'OK') {
            //处理返回参数
            console.log(res);
            response.json({status: 200});
        }
    }, function (err) {
        console.log(err);
        response.json({status: 500});
    })
});

//查询历史水位数据
router.post('/queryHistory', pttAuth.isClientAuthenticated, function (req, res, next) {
    let sites = req.body.sites;
    let sqlCond = "siteid in ('" + sites[0] + "'";
    let top = false;
    let oneDayLoop = 0;

    console.log(JSON.stringify(req.body));

    //let curTime =

    if (cmoment(parseInt(req.body.st)).diff(cmoment(cmoment.now()).utc("+08:00"), "day") == 0) {
        oneDayLoop = 0 - cmoment(parseInt(req.body.st)).diff(cmoment(cmoment.now()).utc("+08:00"), "hours");
    } else {
        oneDayLoop = 23;
    }
    console.log(oneDayLoop + "  oneDayLoop");
    if (sites.length > 0) {
        for (let i = 1; i < sites.length; i++) {
            sqlCond += ",'" + sites[i] + "'";
        }
    }
    sqlCond = sqlCond += ")";
    if (req.body.st && req.body.et) {
        let et = parseInt(req.body.et) + 24 * 3600 * 1000;
        //sqlCond += " and b.createtime > " + req.body.st + " and b.createtime < " + req.body.et;
        sqlCond += " and b.createtime > " + req.body.st + " and b.createtime < " + et;
    } else {
        top = true;
    }
    let querySql = "select a.longitude, a.latitude, a.name, a.description, a.fiveh, a.tenh, a.twentyh, a.thirtyh, a.fiftyh, " +
        "b.* from sites a, contents b where a.id = b.siteid and " + sqlCond + " order by siteid, createtime ";
    console.log(querySql);
    co(function *() {
        let rows = yield p.query(querySql);

        if(rows.length == 0){
            res.json({status: 200, data: {}});
            return;
        }

        if (top) {
            rows = _.reverse(rows);
        }
        let curSiteId = rows[0].siteid;
        let curSiteName = rows[0].name;
        let retData = {};
        let datas = [];
        let xCategory = [];
        let orgRows = [];
        let addCategory = false;

        if (req.body.qType == "m") {
            for (let i = 0; i < 30; i++) {
                let xDate = parseInt(req.body.st) + (i * 24 * 3600 * 1000) + (8 * 3600 * 1000);
                xCategory.push(cmoment(xDate).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss'))
            }
        } else {
            for (let i = 0; i <= oneDayLoop; i++) {
                let xDate = parseInt(req.body.st) + (i * 3600 * 1000);
                xCategory.push(cmoment(xDate).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss'))
            }
        }

        if (rows.length > 0) {
            let tmpAllRows = [];

            if (req.body.qType == "d") {
                for (let k in rows) {
                    orgRows.push({
                        siteid: rows[k].siteid,
                        createtime: cmoment(parseInt(rows[k].createtime)).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss'),
                        flow: parseFloat(rows[k].fiveh) + parseFloat(rows[k].flow)
                    })
                    if (curSiteId == rows[k].siteid) {
                        tmpAllRows.push(rows[k]);
                    } else {
                        let tmpHeightData = [];
                        for (let s = 0; s <= oneDayLoop; s++) {
                            let curTime = req.body.st + (s * 3600 * 1000);
                            for (let i = 0; i < tmpAllRows.length; i++) {
                                if (tmpAllRows[i].createtime >= curTime) {
                                    tmpHeightData.push(parseFloat(tmpAllRows[i].fiveh) + parseFloat(tmpAllRows[i].flow));
                                    break;
                                }
                            }
                        }
                        datas.push({
                            name: curSiteName,
                            siteId: curSiteId,
                            data: tmpHeightData
                        });

                        addCategory = true;
                        tmpAllRows = [];

                        curSiteId = rows[k].siteid;
                        curSiteName = rows[k].name;
                        tmpAllRows.push(rows[k]);
                    }
                }
                let tmpHeightData = [];
                for (let s = 0; s <= oneDayLoop; s++) {
                    let curTime = req.body.st + (s * 3600 * 1000);
                    for (let i = 0; i < tmpAllRows.length; i++) {
                        if (tmpAllRows[i].createtime >= curTime) {
                            tmpHeightData.push(parseFloat(tmpAllRows[i].fiveh) + parseFloat(tmpAllRows[i].flow));
                            break;
                        }
                    }
                }
                datas.push({
                    name: curSiteName,
                    siteId: curSiteId,
                    data: tmpHeightData
                });
            } else {
                oneDayLoop = 30;
                for (let k in rows) {
                    orgRows.push({
                        siteid: rows[k].siteid,
                        createtime: cmoment(parseInt(rows[k].createtime)).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss'),
                        flow: parseFloat(rows[k].fiveh) + parseFloat(rows[k].flow)
                    })
                    if (curSiteId == rows[k].siteid) {
                        tmpAllRows.push(rows[k]);
                    } else {
                        let tmpHeightData = [];
                        for (let s = 0; s <= oneDayLoop; s++) {
                            let curTime = parseInt(req.body.st) + (s * 24 * 3600 * 1000);
                            for (let i = 0; i < tmpAllRows.length; i++) {
                                if (tmpAllRows[i].createtime >= curTime) {
                                    tmpHeightData.push(parseFloat(tmpAllRows[i].fiveh) + parseFloat(tmpAllRows[i].flow));
                                    break;
                                }
                            }
                        }
                        datas.push({
                            name: curSiteName,
                            siteId: curSiteId,
                            data: tmpHeightData
                        });

                        addCategory = true;
                        tmpAllRows = [];

                        curSiteId = rows[k].siteid;
                        curSiteName = rows[k].name;
                        tmpAllRows.push(rows[k]);
                    }
                }
                let tmpHeightData = [];
                for (let s = 0; s <= oneDayLoop; s++) {
                    let curTime = parseInt(req.body.st) + (s * 24 * 3600 * 1000);
                    for (let i = 0; i < tmpAllRows.length; i++) {
                        if (tmpAllRows[i].createtime >= curTime) {
                            tmpHeightData.push(parseFloat(tmpAllRows[i].fiveh) + parseFloat(tmpAllRows[i].flow));
                            break;
                        }
                    }
                }
                datas.push({
                    name: curSiteName,
                    siteId: curSiteId,
                    data: tmpHeightData
                });
            }

            retData = {
                orgData: orgRows,
                xCat: xCategory,
                allDatas: datas
            }
        }
        res.json({status: 200, data: retData});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

//查询内涝点历史水位数据
router.post('/queryLoggingHistory', pttAuth.isClientAuthenticated, function (req, res, next) {
    let sites = req.body.sites;
    let sqlCond = "siteid in ('" + sites[0] + "'";
    let top = false;
    let oneDayLoop = 0;

    console.log(JSON.stringify(req.body));

    //let curTime =

    if (cmoment(parseInt(req.body.st)).diff(cmoment(cmoment.now()).utc("+08:00"), "day") == 0) {
        oneDayLoop = 0 - cmoment(parseInt(req.body.st)).diff(cmoment(cmoment.now()).utc("+08:00"), "hours");
    } else {
        oneDayLoop = 23;
    }
    console.log(oneDayLoop + "  oneDayLoop");
    if (sites.length > 0) {
        for (let i = 1; i < sites.length; i++) {
            sqlCond += ",'" + sites[i] + "'";
        }
    }
    sqlCond = sqlCond += ")";
    if (req.body.st && req.body.et) {
        let et = parseInt(req.body.et) + 24 * 3600 * 1000;
        //sqlCond += " and b.createtime > " + req.body.st + " and b.createtime < " + req.body.et;
        sqlCond += " and b.createtime > " + req.body.st + " and b.createtime < " + et;
    } else {
        top = true;
    }
    let querySql = "select a.longitude, a.latitude, a.name, a.description, a.fiveh, a.tenh, a.twentyh, a.thirtyh, a.fiftyh, " +
        "b.* from sites a, contents b where a.id = b.siteid and " + sqlCond + " order by siteid, createtime ";
    console.log(querySql);
    co(function *() {
        let rows = yield p.query(querySql);

        if(rows.length == 0){
            res.json({status: 200, data: {}});
            return;
        }

        if (top) {
            rows = _.reverse(rows);
        }
        let curSiteId = rows[0].siteid;
        let curSiteName = rows[0].name;
        let retData = {};
        let datas = [];
        let xCategory = [];
        let orgRows = [];
        let addCategory = false;

        if (req.body.qType == "m") {
            for (let i = 0; i < 30; i++) {
                let xDate = parseInt(req.body.st) + (i * 24 * 3600 * 1000) + (8 * 3600 * 1000);
                xCategory.push(cmoment(xDate).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss'))
            }
        } else {
            for (let i = 0; i <= oneDayLoop; i++) {
                let xDate = parseInt(req.body.st) + (i * 3600 * 1000);
                xCategory.push(cmoment(xDate).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss'))
            }
        }

        if (rows.length > 0) {
            let tmpAllRows = [];

            if (req.body.qType == "d") {
                for (let k in rows) {
                    orgRows.push({
                        siteid: rows[k].siteid,
                        createtime: cmoment(parseInt(rows[k].createtime)).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss'),
                        flow: parseFloat(rows[k].fiveh) + parseFloat(rows[k].flow)
                    })
                    if (curSiteId == rows[k].siteid) {
                        tmpAllRows.push(rows[k]);
                    } else {
                        let tmpHeightData = [];
                        for (let s = 0; s <= oneDayLoop; s++) {
                            let curTime = req.body.st + (s * 3600 * 1000);
                            for (let i = 0; i < tmpAllRows.length; i++) {
                                if (tmpAllRows[i].createtime >= curTime) {
                                    tmpHeightData.push(parseFloat(tmpAllRows[i].fiveh) + parseFloat(tmpAllRows[i].flow));
                                    break;
                                }
                            }
                        }
                        datas.push({
                            name: curSiteName,
                            siteId: curSiteId,
                            data: tmpHeightData
                        });

                        addCategory = true;
                        tmpAllRows = [];

                        curSiteId = rows[k].siteid;
                        curSiteName = rows[k].name;
                        tmpAllRows.push(rows[k]);
                    }
                }
                let tmpHeightData = [];
                for (let s = 0; s <= oneDayLoop; s++) {
                    let curTime = req.body.st + (s * 3600 * 1000);
                    for (let i = 0; i < tmpAllRows.length; i++) {
                        if (tmpAllRows[i].createtime >= curTime) {
                            tmpHeightData.push(parseFloat(tmpAllRows[i].fiveh) + parseFloat(tmpAllRows[i].flow));
                            break;
                        }
                    }
                }
                datas.push({
                    name: curSiteName,
                    siteId: curSiteId,
                    data: tmpHeightData
                });
            } else {
                oneDayLoop = 30;
                for (let k in rows) {
                    orgRows.push({
                        siteid: rows[k].siteid,
                        createtime: cmoment(parseInt(rows[k].createtime)).utcOffset("-00:00").format('YYYY-MM-DD HH:mm:ss'),
                        flow: parseFloat(rows[k].fiveh) + parseFloat(rows[k].flow)
                    })
                    if (curSiteId == rows[k].siteid) {
                        tmpAllRows.push(rows[k]);
                    } else {
                        let tmpHeightData = [];
                        for (let s = 0; s <= oneDayLoop; s++) {
                            let curTime = parseInt(req.body.st) + (s * 24 * 3600 * 1000);
                            for (let i = 0; i < tmpAllRows.length; i++) {
                                if (tmpAllRows[i].createtime >= curTime) {
                                    tmpHeightData.push(parseFloat(tmpAllRows[i].fiveh) + parseFloat(tmpAllRows[i].flow));
                                    break;
                                }
                            }
                        }
                        datas.push({
                            name: curSiteName,
                            siteId: curSiteId,
                            data: tmpHeightData
                        });

                        addCategory = true;
                        tmpAllRows = [];

                        curSiteId = rows[k].siteid;
                        curSiteName = rows[k].name;
                        tmpAllRows.push(rows[k]);
                    }
                }
                let tmpHeightData = [];
                for (let s = 0; s <= oneDayLoop; s++) {
                    let curTime = parseInt(req.body.st) + (s * 24 * 3600 * 1000);
                    for (let i = 0; i < tmpAllRows.length; i++) {
                        if (tmpAllRows[i].createtime >= curTime) {
                            tmpHeightData.push(parseFloat(tmpAllRows[i].fiveh) + parseFloat(tmpAllRows[i].flow));
                            break;
                        }
                    }
                }
                datas.push({
                    name: curSiteName,
                    siteId: curSiteId,
                    data: tmpHeightData
                });
            }

            retData = {
                orgData: orgRows,
                xCat: xCategory,
                allDatas: datas
            }
        }
        res.json({status: 200, data: retData});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});


// 设置水位站
router.post('/waterSiteSeting', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let updSql = "update sites set fiveh = '" + req.body.A
            + "', tenh='" + req.body.B
            + "', twentyh='" + req.body.C
            + "', thirtyh='" + req.body.D
            + "', fiftyh='" + req.body.E
            + "' where id = '" + req.body.id + "'";
        console.log(JSON.stringify(updSql));
        let rows = yield p.query(updSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

router.get('/getPoints/:year', function (req, res, next) {
    console.log(fYear);
    console.log(req.params.year);
    if (req.params.year == "5") {
        res.json({status: 200, data: {points: fYear, paths: fPath, bg: bgPoints}});
    } else if (req.params.year == "10") {
        res.json({status: 200, data: {points: tYear, paths: tPath, bg: bgPoints}});
    }   else if (req.params.year == "20") {
        res.json({status: 200, data: {points: twYear, paths: twPath, bg: bgPoints}});
    }else if (req.params.year == "50") {
        res.json({status: 200, data: {points: fifYear, paths: fifPath, bg: bgPoints}});
    }else if (req.params.year == "100") {
        res.json({status: 200, data: {points: huYear, paths: huPath, bg: bgPoints}});
    } else {
        res.json({status: 200, data: {points: [], paths: []}});
    }
});

// 水位内容校对
router.get('/outerJsonP', function (req, res, next) {
    let querySql = " select c.id as siteId, c.name as siteName, a.flow as siteFlow, c.type as siteType, " +
        "c.longitude as 'long', c.latitude as lat  from contents as a  , sites c where a.createtime = " +
        "(select max(b.createtime) from contents as b where a.siteid = b.siteid ) " +
        "and a.siteid = c.id ";
    if (req.query.siteId != "0") {
        querySql += " and a.siteid = '" + req.query.siteId + "'";
    }
    console.log(querySql);
    co(function *() {
        let rows = yield p.query(querySql);
        console.log(JSON.stringify(rows));
        data = JSON.stringify(rows);
        var callback = req.query.cb + '(' + data + ');';
        res.end(callback);
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 水位内容校对
router.post('/contentsUpd', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let updSql = "update contents set speed = '" + req.body.speed + "', flow='" + req.body.flow + "' where id = '" + req.body.id + "'";
        console.log(JSON.stringify(updSql));
        let rows = yield p.query(updSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 查询所有水位站或者内涝点
router.get('/queryContents', pttAuth.isClientAuthenticated, function (req, res, next) {
    console.log("select a.type, a.longitude, a.latitude, a.name, a.description, a.fiveh, a.tenh, a.twentyh, a.thirtyh, a.fiftyh, " +
        "b.* from sites a, contents b where a.id = b.siteid and " +
        "siteid = '" + req.query.siteid + "' order by createtime desc limit 200 ");
    co(function *() {
        let rows = yield p.query("select a.type, a.longitude, a.latitude, a.name, a.description, a.fiveh, a.tenh, a.twentyh, a.thirtyh, a.fiftyh, " +
            "b.* from sites a, contents b where a.id = b.siteid and" +
            " siteid = '" + req.query.siteid + "' order by createtime desc limit 200 ");
        //let retRows = [];
        //20190523李晓波要求取消内涝占加数字.
        for (let i in rows) {
            if(rows[i].type == "1"){
                rows[i].flow = parseFloat(rows[i].fiveh) + parseFloat(rows[i].flow);
            }else{
                rows[i].flow = 0.0 + parseFloat(rows[i].flow);
            }
        }
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 查询所有水位站或者内涝点
router.get('/querySitesById', pttAuth.isClientAuthenticated, function (req, res, next) {
    console.log("select * from sites where id = '" + req.query.id + "'");
    co(function *() {
        let rows = yield p.query("select * from sites where id = '" + req.query.id + "'");
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows[0]});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 查询所有水位站或者内涝点
router.get('/querySitesByType', pttAuth.isClientAuthenticated, function (req, res, next) {
    console.log('select * from sites where type = ' + req.query.type);
    let cond = req.query.type ? "and type = " + req.query.type : "";
    co(function *() {
        let rows = yield p.query('select * from sites where 1 =1 ' + cond + ' order by picurl');
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 列出所有图件-分页
router.get('/viewPlanRelMgt', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let pageStart = (req.query.page - 1) * 10;

        let querySql = "select a.*  from attachMent a, docRelAttach b " +
            "where a.id = b.attachid and b.docid = '" + req.query.docid + "'";

        let cnt = yield p.query("select count(*) as cnt from attachMent a, docRelAttach b " +
            "where a.id = b.attachid and b.docid = '" + req.query.docid + "'");

        querySql += " limit " + pageStart + ", 10";

        console.log(querySql);
        console.log(JSON.stringify(cnt));

        let rows = yield p.query(querySql);
        res.json({
            status: 200, data: {
                cnt: cnt[0].cnt,
                rows: rows
            }
        });
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 列出所有图件-分页
router.get('/planRelImgMgt', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let pageStart = (req.query.page - 1) * 10;
        let querySql = "select * from attachMent where 1=1 ";
        let condition = "";
        if (!Util.isNullOrSpace(req.query.name)) {
            condition += "and name like '%" + req.query.name + "%' ";
        }
        if (!Util.isNullOrSpace(req.query.imgType) && req.query.imgType != "0") {
            condition += "and type = '" + req.query.imgType + "' ";
        }
        if (!Util.isNullOrSpace(req.query.wpName)) {
            condition += "and waterPerson like '%" + req.query.wpName + "%' ";
        }
        let cnt = yield p.query('select count(*) as cnt from attachMent where 1=1 ' + condition);

        querySql += condition + "limit " + pageStart + ", 10";

        console.log(querySql);

        let rows = yield p.query(querySql);

        let queryImgs = "select attachid from docRelAttach where docid = '" + req.query.docid + "'";
        let relRows = yield p.query(queryImgs);

        console.log(queryImgs);

        res.json({
            status: 200, data: {
                cnt: cnt[0].cnt,
                rows: rows,
                relRows: relRows
            }
        });
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 预案关联图件
router.post('/planRelImg', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log(req.body.docId);
        let curPageSels = req.body.curPageSelected;
        let realSelected = req.body.realSelected;
        if (curPageSels.length > 0) {
            let delCond = "";
            //先删除当页已经选择了的数据
            for (var i in curPageSels) {
                delCond += "'" + curPageSels[i] + "',";
            }
            delCond = delCond.substr(0, delCond.length - 1);
            let delSql = "delete from docRelAttach where docid = '" + req.body.docId + "' and attachid in (" + delCond + ");";
            console.log(JSON.stringify(delSql));
            let rows = yield p.query(delSql);
        }
        //再将当页数据全面插入
        for (var i in realSelected) {
            let insertSql = "insert into docRelAttach values (" + ID.get() + ",'" + req.body.docId + "','" + realSelected[i] + "')";
            console.log(JSON.stringify(insertSql));
            yield p.query(insertSql);
        }

        res.json({status: 200});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 修改预案
router.post('/planMgtUpd', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let updSql = "update docuMent set name = '" + req.body.planName + "', docDesc='" + req.body.planDesc + "' where id = '" + req.body.id + "'";
        console.log(JSON.stringify(updSql));
        let rows = yield p.query(updSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 删除预案
router.post('/planMgtDel', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let delSql = "delete from docuMent where id = '" + req.body.id + "'";
        console.log(JSON.stringify(delSql));
        let rows = yield p.query(delSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 添加预案-分页
router.post('/planMgt', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {

        //planName: planName,
        //    planDesc: planDesc,
        //uploadFileName: $('#uploadFileName').val(),
        //    uploadFileExtName: $('#uploadFileName').val(),
        //    uploadFilePath: $('#uploadFileName').val()

        console.log(JSON.stringify(req.body));
        let insertSql = "insert into docuMent value (" + ID.get() + ",'" + req.body.planName + "','" + req.body.planDesc + "','" + req.body.uploadFileName + "','" + req.body.uploadFileExtName + "','3','" + req.body.uploadFilePath + "','','','" + Date.now() + "')";
        console.log(JSON.stringify(insertSql));
        let rows = yield p.query(insertSql)

        res.json({status: 200, data: {res: "success"}});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 列出所有的预案-分页
router.get('/planMgt', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {

        console.log(JSON.stringify(req.body));
        let querySql = "select * from docuMent ";
        console.log(JSON.stringify(querySql));
        let rows = yield p.query(querySql)
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 列出单页
router.get('/singleImgMgt', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let pageStart = (req.query.page - 1);

        let querySql = "select * from attachMent where 1=1 ";
        let condition = "";

        if (!Util.isNullOrSpace(req.query.imgType) && req.query.imgType != "0") {
            condition += "and type = '" + req.query.imgType + "' ";
        }

        if (!Util.isNullOrSpace(req.query.waterState)) {
            condition += "and latitude ='" + req.query.waterState + "' ";
        }
        let cnt = yield p.query('select count(*) as cnt from attachMent where 1=1 ' + condition);

        querySql += condition + "limit " + pageStart + ", 1";

        console.log(querySql);

        let rows = yield p.query(querySql);
        res.json({
            status: 200, data: {
                cnt: cnt[0].cnt,
                rows: rows
            }
        });
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 列出所有图件-分页
router.get('/imgMgt', pttAuth.isClientAuthenticated, function (req, res, next) {

    console.log(req.query.imgType);

    co(function *() {
        let pageStart = (req.query.page - 1) * 10;

        let querySql = "select * from attachMent where 1=1 ";
        let condition = "";
        if (!Util.isNullOrSpace(req.query.name)) {
            condition += "and name like '%" + req.query.name + "%' ";
        }
        if (!Util.isNullOrSpace(req.query.imgType) && req.query.imgType != "0") {
            condition += "and type = '" + req.query.imgType + "' ";
        }
        if (!Util.isNullOrSpace(req.query.wpName)) {
            condition += "and waterPerson like '%" + req.query.wpName + "%' ";
        }
        if (!Util.isNullOrSpace(req.query.waterState)) {
            condition += "and latitude ='" + req.query.waterState + "' ";
        }
        let cnt = yield p.query('select count(*) as cnt from attachMent where 1=1 ' + condition);

        querySql += condition + "limit " + pageStart + ", 10";

        console.log(querySql);

        let rows = yield p.query(querySql);
        res.json({
            status: 200, data: {
                cnt: cnt[0].cnt,
                rows: rows
            }
        });
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 添加图件-分页
router.post('/imgMgt', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {

        console.log(JSON.stringify(req.body));
        let images = req.body.images;

        for (var i in images) {
            let curImgPath = JSON.parse(images[i]);
            console.log("------" + curImgPath.data.fp);
            let insertSql = "insert into attachMent values (" + ID.get() + ",'" +
                req.body.imgMgtName + "','" + req.body.imgMgtType + "','" + curImgPath.data.fp + "','" + curImgPath.data.thumbFp + "','" + req.body.imgMgtWpName + "','long','" + req.body.imgMgtWater + "','" + Date.now() + "')";
            console.log(JSON.stringify(insertSql));
            let rows = yield p.query(insertSql)

        }

        res.json({status: 200, data: {res: "success"}});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 删除图件
router.post('/delImgMgt', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let delSql = "delete from attachMent where id = '" + req.body.id + "'";
        console.log(JSON.stringify(delSql));
        let rows = yield p.query(delSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 列出所有防汛负责人
router.get('/waterPerson', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {

        console.log('++++++++++++++++++mdb+++++++++++++++++++++');
        let rows = yield p.query('select * from waterPerson');
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 添加防汛负责人
router.post('/waterPerson', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let insertSql = "insert into waterPerson values (" + ID.get() + ",'" + req.body.wpName + "','" + req.body.wpPosition + "','" + req.body.wpNbr + "','" + req.body.wpEmgNbr + "', '" + Date.now() + "')";
        console.log(JSON.stringify(insertSql));
        let rows = yield p.query(insertSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 修改防汛负责人
router.post('/waterPersonUpd', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let updSql = "update waterPerson set name = '" + req.body.wpName + "', position='" + req.body.wpPosition + "', phoneNbr = '" + req.body.wpNbr + "',emergencyNbr = '" + req.body.wpEmgNbr + "' where id = '" + req.body.id + "'";
        console.log(JSON.stringify(updSql));
        let rows = yield p.query(updSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

// 删除防汛负责人
router.post('/waterPersonDel', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let delSql = "delete from waterPerson where id = '" + req.body.id + "'";
        console.log(JSON.stringify(delSql));
        let rows = yield p.query(delSql);
        console.log(JSON.stringify(rows));
        res.json({status: 200, data: rows});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

module.exports = router;