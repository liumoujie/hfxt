var express = require('express');
var router = express.Router();
var _ = require('lodash');
var pttAuth = require('../common/ptt-auth');
//pttAuth.isClientAuthenticated,
router.get('/lists', pttAuth.isClientAuthenticated, function (req, res, next) {
    console.log(req.query.id);
    res.render('history', {
        status: 200,
        siteId: req.query.id,
        st: req.query.st,
        et: req.query.et,
        type: req.query.qType
    });
});

router.get('/loggingLists', pttAuth.isClientAuthenticated, function (req, res, next) {
    console.log(req.query.id);
    res.render('loggingHistory', {
        status: 200,
        siteId: req.query.id,
        st: req.query.st,
        et: req.query.et,
        type: req.query.qType
    });
});

module.exports = router;