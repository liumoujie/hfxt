/**
 * Created by zhoulanhong on 4/26/16.
 */
var express = require('express');
var multer = require('multer');
var pttAuth = require('../common/ptt-auth');
var PttUtil = require('../common/ptt-util');

var Excel = require('exceljs');
var PttContactUser = require('../models/ptt-contact-user');
var PttEnterprise = require('../models/ptt-enterprise');
var PttNumberGenerator = require('../models/ptt-number-gennerator');
var co = require('co');
var workbook = new Excel.Workbook();

var router = express.Router();

var now = Date.now();

const MaxContactUserNumber = 100000;

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads');
    },
    filename: function (req, file, cb) {
        cb(null, now + file.originalname);
    }
})

var upload = multer({storage: storage});

function getHeadText(row, cellIdx) {
    switch (row.getCell(cellIdx).type) {
        case Excel.ValueType.Hyperlink:
            return row.getCell(cellIdx).value.text;
        case Excel.ValueType.RichText:
            var texts = row.getCell(cellIdx).value.richText;
            var retText = "";
            for (var i in texts) {
                retText += texts[i].text;
            }
            return retText;
        case Excel.ValueType.Formula:
            return row.getCell(cellIdx).result;
        case Excel.ValueType.Error:
            return row.getCell(cellIdx).value.error;
        default:
            return row.getCell(cellIdx).value;
    }
}

router.post('/importUsers', pttAuth.isAuthenticated, upload.single('userFile'), function (req, res, next) {

    var st = Date.now();

    if (!req.file.originalname.endsWith('.xlsx')) {
        res.json({status: 500, message: "只接受.xlsx文件"});
        return;
    }

    var rdExcel = Date.now();

    workbook.xlsx.readFile('uploads/' + now + req.file.originalname)
        .then(function (workbook) {

                var nowSt = Date.now();

                // use workbook
                var workSheet = workbook.getWorksheet(1);
                if (workSheet._rows.length > 5001 || workSheet._rows.length < 2) {
                    res.json({status: 500, message: "单次最多只能导入5000个用户，最少必须导入1条"});
                    return;
                }
                //check import data head
                var impHead = workSheet.getRow(1);
                if (getHeadText(impHead, 1) !== '用户名') {
                    res.json({status: 500, message: '导入数据的第一列必须是"用户名"'});
                    return;
                }
                if (getHeadText(impHead, 2) !== '密码') {
                    res.json({status: 500, message: '导入数据的第二列必须是"密码"'});
                    return;
                }
                if (getHeadText(impHead, 3) !== '手机号码') {
                    res.json({status: 500, message: '导入数据的第三列必须是"手机号码"'});
                    return;
                }
                if (getHeadText(impHead, 4) !== '电子邮箱') {
                    res.json({status: 500, message: '导入数据的第四列必须是"电子邮箱"'});
                    return;
                }
                if (getHeadText(impHead, 5) !== '权限等级') {
                    res.json({status: 500, message: '导入数据的第五列必须是"权限等级"'});
                    return;
                }
                if (getHeadText(impHead, 6) !== '发起临时组') {
                    res.json({status: 500, message: '导入数据的第六列必须是"发起临时组"'});
                    return;
                }
                if (getHeadText(impHead, 7) !== '发起单呼') {
                    res.json({status: 500, message: '导入数据的第七列必须是"发起单呼"'});
                    return;
                }
                if (getHeadText(impHead, 8) !== '接受单呼') {
                    res.json({status: 500, message: '导入数据的第八列必须是"接受单呼"'});
                    return;
                }
                if (getHeadText(impHead, 9) !== '接受临时组') {
                    res.json({status: 500, message: '导入数据的第九列必须是"接受临时组"'});
                    return;
                }
                if (getHeadText(impHead, 10) !== '发起外呼') {
                    res.json({status: 500, message: '导入数据的第十列必须是"发起外呼"'});
                    return;
                }
                if (getHeadText(impHead, 11) !== '接受外呼') {
                    res.json({status: 500, message: '导入数据的第十一列必须是"接受外呼"'});
                    return;
                }
                if (getHeadText(impHead, 12) !== '禁止发言') {
                    res.json({status: 500, message: '导入数据的第十二列必须是"禁止发言"'});
                    return;
                }

                var inserUsers = [];
                var totalLine = workSheet._rows.length;
                var curSize = 0;
                var telDupSet = new Set();
                var mailDupSet = new Set();
                co(function *() {
                    //取出企业的编号
                    var enterprise = yield PttEnterprise.findOne({_id: req.user._id}).exec();

                    if (!enterprise) {
                        res.json({status: 500, message: "通过" + req.user._id + "获取企业信息失败"});
                        return
                    }

                    var curUsers = yield PttContactUser.find({enterprise: req.user._id}).exec();

                    if (curUsers.length + totalLine - 1 > enterprise.quota.staff) {
                        res.json({status: 500, message: "仅能导入" + (enterprise.quota.staff - (curUsers.length)) + "个用户"});
                        return
                    }

                    var counter = yield PttNumberGenerator.getBatchNextContactUserNumber(
                        enterprise.idNumber, totalLine - 1
                    ).exec();

                    counter = counter._id * MaxContactUserNumber + counter.seq;

                    // 除去第一行标题栏
                    for (var i = 2; i < totalLine + 1; i++) {
                        var row = workSheet.getRow(i);
                        for (var j = 1; j < 13; j++) {
                            var cur = getHeadText(row, j);
                            //check content
                            if (j != 3 && j != 4) {
                                if (!cur || cur == 'undefined' || cur == 'null' || cur.length == 0) {
                                    //console.log('第' + i + '行，' + workSheet.getRow(1).values[j] + '不能为空');
                                    res.json({
                                        status: 500,
                                        message: '第' + i + '行，' + workSheet.getRow(1).values[j] + '不能为空'
                                    });
                                    return;
                                }
                            }

                            //deal with telephone number;
                            if (j == 3) {
                                //check if the telphone number according the format
                                //we only check three rules currently
                                // first is all data must be numbers
                                // second is the length need equal 11
                                // third is the first number must be 1
                                if (cur) {
                                    if (!PttUtil.checkPhone(cur)) {
                                        res.json({status: 500, message: '第' + i + '行，电话号码：' + cur + '不符合规则'});
                                        return;
                                    }

                                    //check if the import file has the same phone number data
                                    curSize = telDupSet.size;
                                    telDupSet.add(cur);
                                    if (telDupSet.size == curSize) {
                                        res.json({status: 500, message: '第' + i + '行，电话号码：' + cur + '在导入表中重复'});
                                        return;
                                    }

                                    var phone = yield PttContactUser.findOne({phoneNbr: cur}).exec();

                                    if (phone) {
                                        res.json({status: 500, message: '第' + i + '行，电话号码：' + cur + '重复'});
                                        return;
                                    }

                                }
                            }
                            // deal with email
                            if (j == 4) {
                                if (cur) {
                                    if (!PttUtil.checkMail(cur)) {
                                        res.json({status: 500, message: '第' + i + '行，电子邮箱：' + cur + '不符合规则'});
                                        return;
                                    }

                                    curSize = mailDupSet.size;
                                    mailDupSet.add(cur);
                                    if (mailDupSet.size == curSize) {
                                        res.json({status: 500, message: '第' + i + '行，电子邮箱：' + cur + '在导入表中重复'});
                                        return;
                                    }

                                    var mail = yield PttContactUser.findOne({mail: cur}).exec();

                                    if (mail) {
                                        res.json({status: 500, message: '第' + i + '行，电子邮箱：' + cur + '重复'});
                                        return;
                                    }

                                }
                            }
                            //check priority level
                            if (j == 5 && (['1', '2', '3', '4', '5', '6', '7', '8', '9'].indexOf("" + cur) < 0)) {
                                ////console.log('第' + i + '行，"' + workSheet.getRow(1).values[j] + '"值只能是Y或者N');
                                res.json({
                                    status: 500,
                                    message: '第' + i + '行"' + workSheet.getRow(1).values[j] + '"值只能是1-9级,当前值' + cur
                                });
                                return;
                            }
                            //check priority value
                            if (j > 5 && !(cur == 'Y' || cur == 'N')) {
                                //console.log('第' + i + '行，"' + workSheet.getRow(1).values[j] + '"值只能是Y或者N');
                                res.json({
                                    status: 500,
                                    message: '第' + i + '行"' + workSheet.getRow(1).values[j] + '"值只能是Y或者N，当前值' + cur
                                });
                                return;
                            }
                        }

                        inserUsers[inserUsers.length] = {
                            idNumber: counter--,
                            phoneNbr: getHeadText(row, 3),
                            mail: getHeadText(row, 4),
                            //权限等级,发起临时组,发起单呼,接受单呼,接受临时组,发起外呼,接受外呼,禁止发言
                            name: getHeadText(row, 1),
                            enterprise: req.user._id,
                            password: PttUtil.md5(getHeadText(row, 2) + ""),
                            privileges: {
                                callAble: (getHeadText(row, 7) == 'Y'),//发起单呼
                                groupAble: (getHeadText(row, 6) == 'Y'),//发起群呼
                                calledAble: (getHeadText(row, 8) == 'Y'),//接受呼
                                joinAble: (getHeadText(row, 9) == 'Y'),//接受群呼[比较特殊,仅用于在会话组添加成员的时候禁止
                                forbidSpeak: (getHeadText(row, 12) == 'Y'),//是否允许发言
                                callOuterAble: (getHeadText(row, 10) == 'Y'),//是否可以呼其它单位
                                calledOuterAble: (getHeadText(row, 11) == 'Y'),//是否可以被其它单位呼.
                                priority: getHeadText(row, 5)
                            },
                            impstatus: "init"
                        };
                    }

                    console.log("--- will insert "+JSON.stringify(inserUsers));

                    yield PttContactUser.insertMany(inserUsers);

                    //导入成功后，将所有 {impstatus:"init"}的，修改为空，标志数据上传成功
                    yield PttContactUser.update({
                        enterprise: req.user._id,
                        impstatus: "init"
                    }, {$set: {impstatus: "suc"}}, {multi: true});

                    res.json({status: 200, message: "成功导入 " + inserUsers.length + " 条数据"});

                    //更新通讯录状态
                    yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

                    return;

                }).catch(function (coe) {

                    console.log(coe);
                    co(function * () {
                        //出现异常，删除所有刚才导入的数据，fileter {impstatus:"init"}
                        yield PttContactUser.remove({enterprise: req.user._id, impstatus: "init"});
                        if (coe.code === 11000) {
                            var repNumber = JSON.parse(JSON.stringify(coe));
                            //console.log(repNumber.op);
                            res.json({status: 500, message: "电话号码：" + repNumber.op.phoneNbr + "重复"});
                        } else {
                            res.json({status: 500, message: "系统错误，请记住操作步骤，并联系管理员。错误内容：" + coe});
                        }
                        return;
                    });
                });
            }
        ).catch(function (e) {
        console.log(e);
        res.json({status: 500, message: "系统错误，请记住操作步骤，并联系管理员。错误内容：" + e});
        return;
    });

});

module.exports = router;