/**
 * Created by zhoulanhong on 5/15/18.
 */
var express = require('express');
var router = express.Router();
var _ = require('lodash');
var pttAuth = require('../common/ptt-auth');
var http = require('http');
//pttAuth.isClientAuthenticated,

//打开屏幕
router.post('/pon', pttAuth.isClientAuthenticated, function (req, response, next) {

    console.log(JSON.stringify(req.body));
    let vpostData = JSON.stringify(req.body);
    var options = {
        hostname: "localhost",
        port: "9090",
        path: "/on",
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': vpostData.length
        }
    };
    JSON.stringify(JSON.stringify(options));
    var body = "";
    var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on('end', function () {
            response.json({status: 200, data: {ret: "ok"}});
        });
    });

    req.on('error', function (e) {
        response.json({status: 500, message: JSON.stringify(e)})
    });

    req.write(vpostData);
    req.end();

});

//关闭屏幕
router.post('/poff', pttAuth.isClientAuthenticated, function (req, response, next) {

    console.log(JSON.stringify(req.body));
    let vpostData = JSON.stringify(req.body);
    var options = {
        hostname: "localhost",
        port: "9090",
        path: "/off",
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': vpostData.length
        }
    };
    JSON.stringify(JSON.stringify(options));
    var body = "";
    var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on('end', function () {
            response.json({status: 200, data: {ret: "ok"}});
        });
    });

    req.on('error', function (e) {
        response.json({status: 500, message: JSON.stringify(e)})
    });

    req.write(vpostData);
    req.end();

});


//发送消息
router.post('/sendAll', pttAuth.isClientAuthenticated, function (req, response, next) {

    let utf8Length =function (str) {
        var realLength = 0;
        var len = str.length;
        var charCode = -1;
        for (var i = 0; i < len; i++) {
            charCode = str.charCodeAt(i);
            if (charCode >= 0 && charCode <= 128) {
                realLength += 1;
            } else {
                // 如果是中文则长度加3
                realLength += 3;
            }
        }
        return realLength;
    }

    let allScreenIds = ['240305003095877','240305003095841','240305003095837',
        '240305003095871','240305003095868','240305003095838','240305003095880',
        '240305003095843','240305003095840'];
    let body = req.body;
    let reqs = [];
    for(let i =0 ; i < allScreenIds.length; i ++){
        let curReq = JSON.parse(JSON.stringify(body));
        curReq.imei = allScreenIds[i];
        reqs[i] = curReq;
    }

    let count = 0;
    for(let i = 0 ; i < allScreenIds.length;  i ++){

        let vpostData =JSON.stringify(reqs[i]);

        var options = {
            hostname: "localhost",
            port: "9090",
            path: "/send",
            method: 'POST',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': utf8Length(vpostData)
            }
        };
        console.log(options);
       var req = http.request(options, function (res) {
            res.setEncoding('utf8');
            res.on('data', function (chunk) {
                body += chunk;
            });
            res.on('end', function () {
                count ++;
                if(count == allScreenIds.length){
                    response.json({status: 200, data: {ret: "ok"}});
                }
            });
        });

        req.on('error', function (e) {
            count ++;
            if(count == allScreenIds.length){
                response.json({status: 500, message: JSON.stringify(e)})
            }
        });
        req.write(vpostData);
        req.end();
        console.log(allScreenIds[i] + "----- submitted.");
    }
});


//发送消息
router.post('/send', pttAuth.isClientAuthenticated, function (req, response, next) {

    let utf8Length =function (str) {
        var realLength = 0;
        var len = str.length;
        var charCode = -1;
        for (var i = 0; i < len; i++) {
            charCode = str.charCodeAt(i);
            if (charCode >= 0 && charCode <= 128) {
                realLength += 1;
            } else {
                // 如果是中文则长度加3
                realLength += 3;
            }
        }
        return realLength;
    }

    console.log(JSON.stringify(req.body));
    let vpostData = JSON.stringify(req.body);
    var options = {
        hostname: "localhost",
        port: "9090",
        path: "/send",
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': utf8Length(vpostData)
        }
    };
    console.log(vpostData);
    var body = "";
    var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on('end', function () {
            response.json({status: 200, data: {ret: "ok"}});
        });
    });

    req.on('error', function (e) {
        response.json({status: 500, message: JSON.stringify(e)})
    });

    req.write(vpostData);
    req.end();

});

//{"imei":"240305003095877","fontSize":"16","fontColor":1,"content":"欢迎来到资阳市欢迎来到资阳市"}

//清除屏幕
router.post('/clear', pttAuth.isClientAuthenticated, function (req, response, next) {

    console.log(JSON.stringify(req.body));
    let vpostData = JSON.stringify(req.body);
    var options = {
        hostname: "localhost",
        port: "9090",
        path: "/clear",
        method: 'POST',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': vpostData.length
        }
    };
    JSON.stringify(JSON.stringify(options));
    var body = "";
    var req = http.request(options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            body += chunk;
        });
        res.on('end', function () {
            response.json({status: 200, data: {ret: "ok"}});
        });
    });

    req.on('error', function (e) {
        response.json({status: 500, message: JSON.stringify(e)})
    });

    req.write(vpostData);
    req.end();

});

module.exports = router;