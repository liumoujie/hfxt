var express = require('express');
var router = express.Router();
var _ = require('lodash');

var appUrl = process.env.PROD_APP_URL || 'http://'+process.env['SIGNAL_SERVER_URL']+':10005/app/latest/';

module.exports = function (passport) {

    router.get('/test', function (req, res, next) {
        res.render('logging', {
            status: 500,
            appUrl: appUrl,
            version: process.env['VERSION']
        });
    });

    return router;
}