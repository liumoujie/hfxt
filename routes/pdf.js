/**
 * Created by zhoulanhong on 5/15/18.
 */
var express = require('express');
var router = express.Router();
var _ = require('lodash');
var pttAuth = require('../common/ptt-auth');
//pttAuth.isClientAuthenticated,
router.get('/show', pttAuth.isClientAuthenticated, function (req, res, next) {
    console.log(req.query.fname);
    res.render('showPdf', {
        status: 200,
        fname: req.query.fname
    });
});

module.exports = router;