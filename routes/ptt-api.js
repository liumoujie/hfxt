/**
 * Created by suke on 15/12/8.
 */
var express = require('express');
var co = require('co');
var PttContactUser = require('../models/ptt-contact-user');
var PttNumberGenerator = require('../models/ptt-number-gennerator');
var PttEnterprise = require('../models/ptt-enterprise');
var PttMessage = require('../models/ptt-message');
var PttAdministrator = require('../models/ptt-administrator');
var PttContactGroup = require('../models/ptt-contact-group');
var PttDepartment = require("../models/ptt-enterprise-department");
var PttPrivilege = require("../models/ptt-privilege");
var PttPosition = require("../models/ptt-position");
var PttChatRoom = require("../models/ptt-chat-room");
const PttGisLocation = require("../models/ptt-gis-location");
const PttMarker = require("../models/ptt-marker");
const LocationHandler = require('../signal/location');
var PttToken = require('../models/ptt-token');
var PttAudit = require('../models/ptt-audit');
var cmoment = require('moment');
var mongoose = require('mongoose');

var PttUtil = require('../common/ptt-util');
var pttAuth = require('../common/ptt-auth');
var _ = require('lodash');
var Promise = require('bluebird');
var Excel = require('exceljs');
var path = require('path');
var log = require('loglevel');

//一个企业中通讯录组的总数不能超过该数.
const MaxContactGroupNumber = 10000000;
//一个企业中用户号码的个数不能超这个数.
const MaxContactUserNumber = 100000;

const crypto = require('crypto');
const secret = 'pttsignalserver';

var router = express.Router();

//重要://///////////////////////////////////////
//                                           //
//  req.user这儿保存的就是当前登录进行管理的企业  //
//                                           //
///////////////////////////////////////////////

//测试用,用于在数据库中生成企业,管理账号.
router.get('/init', function (req, res, next) {

    co(function *() {
            let ret = {};
            //初始化超级管理员企业帐号
            let enterprise = yield PttEnterprise.findOne({idNumber: 0}).exec();

            if (enterprise) {
                //return res.json({message: "have been initialized."})
                ret.enterprise = enterprise;
            } else {
                //兼容上一个版本已有的企业,自动补一个密码全为0的入口项
                yield PttEnterprise.update({}, {'$set': {'managerPwd': PttUtil.md5("000000")}}, {multi: true}).exec();

                //创建一个企业.
                var tmpEnterprise = new PttEnterprise;
                tmpEnterprise.name = "onlyForSuperManager";
                tmpEnterprise.phoneNumber = "18888888888";
                tmpEnterprise.idNumber = 0;
                tmpEnterprise.managerPwd = PttUtil.md5("000000");

                yield tmpEnterprise.save();
                ret.enterprise = tmpEnterprise;
            }

            //初始化权限项
            let privileges = yield PttPrivilege.find().exec();
            if (privileges.length > 0) {
                ret.privileges = privileges;
            } else {
                let tmpPrivs = [{
                    code: "userManage",
                    name: "用户权限",
                    description: "用于管理用户权限"
                }, {
                    code: "systemInfo",
                    name: "系统信息",
                    description: "显示系统信息"
                }, {
                    code: "positionManage",
                    name: "职位管理",
                    description: "用户进行职位管理权限操作"
                }, {
                    code: "locationManage",
                    name: "定位管理",
                    description: "允许企业自定义员工的定位时间"
                }, {
                    code: "groupManage",
                    name: "群组管理",
                    description: "允许用户操作群组"
                }, {
                    code: "gisManage",
                    name: "位置管理",
                    description: "允许用户管理位置"
                }, {
                    code: "departmentManage",
                    name: "部门管理",
                    description: "允许用户进行部门管理"
                }, {
                    code: "auditManage",
                    name: "日志管理",
                    description: "允许用户管理日志"
                }];
                yield PttPrivilege.insertMany(tmpPrivs);
                ret.privileges = tmpPrivs;
            }
            res.json(
                {
                    status: 200,
                    ret: ret
                }
            );

        }
    ).catch(function (err) {
        res.json(err);
    });
});

//生成PTT用户的号码
router.get('/contact/usernumber', pttAuth.isAuthenticated, function (req, res, next) {

    co(function *() {
        //取出企业的编号
        var enterprise = yield PttEnterprise.findOne({_id: req.user._id}).exec();

        if (!enterprise)
            return res.json({status: 500});

        //取企业下一个号码
        var counter = yield PttNumberGenerator.getNextContactUserNumber(enterprise.idNumber).exec();

        if (!counter)
            return res.json({status: 500});

        res.json({status: 200, number: counter._id * MaxContactUserNumber + counter.seq});
    });

});

//========================================= *标记相关代码开始* =================================================
//新建一个标记
router.post('/marker', pttAuth.isAuthenticated, function (req, res, next) {
//router.post('/marker', function (req, res, next) {
    co(function *() {
        let saveRet = null;
        var marker = new PttMarker(req.body);
        marker.enterprise = req.user._id;

        var fileIds = [];
        for (var i in req.body.images) {
            var names = req.body.images[i].split('\/');
            var fileId = names[names.length - 5] + "/" +
                names[names.length - 4] + "/" +
                names[names.length - 3] + "/" +
                names[names.length - 2] + "/" +
                names[names.length - 1];
            fileIds.push(fileId);
        }
        marker.images = fileIds;

        yield saveRet = marker.save();
        console.log(JSON.stringify(saveRet));
        res.json({status: 200, marker: saveRet.emitted.fulfill[0]});
    }).catch(function (err) {
        res.json({status: 500, message: err.message});

    });
});

//修改一个标记
router.post('/updMarker', pttAuth.isAuthenticated, function (req, res, next) {
//router.post('/updMarker', function (req, res, next) {
    co(function *() {

        var fileIds = [];
        for (var i in req.body.images) {
            var names = req.body.images[i].split('\/');
            var fileId = names[names.length - 5] + "/" +
                names[names.length - 4] + "/" +
                names[names.length - 3] + "/" +
                names[names.length - 2] + "/" +
                names[names.length - 1];
            fileIds.push(fileId);
        }

        var ret = yield PttMarker.findOneAndUpdate({_id: req.body._id}, {
            $set: {
                name: String,
                description: req.body.description,
                type: req.body.type,
                location: req.body.location,
                images: fileIds
            }

        }, {new: true}).exec();


        res.json({status: 200});
    }).catch(function (err) {
        res.json({status: 500, message: err.message});

    });
});

router.get('/marker', pttAuth.isAuthenticated, function (req, res, next) {
    PttMarker.find({enterprise: req.user._id})
        .sort({code: -1})
        .exec(function (err, data) {
            if (err) {
                res.json({status: 500, message: err.message});
            } else {
                for (var i in data) {
                    var tmpImgUrl = [];
                    for (var j in data[i].images) {
                        //tmpImgUrl.push("http://" + process.env['FDFS_IMAGE_SERV_URL'] + ":" + process.env['FDFS_IMAGE_SERV_PORT'] + "/" + data[i].images[j])
                        tmpImgUrl.push("http://" + req.hostname + ":" + process.env['FDFS_IMAGE_SERV_PORT'] + "/" + data[i].images[j])
                    }
                    data[i].images = tmpImgUrl;
                    console.log("-------marker list---------" + JSON.stringify(data[i]));
                }
                res.json({status: 200, data: data});
            }
        });
});

router.post('/delMarker', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
        yield PttMarker.remove({_id: req.body._id}).exec();
        return res.json({status: 200});
    }).catch(function (err) {
        res.json({status: 500, message: err.message});
    });
});


//========================================= *权限相关代码开始* =================================================
//列出当前所有的权限
router.get('/privilege', pttAuth.isAuthenticated, function (req, res, next) {

    PttPrivilege.find()
        .sort({code: -1})
        .exec(function (err, data) {
            if (err) {
                res.json({status: 500, message: err.message});
            } else {
                res.json({status: 200, data: data});
            }
        });
});

//列出当前所有的权限,并标识出当前企业已经拥有的权限
router.get('/enpPrivilege/:idNumber', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {

        //查找当前企业下的所有权限
        var enterprise = yield PttEnterprise.findOne({idNumber: req.params.idNumber}).exec();

        //查找企业下的所有成员
        var privilege = yield PttPrivilege.find().exec();

        var data = [];

        //标识出来已是成员
        for (var i = 0; i < privilege.length; i++) {

            var item = {
                code: privilege[i].code,
                name: privilege[i].name,
                desc: privilege[i].desc
            };

            if (enterprise.privileges.indexOf(privilege[i].code) < 0) {
                item.hasPrivilege = false;
            } else {
                item.hasPrivilege = true;
            }

            data.push(item);
        }

        //输出结果
        res.json({'status': 200, 'data': data});

    }).catch(function (err) {
        console.log(err);
        return res.json({status: 500, message: err.message});
    });

});

//新建一个权限
router.post('/privilege', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {

        var privilege = new PttPrivilege(req.body);

        yield privilege.save();

        res.json({status: 200});
    }).catch(function (err) {
        try {
            var repNumber = JSON.parse(JSON.stringify(err));
            if (repNumber.code === 11000) {
                res.json({status: 500, message: "权限编码：" + req.body.code + " 已存在"});
                return;
            }
        } catch (e) {

        }
        res.json({status: 500, message: err.message});

    });
});

//编辑一个权限
router.post('/privilegeUpd', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {

        var ret = yield PttPrivilege.findOneAndUpdate({_id: req.body._id}, {
            $set: {
                code: req.body.code,
                name: req.body.name,
                description: req.body.description
            }
        }, {new: true}).exec();

        return res.json({status: 200});

    }).catch(function (err) {
        try {
            var repNumber = JSON.parse(JSON.stringify(err));
            if (repNumber.code === 11000) {
                res.json({status: 500, message: "权限编码：" + req.body.code + " 已存在"});
                return;
            }
        } catch (e) {

        }
        res.json({status: 500, message: err.message});

    });
});

//删除一个权限
router.post('/privilegeDelete', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {

        var exist = yield PttEnterprise.findOne({privileges: req.body.privilege}).exec();

        if (exist) {
            return res.json({status: 500, message: '企业' + exist.idNumber + '拥有此权限,请先取消企业此权限再删除.'});
        }

        var ret = yield PttPrivilege.remove({_id: req.body._id}).exec();
        return res.json({status: 200});

    }).catch(function (err) {
        res.json({status: 500, message: err.message});
    });
});

// 为企业设置一个权限
router.put('/privilege', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {

        var enterprise = yield PttEnterprise.findOne({idNumber: req.body.idNumber}).exec();

        var minusPrivilege = _.difference(enterprise.privileges, req.body.cancels, [NaN]);
        var addPrivilege = _.union(minusPrivilege, req.body.members);

        var ret = yield PttEnterprise.findOneAndUpdate({idNumber: req.body.idNumber}, {$set: {privileges: addPrivilege}}, {new: true}).exec();

        res.json({status: 200});
    }).catch(function (err) {
        res.json({status: 500, message: err.message});
    })
});
//========================================= *权限相关代码结束* =================================================


//========================================= *职位相关代码开始* =================================================
//列出当前所有的职位
router.get('/position', pttAuth.isAuthenticated, function (req, res, next) {

    PttPosition.find({enterprise: req.user._id})
        .sort({code: -1})
        .exec(function (err, data) {
            if (err) {
                res.json({status: 500, message: err.message});
            } else {

                for (var i in data) {

                    //data[i].icon = "http://" + process.env['FDFS_IMAGE_SERV_URL'] + ":" + process.env['FDFS_IMAGE_SERV_PORT'] + "/" + data[i].image;
                    data[i].icon = "http://" + req.hostname + ":" + process.env['FDFS_IMAGE_SERV_PORT'] + "/" + data[i].image;
                    console.log("----" + JSON.stringify(data[i]));
                }

                res.json({status: 200, data: data});
            }
        });
});

//新建一个职位
router.post('/position', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
        var position = new PttPosition(req.body);
        if (req.body.icon) {
            var icon = req.body.icon;
            var names = icon.split('\/');
            position.image = names[names.length - 5] + "/" +
                names[names.length - 4] + "/" +
                names[names.length - 3] + "/" +
                names[names.length - 2] + "/" +
                names[names.length - 1];
        }
        position.enterprise = req.user._id;
        yield position.save();

        res.json({status: 200});
    }).catch(function (err) {
        try {
            var repNumber = JSON.parse(JSON.stringify(err));
            if (repNumber.code === 11000) {
                res.json({status: 500, message: "职位名称：" + req.body.name + " 已存在"});
                return;
            }
        } catch (e) {

        }
        res.json({status: 500, message: err.message});

    });
});

//编辑一个职位
router.post('/positionUpd', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {

        console.log(req.body);
        var updData = {
            icon: req.body.icon,
            name: req.body.name,
            description: req.body.description,
            enterprise: req.user._id
        };

        if (req.body.icon) {
            var icon = req.body.icon;
            var names = icon.split('\/');
            updData.image = names[names.length - 5] + "/" +
                names[names.length - 4] + "/" +
                names[names.length - 3] + "/" +
                names[names.length - 2] + "/" +
                names[names.length - 1];
        }

        var ret = yield PttPosition.findOneAndUpdate({_id: req.body._id}, {
            $set: updData
        }, {new: true}).exec();

        return res.json({status: 200});

    }).catch(function (err) {
        try {
            var repNumber = JSON.parse(JSON.stringify(err));
            if (repNumber.code === 11000) {
                res.json({status: 500, message: "职位名称：" + req.body.name + " 已存在"});
                return;
            }
        } catch (e) {

        }
        res.json({status: 500, message: err.message});

    });
});

//删除一个职位
router.post('/positionDelete', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
        var exist = yield PttContactUser.findOne({position: req.body.position}).exec();

        if (exist) {
            // return res.json({status: 500, message: '员工' + exist.name + '拥有此权限,请先为员工重设职位再删除.'});
            return res.json({status: 500, message: '该职位有员工任职，请为员工重设职位后删除.'});
        }

        var ret = yield PttPosition.remove({_id: req.body.position}).exec();
        return res.json({status: 200});

    }).catch(function (err) {
        res.json({status: 500, message: err.message});
    });
});

//========================================= *职位相关代码结束* =================================================


//列出当前组织的所有用户
router.get('/contact/user', pttAuth.isAuthenticated, function (req, res, next) {

    PttContactUser.find({enterprise: req.user._id})
        .sort({_id: -1})
        .exec(function (err, data) {
            if (err) {
                res.json({status: 500, message: err.message});
            } else {
                res.json({status: 200, data: data});
            }
        });
});

//列出当前组织的指挥帐户
router.get('/contact/commander', pttAuth.isAuthenticated, function (req, res, next) {

    PttContactUser.findOne({enterprise: req.user._id, commander: 1})
        .exec(function (err, data) {
            if (err) {
                res.json({status: 500, message: err.message});
            } else {
                res.json({status: 200, data: data});
            }
        });

});


//实时追踪用户
router.post('/contact/realTrace', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
        if (req.body.idNumber.length > 0) {

            let retUpd = {
                locationEnable: true,
                locationScanInterval: 5000,
                locationReportInterval: 5000,
            };
            if (req.body.type == "cancel") {
                retUpd = {
                    locationEnable: false,
                    locationScanInterval: 0,
                    locationReportInterval: 0,
                };
            }

            yield PttContactUser.update({idNumber: {$in: req.body.idNumber}}, {
                $set: retUpd
            }, {new: true}).exec();

            // 将用户信息发送给用户以进行更新
            for (let i in req.body.idNumber) {
                process.signalServer.onUserUpdated(req.body.idNumber[i]);
            }
        }
        res.json({status: 200});

    }).catch(function (err) {
        log.info(err);
        res.json({status: 500, message: err.message});
    });
});

//修改一个用户的定位设置信息
router.post('/contact/updUserLocateSet', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
            let updValue = {
                locationWeekly: req.body.locationWeekly,
                locationTime: req.body.locationTime,
                locationScanInterval: req.body.locationScanInterval,
                locationReportInterval: req.body.locationReportInterval,
                locationEnable: req.body.locationEnable
            }
            //if (!req.body.locationEnable) {
            //    updValue = {
            //        locationEnable: req.body.locationEnable
            //    }
            //}
            yield PttContactUser.update({idNumber: {$in: req.body.idNumber}}, {
                $set: updValue
            }, {new: true, multi: true}).exec();

            //更新通讯录状态
            yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

            // 将用户信息发送给用户以进行更新
            process.signalServer.onUserUpdated(req.body.idNumber);

            //yield user.save();
            res.json({status: 200});

        }
    ).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: err.message});
    });
});

//修改一个用户的定位设置和权限设置信息
router.post('/contact/gisUpdUserLocate', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
            let updValue = {
                locationWeekly: req.body.locationWeekly,
                locationTime: req.body.locationTime,
                locationScanInterval: req.body.locationScanInterval,
                locationReportInterval: req.body.locationReportInterval,
                locationEnable: req.body.locationEnable,
                privileges: req.body.privileges
            }

            console.log(JSON.stringify(updValue));

            yield PttContactUser.update({idNumber: {$in: req.body.idNumber}}, {
                $set: updValue
            }, {new: true, multi: true}).exec();

            //更新通讯录状态
            yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

            // 将用户信息发送给用户以进行更新
            process.signalServer.onUserUpdated(req.body.idNumber);

            res.json({status: 200});

        }
    ).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: err.message});
    });
});

//修改一个用户
router.post('/contact/updUser', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {

            if (req.body.phoneNbr) {
                if (!PttUtil.checkPhone(req.body.phoneNbr)) {
                    res.json({status: 500, message: "电话号码:" + req.body.phoneNbr + "不符合规则"});
                    return;
                }
                var phone = yield PttContactUser.findOne({phoneNbr: req.body.phoneNbr}).exec();

                if (phone && phone.idNumber != req.body.idNumber) {
                    res.json({status: 500, message: "电话号码:" + req.body.phoneNbr + "已存在"});
                    return;
                }
            }

            if (req.body.mail) {
                if (!PttUtil.checkMail(req.body.mail)) {
                    res.json({status: 500, message: "电子邮箱:" + req.body.phoneNbr + "不符合规则"});
                    return;
                }
                var phone = yield PttContactUser.findOne({mail: req.body.mail}).exec();

                if (phone && phone.idNumber != req.body.idNumber) {
                    res.json({status: 500, message: "电子邮箱:" + req.body.mail + "已存在"});
                    return;
                }
            }

            var updObj = {
                "name": req.body.name,
                "phoneNbr": req.body.phoneNbr,
                "mail": req.body.mail,
                "father": req.body.father,
                "position": req.body.position,
                "devices": req.body.devices,
                "privileges": {
                    "callAble": req.body.privileges.callAble,
                    "groupAble": req.body.privileges.groupAble,
                    "calledAble": req.body.privileges.calledAble,
                    "joinAble": req.body.privileges.joinAble,
                    "forbidSpeak": req.body.privileges.forbidSpeak,
                    "callOuterAble": req.body.privileges.callOuterAble,
                    "calledOuterAble": req.body.privileges.calledOuterAble,
                    "powerInviteAble": req.body.privileges.powerInviteAble,
                    "muteAble": req.body.privileges.muteAble,
                    "viewMap": req.body.privileges.viewMap,
                    "emergency": req.body.privileges.emergency,
                    "boardcast": req.body.privileges.boardcast,
                    "groupBoardcast": req.body.privileges.groupBoardcast,
                    "noDevice": req.body.privileges.noDevice,
                    "priority": req.body.privileges.priority
                },
                "emergencyPerson": req.body.emergencyPerson,
                "nearbyPerson": req.body.nearbyPerson
            };

            //如果密码不为'********'，则需要更新密码字段
            if (req.body.password !== '********') {
                updObj.password = PttUtil.md5(req.body.password);
            }

            yield PttContactUser.findOneAndUpdate({idNumber: req.body.idNumber}, {
                $set: updObj
            }, {new: true}).exec();

            //更新通讯录状态
            yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

            // 将用户信息发送给用户以进行更新
            process.signalServer.onUserUpdated(req.body.idNumber);

            //yield user.save();
            res.json({status: 200});

        }
    ).catch(function (err) {
        try {
            var repNumber = JSON.parse(JSON.stringify(err));
            if (repNumber.code === 11000) {
                res.json({status: 500, message: "电话号码：" + req.body.phoneNbr + "已存在"});
                return;
            }
        } catch (e) {

        }
        res.json({status: 500, message: err.message});
    });
});

//直接给用户发一个信号让它立即上报
router.post('/contact/locateUser', pttAuth.isAuthenticated, function (req, res, next) {
    process.signalServer.sendMessageTo(req.body.userid, "s_user_locate", {msg: 'upload location immediately'});
    res.send({status: 200, message: "success"})
});

//新建一个用户
router.post('/contact/user', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {

        console.log("body: " + JSON.stringify(req.body));

        if (req.body.phoneNbr) {
            if (!PttUtil.checkPhone(req.body.phoneNbr)) {
                res.json({status: 500, message: "电话号码:" + req.body.phoneNbr + "不符合规则"});
                return;
            }

            var phone = yield PttContactUser.findOne({phoneNbr: req.body.phoneNbr}).exec();

            if (phone) {
                res.json({status: 500, message: "电话号码:" + req.body.phoneNbr + "已存在"});
                return;
            }
        }
        //
        if (req.body.mail) {
            if (!PttUtil.checkMail(req.body.mail)) {
                res.json({status: 500, message: "电子邮箱:" + req.body.mail + "不符合规则"});
                return;
            }

            var mail = yield PttContactUser.findOne({mail: req.body.mail}).exec();

            if (mail) {
                res.json({status: 500, message: "电子邮箱:" + req.body.mail + "已存在"});
                return;
            }
        }

        var curUsers = yield PttContactUser.find({enterprise: req.user._id}).exec();
        if (curUsers.length >= req.user.quota.staff) {
            res.json({status: 500, message: "已达到人员上限，无法再添加人员"});
            return
        }

        var counter = yield PttNumberGenerator.getNextContactUserNumber(req.user.idNumber).exec();

        if (!counter)
            return res.json({status: 500, message: "生成idNumber出错"});

        let userIdNumber = counter._id * MaxContactUserNumber + counter.seq;

        var user = new PttContactUser(req.body);

        user.idNumber = userIdNumber;
        user.enterprise = req.user._id;
        user.password = PttUtil.md5(user.password);

        yield user.save();

        //更新通讯录状态
        yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

        res.json({status: 200});
        //}
    }).catch(function (err) {
        try {
            var repNumber = JSON.parse(JSON.stringify(err));
            if (repNumber.code === 11000 && err.toString().indexOf('phoneNbr_1') > -1) {
                res.json({status: 500, message: "电话号码：" + req.body.phoneNbr + "已存在"});
                return;
            }
        } catch (e) {

        }
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//启用停用用户
//0启用
//1停用
router.post('/contact/enableDisableUser', pttAuth.isAuthenticated, function (req, res, next) {

    co(function *() {

        //更新当前用户的状态
        var ret = yield PttContactUser.findOneAndUpdate({idNumber: req.body.idNumber}, {$set: {status: req.body.set_status}}, {new: true}).exec();

        // 将用户信息发送给用户以进行更新
        process.signalServer.onUserUpdated(req.user.idNumber);

        res.json({status: 200});

    }).catch(function (err) {
        res.json({status: 500, message: err.message});
    });
});

//删除一个用户
//注销些 api 改用停用和启用
//router.post('/contact/delUser', pttAuth.isAuthenticated, function (req, res, next) {
//
//    co(function *() {
//        //查看当前企业下是否已经存在此号码
//        var eUser = yield PttContactUser.findOne({enterprise: req.user._id, _id: req.body.idNumber}).exec();
//
//        if (!eUser) {
//            res.json({status: 200});//如果不存在，说明已经被其它用户删除
//        } else {
//            yield eUser.remove();
//
//            //更新通讯录状态
//            yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();
//
//            res.json({status: 200});
//        }
//    }).catch(function (err) {
//        res.json({status: 500, message: err.message});
//    });
//});

//获取所有部门和人员
router.get('/contact/organization', pttAuth.isClientAuthenticated, function (req, res, next) {

    co(function*() {

        //取出企业内的所有职位
        var positions = yield PttPosition.find({enterprise: req.user._id}).exec();
        //取出企业内的所有部门
        var departments = yield PttDepartment.find({enterprise: req.user._id}).exec();
        //增加根节点
        departments.push({father: -1, name: req.session.enterprise.name, _id: req.user.id, isParent: true});

        var users = yield PttContactUser.find({enterprise: req.user._id}).exec();

        for (let i = 0; i < users.length; i++) {
            let user = users[i];
            for (let j = 0; j < positions.length; j++) {
                let position = positions[j];
                if (user.position && user.position.toString() === position._id.toString()) {
                    //user.icon = "http://" + process.env.FDFS_IMAGE_SERV_URL + ":" +
                    user.icon = "http://" + req.hostname + ":" +
                        process.env.FDFS_IMAGE_SERV_PORT + "/img/" +
                        position.image + "_16_16";
                    user.positionname = position.name;
                    break;
                } else {
                    //user.icon = "http://" + process.env.FDFS_IMAGE_SERV_URL + ":" +
                    user.icon = "http://" + req.hostname + ":" +
                        process.env.FDFS_IMAGE_SERV_PORT +
                        "/img/group1/M00/00/00/eSkWC1iH-NWAY6gaAACZEHru_Xc159.png_16_16";
                    user.positionname = '暂无职位';
                }
            }
            user.lastLocationTimeCST = user.lastLocationTime ? (cmoment(user.lastLocationTime).utcOffset("+08:00").format('YYYY-MM-DD HH:mm:ss')) : '';
        }
        return res.json({status: 200, data: users.concat(departments)});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//获取公司所有部门
router.get('/contact/departments', pttAuth.isClientAuthenticated, function (req, res, next) {

    co(function*() {
        //取出企业内的所有部门
        var departments = yield PttDepartment.find({enterprise: req.user.enterprise}).exec();
        //增加根节点
        departments.push({father: -1, name: req.session.enterprise.name, _id: req.user.id});

        return res.json({status: 200, data: departments});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//获取一个部门的下属部门
router.get('/contact/departments/:father', pttAuth.isClientAuthenticated, function (req, res, next) {

    co(function*() {

        //取出企业内的所有部门
        var departments = yield PttDepartment.find({father: req.param.father}).exec();
        //增加根节点
        departments.push({father: -1, name: req.session.enterprise.name, _id: req.user.id});

        return res.json({status: 200, data: departments});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//为前端异步使用
router.get('/contact/syncUsers/:father', pttAuth.isClientAuthenticated, function (req, res, next) {

    co(function*() {
        //取出所有人员
        var users = yield PttContactUser.find({father: req.params.father}).exec();

        return res.json({status: 200, data: users});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//获取公司所有部门
router.get('/contact/queryDepartments', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {
        //取出企业内的所有部门
        var departments = yield PttDepartment.find({enterprise: req.user._id}).exec();
        //增加根节点
        departments.push({father: -1, name: req.session.enterprise.name, _id: req.user.id});

        return res.json({status: 200, data: departments});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//新建一个部门
router.post('/contact/department', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {

        var department = new PttDepartment(req.body);
        department.enterprise = req.user._id;

        yield department.save();

        //更新通讯录状态
        yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

        return res.json({status: 200, data: department});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//修改一个部门
router.post('/contact/departmentUpd', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {
        var ret = yield PttDepartment.findOneAndUpdate({_id: req.body._id}, {
            $set: {
                name: req.body.name,
                father: req.body.father
            }
        }, {new: true}).exec();

        //更新通讯录状态
        yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

        return res.json({status: 200});
    }).catch(function (err) {
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//删除一个部门
router.post('/contact/delDepartment', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {

        var department = yield PttDepartment.find(req.body);
        if (department.length == 0) {
            return res.json({status: 500, message: '不是部门,不能删除'});
        }

        //有人员在部门下时不能删除
        var users = yield PttContactUser.find({father: req.body});
        if (users.length > 0) {
            return res.json({status: 500, message: '部门下面还有人员,不能删除'});
        }

        //有子部门时不能删除
        var sonDpts = yield PttDepartment.find({father: req.body});
        if (sonDpts.length > 0) {
            return res.json({status: 500, message: '部门下面还有下级部门,不能删除'});
        }

        yield PttDepartment.remove(req.body);

        //更新通讯录状态
        yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

        return res.json({status: 200});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});


//新建一个通讯录群组
router.post('/contact/group', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {

        var group = new PttContactGroup(req.body);
        group.enterprise = req.user._id;

        //取出企业的编号
        var enterprise = yield PttEnterprise.findOne({_id: req.user._id}).exec();

        if (!enterprise)
            return res.json({status: 500});

        //生成一个群组号
        var groupNumber = yield PttNumberGenerator.getNextContactGroupNumber(enterprise.idNumber).exec();

        if (!groupNumber)
            return res.json({status: 500});

        if (groupNumber.seq >= MaxContactGroupNumber)
            return res.json({status: 500, message: "群组个数超过系统最大权限"});

        group.idNumber = groupNumber._id * MaxContactGroupNumber + groupNumber.seq;

        yield group.save();

        //更新通讯录状态
        yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

        return res.json({status: 200, groupId: group.idNumber});
    }).catch(function (err) {
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//修改一个群组
router.post('/contact/groupUpd', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {
        var ret = yield PttContactGroup.findOneAndUpdate({idNumber: req.body.idNumber}, {
            $set: {
                name: req.body.name,
                description: req.body.description
            }
        }, {new: true}).exec();

        //更新通讯录状态
        yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

        return res.json({status: 200});
    }).catch(function (err) {
        res.json({status: 500, message: '系统错误,请保存好操作步骤并联系管理员'});
    });
});

//列出通讯录群组
router.get('/contact/group', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {
        var groups = yield PttContactGroup.find({enterprise: req.user._id}).exec();

        var enterprise = yield PttEnterprise.findOne({_id: req.user._id}).exec();

        if (!groups || !enterprise)
            return res.json({status: 500});

        var data = [];

        for (var i = 0; i < groups.length; i++) {

            data.push({
                idNumber: groups[i].idNumber,
                name: groups[i].name,
                members: '' + groups[i].members.length + '/' + enterprise.quota.preGroup,
                description: groups[i].description,
                quota: {
                    preGroup: enterprise.quota.preGroup
                }
            });
        }

        res.json({status: 200, data: data});

    }).catch(function (err) {
        return res.json({status: 500});
    });

});

//返回通讯录群组下面有哪些成员,同时返回未被选中的成员.
router.get('/contact/member/:groupid', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {

        //查找出当前组的成员
        var group = yield PttContactGroup.findOne({idNumber: req.params.groupid}).exec();

        //查找企业下的所有成员
        var users = yield PttContactUser.find({enterprise: req.user._id}).select({
            idNumber: 1,
            name: 1,
            phoneNbr: 1
        }).exec();

        var data = [];

        //标识出来已是成员
        for (var i = 0; i < users.length; i++) {

            var item = {
                id: users[i].idNumber,
                name: users[i].name,
                phoneNbr: users[i].phoneNbr
            };

            if (group.members.indexOf(users[i].idNumber) < 0) {
                item.isMember = false;
            } else {
                item.isMember = true;
            }

            data.push(item);
        }

        //输出结果
        res.json({'status': 200, 'data': data});

    }).catch(function (err) {
        return res.json({status: 500, message: err.message});
    });
});

//修改群的用户
router.put('/contact/member/:groupid', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {

        var enterprise = yield PttEnterprise.findOne({_id: req.user._id}).exec();

        //查找出当前组的成员，为了实现分页保存,用户只能在界面上一页一页的进行保存
        var group = yield PttContactGroup.findOne({idNumber: req.params.groupid}).exec();

        var curMembers = _.toArray(group.members);
        //提交上来的JSON数据是字条串,这里转化成Number
        var cancelMembers = _.map(req.body.cancels, function (nbr) {
            return parseInt(nbr);
        });
        var selMembers = _.map(req.body.members, function (nbr) {
            return parseInt(nbr);
        });

        var minusMembers = _.difference(curMembers, cancelMembers, [NaN]);
        var addMembers = _.union(minusMembers, selMembers);

        if (addMembers.length > enterprise.quota.preGroup) {
            return res.json({status: 500, message: '成员超过最大限制:' + enterprise.quota.preGroup});
        }

        var ret = yield PttContactGroup.findOneAndUpdate({idNumber: req.params.groupid}, {$set: {members: addMembers}}, {new: true}).exec();

        //更新通讯录状态
        yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

        res.json({status: 200});
    }).catch(function (err) {
        res.json({status: 500, message: err.message});
    })
});

//通过指定的id列出当前的企业
router.get('/enterprise/:idNumber', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {

        var enterprise = yield PttEnterprise.findOne({idNumber: req.params.idNumber}).exec();
        var commander = yield PttContactUser.findOne({enterprise: req.user._id, commander: 1});

        if (commander) {
            res.json({
                status: 200,
                data: enterprise,
                commander: commander.idNumber
            })
        } else {
            res.json({
                status: 200,
                data: enterprise
            })
        }
    }).catch(function (err) {
        res.json({status: 500, message: err.message});
    });
});

//列出当前的企业
router.get('/enterprise', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {

        var enterprises = yield PttEnterprise.find().exec();

        var data = [];

        for (var i = 0; i < enterprises.length; i++) {
            if (enterprises[i].idNumber === 0)
                continue;

            data.push({
                name: enterprises[i].name,
                idNumber: enterprises[i].idNumber,
                phoneNbr: enterprises[i].phoneNumber,
                privilege: enterprises[i].privilege,
                quota: enterprises[i].quota,
                key: enterprises[i].key,
                expDate: enterprises[i].expTime ? PttUtil.getYYYY_MM_DD(enterprises[i].expTime) : ""
            })
        }

        res.json({
            status: 200,
            data: data
        })
    }).catch(function (err) {
        res.json({status: 500, message: err.message});
    });
});

//删除一个群组
router.post('/group/delGroup', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
        //查看当前企业下是否已经存在此号码
        var eUser = yield PttContactGroup.findOne({idNumber: req.body.idNumber}).exec();

        if (!eUser) {
            res.json({status: 200});//如果不存在，说明已经被其它用户删除
        } else {
            yield eUser.remove();
            res.json({status: 200});
        }

        //更新通讯录状态
        yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {version: Date.now()}}, {new: true}).exec();

    }).catch(function (err) {
        res.json({status: 500, message: err.message});
    });
});

//创建一个企业
router.post('/enterprise', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {

        console.log(JSON.stringify(req.body));

        var enterprise = new PttEnterprise(req.body);

        if (req.body.commanderPhoneNbr) {
            if (!PttUtil.checkPhone(req.body.commanderPhoneNbr)) {
                res.json({status: 500, message: "电话号码:" + req.body.commanderPhoneNbr + "不符合规则"});
                return;
            }
            var phone = yield PttContactUser.findOne({phoneNbr: req.body.commanderPhoneNbr}).exec();

            if (phone) {
                res.json({status: 500, message: "电话号码:" + req.body.commanderPhoneNbr + "已存在"});
                return;
            }
        }

        var enterpriseNumber = yield PttNumberGenerator.getNextEnterpriseNumber().exec();

        enterprise.expTime = PttUtil.transferYYYY_MM_DDToTimestamp(req.body.expDate);
        enterprise.idNumber = enterpriseNumber.seq;
        enterprise.managerPwd = PttUtil.md5(req.body.password);
        enterprise.key = req.body.key;
        enterprise.vedioMode = req.body.vedioMode;
        enterprise.phoneNumber = req.body.commanderPhoneNbr;

        var savedEnp = yield enterprise.save();

        var counter = yield PttNumberGenerator.getNextContactUserNumber(enterprise.idNumber).exec();

        if (!counter)
            return res.json({status: 500, message: "生成idNumber出错"});

        let userIdNumber = counter._id * MaxContactUserNumber + counter.seq;


        var user = new PttContactUser();

        user.name = "指挥号";
        user.idNumber = userIdNumber;
        user.phoneNbr = req.body.commanderPhoneNbr;
        user.enterprise = savedEnp._id;
        user.password = PttUtil.md5(req.body.password);
        user.privileges = {
            "priority": 0,
            "muteAble": true,
            "powerInviteAble": true,
            "calledOuterAble": true,
            "callOuterAble": true,
            "forbidSpeak": true,
            "joinAble": true,
            "calledAble": true,
            "groupAble": true,
            "callAble": true
        }
        user.commander = 1;

        console.log(JSON.stringify(user));

        yield user.save();

        res.json({
            status: 200, message: "添加帐户成功,登录号: " + req.body.commanderPhoneNbr
        })
    }).catch(function (e) {
        res.json({
            status: 500, message: "出错"
        })
    });
});

//修改企业名称
router.post('/enterprise/quota', pttAuth.isAuthenticated, function (req, res, next) {

    co(function*() {
        var ret = yield PttEnterprise.findOneAndUpdate({idNumber: req.body.idNumber}, {$set: {quota: req.body.quota}}, {new: true}).exec();

        res.json({
            status: 200
        });
    }).catch(function (e) {
        res.json({
            status: 500
        });
    });
});

//修改企业过期时间
router.post('/enterprise/chgEntExpTime', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        var ret = yield PttEnterprise.findOneAndUpdate({idNumber: req.body.idNumber}, {$set: {expTime: PttUtil.transferYYYY_MM_DDToTimestamp(req.body.expDate)}}, {new: true}).exec();

        res.json({
            status: 200
        })
    });
});

//修改企业密钥
router.post('/enterprise/chgKey', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        var ret = yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {key: req.body.key}}, {new: true}).exec();

        process.signalServer.onEnterpriseModeChanged(ret._id);

        res.json({
            status: 200
        });

    });
});

//修改企业视分辨率
router.post('/enterprise/chgVedioMode', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        var ret = yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {vedioMode: req.body.vedioMode}}, {new: true}).exec();

        process.signalServer.onEnterpriseModeChanged(ret._id);

        res.json({
            status: 200
        });

    });
});

//修改企业名称
router.post('/enterprise/chgEntName', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        var ret = yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {name: req.body.name}}, {new: true}).exec();

        res.json({
            status: 200
        })
    });
});

//修改企业当前模式(0观察/1指挥)
router.post('/enterprise/chgCmdMode', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        console.log(JSON.stringify(req.user._id));
        console.log(JSON.stringify(req.body));

        let mode = null;
        if (req.body.type == 0) {
            mode = {
                modetype: req.body.type,
                commandTime: null,
                collectFreq: null,
                endTime: null
            }
        } else {
            mode = {
                modetype: req.body.type,
                commandTime: parseInt(req.body.commandTime),
                collectFreq: parseInt(req.body.collectFreq),
                endTime: new Date(req.body.endTime)
            }
        }

        var ret = yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {
            $set: {
                mode: mode
            }
        }, {new: true}).exec();


        process.signalServer.onEnterpriseModeChanged(req.user._id);

        res.json({
            status: 200
        })
    }).catch(function (err) {
        console.log(err);
        res.json({
            status: 500,
            msg: "设置错误,请联系管理员"
        });
    });
});

//修改企业密码-超级管理员(不用校验老密码)
router.post('/enterprise/adminChgPwd', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {

        var ret = yield PttEnterprise.findOneAndUpdate({idNumber: req.body.idNumber}, {$set: {managerPwd: PttUtil.md5(req.body.chgNewPassword)}}, {new: true}).exec();

        res.json({
            status: 200
        })
    });
});

//修改企业密码
router.post('/enterprise/chgpwd', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {

        var enterprise = yield PttEnterprise.findOne({_id: req.user._id}).exec();

        if (enterprise.managerPwd !== PttUtil.md5(req.body.curPassword)) {
            res.json({status: 500, message: "当前密码不正确"});
            return;
        }

        var ret = yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {managerPwd: PttUtil.md5(req.body.chgNewPassword)}}, {new: true}).exec();

        res.json({
            status: 200
        })
    });
});

//修改超级管理员密码
router.post('/enterprise/chgpwd', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {

        var enterprise = yield PttEnterprise.findOne({_id: req.user._id}).exec();

        if (enterprise.managerPwd !== PttUtil.md5(req.body.curPassword)) {
            res.json({status: 500, message: "当前密码不正确"});
            return;
        }

        var ret = yield PttEnterprise.findOneAndUpdate({_id: req.user._id}, {$set: {managerPwd: PttUtil.md5(req.body.chgNewPassword)}}, {new: true}).exec();

        res.json({
            status: 200
        })
    });
});

//提供给 app 和 web 的修改个人密码
router.post('/contact/chgpwd', function (req, res, next) {
    co(function*() {
        var user = yield PttContactUser.findOne({idNumber: req.body._id}).exec();

        if (user.password !== PttUtil.md5(req.body.curPassword)) {
            res.json({status: 500, message: "当前密码不正确"});
            return;
        }

        var ret = yield PttContactUser.findOneAndUpdate({idNumber: req.body._id}, {$set: {password: PttUtil.md5(req.body.chgNewPassword)}}, {new: true}).exec();

        res.json({
            status: 200
        })
    }).catch(function (err) {
        res.json({
            status: 500,
            msg: JSON.stringify(err)
        })
    });
});

//修改企业的指挥号
router.post('/enterprise/chgCommander', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        //更新当前企业的指挥号
        var ret = yield PttContactUser.findOneAndUpdate(
            {idNumber: req.body.commander, enterprise: req.user._id}, {$set: {commander: 1}}, {new: true}).exec();

        if (ret) {

            //将以前的指挥号作更新
            let updateOld = yield PttContactUser.update(
                {
                    commander: 1,
                    idNumber: {$not: {$eq: parseInt(req.body.commander)}},
                    enterprise: req.user._id
                }, {$set: {commander: null}}, {multi: true}).exec();
            res.json({
                status: 200
            });
        } else {
            res.json({
                status: 500,
                msg: "指挥号不属于当前企业或无效"
            });
        }

    }).catch(function (err) {
        res.json({
            status: 500,
            msg: JSON.stringify(err)
        });
    });

});


router.get('/enterprise/number', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        var enterpriseNumber = yield PttNumberGenerator.getNextEnterpriseNumber().exec();

        res.json({
            status: 200,
            number: enterpriseNumber.seq
        })
    });
});

router.get('/curEnterprise', pttAuth.isAuthenticated, function (req, res, next) {
    co(function*() {
        var enterprises = yield PttEnterprise.findOne({"_id": req.user._id}).exec();

        res.json({
            status: 200,
            data: enterprises
        })
    });
});

//返回通讯录群组下面有哪些成员
router.get('/contact/sync/:idNumber/:password/:version', function (req, res, next) {

    co(function*() {
        var user = yield PttContactUser.findOne({
            idNumber: req.params.idNumber,
            password: req.params.password
        }).exec();

        if (!user) {
            res.writeHead(404);
            res.end('User or passwords invalid');
            return;
        }

        var enterp = yield PttEnterprise.findOne({"_id": user.enterprise}).exec();

        if (!enterp) {
            res.writeHead(500);
            res.end(`Enterprise not found for user ${user.idNumber}`);
            return;
        }

        if (enterp.version <= req.params.version) {
            res.writeHead(304);
            res.end("No update for contacts");
            return;
        }

        let resp = yield process.signalServer.sync.doSync(user);

        //输出结果
        res.json({
            version: enterp.version,
            enterpriseMembers: resp.enterpriseMembers.add,
            enterpriseGroups: resp.enterpriseGroups.add,
        });

    }).catch(function (err) {
        res.writeHead(500);
        res.end(JSON.stringify(err));
    });
});

//返回通讯录群组下面有哪些成员
router.get('/contact/sync/:version', pttAuth.isClientAuthenticated, function (req, res, next) {

    co(function*() {
        var enterp = yield PttEnterprise.findOne({"_id": req.session.enterprise}).exec();

        if (!enterp) {
            res.writeHead(500);
            res.end(`Enterprise not found for user ${user.idNumber}`);
            return;
        }

        if (enterp.version <= req.params.version) {
            res.writeHead(304);
            res.end("No update for contacts");
            return;
        }

        let resp = yield process.signalServer.sync.doSync(req.user);

        console.log('Sync result: Member size =', resp.enterpriseMembers.add.length, ', group count =', resp.enterpriseGroups.add.length);

        //输出结果
        res.json({
            version: enterp.version,
            enterpriseMembers: resp.enterpriseMembers.add,
            enterpriseGroups: resp.enterpriseGroups.add,
        });

    }).catch(function (err) {
        res.writeHead(500);
        res.end(JSON.stringify(err));
    });
});


//控制台app用于获取当前企业号下的所有房间信息
router.get('/room/syncAll', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function*() {

        log.info("room/syncAll : ");
        var enterp = yield PttEnterprise.findOne({"_id": req.session.enterprise}).exec();


        log.info("room/syncAll : enterp = " + enterp);

        if (!enterp) {
            res.writeHead(500);
            res.end(`Enterprise not found for user ${user.idNumber}`);
            return;
        }

        if (!req.user.commander) {
            res.writeHead(500);
            res.end(`Access  Deny for not commander ${user.idNumber}`);
            return;
        }

        let allRooms = yield process.signalServer.chatRoom.getAllRoomsByEnterpriseId(enterp.idNumber);
        log.info("room/syncAll : allRooms = " + allRooms);
        //输出结果
        res.json(
            {status: 200, data: allRooms}
        );
    }).catch(function (err) {
        res.writeHead(500);
        res.end(JSON.stringify(err));
    });
});

//第三方用户提供通过用户名和密码来交换token
router.post('/token', function (req, res, next) {

    co(function*() {

        console.log({
            idNumber: req.body.username,
            password: req.body.password
            //password: PttUtil.md5(req.body.password)
        });

        let user = yield PttContactUser.findOne({
            idNumber: req.body.username,
            password: req.body.password
            //password: PttUtil.md5(req.body.password)
        }).exec();

        if (!user) {
            res.writeHead(500);
            res.end('User or password invalid');
            return;
        }
        let now = Date.now();
        let token = crypto.createHmac('sha1', secret)
            .update(req.params.username + 'gotTokenAt' + now)
            .digest('hex');
        var pttToken = new PttToken();

        pttToken.idNumber = req.body.username;
        pttToken.token = token;
        pttToken.createTime = now;

        yield pttToken.save();

        //输出结果
        res.json({
            token: token
        });

    }).catch(function (err) {
        console.log(err);
        res.writeHead(500);
        res.end(JSON.stringify(err));
    });
});

//第三方用户通过token和他的id来换我们系统的ID
router.post('/token/exchange', function (req, res, next) {

    co(function*() {
        let tokenUser = yield PttToken.findOne({token: req.body.token}).exec();
        if (!tokenUser) {
            res.json({
                status: 500,
                data: "Bad token."
            });
            return;
        } else if ((Date.now() - tokenUser.createTime ) > 24 * 3600 * 1000) {
            res.json({
                status: 500,
                data: "Token has expired."
            });
            return;
        } else {
            //没有选择呼叫人员直接打开对讲窗口
            if (req.body.phoneNbrs[0] == "nobody") {
                //输出结果
                res.json({
                    status: 200
                });
            } else {
                //用户签权验证
                let users = yield PttContactUser.find({phoneNbr: {$in: req.body.phoneNbrs}}).select({
                    _id: 0,
                    idNumber: 1,
                    name: 1,
                    phoneNbr: 1
                }).exec();

                if (users.length == 0) {
                    res.json({
                        status: 500,
                        data: "请先注册电话号码"
                    });
                    return;
                }

                //如果有的电话号码没有被成功转换
                if (req.body.phoneNbrs.length != users.length) {
                    var retUsers = []
                    //输出结果
                    for (let i in req.body.phoneNbrs) {
                        let wrong = req.body.phoneNbrs[i];
                        //只保留能查询到的
                        for (let j in users) {
                            if (wrong == users[j].phoneNbr) {
                                retUsers.push(users[j]);
                            }
                        }
                    }
                    //输出结果
                    res.json({
                        status: 200,
                        data: retUsers
                    });
                } else {
                    //输出结果
                    res.json({
                        status: 200,
                        data: users
                    });
                }
            }
        }

    }).catch(function (err) {
        console.log(err);
        res.writeHead(500);
        res.end(JSON.stringify(err));
    });
});

// 日志服务
router.get('/logs', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
        const enterprise = req.session.enterprise;
        const startTime = req.query["startTime"];
        const endTime = req.query["endTime"];

        if (!startTime || !endTime) {
            throw "没有指定查询日志的时间";
        }

        const query = {
            "attrs.enterprise": {
                "$eq": mongoose.Types.ObjectId(enterprise._id),
            },
            "time": {
                "$gte": new Date(parseInt(startTime)),
                "$lte": new Date(parseInt(endTime)),
            }
        };

        let logs = yield PttAudit.find(query).sort('-time').exec();

        const userNameNeedle = req.query["name"];
        if (userNameNeedle) {
            logs = logs.filter(item => item.attrs && item.attrs.userName && item.attrs.userName.indexOf(userNameNeedle) >= 0);
        }
        //将ISODate转换成普通格式
        var after = _.map(logs, function (it) {
            return {
                name: it.attrs.userName,
                time: cmoment(it.time).utcOffset("+08:00").format('YYYY-MM-DD HH:mm:ss'),
                eventName: it.eventName == "user-connected" ? "用户登陆" : "用户断开",
                ip: it.attrs.remoteAddress ? it.attrs.remoteAddress.split(":")[3] : ""
            }
        });

        return res.json({status: 200, data: after});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    });
});

// GIS查询用户最后的位置
router.get('/queryLastLoc', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log("queryLastLoc userid=" + req.query["userId"]);
        let userIds = req.query["userId"].split(",");

        // Map userObjectId => user
        const users = new Map();
        _.forEach(yield PttContactUser.findByIdNumbers(userIds), user => {
            users.set(user._id.toString(), user);
        });

        let locs = [];
        let userObjectIds = Array.from(users.keys());
        for (var index in userObjectIds) {
            let loc = yield LocationHandler.findUserLastLocation([userObjectIds[index]]);
            if (loc != undefined && loc.length == 1) {
                locs.push(loc[0]);
            }
        }

        console.log("queryLastLoc result locs.length = " + locs.length);
        let resultLocs = _.map(locs, function (item) {
            return {
                userId: users.get(item.user.toString()).idNumber,
                lng: item.lng,
                lat: item.lat,
                repTime: cmoment(item.repTime).utcOffset("+08:00").format('YYYY-MM-DD HH:mm:ss'),
            }
        });

        return res.json({status: 200, data: resultLocs});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    })
});

// GIS查询用户最后的位置 (客户端接口）
router.get('/lastLocation', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log("queryLastLoc userid=" + req.query["userId"]);
        let userIds = req.query["userId"].split(",");

        // Map userObjectId => user
        const users = new Map();
        _.forEach(yield PttContactUser.findByIdNumbers(userIds), user => {
            users.set(user._id.toString(), user);
        });

        let locs = [];
        let userObjectIds = Array.from(users.keys());
        for (var index in userObjectIds) {
            let loc = yield LocationHandler.findUserLastLocation([userObjectIds[index]]);
            if (loc != undefined && loc.length == 1) {
                locs.push(loc[0]);
            }
        }

        console.log("queryLastLoc result locs.length = " + locs.length);
        let resultLocs = _.map(locs, function (item) {
            return {

                userId: users.get(item.user.toString()).idNumber,
                lng: item.lng,
                lat: item.lat,
                repTime: item.repTime,
            }
        });

        return res.json({status: 200, data: resultLocs});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    })
});

// GIS查询轨迹回放数据
router.get('/queryLocs', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
        console.log(req.query["userId"], req.query["startTime"], req.query["endTime"]);
        let users = req.query["userId"].split(",");

        let locs = yield LocationHandler.findUserLocation(users, req.query["startTime"], req.query["endTime"]);

        locs = _.map(locs, function (item) {
            return {
                userId: item.user,
                lng: item.lng,
                lat: item.lat,
                repTime: cmoment(item.repTime).utcOffset("+08:00").format('YYYY-MM-DD HH:mm:ss'),
                speed: item.speed
            }
        });

        //根据名字和上传时间去重
        locs = _.uniqBy(locs, function (it) {
            return it.userId + it.repTime;
        });

        return res.json({status: 200, data: locs});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    })
});

// 查询用户所在的所有房间
router.get('/listChatRoomByUser', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        const result = _.map(yield PttChatRoom.listByUser(req.query.userId), r => r.toResponse());
        return res.json({status: 200, data: result});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    })
});

// 查询用户指定时间内活跃过的所有房间
router.get('/listChatRoomByUserIdAndDate', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {

        let result = [];

        //    console.log("userId : " + req.query.userId + ", startDate : " + req.query.startDate + ", endDate : " + req.query.endDate);
        //    if (req.query.userId == null || req.query.startDate == null || req.query.endDate == null)
        //    {
        //        throw "参数错误";
        //    }

        const userId = parseInt(req.query.userId);
        const rooms = _.map(yield PttChatRoom.listByUser(userId), r => r.toResponse());
        //console.log(rooms);

        for (let i = 0; i < rooms.length; i++) {
            let hisMsgs = yield PttMessage.findMessages(userId,
                rooms[i].idNumber,
                req.query.startDate,
                req.query.endDate);
            //console.log(hisMsgs.length);
            if (hisMsgs.length > 0) {
                result.push({room: rooms[i], hisMsgs: hisMsgs});
            }
        }

        return res.json({status: 200, data: result});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    })
});

// 查询历史消息记录
router.get('/messageHistory', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        var result = yield PttMessage.findMessages(req.query.userId, req.query.roomId, req.query.startDate, req.query.endDate);

        return res.json({status: 200, data: result});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    })


});


// GIS查询轨迹回放数据（给App使用的版本）
router.get('/locations', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        console.log(req.query["userIds"], req.query["startTime"], req.query["endTime"]);

        // Map userObjectId => user
        const users = new Map();
        _.forEach(yield PttContactUser.findByIdNumbers(req.query["userIds"]), user => {
            users.set(user._id.toString(), user);
        });

        let locations = _.map(
            yield LocationHandler.findUserLocation(Array.from(users.keys()), req.query["startTime"], req.query["endTime"]),
            item => {
                return {
                    userId: users.get(item.user.toString()).idNumber,
                    lng: item.lng,
                    lat: item.lat,
                    repTime: item.repTime.getTime(),//cmoment(item.repTime).utcOffset("+08:00").format('YYYY-MM-DD HH:mm:ss'),
                    radius: item.radius,
                    speed: item.speed
                }
            });

        //根据名字和上传时间去重
        locations = _.uniqBy(locations, function (it) {
            return it.userId + it.repTime;
        });

        return res.json({status: 200, data: locations});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    })
});


// gis 导出轨迹回放数据
router.get('/exportLocs', pttAuth.isAuthenticated, function (req, res, next) {
    co(function *() {
        console.log(req.query["userId"], req.query["startTime"], req.query["endTime"], req.query["names"]);
        let users = req.query["userId"].split(",");
        let userNames = req.query["names"].split(",");

        //db.gis.location.find({user:{$in:[ObjectId("57c449dc0e9657946a87e3d4"),ObjectId("57ed95f19c73210ee43ef20d")]}});

        let locs = yield LocationHandler.findUserLocation({$in: users}, req.query["startTime"], req.query["endTime"]);

        //根据名字和上传时间去重
        locs = _.uniqBy(locs, function (it) {
            return it.userId + it.repTime;
        });

        let addressPromises = [];
        for (let i in users) {
            let curDatas = _.filter(locs, function (item) {
                return item.user == users[i];
            });
            console.log(JSON.stringify(curDatas));
            addressPromises.push(Promise.map(curDatas, function (p) {
                return PttUtil.queryLoc(p.lng, p.lat);
            }, {concurrency: 100}));
        }

        Promise.all(addressPromises).then(function (data) {
            //console.log("data:" + JSON.stringify(data));
            //res.json({status: 200, data: data});
            var workbook = new Excel.Workbook();
            for (let i in users) {
                var sheet = workbook.addWorksheet(userNames[i]);
                sheet.columns = [
                    {header: '时间', key: 'time', width: 10},
                    {header: '速度', key: 'speed', width: 10},
                    {header: '地址', key: 'address', width: 100}
                ];
                let curDatas = _.filter(locs, function (item) {
                    return item.user == users[i];
                });
                for (let j in curDatas) {
                    sheet.addRow({
                        time: cmoment(curDatas[j].repTime).utcOffset("+08:00").format('YYYY-MM-DD HH:mm:ss'),
                        speed: curDatas[j].speed,
                        address: data[i][j].result.formatted_address
                    });
                }

            }
            let curTime = (new Date()).getTime();
            let fileName = path.resolve(__dirname, '../') + "/public/report/" + curTime + '.xlsx';
            let downUrl = "https://" + process.env.SIGNAL_SERVER_URL + ":" + process.env.SIGNAL_SERVER_SSL_PORT + "/report/" + curTime + '.xlsx';
            console.log(downUrl);
            //downUrl = "https://localhost:8443" + "/report/" + (new Date()).getTime() + '.xlsx';
            workbook.xlsx.writeFile(fileName)
                .then(function () {
                    console.log('success');
                    res.json({status: 200, data: downUrl});
                });
        }).catch(function (err) {
            console.log(err);
            res.json({status: 500, message: JSON.stringify(err)});
        });

        //locs = _.map(locs, function (item) {
        //    return {
        //        userId: item.user,
        //        lng: item.lng,
        //        lat: item.lat,
        //        repTime: cmoment(item.repTime).utcOffset("+08:00").format('YYYY-MM-DD HH:mm:ss'),
        //        speed: item.speed
        //    }
        //});

        //console.log(JSON.stringify(locs));

        //return res.json({status: 200, data: "ok"});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    })
});

// GIS上报服务
router.post('/location', pttAuth.isClientAuthenticated, function (req, res, next) {
//router.post('/location', function (req, res, next) {
    co(function *() {
        yield LocationHandler.saveLocation(req.body, req.user);
        return res.json({status: 200});
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)});
    })
});

// GIS查询服务
router.get('/nearby', pttAuth.isClientAuthenticated, function (req, res, next) {
    co(function *() {
        let users = yield LocationHandler.findUsersWithinBoundary(req.user.enterprise,
            parseFloat(req.query['minLat']), parseFloat(req.query['minLng']), parseFloat(req.query['maxLat']), parseFloat(req.query['maxLng']));

        //变换数据类型
        let userLocations = _.map(users, function (u) {
            return {
                userId: u.idNumber,
                lat: u.lastLocation.coordinates[1],
                lng: u.lastLocation.coordinates[0],
            };
        });

        //去除当前用户
        userLocations = _.filter(userLocations, u => u.idNumber != req.user.idNumber);

        res.json({status: 200, data: userLocations})
    }).catch(function (err) {
        console.log(err);
        res.json({status: 500, message: JSON.stringify(err)})
    })
});

module.exports = router;
