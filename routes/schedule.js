/**
 * Created by mycow on 16/8/22.
 */
//mongod --dbpath=/Users/mycow/development/mongodb/ --logpath=/Users/mycow/development/mongodb/mongdb_log/mongodb.log --logappend &

var express = require('express');
var pttAuth = require('../common/ptt-auth');

var router = express.Router();

/* GET home page. */
router.get('/', pttAuth.isAuthenticated, function (req, res, next) {
    console.log(req.session.enterprise.privileges);
    res.render('schedule', {
        user:{privileges: req.session.enterprise.privileges},
        name: req.session.enterprise.name,
        version: process.env['VERSION'],
        pttsdk: "https://" + process.env.PTT_WEB_SERVER_URL + ":" + process.env['PTT_WEB_PORT']
    });
});

/**
 * 管理系统
 */
//router.get('/admin', pttAuth.isAuthenticated, function (req, res, next) {
//
//    co(function*() {
//        var enterprise = req.user;
//
//        if (!enterprise) {
//
//            res.render('login', {status: 500, errorMsg: "未知错误,请联系管理员."});
//
//        } else {
//
//            if (enterprise.idNumber === 0) {
//                res.render('root', {});
//            } else {
//                res.render('admin', {user: req.user, enterprise: enterprise});
//            }
//
//        }
//    });
//});

module.exports = router;
