/**
 * Created by zhoulanhong on 8/17/16.
 */
var express = require('express');
var multer = require('multer');
var fs = require('fs');
var Util = require('../common/ptt-util');
var Id = require('../common/Id');
var easyimg = require('easyimage');
var router = express.Router();

router.post('/do/image', function (req, res, next) {
    let imgData = req.body.imgData;
    let base64Data = imgData.replace(/^data:image\/\w+;base64,/, "");
    let dataBuffer = new Buffer(base64Data, 'base64');
    let dir = 'public/uploads/' + Util.getYYYYMMDD(null) + '/';
    let curId = Id.get();
    let filePath = dir + curId + '.jpg';
    let thumbFilePath = dir + curId + 'thumb.jpg';

    let resFilePath = '/uploads/' + Util.getYYYYMMDD(null) + '/' + curId + '.jpg';
    let resThumbFilePath = '/uploads/' + Util.getYYYYMMDD(null) + '/' + curId + 'thumb.jpg';

    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }

    console.log(fs.existsSync(dir));

    fs.writeFile(filePath, dataBuffer, function (err) {
        if (err) {
            console.log(err);
            res.json({status: 500, data: {msg: JSON.stringify(err)}});
        } else {

            easyimg.rescrop({
                    src: filePath, dst: thumbFilePath,
                    width: 160, height: 160,
                    x: 0, y: 0
                })
                .then(function (image) {
                    console.log('Resized and cropped: ' + image.width + ' x ' + image.height);
                    res.json({status: 200, data: {fp: resFilePath, thumbFp: resThumbFilePath}});
                })
                .catch(function (err) {
                        console.log(err);
                        res.json({status: 500, data: {msg: JSON.stringify(err)}});
                    }
                );

        }
    });


});

router.post('/do/binary', multer({storage: multer.memoryStorage()}).single('fileUp'), function (req, res, next) {
    console.log("--------------- server  --------------");
    if (req.file) {

        let dir = '/uploads/' + Util.getYYYYMMDD(null) + '/';
        let s = req.file.originalname;
        let fId = Id.get();
        let filePath = "public" + dir + fId + (s.substring(s.lastIndexOf('.')));
        let rpFilePath = dir + fId + (s.substring(s.lastIndexOf('.')));
        console.log(req.file);
        if (!fs.existsSync("public" + dir)) {
            fs.mkdirSync("public" + dir);
        }

        console.log(fs.existsSync("public" + dir));

        fs.writeFile(filePath, req.file.buffer, function (err) {
            if (err) {
                console.log(err);
                res.json({status: 500, data: {msg: JSON.stringify(err)}});
            } else {
                res.json({status: 200, data: {fp: rpFilePath}});
            }

        });
    }

});


module.exports = router;