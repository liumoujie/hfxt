"use strict";

/**
 * Created by suke on 15/12/4.
 *
 * 对讲的信号服务器,针对终端的socket.io的连接.
 *
 */

var io = require('socket.io');
var LoginUser = require('./signal/login-user');
var SyncContacts = require('./signal/sync-contacts');
var ChatRoomHandler = require('./signal/chat-room');
var LocationHandler = require('./signal/location');
var PushClient = require('./signal/push-client');
var Event = require('./signal/event');
var log = require('loglevel');
var co = require('co');
var _ = require('underscore');
var PttEnterprise = require('./models/ptt-enterprise');
var PttContactUser = require('./models/ptt-contact-user');
var PttPosition = require("./models/ptt-position");
const cmoment = require('moment');
const Audit = require('./signal/audit');
const RoomMode = require('./signal/room-modes');
const ptt_job = require('./common/ptt-job');

/**
 * 信号服务器实例
 *
 * @param httpServers {Array<http.Server>} HTTP服务器实例
 * @param voiceServerUrl {string} 语音服务器地址
 * @param voiceServerPort {number} 语音服务器端口
 * @constructor
 */
function SignalServer(key, httpServers, voiceServer, webVoiceServer, pushServerAddr) {

    // 用于发送推送消息的接口
    const pushClient = new PushClient(pushServerAddr, key, {
        cert: "certs/PushVoipCert.pem",
        key: "certs/PushVoipKey.pem",
        passphrase: process.env['SIGNAL_SERVER_URL'], //"netptt.cn", //ios push 证书密码
        production: true //使用ios正式环境push通道
    });

    //登录模块初始化
    const loginUser = new LoginUser(7, key, pushServerAddr);

    //同步模块初始化
    const sync = new SyncContacts();
    this.sync = sync;

    //房间管理
    const chatRoom = new ChatRoomHandler(loginUser, {
            host: voiceServer.host,
            port: voiceServer.port,
            tcpPort: voiceServer.tcpPort,
            protocol: "udp"
        },
        {
            host: webVoiceServer.host,
            port: webVoiceServer.port,
        },
        pushClient
    );
    this.chatRoom = chatRoom;


    //job管理
    new ptt_job(loginUser);
    log.info("Using push server: ", pushServerAddr);
    log.info("Using voice server: udp://", voiceServer.host, ":", voiceServer.port);
    log.info("Using kurento  server: wss://", webVoiceServer.host, ":", webVoiceServer.port);

    const self = this;

    //连接建立
    //io(httpServer, {}).on('connection', function (socket) {
    //加入'transports': ['websocket']参数,直接升级成websocket连接
    //心跳为4分钟 'pingInterval': 4 * 60 * 1000
    var opts = {'transports': ['websocket']};
    if (typeof parseInt(process.env.PING_INTERVAL) == "number") {
        opts.pingInterval = parseInt(process.env.PING_INTERVAL);
    }

    const server = io(opts);
    _.forEach(httpServers, httpServer => server.attach(httpServer));
    server.on('connection', function (socket) {

        log.info('---------------------- somebody connected ----');

        loginUser.doLogin(socket)
            .then(result => {
                const loggedInUser = result.user;
                log.info(`loggedInUser` + JSON.stringify(loggedInUser));
                const oldSocket = result.oldSocket;
                const userId = loggedInUser.idNumber;
                log.info(`User ${userId} logged in successfully`);
                Audit.logUserConnected(loggedInUser, socket.request.connection.remoteAddress);

                // 如果这个用户之前已经存在一个socket，必须清掉房间相关的状态
                if (oldSocket) {
                    log.info("Clearing old room state for ", userId);
                    chatRoom.disconnect(userId, oldSocket);
                }

                // 这里要监听原始的close事件, 以便在signal io清理内部状态前, 先清理我们的数据
                const oldOnClose = socket.onclose;
                socket.onclose = function (reason) {
                    log.info(`User ${userId} closes`);
                    if (loginUser.socketForUser(userId) == socket) {
                        log.info(`Cleaning up user ${userId}'s socket: ${socket}`);
                        //房间清理。这里必须确保这个socket是当前的socket，以防止旧的Socket跑进来捣乱
                        chatRoom.disconnect(userId, socket);
                        //用户删除
                        loginUser.doLogout(this, reason);

                        self.onlineUsers.delete(userId);
                        Audit.logUserDisconnect(loggedInUser, reason);
                    }
                    else {
                        log.info(`User ${userId} closes without clean socket`);
                    }

                    if (oldOnClose) {
                        return oldOnClose(reason);
                    }
                };

                // 将用户在线数据保存
                self.onlineUsers.set(userId, loggedInUser);

                //通讯录同步
                bindSocketEvent(socket, userId, Event.Recv.SYNC_CONTACTS, () => {
                    return sync.doSync(self.onlineUsers.get(userId));
                });

                //创建一个房间
                bindSocketEvent(socket, userId, Event.Recv.CREATE_CHAT_ROOM, (name, groupIds, extraMemberIds) => {
                    return chatRoom.createChatRoom(userId, name,
                        _.map(groupIds, id => parseInt(id)), _.map(extraMemberIds, id => parseInt(id)));
                });

                //进入一个房间
                bindSocketEvent(socket, userId, Event.Recv.JOIN_ROOM, (roomId, fromInvitation, mode, silent) => {
                    // fromInvitation如果不指定（旧版本客户端兼容需要），则默认为false.
                    return chatRoom.joinRoom(
                        self.onlineUsers.get(userId),
                        socket,
                        parseInt(roomId),
                        fromInvitation == undefined ? false : fromInvitation,
                        mode ? mode : RoomMode.NORMAL,
                        silent == undefined ? false: silent
                    );
                });

                //退出一个房间
                bindSocketEvent(socket, userId, Event.Recv.LEAVE_ROOM, (roomId, terminateSession) => {
                    return chatRoom.leaveRoom(userId, socket, parseInt(roomId), terminateSession);
                });

                //请求一个房间的基础数据
                bindSocketEvent(socket, userId, Event.Recv.GET_ROOM_INFO, (roomId) => {
                    return chatRoom.getRoomPublicInfo(parseInt(roomId));
                });

                //调度台请求获取企业号下所有房间信息
                bindSocketEvent(socket, userId, Event.Recv.GET_ALL_ACTIVE_ROOM, () => {
                    const user = self.onlineUsers.get(userId);
                    if(user.commander) {
                        return chatRoom.getActiveRooms();
                    }
                });

                //抢麦
                bindSocketEvent(socket, userId, Event.Recv.CONTROL_MIC, (roomId) => {
                    return chatRoom.requestMic(self.onlineUsers.get(userId), socket, parseInt(roomId));
                });

                //释放麦
                bindSocketEvent(socket, userId, Event.Recv.RELEASE_MIC, (roomId) => {
                    return chatRoom.releaseMic(userId, socket, parseInt(roomId));
                });

                // 修改密码
                bindSocketEvent(socket, userId, Event.Recv.CHANGE_PASSWORD, (oldPassword, newPassword) => {
                    return loginUser.doChangePassword(userId, oldPassword, newPassword);
                });

                // 添加房间成员
                bindSocketEvent(socket, userId, Event.Recv.ADD_ROOM_MEMBERS, (roomId, memberIds) => {
                    return chatRoom.addRoomMembers(self.onlineUsers.get(userId), socket, parseInt(roomId), _.map(memberIds, id => parseInt(id)));
                });

                // 更改房间名称 
                bindSocketEvent(socket, userId, Event.Recv.UPDATE_ROOM_NAME, (roomId, newName) => {
                    return chatRoom.updateRoomName(userId, socket, parseInt(roomId), newName);
                });

                // 邀请房间成员
                bindSocketEvent(socket, userId, Event.Recv.INVITE_OFFLINE_ROOM_MEMBERS, roomId => {
                    return chatRoom.inviteRoomMembers(self.onlineUsers.get(userId), parseInt(roomId));
                });

                bindSocketEvent(socket, userId, Event.Recv.ICE_CANDIDATE, iceCandidate => {
                    return chatRoom.addIceCandidate(userId, iceCandidate);
                });

                bindSocketEvent(socket, userId, Event.Recv.VIDEO_CHAT_OFFER, offer => {
                    return chatRoom.processVideoChatOffer(userId, offer);
                });

                bindSocketEvent(socket, userId, Event.Recv.SEND_ROOM_MESSAGE, message => {
                    return chatRoom.sendMessage(userId, socket, message);
                });

                bindSocketEvent(socket, userId, Event.Recv.QUERY_MESSAGES, requests => {
                    return chatRoom.queryMessages(userId, requests);
                });

                // 上传位置信息
                bindSocketEvent(socket, userId, Event.Recv.UPDATE_LOCATION, locations => {
                    const user = self.onlineUsers.get(userId);
                    if (locations.length > 0) {
                        let latestLocation = locations[locations.length - 1];
                        let commanderSocket = loginUser.getCommanderSocket(userId);
                        if (commanderSocket) {

                            //@todo 这里可以优化成关联查询
                            PttContactUser.findOne({idNumber: userId}).exec(function (err, data) {
                                if (err) {
                                    return err;
                                } else {
                                    PttPosition.findOne({_id: data.position}).exec(function (err, positionData) {
                                        if (err) {
                                            res.json({status: 500, message: err.message});
                                        } else {

                                            var updData = {
                                                _id: data.id,
                                                name: data.name,
                                                speed: "0.0KM/H",
                                                userId: userId,
                                                icon: "http://" + process.env.FDFS_IMAGE_SERV_URL + ":" + process.env.FDFS_IMAGE_SERV_PORT + "/img/eSkWC1iH-NWAY6gaAACZEHru_Xc159.png_24_24",
                                                coordinates: [latestLocation.lng, latestLocation.lat],
                                                positionname: '暂无职位',
                                                lastLocationTimeCST: cmoment(latestLocation.repTime).utcOffset("+08:00").format('YYYY-MM-DD HH:mm:ss')
                                            };

                                            if (positionData) {
                                                updData.icon = "http://" + process.env.FDFS_IMAGE_SERV_URL + ":" + process.env.FDFS_IMAGE_SERV_PORT + "/img/" + positionData.image + "_24_24",
                                                    updData.positionname = positionData.name;
                                            }

                                            commanderSocket.emit(Event.Recv.UPDATE_LOCATION, updData);
                                        }
                                    });
                                }
                            });

                        }
                    }
                    return LocationHandler.saveLocation(locations, user);
                });

                // 注册ios push device token
                bindSocketEvent(socket, userId, Event.Recv.REGISTER_PUSH_DEVICE_TOKEN, (deviceType, deviceToken) => {
                    log.info(`Register Ios push device token ${userId} deviceToken: ${deviceToken}`);
                    const user = self.onlineUsers.get(userId);

                    if (user) {
                        pushClient.registerDeviceToken(userId, deviceType, deviceToken);
                        return true;
                    }

                    return false;
                });
            })
            .catch(e => {
                socket.emit(Event.Send.USER_LOGIN_FAILED, e);
                log.error(`Error logging for socket ${socket.handshake.address}: ${JSON.stringify(e)}`);
                socket.off();
                socket.disconnect();
            });
    });

    /**
     * 在线用户的数据
     *
     * @type {Map<Number, User>}
     */
    this.onlineUsers = new Map();


    /**
     * 通知信号服务器用户数据已经更新
     *
     * @param userId 用户的ID
     */
    this.onUserUpdated = function (userId) {
        const self = this;
        userId = parseInt(userId);
        co(function*() {
            let user = yield PttContactUser.findOne({idNumber: userId}).exec();
            if (!user) {
                log.error(`User(id=${userId}) not found`);
                return;
            }

            let enterprise = yield PttEnterprise.findOne({'_id': user.enterprise}).exec();
            if (!enterprise) {
                log.error(`Enterprise(id=${user.enterprise}) not found`);
                return;
            }

            let response = yield user.toLoginResponse(enterprise);

            if (loginUser.socketForUser(userId)) {
                // 如果用户在线，则需要更新在线数据
                self.onlineUsers.set(userId, user);
            }

            self.sendMessageTo(userId, Event.Send.USER_UPDATED, response);
        });
    };

    /**
     * 通知当前企业下的所有在线人员
     *
     * @param enterpriseId 企业的ID
     */
    this.onEnterpriseModeChanged = function (enterpriseId) {
        const self = this;
        co(function*() {
            log.info("--- enterpriseId : " + enterpriseId);
            let enterprise = yield PttEnterprise.findOne({'_id': enterpriseId}).exec();
            if (!enterprise) {
                log.error(`Enterprise(id=${user.enterprise}) not found`);
                return;
            }

            let users = yield PttContactUser.find({enterprise: enterpriseId}).exec();
            log.info("--- users : " + JSON.stringify(users));
            let commanders = yield PttContactUser.findCommanders(enterpriseId);
            for (let i = 0; i < users.length(); i++) {
                const u = users[i];
                const socket = loginUser.socketForUser(u.idNumber);
                if (socket) {
                    let response = yield u.toLoginResponse(enterprise, commanders);
                    socket.emit(Event.Send.USER_UPDATED, response);
                }
            }
        });
    };

    /**
     * 发送一个消息给指定的用户
     *
     * @param userId 用户 ID
     * @param event 消息类型
     * @param msg 消息正文
     */
    this.sendMessageTo = function (userId, event, msg) {
        var socket = loginUser.socketForUser(userId);
        console.log("-------- userid " + userId + "------");
        if (!socket) {
            log.info(`Error sending message ${msg} to ${userId}: user not online`);
            return;
        }

        socket.emit(event, msg);
    }
}

/**
 * 将从客户端收到的事件转发到自定义函数上, 并将函数执行结果转发回客户端
 * 函数形式: (Socket, Arg1, Arg2,...) -> Promise|*
 *
 * @param socket
 * @param userId {number}
 * @param event {string}
 * @param fn {Function}
 */
function bindSocketEvent(socket, userId, event, fn) {
    socket.on(event, function () {
        const argArray = _.toArray(arguments);
        const realAckFn = _.last(argArray);
        const fnArgs = argArray.slice(0, argArray.length - 1);

        log.debug(`<<<<<<<<<<< Received ${event}(${fnArgs}) from ${userId}(${socket.handshake.address})`);
        const ack = (r) => {
            realAckFn(r);
            if (log.getLevel() <= 1) {
                log.debug(`>>>>>>>>>> Response ${JSON.stringify(r)}`)
            }
        };

        try {
            const result = fn(...fnArgs);
            if (result instanceof Promise) {
                result.then(obj => wrapSuccess(obj))
                    .catch(e => {
                        log.error(`Error processing ${event}: ${e}`);
                        if (e.stack) {
                            log.error(e.stack);
                        }
                        return wrapFailure(e)
                    })
                    .then(ack);
            }
            else {
                ack(wrapSuccess(result));
            }
        } catch (e) {
            log.error(`Error processing ${event}: ${e}`);
            if (e.stack) {
                log.error(e.stack);
            }
            ack(wrapFailure(e));
        }

    })
}

function wrapSuccess(data) {
    return {
        success: true,
        data: data
    }
}

function wrapFailure(err) {
    return {
        success: false,
        error: err
    }
}

module.exports = SignalServer;
