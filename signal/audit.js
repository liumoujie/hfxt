"use strict";

const PttAudit = require('../models/ptt-audit');
const Logger = require('loglevel');

class Audit {

    static logEvent(name, attrs) {
        Logger.debug(`Auditing event(name=${name}, attrs=${JSON.stringify(attrs)}`);
        const evt = new PttAudit();
        evt.eventName = name;
        evt.time = new Date();
        evt.attrs = attrs;
        evt.save();
    }

    static logUserConnected(user, remoteAddress) {
        Audit.logEvent("user-connected", {
            user: user._id,
            userName: user.name,
            remoteAddress: remoteAddress,
            enterprise: user.enterprise,
        })
    }

    static logUserDisconnect(user, reason) {
        Audit.logEvent("user-disconnected", {
            user: user._id,
            userName: user.name,
            enterprise: user.enterprise,
            reason: reason,
        })
    }
}

module.exports = Audit;