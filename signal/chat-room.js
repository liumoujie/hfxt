"use strict";

var co = require('co');
var Event = require('./event');
var PttContactGroup = require('../models/ptt-contact-group');
var PttContactUser = require('../models/ptt-contact-user');
var PttChatRoom = require('../models/ptt-chat-room');
var PttEnterprise = require('../models/ptt-enterprise');
var PttNumberGennerator = require('../models/ptt-number-gennerator');
var PttMessage = require('../models/ptt-message');
var log = require('loglevel');
var LRU = require('lru-cache');
var LoginUser = require('./login-user');
var _ = require('underscore');
const KurentoClient = require('kurento-client');
const RoomMode = require('../signal/room-modes');


class ChatRoomHandler {
    constructor(loginUser, voiceServerConfiguration, webVoiceServerConfiguration, pushClient) {
        /**
         * 登陆用户信息
         * @private {LoginUser}
         */
        this.loginUser = loginUser;

        /**
         * 语音服务器的配置信息
         * @private
         */
        this.voiceServerConfiguration = voiceServerConfiguration;

        /**
         * 房间模型的缓存 id -> PttChatRoom
         *
         * @private {LRU<number, PttChatRoom>}
         */
        this.rooms = LRU(10240);

        /**
         * 房间动态信息的缓存 id -> RoomInfo
         *
         * @private {Map<number, RoomInfo>}
         */
        this.roomInfo = new Map();

        /**
         * 房间对应的定时器缓存 id -> intervalNbr
         * @private {Map<number, number>}
         */

        this.roomInterval = new Map();


        /**
         * web端接入,kurento server信息
         * @private {Map<number, RoomInfo>}
         */
        this.webVoiceServerConfiguration = webVoiceServerConfiguration;


        /**
         * 当用户没有连接到服务器时,通过push(ios apn)通知客户端
         * @private{PushClient}
         */
        this.pushClient = pushClient;

        /**
         * Kurento client 实例（存于Promise中）
         * @type {Promise<KurentoClient>}
         * @private
         */
        this.kurentoPromise = null;

        /**
         * @type {Map<number, UserKurentoEndpoint>}
         */
        this.userKurentoEndpoints = new Map();

        /**
         *
         * @type {Map<number, Promise<GroupChat>>}
         */
        this.groupChats = new Map();
    }

    /**
     * 获得Kurento实例
     * @return {Promise<KurentoClient>}
     */
    getKurentoClient() {
        const self = this;

        if (self.kurentoPromise == null) {
            //const url = `ws://netptt.cn:8888/kurento`;
            const url = "ws://" + process.env['KURENTO_SERVER_URL'] + ":" + process.env['KURENTO_SERVER_PORT'] + "/kurento";
            // const url = `ws://${self.webVoiceServerConfiguration.host}:${self.webVoiceServerConfiguration.port}/kurento`;

            log.info("Connecting to kurento at ", url);
            self.kurentoPromise = KurentoClient(url);
        }

        return self.kurentoPromise;
    }

    /**
     *
     * 查询或者创建房间的动态信息
     *
     * @param roomId {number} 房间ID
     * @param mode {RoomMode} 房间模式。如果不为空，而且，该房间没有被创建过，则在创建房间时使用这个模式，否则将被忽略。
     *
     * @return {RoomInfo} Room information object
     */
    ensureRoomInfo(roomId, mode) {
        log.info("in ensureRoomInfo roomId : ", roomId);
        const self = this;
        return co(function*() {
            let info = self.roomInfo.get(roomId);
            if (!info) {
                info = new RoomInfo();
                info.allMemberIds = yield self.getRoomMemberIds(roomId);
                info.mode = mode ? mode : RoomMode.NORMAL;
                log.info("in ensureRoomInfo allMemberIds : ", info.allMemberIds);
                self.roomInfo.set(roomId, info);
            }

            //var disableUsers =  yield PttContactUser.find({status: 1}).exec();
            //
            //console.log("---- disableUsers ")
            //console.log(JSON.stringify(self.loginUser));

            //加入定时器
            let curInterval = self.roomInterval.get(roomId);
            let curLoginUser = self.loginUser;
            if (!curInterval) {
                curInterval = setInterval(function () {
                    let curRoom = info;
                    if (curRoom && curRoom.needUpdate) {
                        let onLineMembers = curRoom.onlineMemberIds;

                        if (onLineMembers.length > 0) {
                            let firstMember = onLineMembers[0];

                            let firstSocket = curLoginUser.sockets.get(firstMember);
                            let updateData = curRoom.createOnlineMemberUpdateMessage(roomId);

                            if (firstSocket) {
                                firstSocket.emit(Event.Send.ROOM_ONLINE_MEMBER_UPDATE, updateData);
                                firstSocket.to(roomId).emit(Event.Send.ROOM_ONLINE_MEMBER_UPDATE, updateData);
                            }
                            log.info("interval is running : ", updateData);
                        }
                        curRoom.needUpdate = false;
                    }
                }, 1000);

                //将定时器加入到列表中,以后可以直接取出来注销
                self.roomInterval.set(roomId, curInterval);
            }
            log.info("interval set success and will return in the next");


            return info;
        });


    }

    /**
     *
     * 检测指定条件的房间是否已经存在
     *
     * @param name {null|string}
     * @param userId {number} 用户
     * @param groupIds {Array<string>} 与这个会话绑定的组ID
     * @param extraMemberIds {Array<string>} 除了组以外的成员列表
     * @return {Promise<RoomObject>} 房间的详细信息
     */
    isRoomExist(userId, name, groupIds, extraMemberIds){
        return co(function*() {
            let query = self.buildRoomQuery(userId, name, groupIds, extraMemberIds);
            return yield  PttChatRoom.findOne(query).exec() != null;
        });
    }

    buildRoomQuery(userId, name, groupIds, extraMemberIds){
        if (_.size(groupIds) == 0) {
            // 如果创建的房间不包含组, 则需要把自己也加入到extraMemberId中
            extraMemberIds = _.union(extraMemberIds, [userId]);
        }

        // 为了保证查询数据库时的顺序，需要对组ID和用户ID进行统一的排序
        if (groupIds instanceof Array) {
            groupIds = _.sortBy(groupIds, id => parseInt(id));
        }

        if (extraMemberIds instanceof Array) {
            extraMemberIds = _.sortBy(extraMemberIds, id => parseInt(id));
        }

        log.info("createChatRoom : groupIds = ", groupIds, ", extraMemberIds = ", extraMemberIds);

        // 查找已有房间的算法：
        // 1. 若groupId不为空, 则同时匹配groupId和extraMemberId
        // 2. 若groupId为空, 而extraMemberIds不为空, 需要匹配groupId == null及extraMemberIds

        let query = null;
        if (groupIds instanceof Array && groupIds.length > 0) {
            query = {associatedGroups: groupIds, extraMembers: extraMemberIds};
        }
        else if (extraMemberIds instanceof Array && extraMemberIds.length > 0) {
            query = {associatedGroups: null, extraMembers: extraMemberIds};
        }

        return query;
    }

    /**
     *
     * 创建一个会话房间
     *
     * @param name {null|string}
     * @param userId {number} 用户
     * @param groupIds {Array<string>} 与这个会话绑定的组ID
     * @param extraMemberIds {Array<string>} 除了组以外的成员列表
     * @return {Promise<RoomObject>} 房间的详细信息
     */
    createChatRoom(userId, name, groupIds, extraMemberIds) {
        const self = this;
        return co(function*() {
            log.info("createChatRoom groupIds = ", groupIds, ", extraMemberIds = ", extraMemberIds);
            // 查找已有房间的算法：
            // 1. 若groupId不为空, 则同时匹配groupId和extraMemberId
            // 2. 若groupId为空, 而extraMemberIds不为空, 需要匹配groupId == null及extraMemberIds
            let user = yield PttContactUser.findOne({idNumber: userId}).exec();
            if(user == null){
                throw new RoomError(`userId: ${userId} not exist`);
            }

            let enterp = yield PttEnterprise.findOne({"_id": user.enterprise}).exec();
            if(enterp == null){
                throw new RoomError(`enterprise not found for user ${userId}`);
            }

            let query = self.buildRoomQuery(userId, name, groupIds, extraMemberIds);
            if(query == null){
                throw new RoomError("invalid_params");
            }

            let isRoomExist = (yield  PttChatRoom.findOne(query).exec()) != null;

            const room = yield PttChatRoom.findOneAndUpdate(query,
                {
                    $setOnInsert: {
                        enterpriseId: enterp.idNumber,
                        owner: userId,
                        idNumber: (yield PttNumberGennerator.getNextChatRoomNumber().exec()).seq,
                        name: name
                    }
                },
                {
                    new: true,
                    upsert: true
                }).exec();

            if(room.enterpriseId != enterp.idNumber) {
                room.enterpriseId = enterp.idNumber;
                room.save();
            }

            // 将数据库查询缓存
            self.rooms.set(room.idNumber, room);
            log.info("createChatRoom : groupIds = ", groupIds, ", extraMemberIds = ", extraMemberIds, "success", "room.enterpriseId = ", room.enterpriseId);

            let rspRoom = room.toResponse();
            if(!isRoomExist){
                self.loginUser.sendMessageToCommander(Event.Send.ROOM_CREATE, rspRoom);
            }

            return rspRoom;
        });
    }

    /**
     * 获得房间的数据
     *
     * @private
     * @param roomId {number}
     * @returns {*|Promise<PttChatRoom>}
     * @throws {RoomError} 如果房间找不到
     */
    getRoom(roomId) {
        const self = this;
        return co(function*() {
            // 从缓存中拿到room信息
            let room = self.rooms.get(roomId);

            if (!room) {
                // 找不到内存中的房间, 从数据库取
                room = yield PttChatRoom.findOne({idNumber: roomId}).exec();

                if (room) {
                    // 将查询结果缓存
                    self.rooms.set(roomId, room);
                }
                else {
                    throw new RoomError("room_not_exists");
                }
            }

            //log.info("getRoom for :  ", roomId, ", result = ", room);
            return room;
        });
    }

    /**
     * 获得用户可访问的房间数据
     *
     * @param roomId {number}
     * @returns {Promise<RoomResponse>}
     * @throws {RoomError} 如果房间找不到
     */
    getRoomPublicInfo(roomId) {
        return this.getRoom(roomId).then(room => room.toResponse());
    }

    /**
     *
     * 加入一个会话
     *
     * @param user {User}
     * @param socket
     * @param roomId {number} 会话ID
     * @param fromInvitation {boolean} 是否是接受邀请加入的
     * @param mode {RoomMode} 加入房间的类型
     * @throws {RoomError} 如果该用户已经加入了一个房间
     * @param silent {boolean} 静默加入,指挥号以此实现监听房间,不通知其它人此人加入
     * @return {Promise<JoinRoomResult>}
     */
    joinRoom(user, socket, roomId, fromInvitation, mode, silent) {
        const userId = user.idNumber;
        const self = this;
        return co(function*() {
            const room = yield self.getRoom(roomId);
            const roomInfo = yield self.ensureRoomInfo(roomId, mode);
            log.info(userId, "want join room : ", roomId);

            //当前用户被踢出后,通知当前用户已经被踢出
            if (roomInfo.allMemberIds.indexOf(userId) == -1 && !silent) {
                socket.emit(Event.Send.USER_KICK_OUT_ROOM, {roomId: roomId});
                self.leaveRoom(userId, socket, roomId, false);
                return;
            }

            //是否改变房间的类型。房间类型改变的先决条件是：
            // 1. 房间发起人是第一个在线的
            // 2. 房间发起类型比当前类型的优先级高
            let changeRoomMode = false;

            //如果当前用户是受邀的人，当前房间又没有发起者（说明发起者已经退出了），上一个房间发起者没有保持会话（发起者选择了不保持会话），则不允许加入
            //监听模式下,如果房间还没有激活,直接返回,防止监听者把房间激活
            if(silent && roomInfo.onlineMemberIds.length == 0){
                return;
            }

            //如果当前用户是受邀的人，当前房间又没有发起者，上一个房间发起者没有保持会话，则不允许加入
            if (fromInvitation && roomInfo.initiatorId == null && !room.allowInviteesToJoinEmptyRoom) {
                log.debug(`Room ${roomId} has no initiator, and the user is from invitation. Rejected`);
                throw new RoomError('no_initiator');
            }
            //如果当前用户是主动发起房间，而且该房间现在没人，或者这个房间的类型优先级要高于当前的类型，那么它成为发起者
            else if (!fromInvitation &&
                (roomInfo.onlineMemberIds.length == 0 ||
                RoomMode.getPriority(mode) > RoomMode.getPriority(roomInfo.mode))) {
                roomInfo.initiatorId = userId;
                roomInfo.initiatorPriority = user.privileges.priority;
                changeRoomMode = true;
                log.debug(`Room ${roomId}'s initiator set to ${userId}`);
            }

            if (changeRoomMode) {
                roomInfo.mode = mode;
            }
            else if (roomInfo.mode != mode) {
                throw new RoomError("mode_mismatch", "当前房间正在进行" + RoomMode.getDisplayText(roomInfo.mode) + ", 无法加入房间");
            }

            //检查该用户是否加入了这个房间, 若是则直接返回
            if (_.has(socket.rooms, roomId + '')) {
                log.info("User", userId, "has joined the same room", roomId, "before");
                // 虽然检查到用户已经加入了这个房间, 为了确保数据的统一, 还是在当前数据里写清楚
                //roomInfo.onlineMemberIds.add(userId);
                roomInfo.onlineMemberIds = _.union(roomInfo.onlineMemberIds, [userId]);
                return new JoinRoomResult(roomId, roomInfo, self.voiceServerConfiguration, self.webVoiceServerConfiguration, room);
            }
            else if (_.size(socket.rooms) > 0) {
                log.debug(`User ${userId} has joined ${socket.rooms} before`);
                // 遍历退出用户之前所在的房间
                _.forEach(socket.rooms, id => {
                    const idNumber = parseInt(id);
                    if (!isNaN(idNumber)) {
                        log.debug("Force leaving room", id, "for user", userId);
                        self.leaveRoom(userId, socket, parseInt(id), false)
                    } else {
                        log.debug("Room id", id, "not a number");
                    }
                });
            }

            //处于监听状态,不将监听者加到在线名单,防止被其他人看到
            if(!silent)
                roomInfo.onlineMemberIds = _.union(roomInfo.onlineMemberIds, [userId]);
            else //记录监者
                roomInfo.allMonitorIds = _.union(room.allMonitorIds, [userId]);

            //让socket加入room的频道
            self.socketJoinRoom(socket, roomId + '');

            //向频道里面广播我的加入

            //让定时器可以通知用户
            if (!silent) //监听模式不通知其它人
                roomInfo.needUpdate = true;

            // 如果这是第一个加入的成员, 或者房间类型变了，则邀请其它成员来加入
            if (!fromInvitation &&
                (roomInfo.onlineMemberIds.length == 1 || changeRoomMode)) {
                let offlineMemberIds = self.getOfflineRoomMemberIds(room);
                log.debug(`${userId} is sending invite to offline members: ${offlineMemberIds}`);
                self.dispatchInviteToJoin(user, room, mode, offlineMemberIds);
                self.loginUser.sendMessageToCommander(Event.Send.ROOM_ACTIVE,{'mode': roomInfo.mode,
                                                                                'room': room.toResponse()}
                );
            }
            else {
                log.debug(`${userId} will not send out room invite because he's not the first member`)
            }

            if (mode == RoomMode.VIDEO || mode == RoomMode.AUDIO) {
                yield self.joinVideoChat(userId, socket, roomId);
            }

            return new JoinRoomResult(roomId, roomInfo, self.voiceServerConfiguration, self.webVoiceServerConfiguration, room);
        })
    }

    /**
     * 获得房间所有成员
     *
     * @private
     * @param room {PttChatRoom}
     * @return {Promise<Array<string>>}
     */
    getRoomMemberIds(roomId) {
        const self = this;
        return co(function*() {
            let room = yield self.getRoom(roomId);
            const roomMembersSet = new Set();

            // 查找所有房间所属的组
            _.forEach(yield PttContactGroup.find({idNumber: {$in: room.associatedGroups}}), group => {
                _.forEach(group.members, memberId => {
                    roomMembersSet.add(memberId)
                })
            });

            _.forEach(room.extraMembers, memberId => roomMembersSet.add(memberId));
            roomMembersSet.add(room.owner);

            return Array.from(roomMembersSet);
        });
    }

    /**
     * 获得不在线的房间成员
     *
     * @private
     * @param room {PttChatRoom}
     * @return {Array<number>}
     */
    getOfflineRoomMemberIds(room) {
        const roomInfo = this.roomInfo.get(room.idNumber);
        if (!roomInfo) {
            log.warn(`RoomInfo for id ${room.idNumber} doesn't exist`);
            return [];
        }

        if (roomInfo.onlineMemberIds && roomInfo.onlineMemberIds.length > 0) {
            return _.difference(roomInfo.allMemberIds, roomInfo.onlineMemberIds);
        } else {
            return roomInfo.allMemberIds;
        }
    }

    /**
     *
     * 将房间邀请分发到当前不在房间的成员中
     *
     * @param user {PttContactUser}
     * @param room {PttChatRoom}
     * @param roomMode {RoomMode}
     * @param invitees {Array<number>}
     * @return number sent
     */
    dispatchInviteToJoin(user, room, roomMode, invitees) {
        const userId = user.idNumber;
        const self = this;
        const msg = {
            inviterId: userId,
            inviterPriority: user.privileges.priority,
            force: !!user.privileges.powerInviteAble,
            room: room.toResponse(),
            roomMode: roomMode,
        };

        let i = 0;
        let offlineInvitees = [];
        try {
            invitees.forEach(memberId => {
                memberId = parseInt(memberId);
                const socket = this.loginUser.socketForUser(memberId);
                if (socket) {
                    log.debug(`${userId} is sending invite roomId = ${room.idNumber} to ${memberId}(${socket.handshake.address})`);
                    socket.emit(Event.Send.INVITE_TO_JOIN, msg);
                    i++;
                } else {
                    log.info(`${memberId} is not connected to the server.`);
                    offlineInvitees.push(memberId);
                }
            });

            self.pushClient.sendMessage(offlineInvitees, msg);
        } catch (e) {
            log.error(`Error dispatching invite: ${JSON.stringify(e)}`);
        }

        return i;
    }

    /**
     *
     * 离开房间
     *
     * @param userId {number}
     * @param socket {Socket}
     * @param roomId {number}
     * @param terminateSession {boolean} 是否把房间标记为失效并赶走其它人
     * @return {Promise}
     */
    leaveRoom(userId, socket, roomId, terminateSession) {
        const self = this;
        return co(function *() {
            const roomInfo = self.roomInfo.get(roomId);
            const room = yield self.getRoom(roomId);
            if (roomInfo && room) {
                if (roomInfo.mode == RoomMode.VIDEO || roomInfo.mode == RoomMode.AUDIO) {
                    yield self.quitVideoChat(userId, roomId);
                }

                const isMonitor = _.contains(roomInfo.allMonitorIds, userId);
                log.debug(`${userId} is a monitor(${isMonitor}) leave room ${roomId}`);
                //roomInfo.onlineMemberIds.delete(userId);
                if(!isMonitor){
                    roomInfo.onlineMemberIds = _.difference(roomInfo.onlineMemberIds, [userId]);
                    log.debug("After", userId, "leaved, online members:", roomInfo.onlineMemberIds,
                        "terminateSession: ", terminateSession, ",roomInitiator=", roomInfo.initiatorId);
                    roomInfo.needUpdate = true;
                }
                else {
                    roomInfo.allMonitorIds = _.difference(roomInfo.allMonitorIds, [userId]);
                }

                // 按照用户的要求, 将房间中的其它人赶走
                //房间里只有一个人也清除,为解决web端物理断网后,有30秒左右的超时,如果在超时断开前,无法拉起房间
                if ((terminateSession && roomInfo.initiatorId == userId &&
                    roomInfo.onlineMemberIds.length > 0) || roomInfo.onlineMemberIds.length == 1 && !isMonitor) {
                    log.debug(`${userId} is kicking out everyone in room ${roomId}`);
                    _.forEach(roomInfo.onlineMemberIds, id => {
                        const socket = self.loginUser.socketForUser(id);
                        if (socket) {
                            log.debug(`${userId} kicking out ${id}`);
                            socket.emit(Event.Send.ROOM_KICK_OUT, roomId);
                        }
                    })
                }

                if (roomInfo.initiatorId == userId) {
                    roomInfo.initiatorId = null;
                    room.allowInviteesToJoinEmptyRoom = !terminateSession;
                    room.save();
                }

                //清除map中的房间数据
                //房间里只有一个人也清除
                //!isMonitor 如果是监听者,安静的离开
                if (roomInfo.onlineMemberIds.length <= 1 && !isMonitor) {
                    self.roomInfo.delete(roomId);
                    let curInterval = self.roomInterval.get(roomId);
                    if (curInterval) {
                        clearInterval(curInterval);
                    }
                    self.roomInterval.delete(roomId);

                    self.loginUser.sendMessageToCommander(Event.Send.ROOM_DEACTIVE, room.toResponse());
                }

                self.releaseMic(userId, socket, roomId);
                //不用每个离开时都通知，而是采用定时器更新
                /*socket.to(roomId).emit(Event.Send.ROOM_ONLINE_MEMBER_UPDATE, {
                 roomId : roomId,
                 onlineMemberIds: Array.from(roomInfo.onlineMemberIds),
                 speakerId : roomInfo.speakerId
                 })*/
            }

            socket.leave(roomId);
        });
    }

    /**
     * 抢麦
     *
     * @param user {PttContactUser}
     * @param socket {Socket}
     * @param roomId {number}
     * @return {Promise<bool>}
     */
    requestMic(user, socket, roomId) {
        const self = this;
        return co(function*() {
            const userId = user.idNumber;
            const roomInfo = yield self.ensureRoomInfo(roomId);
            const userPriority = user.privileges.priority;
            if (roomInfo.speakerId == null || roomInfo.speakerId == userId ||
                (roomInfo.speakerPriority != null && userPriority != null && userPriority > roomInfo.speakerPriority)) {
                roomInfo.speakerId = userId;
                roomInfo.speakerPriority = userPriority;
                socket.to(roomId).emit(Event.Send.SPEAKER_CHANGED, roomInfo.createSpeakerUpdateMessage(roomId));
                return true;
            }

            return false;
        });
    }

    /**
     * 放麦
     *
     * @param userId {number}
     * @param socket
     * @param roomId {number}
     * @returns {*|Promise}
     */
    releaseMic(userId, socket, roomId) {
        const roomInfo = this.roomInfo.get(roomId);
        if (roomInfo && roomInfo.speakerId == userId) {
            roomInfo.speakerId = null;
            roomInfo.speakerPriority = null;
            socket.to(roomId).emit(Event.Send.SPEAKER_CHANGED, roomInfo.createSpeakerUpdateMessage(roomId));
        }

        return Promise.resolve();
    }

    /**
     * 加入一个视频聊天
     *
     * @param userId {number}
     * @param socket
     * @param roomId {number} 房间号
     */
    joinVideoChat(userId, socket, roomId) {
        const self = this;
        return co(function*() {
            let oldEndpoint = self.userKurentoEndpoints.get(userId);
            if (oldEndpoint) {
                log.info("Disconnecting old endpoint");
                yield self.quitVideoChat(userId, oldEndpoint.roomId);
            }

            let kurentoClient = yield self.getKurentoClient();
            let groupChat = self.groupChats.get(roomId);
            if (groupChat == null) {
                self.groupChats.set(roomId, groupChat = GroupChat.create(kurentoClient));
            }

            groupChat = yield groupChat;

            let kurentoEndpoint = yield UserKurentoEndpoint.create(groupChat, roomId);

            self.userKurentoEndpoints.set(userId, kurentoEndpoint);
            groupChat.connectedEndpoints.push(kurentoEndpoint);
            kurentoEndpoint.endpoint.on('OnIceCandidate', event => {
                log.debug("Got ice candidate from kurento", event.candidate);
                socket.emit(Event.Send.ICE_CANDIDATE, event.candidate);
            });
        });
    }

    processVideoChatOffer(userId, offer) {
        const self = this;
        return co(function*() {
            const kurentoEndpoint = self.userKurentoEndpoints.get(userId);
            if (kurentoEndpoint == null) {
                throw new RoomError("endpoint_not_ready", "Can not add offer without creating webrtc first");
            }

            const answer = yield kurentoEndpoint.endpoint.processOffer(offer);
            kurentoEndpoint.endpoint.gatherCandidates();
            return answer;
        });

    }

    quitVideoChat(userId, roomId) {
        const self = this;
        return co(function *() {
            let endpoint = self.userKurentoEndpoints.get(userId);
            if (endpoint) {
                endpoint.release();
                self.userKurentoEndpoints.delete(userId);
            }
            else {
                log.warn("No webrtc endpoint created for user", userId);
                return;
            }

            let groupChat = self.groupChats.get(roomId);
            if (!groupChat) {
                log.warn("No group chat created for", roomId);
                return;
            }

            groupChat = yield groupChat;

            groupChat.connectedEndpoints = _.without(groupChat.connectedEndpoints, endpoint);
            if (_.isEmpty(groupChat.connectedEndpoints)) {
                groupChat.release();
                self.groupChats.delete(roomId);
            }
        });
    }

    addIceCandidate(userId, iceCandidate) {
        const self = this;
        return co(function *() {
            const endpoint = self.userKurentoEndpoints.get(userId);
            if (!endpoint) {
                throw new RoomError("endpoint_not_ready", "Can not add ice candidate without creating webrtc endpoint first");
            }

            return endpoint.endpoint.addIceCandidate(KurentoClient.getComplexType('IceCandidate')(iceCandidate));
        });
    }


    /**
     * 请求增加房间的成员
     *
     * @param user {User}
     * @param socket
     * @param roomId {number}
     * @param requestMemberIds {Array<number>}
     * @returns {*|Promise<RoomObject>}
     */
    addRoomMembers(user, socket, roomId, requestMemberIds) {
        const self = this;
        return co(function*() {
            const room = yield self.getRoom(roomId);
            const roomInfo = yield self.ensureRoomInfo(roomId);
            let allRoomMembers = roomInfo.allMemberIds;

            const newMemberIds = _.without(requestMemberIds, ...allRoomMembers);
            if (_.size(newMemberIds) == 0) {
                // 没有新的成员
                log.warn(`Requested members ${requestMemberIds} already exist in room ${roomId}`);
                return room.toResponse();
            }

            log.info(`Adding ${newMemberIds} to room ${roomId}`);
            room.extraMembers.push(...newMemberIds);
            room.save();

            roomInfo.allMemberIds.push(...newMemberIds);

            // 如果当前用户在这个房间里， 则邀请新成员加入
            if (_.has(socket.rooms, roomId + '')) {
                self.dispatchInviteToJoin(user, room, roomInfo.mode, newMemberIds);
            }

            // 将组更新信息发给在线成员
            const response = room.toResponse();
            response.onlineMemberIds = roomInfo.onlineMemberIds;
            socket.to(roomId + '').emit(Event.Send.ROOM_UPDATE, response);
            //self.loginUser.sendMessageToCommander(Event.Send.ROOM_ACTIVE, response);

            return response;
        });
    }

    /**
     * 更新房间名称
     *
     * @param userId {number}
     * @param socket
     * @param roomId {number}
     * @param newName {string}
     */
    updateRoomName(userId, socket, roomId, newName) {
        const self = this;
        return co(function*() {
            const room = yield self.getRoom(roomId);
            room.name = newName;
            yield room.save().exec();

            // 将组更新信息发给在线成员
            const response = room.toResponse();
            socket.to(roomId).emit(Event.Send.ROOM_UPDATE, response);

            self.loginUser.sendMessageToCommander(Event.Send.ROOM_UPDATE, response);
            return response;
        });
    }

    /**
     *
     * 邀请房间离线成员加入房间
     *
     * @param user {User}
     * @param roomId {number}
     */
    inviteRoomMembers(user, roomId) {
        const self = this;
        return co(function *() {
            const room = yield self.getRoom(roomId);
            const roomInfo = self.roomInfo.get(room.idNumber);
            if (roomInfo) {
                const invitees = self.getOfflineRoomMemberIds(room);
                return self.dispatchInviteToJoin(user, room, roomInfo.mode, invitees);
            }
        });
    }

    /**
     *
     * 向指定房间发送一条消息
     *
     * @param userId {Number} 当前用户ID
     * @param socket {Socket} 当前用户的Socket
     * @param message {Object} 消息主体。此为PttMessage的子集。
     * @return {Promise<PttMessage>} 发送的消息（带有额外的信息，比如发送时间等）
     */
    sendMessage(userId, socket, message) {
        const self = this;

        return co(function *() {
            if (!message.roomId || !message.localId) {
                throw new RoomError("invalid_message", "消息格式错误");
            }

            log.info("Saving", message, "to database");
            let savedMessage = yield PttMessage.findOneAndUpdate(
                {
                    'senderId': userId,
                    'roomId': message.roomId,
                    'localId': message.localId,
                },
                {
                    'senderId': userId,
                    'sendTime': new Date(),
                    'localId': message.localId,
                    'roomId': message.roomId,
                    'type': message.type,
                    'body': message.body,
                },
                {
                    'new': true,
                    'upsert': true,
                });


            let memberIds = yield self.getRoomMemberIds(parseInt(message.roomId));
            //如果是指挥账号以监听形式进入退出房间,不向其它端发送相关信息
            if("join_room" == message.type || "quit_room" == message.type){
                if(!_.contains(memberIds, userId)){
                    return savedMessage;
                }
            }

            _.forEach(memberIds, memberId => {
                if (_.isEqual(userId, memberId)) {
                    return;
                }

                let targetUser = self.loginUser.socketForUser(memberId);
                if (targetUser) {
                    log.info("Sending", savedMessage, "to member id=", memberId);
                    targetUser.emit(Event.Send.ROOM_MESSAGE, savedMessage);
                }
            });

            return savedMessage;
        });
    }

    /**
     * 批量查询消息。
     * 查询的结构为:
     * {
     *   roomId: String?,
     *   startTime: Long?,
     *   endTime: Long?
     * }
     *
     * 返回结果：
     * [
     *   {
      *    syncTime: String,
      *    roomId: String,
      *    data: Array<PttMessage>
      *  }
     * ]
     *
     * @param userId {Number} 当前用户ID
     * @param requests {Array} 查询条件
     * @return {Promise.<Array<QueryResult>>} 相应的查询结果
     */
    queryMessages(userId, requests) {
        return Promise.all(
            _.map(requests, request => {
                    const endDate = request.endDate ? new Date(request.endDate) : new Date();
                    return PttMessage.findMessages(
                        parseInt(userId),
                        request.roomId ? parseInt(request.roomId) : null,
                        request.startTime ? new Date(request.startTime) : null,
                        endDate)
                        .then(data => {
                            return {
                                "syncTime": endDate,
                                "data": data,
                                "roomId": request.roomId
                            }
                        });
                }
            )
        )
    }

    /**
     * 用户退出时的清理动作
     *
     * @param userId {number}
     * @param socket
     */
    disconnect(userId, socket) {
        const self = this;
        _.forEach(socket.rooms, roomId => {
            self.quitVideoChat(userId);
            const idNumber = parseInt(roomId);
            if (isNaN(idNumber)) {
                return;
            }
            self.leaveRoom(userId, socket, idNumber);
        })
    }

    /**
     * 让socket加入一个房间
     *
     * @param socket
     * @param roomName {String}
     * @returns {Promise}
     * @private
     */
    socketJoinRoom(socket, roomName) {
        return new Promise((resolve, reject) => {
            socket.join(roomName, err => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        });
    }

    /**
     * 获取当前所有活跃房间
     * 返回结果:
     * [
     *  {
     *      'roomId':
     *  },
     *  ...
     * ]
     */
    getActiveRooms(){
        let self = this;
        return co(function*(){
            let rooms = [];
            //log.info(`getActiveRooms this.roomInfo = ${self.roomInfo}, length = ${Array.from(self.roomInfo.keys()).length}`);
            for (var [key, value] of self.roomInfo.entries()) {
                rooms.push({'mode':value.mode,
                            'room': yield self.getRoom(key)});
            }

            //log.info(`getActiveRooms rooms = ${rooms}, length=${rooms.length}`);
            return rooms;
        });
    }

    /**
     *
     * @param enterprisedId {Number} 企业id
     *
     * @return {Array<PttChatRoom>} 所属企业下的所有会话
     */
    getAllRoomsByEnterpriseId(enterprisedId){
        log.info(`ChatRoom.getAllRoomsByEnterpriseId enterprisedId = ${enterprisedId}`);
        return co(function*(){
            let rooms = yield PttChatRoom.find({'enterpriseId': enterprisedId}).exec();
            let roomObjects = [];

            _.forEach(rooms, room=>{
                roomObjects.push(room.toResponse());
            });

            return roomObjects;
        })
    }
}

/**
 * 保存房间的动态信息
 */
class RoomInfo {
    /**
     * 创建一个房间动态信息
     */
    constructor() {
        /**
         * @type {Set<number>}
         */

        //this.onlineMemberIds = new Set();

        //将存储的数据结构修改为 array，解决定时器更新时无法获取Set值的问题
        this.onlineMemberIds = [];
        /**
         * @type {string|null}
         */
        this.speakerId = null;

        /**
         * 当前发言者的等级
         *
         * @type {number|null}
         */
        this.speakerPriority = null;

        /**
         * 当前房间的发起人
         *
         * @type {number|null}
         */
        this.initiatorId = null;

        /**
         * 当前房间发起人的权限
         *
         * @type {number|null}
         */
        this.initiatorPriority = null;

        /**
         * @type{boolean|false}
         */
        this.needUpdate = false;

        /**
         * 当前房间的模式
         *
         * @type {RoomMode}
         */
        this.mode = RoomMode.NORMAL;

        /**
         * 所有成员
         */
        this.allMemberIds = [];

        /**
         *当前房间的所有监听者
         */
        this.allMonitorIds = []
    }

    /**
     * 创建一个向客户端通知的房间发言人变化的通知
     *
     * @param roomId
     * @returns {{roomId: *, speakerId: (string|null), speakerPriority: (number|null)}}
     */
    createSpeakerUpdateMessage(roomId) {
        return {
            roomId: roomId,
            speakerId: this.speakerId,
            speakerPriority: this.speakerPriority
        }
    }

    /**
     * 创建一个向客户端通知房间在线人员变化的通知
     *
     * @param roomId
     * @returns {{roomId: *, onlineMemberIds: Array, speakerId: (string|null), speakerPriority: (number|null)}}
     */
    createOnlineMemberUpdateMessage(roomId) {
        return {
            roomId: roomId,
            onlineMemberIds: this.onlineMemberIds,
            speakerId: this.speakerId,
            speakerPriority: this.speakerPriority
        };
    }
}

/**
 * 存储多人视频聊天的结构
 */
class GroupChat {
    /**
     *
     * @private
     * @param mediaPipeline {MediaPipeline}
     * @param composite {Composite}
     */
    constructor(mediaPipeline, composite) {
        this.mediaPipeline = mediaPipeline;
        this.composite = composite;

        /**
         * @type {Array<UserKurentoEndpoint>}
         */
        this.connectedEndpoints = [];
    }

    release() {
        log.info("Removing group chat");
        this.composite.release();
        this.mediaPipeline.release();
        _.forEach(this.connectedEndpoints, endpoint => endpoint.release());
    }

    /**
     *
     * @param kurentoClient {KurentoClient}
     * @return {Promise<GroupChat>}
     */
    static create(kurentoClient) {
        return co(function *() {
            let pipeline, composite;

            try {
                pipeline = yield kurentoClient.create('MediaPipeline');
                composite = yield pipeline.create('Composite');

                return new GroupChat(pipeline, composite);
            }
            catch (e) {
                if (composite) {
                    composite.release();
                }

                if (pipeline) {
                    pipeline.release();
                }

                throw e;
            }
        });
    }
}

class UserKurentoEndpoint {
    /**
     * @private
     * @param hubPort {HubPort}
     * @param endpoint {WebRtcEndpoint}
     * @param inputFilter {Array<Filter>}
     * @param outputFilter {Array<Filter>}
     * @param roomId {Number}
     */
    constructor(hubPort, endpoint, inputFilters, outputFilters, roomId) {
        /**
         * @type {HubPort}
         */
        this.hubPort = hubPort;

        /**
         * @type {WebRtcEndpoint}
         */
        this.endpoint = endpoint;

        /**
         * @type {Array<Filter>}
         */
        this.inputFilters = inputFilters;

        /**
         * @type {Array<Filter>}
         */
        this.outputFilters = outputFilters;


        /**
         * @type {number}
         */
        this.roomId = roomId;
    }

    release() {
        _.forEach(this.outputFilters, f => f.release());
        _.forEach(this.inputFilters, f => f.release());
        this.hubPort.release();
        this.endpoint.release();
        log.info("Removing user's kurento endpoint");
    }

    /**
     *
     * @param groupChat {GroupChat}
     * @param roomId {number}
     * @return Promise<UserKurentoEndpoint>
     */
    static create(groupChat, roomId) {
        return co(function *() {
            let endpoint, hubPort, inputFilters = [], outputFilters = [];

            try {
                endpoint = yield groupChat.mediaPipeline.create('WebRtcEndpoint');
                //endpoint.setStunServerAddress("121.43.152.43");
                endpoint.setStunServerAddress(process.env['TURN_SERVER_URL']);
                //endpoint.setStunServerPort(3478);
                endpoint.setStunServerPort(parseInt(process.env['TURN_SERVER_PORT']));

                hubPort = yield groupChat.composite.createHubPort();
                // inputFilters.push(yield groupChat.mediaPipeline.create('videobox autocrop=true'));
                // inputFilters.push(yield groupChat.mediaPipeline.create('GStreamerFilter', {command: 'videobox autocrop=true'}));
                // inputFilters.push(yield groupChat.mediaPipeline.create('GStreamerFilter', {command: 'capsfilter caps=video/x-raw,width=512,height=512'}));
                // outputFilters.push(yield groupChat.mediaPipeline.create('GStreamerFilter', {command: 'videobox autocrop=true'}));
                // outputFilters.push(yield groupChat.mediaPipeline.create('GStreamerFilter', {command: 'capsfilter caps=video/x-raw,width=512,height=512'}));

                // Connect input filters to each other...
                for (let i = 0, size = inputFilters.length; i < size - 1; i++) {
                    inputFilters[i].connect(inputFilters[i+1]);
                }

                // Connect input filters between endpoint and hubPort
                if (inputFilters.length > 0) {
                    endpoint.connect(inputFilters[0]);
                    _.last(inputFilters).connect(hubPort);
                } else {
                    endpoint.connect(hubPort);
                }

                // Connect output filters to each other...
                for (let i = 0, size = outputFilters.length; i < size - 1; i++) {
                    outputFilters[i].connect(outputFilters[i+1]);
                }

                // Connect output filters between endpoint and hubPort
                if (outputFilters.length > 0) {
                    hubPort.connect(outputFilters[0]);
                    _.last(outputFilters).connect(endpoint);
                }
                else {
                    hubPort.connect(endpoint);
                }

                return new UserKurentoEndpoint(hubPort, endpoint, inputFilters, outputFilters, roomId);
            }
            catch (e) {
                _.forEach(outputFilters, f => f.release());
                _.forEach(inputFilters, f => f.release());

                if (hubPort) {
                    hubPort.release();
                }

                if (endpoint) {
                    endpoint.release();
                }

                throw e;
            }
        });
    }
}

/**
 * 加入房间的结果
 */
class JoinRoomResult {
    /**
     *
     * @param roomId {Number}
     * @param roomInfo {RoomInfo}
     * @param voiceServerConfiguration
     * @param webVoiceServerConfiguration web端语跟团服务器(kurento)
     * @param room {PttChatRoom}
     */
    constructor(roomId, roomInfo, voiceServerConfiguration, webVoiceServerConfiguration, room) {
        this.room = room.toResponse();
        this.roomId = roomId;
        //this.onlineMemberIds = Array.from(roomInfo.onlineMemberIds);
        this.onlineMemberIds = roomInfo.onlineMemberIds;
        this.initiatorUserId = roomInfo.initiatorId;
        this.initiatorPriority = roomInfo.initiatorPriority;
        this.roomMode = roomInfo.mode;
        this.speakerId = roomInfo.speakerId;
        this.voiceServer = voiceServerConfiguration;
        this.webVoiceServer = webVoiceServerConfiguration;

        this.iceServerUrls = [];
        if (process.env.KMS_STUN_IP && process.env.KMS_STUN_PORT) {
            this.iceServerUrls.push(`stun:${process.env.KMS_STUN_IP}:${process.env.KMS_STUN_PORT}`);
        }
    }
}

class RoomError {
    constructor(name, message) {
        this.name = name;
        this.message = message;
    }
}

module.exports = ChatRoomHandler;
