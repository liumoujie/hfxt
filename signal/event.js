/**
 * Created by suke on 15/12/14.
 */

module.exports = {
    //服务器收到的socket.io事件.
    Recv: {
        //同步通讯录
        SYNC_CONTACTS: "c_sync_contact",

        //新建一个房间
        CREATE_CHAT_ROOM: "c_create_room",
        //加入一个房间
        JOIN_ROOM: "c_join_room",

        //离开一个房间
        LEAVE_ROOM: "c_leave_room",

        //请求一个房间的基础数据
        GET_ROOM_INFO: "c_room_info",

        //调度台请求获取企业号下所有房间信息
        GET_ALL_ACTIVE_ROOM: 'c_all_active_rooms',

        //抢麦
        CONTROL_MIC: "c_control_mic",
        //释放麦
        RELEASE_MIC: "c_release_mic",

        // 修改密码
        CHANGE_PASSWORD: "c_change_pwd",
        
        // 添加房间成员
        ADD_ROOM_MEMBERS: "c_add_room_members",
        
        // 更改房间名称 
        UPDATE_ROOM_NAME: "c_update_room_name",

        // 手动通知房间成员
        INVITE_OFFLINE_ROOM_MEMBERS:"c_invite_room_members",

        // 上报位置信息
        UPDATE_LOCATION: "c_update_location",

        //上报设备的push token
        REGISTER_PUSH_DEVICE_TOKEN: "c_register_push_device_token",

        // 客户端通知有新的ICE CANDIDATE
        ICE_CANDIDATE: 'c_ice_candidate',

        // 客户端发出WebRtc OFFER
        VIDEO_CHAT_OFFER: 'c_offer',

        // 客户端向一个房间发出消息
        SEND_ROOM_MESSAGE: 'c_send_message',

        // 客户端查询消息记录
        QUERY_MESSAGES: 'c_query_messages',
    },

    //服务器发出的socket.io事件.
    Send: {
        //用户登录成功
        USER_LOGON: "s_logon",

        USER_LOGIN_FAILED: "s_login_failed",

        // 用户信息发生变化
        USER_UPDATED: "s_user_updated",

        //群成员变化
        ROOM_UPDATE: "s_member_update",

        //在线成员变化
        ROOM_ONLINE_MEMBER_UPDATE: "s_online_member_update",

        //当前的发言者发生了变化
        SPEAKER_CHANGED: "s_speaker_changed",

        //房间的状态变化的摘要信息:成员和发言者, 这条消息主要用在用户在线,但没在房间的时候通知.
        ROOM_SUMMARY: "s_room_summary",

        //当第一个人加入一个房间时, 向其它人发出邀请加入的信号
        INVITE_TO_JOIN: "s_invite_to_join",

        //新的登录用户把已有用户踢出了
        USER_KICK_OUT: "s_kick_out",

        //用户被踢出房间
        ROOM_KICK_OUT: "s_kick_out_room",

        //用户主动下线通知
        USER_OFFLINE: "s_user_offline",

        //用户上线通知
        USER_ONLINE: "s_user_online",

        //立即上报位置
        USER_LOCATE: "s_user_locate",

        //通知界面指挥时间结束
        COMMAND_MODE_END: "s_command_mode_end",

        //通知客户端服务器端有新的ICE CANDIDATE
        ICE_CANDIDATE: 's_ice_candidate',

        //通知客户端房间里有新消息
        ROOM_MESSAGE: 's_room_message',

        //以下三个事件在指挥账号登录将会发送
        //活跃的房间
        ROOM_ACTIVE: 's_room_active',

        //房间由活跃变得不活跃
        ROOM_DEACTIVE: 's_room_deactive',

        //有新房间创建
        ROOM_CREATE: 's_room_create'
    }
};