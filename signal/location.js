"use strict";

const co = require('co');
const PttGisLocation = require('../models/ptt-gis-location');
let request = require("co-request");
const _ = require('underscore');
const loglevel = require('loglevel');
const PttContactUser = require('../models/ptt-contact-user');


class LocationHandler {

    /**
     * 在指定的边界中查找用户
     *
     * @param userObjectIds Array<ObjectID> 要查找人员的ObjectID
     * @param startTime integer 开始时间
     * @param endTime integer 结束时间
     * @return Promise<Array<PttContactUser>> 找到的用户
     * */
    static findUserLocation(userObjectIds, startTime, endTime) {
        const query = {
            "user": {
                $in: userObjectIds
            },
            "repTime": {
                "$gte": new Date(parseInt(startTime)),
                "$lte": new Date(parseInt(endTime)),
            }
        };

        console.log(query);

        return PttGisLocation.find(query).sort('-time').exec();
    }

    /**
     * 查询用户最后上报的位置信息
     * @param user
     * @return PttGisLocation
     */
    static findUserLastLocation(userObjectId){
        const query = {
            "user": {
                $eq: userObjectId
            },
        };

        console.log(query);
        return PttGisLocation.find(query).sort({'repTime': -1}).limit(1).exec();
    }

    /**
     * 在指定的边界中查找用户
     *
     * @param enterpriseObjectId ObjectID 要查找的企业ObjectID
     * @param minLat float 最小纬度
     * @param minLng float 最小经度
     * @param maxLat float 最大纬度
     * @param maxLng float 最大经度
     * @return Promise<Array<PttContactUser>> 找到的用户
     */
    static findUsersWithinBoundary(enterpriseObjectId, minLat, minLng, maxLat, maxLng) {
        let boundary = {
            type: 'Polygon',
            coordinates: [[
                //topLeft
                [minLng, minLat],

                //topRight
                [maxLng, minLat],

                //bottomRight
                [maxLng, maxLat],

                //bottomLeft
                [minLng, maxLat],

                //topLeft
                [minLng, minLat],
            ]],
        };

        return PttContactUser.find({
            enterprise: enterpriseObjectId,
            lastLocation: {
                $geoWithin: {
                    $geometry: boundary
                }
            }
        }).exec();
    }


    /**
     * 保存位置信息
     *
     * @param locations Array<PttGisLocation> 位置数组
     * @param user PttContactUser 用户
     * @return Promise 保存结果
     */
    static saveLocation(locations, user) {
        return co(function *() {
            if (!_.isArray(locations) || locations.length <= 0) {
                loglevel.warn("SAVE LOCATION: Location data is empty");
                throw new LocError("empty_data", "Location data is empty");
            }

            if (!user) {
                loglevel.warn("SAVE LOCATION: User is empty");
                throw new Location("invalid_user", "User is missing");
            }

            loglevel.info("Saving", JSON.stringify(locations), locations.length, "locations for user", user);

            locations.forEach(loc => {
                loc.user = user._id;
            });

            yield PttGisLocation.insertMany(locations);

            // 找到最新的位置并更新到用户数据里方便以后查询
            let latestLocation = _.max(locations, loc => loc.repTime);
            if (latestLocation) {
                loglevel.info("Update latest location", latestLocation, "for user", user.idNumber);
                yield PttContactUser.update({_id: user._id}, {
                    $set: {
                        lastLocation: {
                            type: 'Point',
                            coordinates: [latestLocation.lng, latestLocation.lat]
                        },

                        lastLocationTime: latestLocation.repTime,
                    }
                });
            }

            return locations.length;
        });
    }

}

class LocError {
    constructor(name, message) {
        this.name = name;
        this.message = message;
    }
}

module.exports = LocationHandler;
