/**
 * Created by suke on 15/12/14.
 *
 * 信号服务器对登录的处理
 *
 */

var PttEnterprise = require('../models/ptt-enterprise');
var PttContactUser = require('../models/ptt-contact-user');
var PttUtil = require('../common/ptt-util');
var PttToken = require('../models/ptt-token');
var Event = require('./event');
var co = require('co');
var log = require('loglevel');
var crypto = require('crypto');
var _ = require('underscore');

function LoginUser(saltLength, cipherKey, pushServerAddr) {
    // socketId -> userId
    this.users = new Map();

    // userId -> socket
    this.sockets = new Map();

    // enterpriseId -> commander
    this.commanders = new Map();

    // 加密salt长度
    this.saltLength = saltLength;

    // 加密密钥
    this.cipherKey = Buffer.from(cipherKey, 'ascii');

    // 推送服务器的地址
    this.pushServerAddr = pushServerAddr;
}

/**
 * 获取Socket header中包含的设备号
 *
 * @param socket Socket
 * @returns string?
 */
function getDeviceId(socket) {
    return socket.handshake.headers["x-device-id"];
}

/**
 * 执行登陆。
 * 若成功，则返回用户数据以及已经存在的老的Socket
 *
 * @param socket
 * @returns {*|Promise<PttContactUser, Socket>}
 */
LoginUser.prototype.doLogin = function (socket) {
    var _this = this;

    return co(function*() {
        //不能为空
        if (!socket) {
            throw new LoginError("invalid_arguments", "Socket can't be null");
        }

        log.info('Received connection from client', socket.handshake.address, ",deviceId:", getDeviceId(socket));

        //FIXME:authorization变为小写的总是,以及Basic标准验证方式.


        //首先对接入的socket进入验证
        var auth = socket.handshake.headers.authorization;

        if (log.getLevel() <= 1) {
            log.debug("headers : " + JSON.stringify(socket.handshake.headers));
            log.debug(auth);
        }

        //web界面登陆的websocket直接从 cookie 里取 Authorization 信息
        if (!auth) {
            var reqCookies = socket.handshake.headers.cookie;

            log.debug("reqCookies : " + reqCookies);
            if (reqCookies) {
                var cookieKVs = reqCookies.split(";");
                for (var i in cookieKVs) {
                    log.debug(cookieKVs[i]);
                    var authIndex = cookieKVs[i].indexOf("Authorization=");
                    log.debug(authIndex);
                    if (authIndex >= 0) {
                        //var auth = cookieKVs[i].substring((authIndex + 14),(cookieKVs[i].length - 6));  //localhost
                        //var auth = cookieKVs[i].substring((authIndex + 14),(cookieKVs[i].length - 3));  //localhost phone&phone
                        //var auth = cookieKVs[i].substring((authIndex + 14),(cookieKVs[i].length - 3));  //localhost mail
                        var auth = cookieKVs[i].substring((authIndex + 14), (cookieKVs[i].length - 6));
                        break;
                    }
                }
            } else {
                throw new LoginError("invalid_auth", "Authorization header can't be null");
            }
        }

        console.log(auth);

        //Base64 Decode   [user:password]
        var logInfo = new Buffer(auth.substr(6), 'base64').toString().split(':');

        if (log.getLevel() <= 1) {
            log.debug("Login Info:", JSON.stringify(logInfo));
        }

        //既不是用户名和密码的方式,也不是token的方式,也不是电话号码+密码或者电子邮箱+密码的方式
        if (_.size(logInfo) != 3 && _.size(logInfo) != 2 && _.size(logInfo) != 1) {
            throw new LoginError("invalid_auth", "Authorization header is invalid");
        }

        let user = null;

        //对于只传入了一个参数的情况,当作为传入的是token处理
        if (_.size(logInfo) == 1) {
            let tokenUser = yield PttToken.findOne({token: logInfo[0]}).exec();
            if (!tokenUser) {
                throw new LoginError("invalid_auth", "Bad token");
            } else if ((Date.now() - tokenUser.createTime ) > 24 * 3600 * 1000) {
                throw new LoginError("invalid_auth", "Token has expired");
            } else {
                //用户签权验证
                user = yield PttContactUser.findOne({idNumber: tokenUser.idNumber}).exec();
            }
        }

        if (_.size(logInfo) == 2) {
            //用户签权验证
            log.debug("before query 使用用户名:" + logInfo[0] + " 密码: " + logInfo[1] + " 登录");
            user = yield PttContactUser.findOne({idNumber: logInfo[0], password: logInfo[1]}).exec();
            log.debug("使用用户名:" + logInfo[0] + " 密码: " + logInfo[1] + " 登录");
            log.debug(logInfo[1]);
            if (!user) {
                throw new LoginError("invalid_password", "用户名/密码不匹配");
            }
        }

        if (_.size(logInfo) == 3) {
            if (logInfo[2] === 'PHONE') {
                if (PttUtil.checkPhone(logInfo[0])) {
                    //电话号码+密码的方式
                    user = yield PttContactUser.findOne({phoneNbr: logInfo[0], password: logInfo[1]}).exec();

                    if (!user) {
                        log.debug("使用电话号码:" + logInfo[0] + " 密码: " + logInfo[1] + " 登录不成功");
                        throw new LoginError("invalid_password", "电话号码/密码不匹配");
                    }
                } else {
                    log.debug("使用电话号码:" + logInfo[0] + " 密码: " + logInfo[1] + " 登录不成功");
                    throw new LoginError("invalid_password", "电话号码格式不对");
                }
            } else if (logInfo[2] === 'MAIL') {
                if (PttUtil.checkMail(logInfo[0])) {
                    //电话邮箱+密码的方式
                    user = yield PttContactUser.findOne({mail: logInfo[0], password: logInfo[1]}).exec();

                    if (!user) {
                        log.debug("使用邮箱:" + logInfo[0] + " 密码: " + logInfo[1] + " 登录不成功");
                        throw new LoginError("invalid_password", "电子邮箱/密码不匹配");
                    }
                } else {
                    log.debug("使用电话号码:" + logInfo[0] + " 密码: " + logInfo[1] + " 登录不成功");
                    throw new LoginError("invalid_password", "电子邮箱格式不对");
                }
            } else {
                log.debug("不支持'" + logInfo[2] + "',这种登录方式");
                throw new LoginError("invalid_password", "不支持此种类型的登陆方式");
            }
        }

        //log.debug("Found user: ", user);
        //此用户已停用
        if (user.status == 1) {
            throw new LoginError("users_disable", "该用户已停用");
        }

        //如果当前用户已经登录,再次登录需要把前一个先断开.
        var oldSocket = _this.socketForUser(user.idNumber);
        if (oldSocket && getDeviceId(oldSocket) && getDeviceId(socket) && !(_.isEqual(getDeviceId(oldSocket), getDeviceId(socket)))) {
            log.info("Kicking off old user:", user.idNumber, " becase old device id is",
                getDeviceId(oldSocket), "where new id is", getDeviceId(socket));
            // 给这个用户一个下线通知
            oldSocket.emit(Event.Send.USER_KICK_OUT, {});

            // 断开用户
            // FIXME:需要测试是否OK.
            oldSocket.disconnect();
        }

        var enterprise = yield PttEnterprise.findOne({'_id': user.enterprise}).exec();

        log.debug(enterprise.expTime + "               " + Date.now());
        log.debug("++++++ user is : " + JSON.stringify(user));
        if (enterprise.expTime) {
            if (enterprise.expTime <= Date.now()) {
                var expUser = yield PttContactUser.find({'enterprise': user.enterprise}).exec();
                for (var u = 0; u < expUser.length; u++) {
                    if (user.idNumber != expUser[u].idNumber) {
                        var expSocket = _this.sockets.get(expUser[u].idNumber);
                        if (expSocket) {
                            expSocket.disconnect();
                        }
                        _this.sockets.delete(expUser[u].idNumber);
                    }
                }
                log.debug("enterprise_exp", "企业帐号已过期");
                // socket.close();
                throw new LoginError("enterprise_exp", "企业帐号已过期");
            }
        }

        // 将用户和Socket一一对应起来
        _this.users.set(socket.id, user.idNumber);
        _this.sockets.set(user.idNumber, socket);

        log.debug("sockets : " + _this.sockets);
        const response = yield user.toLoginResponse(enterprise);
        log.debug("this is the response : " + JSON.stringify(response));
        //登录成功响应
        socket.emit(Event.Send.USER_LOGON, response);

        log.debug("---------commaders : " + _this.commanders.size);

        //如果当前登录的人不是 commander  通过企业查找commander 并且 commander web 在线,直接通知 web 此员工的上线信息
        //如果当前登录人就是 commander 直接设置 commanders 这个 map
        if (user.commander) {
            _this.commanders.set(enterprise.idNumber, socket);
            log.debug("-- set commander map success key : " + enterprise.idNumber + "  value : " + user.idNumber);
            // 找出已经本企业已经登录过的人,并通过指挥号这些用户是在线的
            _this.sockets.forEach(function (value, key, map) {
                var localKey = key + "";
                log.debug(localKey.indexOf(enterprise.idNumber + ""));
                //log.debug("keys : = " + _.keys(_this.sockets));
                if (localKey.indexOf(enterprise.idNumber + "") == 0 && user.idNumber != localKey) {
                    log.debug(localKey + " will be emit ");
                    socket.emit(Event.Send.USER_ONLINE, localKey);
                }
            });

        } else {
            var userCommanderSocket = _this.commanders.get(enterprise.idNumber);
            //log.debug("--- found Commander : " + userCommanderIdNumber)
            if (userCommanderSocket) {
                log.debug("on line status emited");
                userCommanderSocket.emit(Event.Send.USER_ONLINE, user.idNumber);
            }
        }
        //if(enterPriseCommander.idNumber != user.idNumber){
        //    var commandSocket = _this.sockets.get(enterPriseCommander.idNumber);
        //    if(commandSocket){
        //        log.debug("on line status emited");
        //        commandSocket.emit(Event.Send.ROOM_ONLINE_MEMBER_UPDATE,user);
        //    }
        //}

        return {
            "user": user,
            "oldSocket": oldSocket
        };
    });
};

//执行退出
LoginUser.prototype.doLogout = function (socket, reason) {
    var _this = this;
    // 先判断这个socket是否是存在于在线用户中, 再决定是否删除内存数据
    var userId = this.userForSocket(socket.id);
    log.info('Client disconnected, userId=', userId, '(', socket.handshake.address, ') >>> Reason: ', reason);
    if (userId) {
        this.users.delete(socket.id);
        this.sockets.delete(userId);
    }

    let commanderSocket = _this.getCommanderSocket(userId);
    if (commanderSocket) {
        commanderSocket.emit(Event.Send.USER_OFFLINE, userId);
    }

};

LoginUser.prototype.getCommanderSocket = function (userId) {
    var ret = null;
    var _this = this;
    _this.commanders.forEach(function (value, key, map) {
        var localKey = key + "";
        //log.debug("keys : = " + _.keys(_this.sockets));
        if ((userId + "").indexOf(localKey) == 0) {
            //log.debug(localKey + " commander key found ");
            //value.emit(Event.Send.USER_OFFLINE, userId);
            ret = value;
        }
    });
    console.log("------- get commande called   -----" + ret == null ? " null " : "socket");
    return ret;
}

/**
 * 向指挥账号发送消息
 * @param eventName{String}事件名
 * @param msg{json} 事件内容
 * @param idNumber{String & undefined} undefined时表示向所有指挥号发送信息
 * @returns {*}
 */
LoginUser.prototype.sendMessageToCommander = function(eventName, msg, idNumber){
    if(idNumber != undefined){
        let socket = this.getCommanderSocket(userId);

        socket.emit(eventName, msg);
        return;
    }

    var ret = null;
    var _this = this;
    _this.commanders.forEach(function (value, key, map) {
        value.emit(eventName, msg);
    });
}

/**
 * 修改密码
 *
 * @param userId {number}
 * @param oldPassword {string}
 * @param newPassword {string}
 * @returns {Promise}
 */
LoginUser.prototype.doChangePassword = function (userId, oldPassword, newPassword) {
    return co(function*() {
        "use strict";

        log.info(`User ${userId} requested changing password`);

        let err, numAffected = yield PttContactUser.update({
            idNumber: userId,
            password: oldPassword
        }, {$set: {password: newPassword}}).exec();
        if (err) {
            throw {name: "server_error", message: "更改密码失败: 服务器错误"};
        }
        else if (numAffected.n == 0) {
            throw {name: "invalid_password", message: "更改密码失败: 当前密码错误"};
        }
    });
};

//判断当前用户对应的socket.
LoginUser.prototype.socketForUser = function (userId) {
    return this.sockets.get(userId);
};

LoginUser.prototype.userForSocket = function (socketId) {
    return this.users.get(socketId);
};


/**
 * @struct
 * @param name {string}
 * @param message {string}
 */
function LoginError(name, message) {
    this.name = name;
    this.message = message;
}


module.exports = LoginUser;