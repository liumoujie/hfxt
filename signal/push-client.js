"use strict";

var crypto = require('crypto');
var rp = require('request-promise');
var LRU = require('lru-cache');
var log = require('loglevel');
var co = require('co');
var _ = require('underscore');
const apn = require("apn");
var Event = require('./event');
var PttPushToken = require('../models/ptt-push-token');

class PushClient {
    constructor(pushServerAddr, cipherKey, iosPushCert) {
        this.pushServerUrl = `http://${pushServerAddr}`;
        this.pushServerMsgUrl = `${this.pushServerUrl}/msg`;
        this.pushUser = "admin";

        const cipher = crypto.createCipheriv('aes-128-ecb', cipherKey, '');
        cipher.setAutoPadding(true);
        this.pushToken = cipher.update("adminsalt123", 'utf8', 'base64')
        this.pushToken += cipher.final('base64');

        /**
         * ios push所用的证书相关参数
         * @private
         */
        this.iospushCert = iosPushCert;

        /**
         * 缓存最近使用到的tokens,避免频繁查询数据库
         * @private {LRU<idNumber, PttPushToken>}
         */
        this.pushTokens = new LRU(10240);
    }

    sendMessage(userIds, msg){
        const self = this;
        co(function*(){
            log.info(`sendMessage ${userIds}`);
            let iosTokens = [];
            let androidUsers = [];

            //由于不同平台的push发送方式不一样,先区分出不同的设备
            for(var index in userIds){
                let token = yield self.getDeviceToken(userIds[index]);
                log.info(`sendMessage ${userIds[index]} ->${token}`);
                if(token){ //deviceType 0 for ios
                    if(token.deviceType == 0)
                        iosTokens.push(token.token);
                    else if(token.deviceType == 1){
                        androidUsers.push(userId);
                    }
                }
            }

            //分设备发送 push
            if(_.size(iosTokens) > 0)
                self.sendMessageToIOS(iosTokens, msg);

            if(_.size(androidUsers))
                self.sendMessageToAndroid(androidUsers, msg);
        }).then(()=>{
            log.info(`sendMessage ${userIds} complete`);
        });
    }



    sendMessageToAndroid(userIds, msg) {
        const opt = {
            uri: this.pushServerMsgUrl,
            headers: {
                'X-User-Id': this.pushUser,
                'X-User-Token': this.pushToken
            },
            json: true,
            body: {
                body: JSON.stringify(msg),
                userIds: userIds
            },
            method: 'POST'
        };
        return rp(opt);
    }

    /**
     * 向ios用户发送push消息
     * @param tokens{Array(string)} 每个token代表一个ios用户
     * @param msg 要发送的数据内容
     */
    sendMessageToIOS(tokens, msg){
        log.info(`sendMessageToIOS tokens=${tokens}, this.iospushCert=${this.iospushCert}`);
        const self = this;
        let service = new apn.Provider(self.iospushCert);
        const payload = {"eventName": Event.Send.INVITE_TO_JOIN, "params": JSON.stringify(msg)};
        log.info(`sendMessageToIOS5 token = ${tokens}`);
        let note = new apn.Notification();
        try {
            note.payload = payload;
            // The topic is usually the bundle identifier of your application.
            //note.topic = "com.xianzhitech.ptt.ctalk";

            log.info(`Sending: ${note.compile()} to ${tokens}`);

            service.send(note, tokens).then( result => {
                log.info("sent:", result.sent.length);
                log.info("failed:", result.failed.length);
                log.info(result.failed);

                _.forEach(result.failed, (item)=>{
                    log.info("sendMessageToIOS failed for item = ", item)
                    if(item && item.status == '400' && item.response.reason == 'BadDeviceToken'){
                        self.removeByDeviceToken(item.device);
                    }
                });
            });
        }catch(e) {
            log.info("sendMessageToIOS failed for token = ", tokens)
        }

        // For one-shot notification tasks you may wish to shutdown the connection
        // after everything is sent, but only call shutdown if you need your
        // application to terminate.
        service.shutdown();
    }


    /**
     * 注册或更新一个设备的push device token.
     * 可以通过ios 的apn向苹果设备(device token) 发送消息
     * @param userId 用户id
     * @param deviceType 设备类型,用于标志ios(iphone)
     * @param devicePushToken 设备号(voip device token)
     * @returns {Promise}
     */
    registerDeviceToken(userId, deviceType, devicePushToken) {
        log.info("registerDeviceToken : userId = ");
        const self = this;
        return co(function*() {
            log.info("registerDeviceToken : userId = ", userId, ", deviceType = ", deviceType, "devicePushToken =", devicePushToken);
            if(typeof userId != "number")
                throw new PushTokenError("invalid_params");

            const pttToken = yield PttPushToken.findOneAndUpdate({idNumber: userId},
                {
                    $set: {
                        deviceType: deviceType,
                        token: devicePushToken
                    }
                },
                {
                    new: true,
                    upsert: true
                }).exec();


            if(pttToken)
                self.pushTokens.set(userId, pttToken);

            //删除其它用户关联的相同的device token,用户可能会在同一个设备上登录不现的账号,每设备的device token只跟最近一次登录的用户保持关联,删除其它账号的关联信息
            self.removeByDeviceToken(devicePushToken, userId);

            log.info("registerDeviceToken : userId = ", userId, ", deviceType = ", deviceType, "devicePushToken =", devicePushToken, "result = ", pttToken);
            return true;
        });
    }


    /**
     * 根据deviceToken 删除相应的记录,但不删除idNumber与excludeIdNumber相等的
     * @param deviceToken
     * @param excludeIdNumber
     * @returns {*|Promise}
     */
    removeByDeviceToken(deviceToken, excludeIdNumber){
        log.info("removeByDeviceToken : deviceToken = ", deviceToken);
        const self = this;
        return co(function*() {
            if(typeof deviceToken != "string")
                throw new PushTokenError("invalid_params");

            //根据device token id 找到对应的用户
            const tokens = yield PttPushToken.find({token: deviceToken}).exec();

            _.forEach(tokens, (token)=>{
                if(token.idNumber != excludeIdNumber)
                    self.removeByIdNumber(token.idNumber);
            });
        });
    }

    /**
     * 根据用户id删除其push token表中的记录
     * @param idNumber
     * @returns {*|Promise}
     */
    removeByIdNumber(idNumber){
        const self = this;
        return co(function*() {
            log.info("removeByIdNumber : idNumber = ", idNumber);
            if(typeof idNumber != "number")
                throw new PushTokenError("invalid_params");

            //从数据库中删除
            yield PttPushToken.remove({idNumber: idNumber}).exec();

            //从lru表中删除,note:使用set而不是del操作
            //避免后续查找时,又会从数据库里查询的问题(只需要pushTokens.has检测一下)
            self.pushTokens.set(idNumber, undefined);
        });
    }

    /**
     * 根据用户id查询其对应的 push token
     * @param idNumber
     * @returns {*|Promise}
     */
    getDeviceToken(idNumber){
        const self = this;
        return co(function*(){
           if(typeof idNumber != "number"){
                return undefined;
            }

            if(self.pushTokens.has(idNumber))
                return self.pushTokens.get(idNumber);


            //如果从Cache里找不到就从数据库里查找
            let pttToken = yield PttPushToken.findOne({idNumber: idNumber}).exec();

            if(pttToken)
                self.pushTokens.set(idNumber, pttToken);

            return pttToken;
        });
    }

}


class PushTokenError {
    constructor(name, message) {
        this.name = name;
        this.message = message;
    }
}


module.exports = PushClient;