
const RoomMode = {
    NORMAL: "normal",
    EMERGENCY: "emergency",
    BROADCAST: "broadcast",
    SYSTEM_BROADCAST: "system_broadcast",
    VIDEO: "video",
    AUDIO: "audio",

    /**
     * 获取对应房间类型的优先级
     *
     * @param mode {RoomMode} 房间类型
     * @return {number} 优先级。数值越大，越优先。
     */
    getPriority: function (mode) {
        switch (mode) {
            case this.AUDIO:
            case this.VIDEO:
                return 0;

            case this.NORMAL:
                return 1;

            case this.BROADCAST:
                return 2;

            case this.SYSTEM_BROADCAST:
                return 3;

            case this.EMERGENCY:
                return 4;
        }

        throw "Unknown room mode: " + mode
    },

    getDisplayText: function (mode) {
        switch (mode) {
            case this.AUDIO:
                return "音频会话";

            case this.VIDEO:
                return "视频会话";

            case this.NORMAL:
                return "对讲";

            case this.BROADCAST:
                return "广播";

            case this.SYSTEM_BROADCAST:
                return "系统广播";

            case this.EMERGENCY:
                return "紧急呼叫";
        }

        return "未知类型的会话"
    },
};

module.exports = RoomMode;