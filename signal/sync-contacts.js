"use strict";

/**
 * Created by suke on 15/12/14.
 *
 * 通讯录同步
 *
 */
var co = require('co');
var PttContactUser = require('../models/ptt-contact-user');
var PttContactGroup = require('../models/ptt-contact-group');
var log = require('loglevel');

class SyncContacts {

    /**
     * 执行通讯录同步
     * @param user {User}
     * @return {Promise}
     */
    doSync(user) {
        return co(function*() {
            //查询本组织下所有启用的用户, 但不包括自己.
            var users = yield PttContactUser.find({
                enterprise: user.enterprise,
                _id: {$ne: user._id},
                $or: [
                    { status: 0 },
                    { status: { $exists: false } }
                ], // status == 0 || !exists(status),

            }).select({idNumber: 1, name: 1, _id: 0, privileges: 1, phoneNbr: 1, father: 1}).exec();

            var groups = yield PttContactGroup.find({enterprise: user.enterprise, members: user.idNumber})
                .select({_id: 0, enterprise: 0, __v: 0}).exec();

            return {
                enterpriseMembers: {
                    add: users
                },

                enterpriseGroups: {
                    add: groups
                }
            }
        });

    }
}

module.exports = SyncContacts;